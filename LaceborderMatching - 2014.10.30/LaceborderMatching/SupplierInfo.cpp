// SupplierInfo.cpp : 实现文件
//

#include "stdafx.h"
#include "LaceborderMatching.h"
#include "SupplierInfo.h"
#include "afxdialogex.h"


// CSupplierInfo 对话框

IMPLEMENT_DYNAMIC(CSupplierInfo, CDialogEx)

CSupplierInfo::CSupplierInfo(CWnd* pParent /*=NULL*/)
	: CDialogEx(CSupplierInfo::IDD, pParent)
{

}

CSupplierInfo::~CSupplierInfo()
{
}

void CSupplierInfo::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CSupplierInfo, CDialogEx)
	ON_BN_CLICKED(IDC_BUTTON1, &CSupplierInfo::OnGetSupplierInfo)
END_MESSAGE_MAP()


// CSupplierInfo 消息处理程序
BOOL CSupplierInfo::OnInitDialog()
{
	CDialogEx::OnInitDialog();


	// TODO: 在此添加额外的初始化代码
	if (theApp.pLaceborderMatchingDlg)
	{
		int i;
		int num=(int)theApp.pLaceborderMatchingDlg->allfile.size();
		CString DirName;
		CString Index;

		for (i=0;i<num;i++)
		{
			DirName=theApp.pLaceborderMatchingDlg->allfile[i];
			int pos =DirName.ReverseFind('\\')+1; 
			DirName =DirName.Right(DirName.GetLength()-pos);

			Index.Format("%d",(i+1));

			DirName=Index+"   "+DirName;
			if (DirName.GetLength()>43)
			{
				DirName=DirName.Left(DirName.GetLength()-39);
			}
			
			((CListBox* )GetDlgItem(IDC_LIST1))->InsertString(i,DirName);
		}
		
		Index.Format("%d",num);

		DirName="共选择 "+Index+" 个文件夹，当前是 第 ";

		Index.Format("%d",theApp.pLaceborderMatchingDlg->CurrentDir);

		DirName=DirName+Index+" 个";

		CWnd *pWnd= GetDlgItem(IDC_STATIC_All);//文件名	
		pWnd->SetWindowText(DirName);

		pWnd= GetDlgItem(IDC_STATIC_CUR);//文件名	
		DirName=theApp.pLaceborderMatchingDlg->allfile[theApp.pLaceborderMatchingDlg->CurrentDir-1];

		int pos =DirName.ReverseFind('\\')+1; 
		DirName =DirName.Right(DirName.GetLength()-pos);
		DirName=DirName.Left(DirName.GetLength()-39);
		pWnd->SetWindowText(DirName);

	}

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CSupplierInfo::OnGetSupplierInfo()
{
	int i;
	CString tmp;

	for (i=0;i<8;i++)
	{
		theApp.pLaceborderMatchingDlg->Supplierinfo.push_back("EMPTY");
	}
	for (i=0;i<6;i++)
	{
		GetDlgItemText(IDC_EDIT10+i, tmp);
		if (tmp=="")
		{
			tmp="EMPTY";
		}
		theApp.pLaceborderMatchingDlg->Supplierinfo.push_back(tmp);
	}
	theApp.pLaceborderMatchingDlg->Supplierinfo.push_back("EMPTY");//备注

	this->OnOK();
}
