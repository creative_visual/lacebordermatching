// PictureInfo.cpp : 实现文件
//

#include "stdafx.h"
#include "LaceborderMatching.h"
#include "PictureInfo.h"
#include "afxdialogex.h"


// CPictureInfo 对话框

IMPLEMENT_DYNAMIC(CPictureInfo, CDialogEx)

CPictureInfo::CPictureInfo(CWnd* pParent /*=NULL*/)
	: CDialogEx(CPictureInfo::IDD, pParent)
{

}

CPictureInfo::~CPictureInfo()
{
}

void CPictureInfo::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT1, m_edit);
}


BEGIN_MESSAGE_MAP(CPictureInfo, CDialogEx)
	ON_BN_CLICKED(IDC_BUTTON1, &CPictureInfo::OnSure)
	ON_BN_CLICKED(IDC_CHECK1, &CPictureInfo::OnReName)
END_MESSAGE_MAP()


// CPictureInfo 消息处理程序
BOOL CPictureInfo::OnInitDialog()
{
	CDialogEx::OnInitDialog();


	// TODO: 在此添加额外的初始化代码
	if (theApp.pLaceborderMatchingDlg)
	{
		CString s=theApp.pLaceborderMatchingDlg->CurrentSelectedPicturePath;
		int pos =s.ReverseFind('\\')+1; 
		s =s.Right(s.GetLength()-pos);
		s=s.Left(s.GetLength()-4);//文件格式不可以改

		OnReadPictureInfo();

		CWnd *pWnd= GetDlgItem(IDC_EDIT2);//文件名	
		pWnd->SetWindowText(s);

		((CButton*)GetDlgItem(IDC_CHECK1))->SetCheck(FALSE);
		((CButton*)GetDlgItem(IDC_EDIT2))->EnableWindow(FALSE);//默认文件名不可更改


		if (theApp.pLaceborderMatchingDlg->CurrentMenu!="database")
		{
			for (int i=0;i<15;i++)
			{
				((CEdit*)GetDlgItem(IDC_EDIT1+i))->EnableWindow(FALSE);
			}
			((CButton*)GetDlgItem(IDC_CHECK1))->EnableWindow(FALSE);
			((CButton*)GetDlgItem(IDC_BUTTON1))->EnableWindow(FALSE);
		}

	}

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CPictureInfo::OnSure()//确定编辑
{
	//读取信息
	vector<CString>  info;
	CString tmp;
	int i;
	for (i=0;i<14;i++)
	{
		GetDlgItemText(IDC_EDIT2+i, tmp);
		if (tmp=="")
		{
			tmp="EMPTY";
		}
		info.push_back(tmp);
	}

/////////////////////////////////////////////读取 备注//////////////////////////////////////////////////
	char* cc=NULL;

	//m_edit 是 edit control 控件关联的变量
	int linenum=m_edit.GetLineCount();//获取edit control 控件有多少行文本

	if (linenum>0)
	{
		for (i=0;i<linenum;i++)
		{
			int k=m_edit.LineLength(m_edit.LineIndex(i));//获取第i行文本字符的长度

			cc=new char[k+1];
			m_edit.GetLine(i,cc);//将第i行字符保存在 cc字符数组
			cc[k]='\0';

			tmp.Format("%s",cc);//转换为 CString
			delete[]cc;
			if (tmp=="")
			{
				tmp="EMPTY";
			}
			info.push_back(tmp);
		}
	}
	else
	{
		tmp="EMPTY";
		info.push_back(tmp);
	}
	
	int len=info[0].GetLength();
	if (len>32)
	{
		AfxMessageBox("图片文件名长度超过限制！\n必须在 32个英文字符 或者 16个汉字以内！！\n（一个汉字长度等于两个英文字符）");
		return;
	}
	if (len<1||info[0]=="EMPTY")
	{
		AfxMessageBox("图片文件名不可以为空！！");
		return;
	}

	theApp.pLaceborderMatchingDlg->OnWritePictureInfo(info);

	this->OnOK();
	// TODO: 在此添加控件通知处理程序代码
}


void CPictureInfo::OnReName()
{
	if (BST_CHECKED==((CButton*)GetDlgItem(IDC_CHECK1))->GetCheck())//原来选中，现在取消
	{
		((CButton*)GetDlgItem(IDC_EDIT2))->EnableWindow(TRUE);//默认文件名不可更改
	}
	else if (BST_UNCHECKED==((CButton*)GetDlgItem(IDC_CHECK1))->GetCheck())
	{
		((CButton*)GetDlgItem(IDC_EDIT2))->EnableWindow(FALSE);//默认文件名不可更改
	}
}

void CPictureInfo::OnReadPictureInfo()
{
	vector<CString>  info;
	theApp.pLaceborderMatchingDlg->OnReadPictureInfo(info);
	if (info.empty())
	{
		return;
	}
	CString tmp;
	int i;
	for (i=0;i<13;i++)
	{
		tmp=info[i];
		if (tmp=="EMPTY")
		{
			tmp="";
		}
		((CEdit*)GetDlgItem(IDC_EDIT3+i))->SetWindowText(tmp);
	}
	int k=(int)info.size();

	tmp="";
	for (i=13;i<k;i++)
	{
		if (info[i]!="EMPTY")
		{
			tmp+=info[i];
		}
		
		if ((i+1)<k)
		{
			tmp+="\r\n";
		}
		
	}
	((CEdit*)GetDlgItem(IDC_EDIT1))->SetWindowText(tmp);


	if (!info.empty())
	{
		info.clear();
		vector<CString>().swap(info);
	}

	
}