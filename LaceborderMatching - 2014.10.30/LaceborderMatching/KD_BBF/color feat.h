#ifndef __COLORFEAT_H__
#define __COLORFEAT_H__

#include "opencv2/highgui/highgui.hpp"  
#include "opencv2/imgproc/imgproc.hpp"  
#include "opencv2/nonfree/nonfree.hpp"  
#include "opencv2/nonfree/features2d.hpp"  
#include "color space.h"
#include "cv.h"
#include <iostream>  
#include <stdio.h>  
#include <stdlib.h>  

using namespace cv;  
using namespace std; 


Mat filt_hist(CvHistogram *hist,int size);
void Extract_Color_Hist(IplImage* src,Mat& colhist);






#endif