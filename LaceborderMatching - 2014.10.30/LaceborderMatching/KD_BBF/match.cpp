#include "match.h"
#include <string>
#include <cstring>
#include <iostream>  
#include <stdio.h>  
#include <stdlib.h>  
#include <fstream>
#include "io.h"  
#include "direct.h"


using namespace std;


void Extract_FV_features(char *images_dict, FILENAME in_file)
{
	int i,j;
	int DBnum=0;
	int m_it;
	string dir(images_dict);
	int pos=dir.find_last_of('\\');
	string dir_name(dir.substr(pos+1));
	char *curpath;
	int s=0;
	int Dim,WordsNumber;
	curpath = getcwd(NULL, 0);
	char HSfile[1024];
	sprintf(HSfile,"%s\\hs_file_name%s.txt",curpath,dir_name.c_str());
	vector<string> AllFeatureDATAfilename;
	Extract_HS_features(images_dict,HSfile,in_file.dbimagesname_file,in_file.hs_num_one_img_file,in_file.eigen_vectors_file);
	Generate_GMM_Model(HSfile,in_file.Words_File,in_file.hs_num_one_img_file);

	VlGMM *gmm=Read_Into_GMM(in_file.Words_File,Dim,WordsNumber);
	int FV_DIM=2 * Dim * WordsNumber;

	ifstream read_hs_p;
	read_hs_p.open(HSfile);
	if (!read_hs_p)
	{
		cout<<"Can't open HSFile. match.cpp,32 line\n";
		exit(-1);
	}
	while (!read_hs_p.eof())
	{
		char read_img_name[1024];
		read_hs_p.getline(read_img_name,1024,'\n');
		string read_img_tmp(read_img_name);
		AllFeatureDATAfilename.push_back(read_img_tmp);

		DBnum++;
	}
	read_hs_p.close();

	ifstream fread_num;
	fread_num.open(in_file.hs_num_one_img_file);
	if (!fread_num)
	{
		cout<<"Can't open featnum file in proc_func line 356.\n";
		exit(-1);
	}
	int *each_num=new int[DBnum];
	int ii=0;
	while(!fread_num.eof())
	{
		fread_num>>each_num[ii];
		ii++;
	}
	fread_num.close();

	FILE *out_bow;
	out_bow=fopen(in_file.bowfeat_filename,"w");

	for (m_it=0;m_it<DBnum;m_it++)
	{
		FILE *read_feat;
		char file_temp_name[1024];
		strcpy(file_temp_name,AllFeatureDATAfilename[m_it].c_str());
		read_feat=fopen(file_temp_name,"r");
		if(!read_feat)
			continue;  // no feature file exist for this one.
		int index;
		fscanf(read_feat,"%d",&index);
		float *local_feat=new float[each_num[m_it]*HS_DIM];
		
		int s=0;
		while(!feof(read_feat))
		{
			for (j=0;j<HS_DIM;j++)  
				fscanf(read_feat,"%f",&local_feat[s*HS_DIM+j]);
			s++;			
		}
		HSBowFeature CurBowFeat;
		CurBowFeat.index=index;
		CurBowFeat.HSBowFVSize=FV_DIM;	
		CurBowFeat.HSBowFV=Fisher_Vector_Exp(local_feat,gmm,HS_DIM,each_num[m_it]);
		fprintf(out_bow,"%d ",CurBowFeat.index);
		fprintf(out_bow,"%d ",CurBowFeat.HSBowFVSize);
		for (j=0;j<CurBowFeat.HSBowFVSize;j++)
			fprintf(out_bow,"%f ",CurBowFeat.HSBowFV[j]);
		fprintf(out_bow,"\n");	

		delete[] CurBowFeat.HSBowFV;
		delete[] local_feat;
		fclose(read_feat);
	}
	delete[] each_num;
// 	free (gmm->priors) ;
// 	free (gmm->means) ;
// 	free (gmm->covariances) ;
// 	free (gmm->posteriors) ;
	fclose(out_bow);

}




bool compare_SimilarInfo(SimilarInfo a, SimilarInfo b)
{
	return (a.simil_ratio > b.simil_ratio);
}



void Extrace_COLOR_features(char *images_dict, FILENAME in_file)
{	
	vector<string> AllImagefilename;
	FindAllFiles(images_dict,in_file.dbimagesname_file);
	int DBnum = 0;
	int m_nFrameInd,i,j;
	ifstream read_fp;
	read_fp.open(in_file.dbimagesname_file);
	if (!read_fp)
	{
		cout<<"Can't open image dictionary file\n";
		exit(-1);
	}
	while(!read_fp.eof())
	{
		char readfilename[1024];
		read_fp.getline(readfilename,1024,'\n');
		if(strcmp(readfilename,"overkane")==0)
			break;
		char img_file_name[1024];
		sprintf(img_file_name,"%s\\%s",images_dict,readfilename);
		string img_file_tmp(img_file_name);
		AllImagefilename.push_back(img_file_tmp);
		DBnum++;
	}
	read_fp.close();
	vector<COLOR_FEAT> AllcolorFeat;
	for (m_nFrameInd = 0; m_nFrameInd < DBnum; m_nFrameInd ++)
	{
		COLOR_FEAT cur_colfeat;
		char input_img_name[1024];
		strcpy(input_img_name,AllImagefilename[m_nFrameInd].c_str());
		cout<<input_img_name<<endl;
		IplImage *img = cvLoadImage(input_img_name);
		Extract_Color_Hist(img,cur_colfeat.hist);
		cur_colfeat.index=m_nFrameInd;
		AllcolorFeat.push_back(cur_colfeat);
		cvReleaseImage(&img);
	}
	FILE *out_bow;
	out_bow=fopen(in_file.colorfeat_file,"w");
	for(i=0;i<AllcolorFeat.size()-1;i++)
	{
		fprintf(out_bow,"%d ",AllcolorFeat[i].index);
		fprintf(out_bow,"%d ",AllcolorFeat[i].hist.cols);
		for (j=0;j<AllcolorFeat[i].hist.cols;j++)
			fprintf(out_bow,"%f ",AllcolorFeat[i].hist.at<float>(j));
		fprintf(out_bow,"\n");	
	}
	fprintf(out_bow,"%d ",AllcolorFeat[i].index);
	fprintf(out_bow,"%d ",AllcolorFeat[i].hist.cols);
	for (j=0;j<AllcolorFeat[i].hist.cols-1;j++)
		fprintf(out_bow,"%f ",AllcolorFeat[i].hist.at<float>(j));
	fprintf(out_bow,"%f",AllcolorFeat[i].hist.at<float>(j));

	fclose(out_bow);

}


 void  Extract_Query_Colorfeat(IplImage *img, HSBowFeature& TestHSBowFeature)
{
	int i;
	Mat colorfeat;
	Extract_Color_Hist(img,colorfeat);
	TestHSBowFeature.HSBowFVSize=colorfeat.cols;
	TestHSBowFeature.HSBowFV = (float *)calloc(TestHSBowFeature.HSBowFVSize,sizeof(float));
	for (i=0;i<TestHSBowFeature.HSBowFVSize;i++)
		TestHSBowFeature.HSBowFV[i]=colorfeat.at<float>(i);
}


void Extract_BOW_features(char *images_dict, FILENAME in_file)
{
	int i,j;
	int DBnum=0;
	int m_it,Dim,WordsNumber;
	float *Words=NULL;
	string dir(images_dict);
	int pos=dir.find_last_of('\\');
	string dir_name(dir.substr(pos+1));
	char *curpath;
	int s=0;
	curpath = getcwd(NULL, 0);
	char HSfile[1024];
	sprintf(HSfile,"%s\\hs_file_name%s.txt",curpath,dir_name.c_str());
	vector<string> AllFeatureDATAfilename;
	Extract_HS_features(images_dict,HSfile,in_file.dbimagesname_file,in_file.hs_num_one_img_file,in_file.eigen_vectors_file);
	Generate_hierarchy_kmeans_dictionary(HSfile,in_file.Words_File,in_file.hs_num_one_img_file);
	Words = ReadintoWords(in_file.Words_File,Dim,WordsNumber);
	int VLAD_DIM=Dim*WordsNumber;
	ifstream read_hs_p;
	read_hs_p.open(HSfile);
	if (!read_hs_p)
	{
		cout<<"Can't open HSFile. match.cpp,32 line\n";
		exit(-1);
	}
	while (!read_hs_p.eof())
	{
		char read_img_name[1024];
		read_hs_p.getline(read_img_name,1024,'\n');
		string read_img_tmp(read_img_name);
		AllFeatureDATAfilename.push_back(read_img_tmp);

		DBnum++;
	}
	read_hs_p.close();

	ifstream fread_num;
	fread_num.open(in_file.hs_num_one_img_file);
	if (!fread_num)
	{
		cout<<"Can't open featnum file in proc_func line 356.\n";
		exit(-1);
	}
	int *each_num=new int[DBnum];
	int ii=0;
	while(!fread_num.eof())
	{
		fread_num>>each_num[ii];
		ii++;
	}
	fread_num.close();
	vector<HSBowFeature> AllBowFeat;
	for (m_it=0;m_it<DBnum;m_it++)
	{
		FILE *read_feat;
		char file_temp_name[1024];
		strcpy(file_temp_name,AllFeatureDATAfilename[m_it].c_str());
		read_feat=fopen(file_temp_name,"r");
		if(!read_feat)
			continue;  // no feature file exist for this one.
		int index;
		fscanf(read_feat,"%d",&index);
		Mat CurHSfeat(each_num[m_it],HS_DIM,CV_32FC1);
		HEDescrip temp;
		
		int s=0;
		while(!feof(read_feat))
		{
			for (j=0;j<HS_DIM;j++)  
				fscanf(read_feat,"%f",&CurHSfeat.at<float>(s,j));
			s++;			
		}
		HSBowFeature CurBowFeat;
		CurBowFeat.index=index;
		CurBowFeat.HSBowFVSize=VLAD_DIM;
		CurBowFeat.HSBowFV=(float *)calloc(CurBowFeat.HSBowFVSize,sizeof(float));
		Bow(CurHSfeat,Dim, WordsNumber, Words,CurBowFeat.HSBowFV/*,m_it*/);
		AllBowFeat.push_back(CurBowFeat);
		fclose(read_feat);
	}
	free(Words);
	delete[] each_num;
	FILE *out_bow;
	out_bow=fopen(in_file.bowfeat_filename,"w");
	for(i=0;i<AllBowFeat.size()-1;i++)
	{
		fprintf(out_bow,"%d ",AllBowFeat[i].index);
		fprintf(out_bow,"%d ",AllBowFeat[i].HSBowFVSize);
		for (j=0;j<AllBowFeat[i].HSBowFVSize;j++)
			fprintf(out_bow,"%f ",AllBowFeat[i].HSBowFV[j]);
		fprintf(out_bow,"\n");	
	}
	fprintf(out_bow,"%d ",AllBowFeat[i].index);
	fprintf(out_bow,"%d ",AllBowFeat[i].HSBowFVSize);
	for (j=0;j<AllBowFeat[i].HSBowFVSize-1;j++)
		fprintf(out_bow,"%f ",AllBowFeat[i].HSBowFV[j]);
	fprintf(out_bow,"%f",AllBowFeat[i].HSBowFV[j]);
	fclose(out_bow);

	for(i=0;i<AllBowFeat.size();i++)
		free(AllBowFeat[i].HSBowFV);

	
}


struct kd_node* Read_db_features(char *AllFeaturefile )
{	
	int i,j;
	struct kd_node *kd_root;
	vector <HSBowFeature> DBImageHSBowFeature;
	DBImageHSBowFeature.clear();
	ReadImageHSBowTFeatures(AllFeaturefile,DBImageHSBowFeature);
	int dbf_num=DBImageHSBowFeature.size();
	struct feature *feat_db=new struct feature[dbf_num];
	int dim=DBImageHSBowFeature[0].HSBowFVSize;
	for (i=0;i<dbf_num;i++)
	{
		feat_db[i].descr=new double[dim];
		for (j=0;j<dim;j++)
		{
			feat_db[i].descr[j]=DBImageHSBowFeature[i].HSBowFV[j];
		}
		feat_db[i].d=dim;
		feat_db[i].index=DBImageHSBowFeature[i].index;
	}

	kd_root = kdtree_build( feat_db,dbf_num );

	for(i=0;i<DBImageHSBowFeature.size();i++)
		free(DBImageHSBowFeature[i].HSBowFV);
	return kd_root;
}

void  Extract_Query_feat(IplImage *img, char* WordsFilename, HSBowFeature& TestHSBowFeature,char *eignfile)
{
	int i,j;
	
	VlGMM *gmm;
	int Dim,WordsNumber;
	gmm=Read_Into_GMM(WordsFilename,Dim,WordsNumber);

	int FV_DIM=2*Dim*WordsNumber;
	IplImage *img1;
	int in_size=img->width*img->height;
	int rel_flag=0;
	if (in_size<90000)
		img1=img; 
	else
	{
		rel_flag=1;
		img1=crop_image(img);
	}

	Mat image(img1);
	int firstlevel_size=image.rows*image.cols;
	if (firstlevel_size>400000)
	{
		do
		{
// 			image = gaussianBlur(image, 3.2);
// 			image = kane_halfImage(image);
			pyrDown( image, image, Size( image.cols/2, image.rows/2 ) );
			firstlevel_size=image.rows*image.cols;
		}while(firstlevel_size>400000);

	}


	///////////////////////////////////////////////
	//		PCA ANALYSIS
//	 char fan_hsf[1024]="F:\\eignfile.txt";
	PCA sip=read_pca(eignfile);
	///////////////////////////////////////////////////////////





	Mat second_down;
	pyrDown( image, second_down, Size( image.cols/2, image.rows/2 ) );

	
	vector<KeyPoint> keypoints;//构造2个专门由点组成的点向量用来存储特征点

	if (image.rows<100||image.cols<100)
		dense_sampling_detect(image,keypoints,0);
	else
		dense_sampling_detect(image,keypoints,1);
	SurfDescriptorExtractor extractor;//定义描述子对象
	Mat descriptors;//存放特征向量的矩阵
	extractor.compute(image,keypoints,descriptors);
	Mat eig=sip.project(descriptors);

	
	vector<KeyPoint> keypoints1;//构造2个专门由点组成的点向量用来存储特征点
	dense_sampling_detect(second_down,keypoints1,1);
	SurfDescriptorExtractor extractor1;//定义描述子对象
	Mat descriptors1;//存放特征向量的矩阵
	extractor1.compute(second_down,keypoints1,descriptors1);
	Mat eig1;
	if (descriptors1.rows>0)
		eig1=sip.project(descriptors1);
		

	if (rel_flag)
	{
		cvReleaseImage(&img1);
	}
	int NewSFNum=eig1.rows+eig.rows;

	

	float* qfeat=new float[NewSFNum*HS_DIM];
	int s=0;
	for (i=0;i<eig.rows;i++)
	{
		for (j=0;j<HS_DIM;j++)
		{
			qfeat[s*HS_DIM+j]=eig.at<float>(i,j);
		}
		s++;
	}
	for (i=0;i<eig1.rows;i++)
	{
		for (j=0;j<HS_DIM;j++)
		{
			qfeat[s*HS_DIM+j]=eig1.at<float>(i,j);
		}
	
		s++;
	}
	TestHSBowFeature.HSBowFVSize = FV_DIM;
	TestHSBowFeature.HSBowFV=Fisher_Vector_Exp(qfeat,gmm,HS_DIM,NewSFNum);
	free(gmm->priors);
	free(gmm->covariances);
	free(gmm->means);
	free(gmm->sigmaLowBound);
	free(gmm);
}


// input query image's filename and dictionary, store features in TestHSBowFeature




//input query image's feature and database's features, use BBF algorithm to match, output the index of similar images. 
void Match_Query_from_db(kd_node* kd_root, HSBowFeature TestHSBowFeature, vector<SimilarInfo>& match_info,char*images_name_file)
{
	int i,j;
	struct feature *img_feat = new struct feature;
	img_feat->d=TestHSBowFeature.HSBowFVSize;
	img_feat->descr = new double[img_feat->d];
	img_feat->index=0;
	for (i=0;i<img_feat->d;i++)
	{
		img_feat->descr[i]=TestHSBowFeature.HSBowFV[i]; 
		//cout<<img_feat->descr[i]<<"  ";
	}

	//pretranverse(kd_root);
	struct feature** nbrs;
	int k;
	k = kdtree_bbf_knn( kd_root, img_feat, K, &nbrs, KDTREE_BBF_MAX_NN_CHKS );
	vector<string> all_img_name;
	ifstream read_fp;
	read_fp.open(images_name_file);
	if (!read_fp)
	{
		cout<<"Can't open image dictionary file\n";
		exit(-1);
	}

	while(!read_fp.eof())
	{
		char readfilename[1024];
		read_fp.getline(readfilename,1024,'\n');

		if(strcmp(readfilename,"overkane")==0)
			break;
		string img_file_tmp(readfilename);
		all_img_name.push_back(img_file_tmp);
	}
	read_fp.close();
	for( i = 0; i < K; i++ )
	{
		SimilarInfo temp_info;
		temp_info.matched_img_name=all_img_name[nbrs[i]->index];
		float *nbrs_tmp=(float *)calloc(TestHSBowFeature.HSBowFVSize,sizeof(float));
		for (j=0;j<TestHSBowFeature.HSBowFVSize;j++)
			nbrs_tmp[j]=nbrs[i]->descr[j];
		temp_info.simil_ratio=vector_correlation(TestHSBowFeature.HSBowFV,nbrs_tmp,TestHSBowFeature.HSBowFVSize);//error
		match_info.push_back(temp_info);
	}

	kdtree_release( kd_root );
	delete[] img_feat->descr;
	delete img_feat;
	free(nbrs);
	free(TestHSBowFeature.HSBowFV);


}

void  Image_COLOR_Match(IplImage *query,FILENAME input_file,vector<SimilarInfo>&match_info)
{
	//vector<SimilarInfo> match_info;
	HSBowFeature TestHSBowFeature;
	vector<int> Similar_Index;
	kd_node *root;
	root=Read_db_features(input_file.colorfeat_file);
	Extract_Query_Colorfeat(query,TestHSBowFeature);
	Match_Query_from_db(root,TestHSBowFeature,match_info,input_file.dbimagesname_file);

	//return match_info;

}

void  Image_BOW_Match(IplImage *query,FILENAME input_file,vector<SimilarInfo>&match_info)
{
	//vector<SimilarInfo> match_info;
	HSBowFeature TestHSBowFeature;
	vector<int> Similar_Index;
	kd_node *root=NULL;
	Extract_Query_feat(query,input_file.Words_File,TestHSBowFeature,input_file.eigen_vectors_file);
	root=Read_db_features(input_file.bowfeat_filename);	
	
	Match_Query_from_db(root,TestHSBowFeature,match_info,input_file.dbimagesname_file);
//	free(TestHSBowFeature.HSBowFV);
	//return match_info;

}

// void read_db_vlad_features(char *file,vector<Db_Feats>& all_vlad_feats)
// {
// 	FILE *f1;
// 	f1=fopen(file,"r");
// 	int j,s=0;
// 	if (!f1)
// 	{
// 		cout<<"Can't open vlad file.\n";
// 		exit(-1);
// 	}
// 	while(!feof(f1))
// 	{
// 		Db_Feats CurFeat;
// 		fscanf(f1,"%d",&CurFeat.index);
// 		fscanf(f1,"%d",&CurFeat.FeaturesNum);
// 		CurFeat.features=(float*)calloc(CurFeat.FeaturesNum,sizeof(float));
// 		for (j=0;j<CurFeat.FeaturesNum;j++)	
// 			fscanf(f1,"%f",&CurFeat.features[j]);
// 		all_vlad_feats.push_back(CurFeat);
// 	}
// 	fclose(f1);
// }

void find_first_thirty(float *a,int n,float *b,int *c)
{
	int i,j,x;
 
	for(i=0;i<n;c[i]=i++)
		b[i]=a[i];
	for (i=0;i<n;i++)
	{		
		for (x=i,j=x+1;j<n;j++)
			if (b[x]<b[j])				
				x=j;
		if (x!=j)
		{
			float tmp;
			tmp=b[i];
			b[i]=b[x];
			b[x]=tmp;
			j=c[i];
			c[i]=c[x];
			c[x]=j;
		}
	}
}


//标准化欧氏距离
float vector_correlation_1(float *vec1,float *vec2, int num)
{
	float s1,s2,s3;
	float return_value;

	s1 =s2 = s3 = 0.0;
	float *sd=new float[num];
	float m=2.0;
	for (int i = 0; i < num; i ++)
	{
		sd[i]=(vec1[i]+vec2[i])/num;
		sd[i]=sqrt((((vec1[i]-sd[i])*(vec1[i]-sd[i])+(vec2[i]-sd[i])*(vec2[i]-sd[i]))/num));
//		sd[i]=abs(vec1[i]-vec2[i])/sqrt(m);
//		cout<<sd[i]<<" "<<(vec1[i]-vec2[i])/sd[i]<<" ";
		s1+=abs((vec1[i]-vec2[i])/sd[i]);
//		cout<<s1<<" ";
	}

	return_value =s1;

	//return_value = (float)((int)(return_value*100)/100.0f);
	delete[] sd;
	return (return_value);
}

void ImageBruteMatch(char *directfile,HSBowFeature TestHSBowFeature,vector <HSBowFeature> DBImageHSBowFeature,vector<SimilarInfo>& match_info,char*images_name_file)
{
	int i,j;
	float *rate_info=new float[DBImageHSBowFeature.size()];
	float *asend_rate=new float[DBImageHSBowFeature.size()];
	for(i=0;i<DBImageHSBowFeature.size();i++)
	{
		rate_info[i]=vector_correlation(TestHSBowFeature.HSBowFV,DBImageHSBowFeature[i].HSBowFV,TestHSBowFeature.HSBowFVSize);
	}
	int *asending_index=new int[DBImageHSBowFeature.size()];
	find_first_thirty(rate_info,DBImageHSBowFeature.size(),asend_rate,asending_index);
	vector<string> all_img_name;
	vector<string> all_img_dir;
	ifstream read_fp;
	read_fp.open(images_name_file);
	if (!read_fp)
	{
		cout<<"Can't open image dictionary file\n";
		exit(-1);
	}

	while(!read_fp.eof())
	{
		char readfilename[1024];
		read_fp.getline(readfilename,1024,'\n');

		if(strcmp(readfilename,"overkane")==0)
			break;
		string img_file_tmp(readfilename);
		all_img_name.push_back(img_file_tmp);

		char imgdir[1024];
		sprintf(imgdir,"%s\\%s",directfile,readfilename);
//		cout<<imgdir<<endl;
		string imgdir_t(imgdir);
		all_img_dir.push_back(imgdir_t);
	}
	read_fp.close();
	match_info.clear();


	char fan_808_30_matches[234]="F:\\1_pca_fv_matches";
	for( i = 0; i < K; i++ )
	{
		SimilarInfo temp_info;
//		cout<<DBImageHSBowFeature[asending_index[i]].index<<"  ";
		temp_info.matched_img_name=all_img_name[DBImageHSBowFeature[asending_index[i]].index];
		temp_info.simil_ratio=asend_rate[i];
		match_info.push_back(temp_info);
		char matches_t[1024];
		strcpy(matches_t,all_img_dir[DBImageHSBowFeature[asending_index[i]].index].c_str());
		char save_30_file[1024];
	//	sprintf(save_30_file,"%s\\%s",fan_808_30_matches,all_img_name[DBImageHSBowFeature[asending_index[i]].index].c_str());
	    sprintf(save_30_file,"%s\\%s_0000000%d.jpg",fan_808_30_matches,all_img_name[DBImageHSBowFeature[asending_index[i]].index].c_str(),i);
		Mat matches_save=imread(matches_t);
		imwrite(save_30_file,matches_save);
	}
	delete[] rate_info;
	delete[] asend_rate;
	delete[] asending_index;
	for(i=0;i<DBImageHSBowFeature.size();i++)
		free(DBImageHSBowFeature[i].HSBowFV);
	free(TestHSBowFeature.HSBowFV);
}

void  Image_BOW_Match_BruteForce(char *direct_img,IplImage *query,FILENAME input_file,vector<SimilarInfo>&match_info)
{
	//vector<SimilarInfo> match_info;
	HSBowFeature TestHSBowFeature;
	vector<int> Similar_Index;

	vector <HSBowFeature> DBImageHSBowFeature;
	DBImageHSBowFeature.clear();
	Extract_Query_feat(query,input_file.Words_File,TestHSBowFeature,input_file.eigen_vectors_file);
	ReadImageHSBowTFeatures(input_file.bowfeat_filename,DBImageHSBowFeature);
	
	ImageBruteMatch(direct_img,TestHSBowFeature,DBImageHSBowFeature,match_info,input_file.dbimagesname_file);
	//	free(TestHSBowFeature.HSBowFV);
	//return match_info;

}


void  Image_Color_Match_BruteForce(char *direct_img,IplImage *query,FILENAME input_file,vector<SimilarInfo>&match_info)
{
	//vector<SimilarInfo> match_info;
	HSBowFeature TestHSBowFeature;
	vector<int> Similar_Index;
	vector <HSBowFeature> DBImageHSBowFeature;
	DBImageHSBowFeature.clear();
	
	Extract_Query_Colorfeat(query,TestHSBowFeature);
	ReadImageHSBowTFeatures(input_file.colorfeat_file,DBImageHSBowFeature);

	ImageBruteMatch(direct_img,TestHSBowFeature,DBImageHSBowFeature,match_info,input_file.dbimagesname_file);

}

