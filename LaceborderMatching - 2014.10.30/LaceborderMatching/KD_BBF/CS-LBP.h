

#ifndef _LBP_H
#define _LBP_H

//#include <values.h>
#include <math.h>
#include "string.h"
#include "opencv2/highgui/highgui.hpp"  
#include "opencv2/imgproc/imgproc.hpp"  
#include "opencv2/nonfree/nonfree.hpp"  
#include "opencv2/nonfree/features2d.hpp"  
#include <iostream>  
#include <stdio.h>  
#include <stdlib.h>  
#include "gist.h"
using namespace cv;  
using namespace std;  

#define patchsize 41

/* Predicate 1 for the 3x3 neighborhood */
#define predicate 1
/* The number of bits */
#define HS_DIM 256

#define bits 8
#define KANE_T 5
#define GRID_NUM 4
#define ALL_GRID 16
#define LBP_NUM 16
#define HIST_NUM (ALL_GRID*LBP_NUM)

/* Compare a value pointed to by 'ptr' to the 'center' value and
 * increment pointer. Comparison is made by masking the most
 * significant bit of an integer (the sign) and shifting it to an
 * appropriate position. */

#define compab_mask_inc(ptr,counter_ptr,shift) { value |= ((*ptr-*counter_ptr)>KANE_T)<<shift;ptr++;counter_ptr++;}

/* Compare a value 'val' to the 'center' value. */
//#define compab_mask(val,shift) { value |= ((unsigned int)(*center - (val) - 1) & 0x80000000) >> (31-shift); }


typedef struct
{
	int x,y;
} integerpoint;

typedef struct
{
	double x,y;
} doublepoint;

void calculate_points(void);
inline double interpolate_at_ptr(int* upperLeft, int i, int columns);
int * cs_lbp_histogram(float* img, int rows, int columns, int interpolated);
void deal_with_hist(int *hist ,float *norm_hist);
void normlize_pt(Mat &in,Mat &norm_mat);
int *connect_hist(int *hist[],int grid_n,int hist_m);
float * deal_with_hist(int *hist);
float * Compute_CS_LBP_Descriptors(Mat &patch);
Mat compute_descriptors_kane(Mat in,vector<KeyPoint> keypoints);
#endif
