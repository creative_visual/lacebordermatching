#ifndef  GMM_H
#define  GMM_H

#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/flann/flann.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <stdio.h>
#include <iostream>
#include <stdlib.h>
using namespace std;
using namespace cv;

// typedef enum _VlGMMInitialization
// {
// 	VlGMMKMeans, /**< Initialize GMM from KMeans clustering. */
// 	VlGMMRand,   /**< Initialize GMM parameters by selecting points at random. */
// 	VlGMMCustom  /**< User specifies the initial GMM parameters. */
// } VlGMMInitialization ;


typedef struct _VlGMM
{
//	int dataType ;                  /**< Data type. */
	int dimension ;                 /**< Data dimensionality. */
	int numClusters ;               /**< Number of clusters  */
	int numData ;                   /**< Number of last time clustered data points.  */
	int maxNumIterations ;          /**< Maximum number of refinement iterations. */
	int numRepetitions   ;          /**< Number of clustering repetitions. */
	int     verbosity ;                 /**< Verbosity level. */
	float *  means;                      /**< Means of Gaussian modes. */
	float *  covariances;                /**< Diagonals of covariance matrices of Gaussian modes. */
	float *  priors;                     /**< Weights of Gaussian modes. */
	float *  posteriors;                 /**< Probabilities of correspondences of points to clusters. */
	double * sigmaLowBound ;            /**< Lower bound on the diagonal covariance values. */
//	VlGMMInitialization initialization; /**< Initialization option */
//	VlKMeans * kmeansInit;              /**< Kmeans object for initialization of gaussians */
	double LL ;                         /**< Current solution loglikelihood */
//	vl_bool kmeansInitIsOwner; /**< Indicates whether a user provided the kmeans initialization object */
}VlGMM ;


enum _VlVectorComparisonType 
{
	VlDistanceL1,        /**< l1 distance (squared intersection metric) */
	VlDistanceL2,        /**< squared l2 distance */
	VlDistanceChi2,      /**< squared Chi2 distance */
	VlDistanceHellinger, /**< squared Hellinger's distance */
	VlDistanceJS,        /**< squared Jensen-Shannon distance */
	VlDistanceMahalanobis,     /**< squared mahalanobis distance */
	VlKernelL1,          /**< intersection kernel */
	VlKernelL2,          /**< l2 kernel */
	VlKernelChi2,        /**< Chi2 kernel */
	VlKernelHellinger,   /**< Hellinger's kernel */
	VlKernelJS           /**< Jensen-Shannon kernel */
} ;

#define VALIGNED(x) (! (((int)(x)) & 0xF))
#define VALIGNEDavx(x) (! (((int)(x)) & 0x1F))
#define VL_GMM_MIN_VARIANCE 1e-6
#define VL_GMM_MIN_POSTERIOR 1e-2
#define VL_GMM_MIN_PRIOR 1e-6
#define VL_PI 3.141592653589793

#define VL_TYPE_FLOAT   1     /**< @c float type */
#define FLT VL_TYPE_FLOAT
#define VL_EXPORT extern "C" __declspec(dllimport)
/** @typedef VlFloatVector3ComparisonFunction
 ** @brief Pointer to a function to compare 3 vectors of doubles
 **/
#define COMPARISONFUNCTION3_TYPE VlFloatVector3ComparisonFunction

typedef enum _VlVectorComparisonType VlVectorComparisonType ;



VlGMM *kane_gmm_new(int dim, int nCompont);
double kane_gmm_cluster(VlGMM *self, float* data,int numData);
void  kane_gmm_maximization(VlGMM *self, float *posteriors, float *priors,float *covariances,float *means, float *data,int numData);
void kane_gmm_apply_bounds(VlGMM * self);
double kane_gmm_em(VlGMM * self,float  * data,int numData);
void kane_gmm_prepare_for_data (VlGMM* self, int numData);
double kane_get_gmm_data_posteriors(float * posteriors,int numClusters,int numData,float  * priors,float  * means,int dimension,float  * covariances,float  * data);
int kane_gmm_restart_empty_modes(VlGMM * self, float * data);
float kane_distance_mahalanobis_3(int dimension, float * X, float * MU, float * S);
void kane_gmm_compute_init_sigma(VlGMM * self,float * data,float * initSigma,int dimension,int numData);
float kane_distance_mahalanobis(int dimension, float * X, float * MU, float * S);
float kane_distance_mahalanobis_sq_avx(int dimension, float * X,float * MU,float* S);
float kane_distance_mahalanobis_sq_sse2(int dimension, float * X, float * MU, float * S);
float kane_vhsum_avx(__m256 x);
float kane_vhsum_sse2(__m128 x);
void  kane_weighted_mean_sse2(int dimension, float * MU, float * X, float W);
void kane_weighted_sigma_sse2(int dimension, float * S, float * X, float* Y, float W);

#endif