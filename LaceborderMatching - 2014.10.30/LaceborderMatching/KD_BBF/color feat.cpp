// color hist gram.cpp : 定义控制台应用程序的入口点。
//

#include "color feat.h"




Mat filt_hist(CvHistogram *hist,int size)
{
	int i,j;
	Mat color_feat(1,size,CV_32F,cvScalar(0));
	Mat tmp(1,size+4,CV_32F,cvScalar(0));
	tmp.at<float>(0)=cvQueryHistValue_1D(hist,size-2);
	tmp.at<float>(1)=cvQueryHistValue_1D(hist,size-1);
	for (i=2;i<size+2;i++)
	{
		tmp.at<float>(i)=cvQueryHistValue_1D(hist,i-2);
	}
	tmp.at<float>(i)=cvQueryHistValue_1D(hist,0);
	tmp.at<float>(i+1)=cvQueryHistValue_1D(hist,1);
	j=2;
	for (i=0;i<size;i++)
	{
		float bin_val0 = tmp.at<float>(j-2);
		float bin_val1 = tmp.at<float>(j-1);
		float bin_val2 = tmp.at<float>(j);
		float bin_val3 = tmp.at<float>(j+1);
		float bin_val4 = tmp.at<float>(j+2);
		color_feat.at<float>(i)=bin_val0+bin_val4+4*(bin_val1+bin_val3)+6*bin_val2;
		j++;
	}
	cvReleaseHist(&hist);
	return color_feat;

}





void Extract_Color_Hist(IplImage* src,Mat& colhist)
{
	int i,j;
	IplImage *hsv=cvCreateImage(cvGetSize(src),IPL_DEPTH_32F,src->nChannels);
	IplImage *h_plane=cvCreateImage(cvGetSize(src),IPL_DEPTH_32F,1);
	IplImage *s_plane=cvCreateImage(cvGetSize(src),IPL_DEPTH_32F,1);
	IplImage *v_plane=cvCreateImage(cvGetSize(src),IPL_DEPTH_32F,1);
	uchar *data=(uchar*)src->imageData;
	int channel=src->nChannels;
	int steps=src->widthStep;
	float r,g,b,h,s,v;

	for (i=0;i<src->height;i++)
		for (j=0;j<src->width;j++)
		{
			b=(float)*(data+i*steps+j*channel+0)/255.0;
			g=(float)*(data+i*steps+j*channel+1)/255.0;
			r=(float)*(data+i*steps+j*channel+2)/255.0;
			RGBtoHSV(r,g,b,&h,&s,&v);
			((float*)(hsv->imageData+i*hsv->widthStep))[j*hsv->nChannels+0]=h;
			((float*)(hsv->imageData+i*hsv->widthStep))[j*hsv->nChannels+1]=s;
			((float*)(hsv->imageData+i*hsv->widthStep))[j*hsv->nChannels+2]=v;
		}


		cvSplit(hsv,h_plane,s_plane,v_plane,NULL);

		cvReleaseImage(&hsv);
		cvReleaseImage(&v_plane);
		cvReleaseImage(&s_plane);
		CvHistogram *hist = 0;
		int hist_size = 36;     
		int hist_height = 100;  
		float range[] = {0,360};  
		float* ranges[]={range};
		hist = cvCreateHist( 1, &hist_size, CV_HIST_ARRAY, ranges, 1 );  
		cvCalcHist(&h_plane,hist,0,0);
		cvNormalizeHist(hist,1.0);
		cvReleaseImage(&h_plane);
		colhist=filt_hist(hist,hist_size);
}


