#ifndef FISHER_H
#define FISHER_H

#include "gmm.h"

/** @name Fisher vector options*/
#define VL_FISHER_FLAG_SQUARE_ROOT          (0x1 << 0)
#define VL_FISHER_FLAG_NORMALIZED           (0x1 << 1)
#define VL_FISHER_FLAG_IMPROVED             (VL_FISHER_FLAG_NORMALIZED|VL_FISHER_FLAG_SQUARE_ROOT)
#define VL_FISHER_FLAG_FAST       (0x1 << 2)

/** @def VL_FISHER_FLAG_FAST
 ** @brief Fast but more approximate calculations (@ref fisher-fast).
 ** Keep only the larges data to cluster assignment (posterior).
 **/
int kane_fisher_encode(float * enc,float * means, int dimension, int numClusters,float* covariances,float* priors,float* data, int numData,int flags);


#endif