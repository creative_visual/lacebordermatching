#include "match.h"
#include <string>
#include <cstring>
#include <iostream>  
#include <stdio.h>  
#include <stdlib.h>  
#include <fstream>
#include "io.h"  
#include "direct.h"


using namespace std;

bool compare_SimilarInfo(MySimilarInfo a, MySimilarInfo b)
{
	return (a.simil_ratio > b.simil_ratio);
}
bool compare_SimilarInfo(SimilarInfo a, SimilarInfo b)
{
	return (a.simil_ratio > b.simil_ratio);
}

void pretranverse(kd_node* root)
{
	if (root!=NULL)
	{
		cout<<root->features[0].d<<"  ";
		cout<<root->leaf<<endl;
		pretranverse(root->kd_left);
		pretranverse(root->kd_right);


	}
}

void Extrace_COLOR_features(char *images_dict, FILENAME in_file)
{	
	vector<string> AllImagefilename;
	FindAllFiles(images_dict,in_file.dbimagesname_file);
	int DBnum = 0;
	int m_nFrameInd,i,j;
	ifstream read_fp;
	read_fp.open(in_file.dbimagesname_file);
	if (!read_fp)
	{
		cout<<"Can't open image dictionary file\n";
		exit(-1);
	}
	while(!read_fp.eof())
	{
		char readfilename[128];
		read_fp.getline(readfilename,128,'\n');
		if(strcmp(readfilename,"overkane")==0)
			break;
		char img_file_name[128];
		sprintf(img_file_name,"%s\\%s",images_dict,readfilename);
		string img_file_tmp(img_file_name);
		AllImagefilename.push_back(img_file_tmp);
		DBnum++;
	}
	read_fp.close();
	vector<COLOR_FEAT> AllcolorFeat;
	for (m_nFrameInd = 0; m_nFrameInd < DBnum; m_nFrameInd ++)
	{
		COLOR_FEAT cur_colfeat;
		char input_img_name[128];
		strcpy(input_img_name,AllImagefilename[m_nFrameInd].c_str());
		cout<<input_img_name<<endl;
		IplImage *img = cvLoadImage(input_img_name);
		Extract_Color_Hist(img,cur_colfeat.hist);
		cur_colfeat.index=m_nFrameInd;
		AllcolorFeat.push_back(cur_colfeat);
		cvReleaseImage(&img);
	}
	FILE *out_bow;
	out_bow=fopen(in_file.colorfeat_file,"w");
	for(i=0;i<AllcolorFeat.size()-1;i++)
	{
		fprintf(out_bow,"%d ",AllcolorFeat[i].index);
		fprintf(out_bow,"%d ",AllcolorFeat[i].hist.cols);
		for (j=0;j<AllcolorFeat[i].hist.cols;j++)
			fprintf(out_bow,"%f ",AllcolorFeat[i].hist.at<float>(j));
		fprintf(out_bow,"\n");	
	}
	fprintf(out_bow,"%d ",AllcolorFeat[i].index);
	fprintf(out_bow,"%d ",AllcolorFeat[i].hist.cols);
	for (j=0;j<AllcolorFeat[i].hist.cols-1;j++)
		fprintf(out_bow,"%f ",AllcolorFeat[i].hist.at<float>(j));
	fprintf(out_bow,"%f",AllcolorFeat[i].hist.at<float>(j));

	fclose(out_bow);

}


 void  Extract_Query_Colorfeat(IplImage *img, HSBowFeature& TestHSBowFeature)
{
	int i;
	Mat colorfeat;
	Extract_Color_Hist(img,colorfeat);
	TestHSBowFeature.HSBowFVSize=colorfeat.cols;
	TestHSBowFeature.HSBowFV = (float *)calloc(TestHSBowFeature.HSBowFVSize,sizeof(float));
	for (i=0;i<TestHSBowFeature.HSBowFVSize;i++)
		TestHSBowFeature.HSBowFV[i]=colorfeat.at<float>(i);
}


void Extract_BOW_features(char *images_dict, FILENAME in_file)
{
	int i,j;
	int DBnum=0;
	int m_it,Dim,WordsNumber;
	float *Words=NULL;
	string dir(images_dict);
	int pos=dir.find_last_of('\\');
	string dir_name(dir.substr(pos+1));
	char *curpath;
	int s=0;
	curpath = getcwd(NULL, 0);
	char HSfile[1024];
	sprintf(HSfile,"%s\\hs_file_name%s.txt",curpath,dir_name.c_str());
	vector<string> AllFeatureDATAfilename;
	Extract_HS_features(images_dict,HSfile,in_file.dbimagesname_file,in_file.hs_num_one_img_file);
//	Generate_kmeans_dictionary(HSfile,in_file.Words_File,in_file.hs_num_one_img_file);
	Generate_hierarchy_kmeans_dictionary(HSfile,in_file.Words_File,in_file.hs_num_one_img_file);
	Words = ReadintoWords(in_file.Words_File,Dim,WordsNumber);

	ifstream read_hs_p;
	read_hs_p.open(HSfile);
	if (!read_hs_p)
	{
		cout<<"Can't open HSFile. match.cpp,32 line\n";
		exit(-1);
	}
	while (!read_hs_p.eof())
	{
		char read_img_name[1024];
		read_hs_p.getline(read_img_name,1024,'\n');
		string read_img_tmp(read_img_name);
		AllFeatureDATAfilename.push_back(read_img_tmp);

		DBnum++;
	}
	read_hs_p.close();
	vector<HSBowFeature> AllBowFeat;
	for (m_it=0;m_it<DBnum;m_it++)
	{
		FILE *read_feat;
		char file_temp_name[1024];
		strcpy(file_temp_name,AllFeatureDATAfilename[m_it].c_str());
		read_feat=fopen(file_temp_name,"r");
		if(!read_feat)
			continue;  // no feature file exist for this one.
		int index;
		fscanf(read_feat,"%d",&index);
		int feat_numb;
		fscanf(read_feat,"%d",&feat_numb);
		Mat CurHSfeat(feat_numb,HS_DIM,CV_32FC1);
		HEDescrip temp;
		
		int s=0;
		while(!feof(read_feat))
		{
			for (j=0;j<HS_DIM;j++)  
				fscanf(read_feat,"%f",&CurHSfeat.at<float>(s,j));
			s++;			
		}
		HSBowFeature CurBowFeat;
		CurBowFeat.index=index;
		CurBowFeat.HSBowFVSize=WordsNumber;
		CurBowFeat.HSBowFV=(float *)calloc(WordsNumber,sizeof(float));
		Bow(CurHSfeat,Dim, WordsNumber, Words,CurBowFeat.HSBowFV);
		AllBowFeat.push_back(CurBowFeat);
		fclose(read_feat);
	}
	free(Words);
	FILE *out_bow;
	out_bow=fopen(in_file.bowfeat_filename,"w");
	for(i=0;i<AllBowFeat.size()-1;i++)
	{
		fprintf(out_bow,"%d ",AllBowFeat[i].index);
		fprintf(out_bow,"%d ",AllBowFeat[i].HSBowFVSize);
		for (j=0;j<WordsNumber;j++)
			fprintf(out_bow,"%f ",AllBowFeat[i].HSBowFV[j]);
		fprintf(out_bow,"\n");	
	}
	fprintf(out_bow,"%d ",AllBowFeat[i].index);
	fprintf(out_bow,"%d ",AllBowFeat[i].HSBowFVSize);
	for (j=0;j<WordsNumber-1;j++)
		fprintf(out_bow,"%f ",AllBowFeat[i].HSBowFV[j]);
	fprintf(out_bow,"%f",AllBowFeat[i].HSBowFV[j]);
	for(i=0;i<AllBowFeat.size();i++)
		free(AllBowFeat[i].HSBowFV);

	fclose(out_bow);
}


struct kd_node* Read_db_features(char *AllFeaturefile )
{	
	int i,j;
	struct kd_node *kd_root;
	vector <HSBowFeature> DBImageHSBowFeature;
	DBImageHSBowFeature.clear();
	ReadImageHSBowTFeatures(AllFeaturefile,DBImageHSBowFeature);
	int dbf_num=DBImageHSBowFeature.size();
	struct feature *feat_db=new struct feature[dbf_num];
	int dim=DBImageHSBowFeature[0].HSBowFVSize;
	for (i=0;i<dbf_num;i++)
	{
		feat_db[i].descr=new double[dim];
		for (j=0;j<dim;j++)
		{
			feat_db[i].descr[j]=DBImageHSBowFeature[i].HSBowFV[j];
		}
		feat_db[i].d=dim;
		feat_db[i].index=DBImageHSBowFeature[i].index;
	}

	kd_root = kdtree_build( feat_db,dbf_num );

	for(i=0;i<DBImageHSBowFeature.size();i++)
		free(DBImageHSBowFeature[i].HSBowFV);
	return kd_root;
}

void  Extract_Query_feat(IplImage *img, char* WordsFilename, HSBowFeature& TestHSBowFeature)
{
	int i,j;
	float *Words=NULL;
	int Dim,WordsNumber;
	Words = ReadintoWords(WordsFilename,Dim,WordsNumber);
	IplImage *img1;
	img1=crop_image(img);

	Mat image(img1);
	

 	initModule_nonfree();//初始化模块，使用SIFT或SURF时用到  
 	Ptr<FeatureDetector> detector = FeatureDetector::create( "SIFT" );//创建SIFT特征检测器  
 	Ptr<DescriptorExtractor> descriptor_extractor = DescriptorExtractor::create( "SIFT" );//创建特征向量生成器  
 	if( detector.empty() || descriptor_extractor.empty() )  
 		cout<<"fail to create detector!";  
 	vector<KeyPoint> keypoints1;  
 	detector->detect( image, keypoints1 );//检测img1中的SIFT特征点，存储到keypoints1中 
 	Mat descriptors;  
 	descriptor_extractor->compute( image, keypoints1, descriptors );  
	cvReleaseImage(&img1);
/*	FastFeatureDetector detector(100);
	BriefDescriptorExtractor extractor(32); //this is really 32 x 8 matches since they are binary matches packed into bytes

	vector<KeyPoint> kpts_1;
	detector.detect(image, kpts_1);
	Mat descriptors;
	extractor.compute(image, kpts_1, descriptors);*/


	//start extracting SURF features.
// 	int minHessian=1000;
// 
// 	SurfFeatureDetector detector(minHessian);
// 	std::vector<KeyPoint> keypoints;//构造2个专门由点组成的点向量用来存储特征点
// 	detector.detect(image,keypoints);//将img_1图像中检测到的特征点存储起来放在keypoints_1中
// 
// 	SurfDescriptorExtractor extractor;//定义描述子对象
// 	Mat descriptors;//存放特征向量的矩阵
// 	extractor.compute(img,keypoints,descriptors);
// 	if (descriptors.rows<MIN_HS_FEAT_NUM1)
// 	{
// 		minHessian=500;
// 
// 		SurfFeatureDetector detector1(minHessian);
// 		std::vector<KeyPoint> keypoints1;//构造2个专门由点组成的点向量用来存储特征点
// 
// 		detector1.detect(img,keypoints1);//将img_1图像中检测到的特征点存储起来放在keypoints_1中
// 		SurfDescriptorExtractor extractor1;//定义描述子对象
// 
// 		descriptors.release();
// 		extractor1.compute(img,keypoints1,descriptors);
// 
// 	}


	TestHSBowFeature.HSBowFVSize = WordsNumber;
	TestHSBowFeature.HSBowFV = (float *)calloc(TestHSBowFeature.HSBowFVSize,sizeof(float));
	Bow(descriptors,Dim, WordsNumber, Words,TestHSBowFeature.HSBowFV);
	

}


// input query image's filename and dictionary, store features in TestHSBowFeature




//input query image's feature and database's features, use BBF algorithm to match, output the index of similar images. 
void Match_Query_from_db(kd_node* kd_root, HSBowFeature TestHSBowFeature, vector<SimilarInfo>& match_info,char*images_name_file)
{
	int i,j;
	struct feature *img_feat = new struct feature;
	img_feat->d=TestHSBowFeature.HSBowFVSize;
	img_feat->descr = new double[img_feat->d];
	img_feat->index=0;
	for (i=0;i<img_feat->d;i++)
	{
		img_feat->descr[i]=TestHSBowFeature.HSBowFV[i]; 
		//cout<<img_feat->descr[i]<<"  ";
	}

	//pretranverse(kd_root);
	struct feature** nbrs;
	int k;
	k = kdtree_bbf_knn( kd_root, img_feat, K, &nbrs, KDTREE_BBF_MAX_NN_CHKS );
	vector<string> all_img_name;
	ifstream read_fp;
	read_fp.open(images_name_file);
	if (!read_fp)
	{
		cout<<"Can't open image dictionary file\n";
		exit(-1);
	}
	ofstream fk;
	fk.open("F:\\kane_imgname.txt");
	while(!read_fp.eof())
	{
		char readfilename[128];
		read_fp.getline(readfilename,1024,'\n');
		fk<<readfilename<<endl;
		if(strcmp(readfilename,"overkane")==0)
			break;
		string img_file_tmp(readfilename);
		all_img_name.push_back(img_file_tmp);
	}
	read_fp.close();
	fk.close();
	for( i = 0; i < K; i++ )
	{
		SimilarInfo temp_info;
		temp_info.matched_img_name=all_img_name[nbrs[i]->index];
		float *nbrs_tmp=(float *)calloc(TestHSBowFeature.HSBowFVSize,sizeof(float));
		for (j=0;j<TestHSBowFeature.HSBowFVSize;j++)
			nbrs_tmp[j]=nbrs[i]->descr[j];
		temp_info.simil_ratio=vector_correlation(TestHSBowFeature.HSBowFV,nbrs_tmp,TestHSBowFeature.HSBowFVSize);//error
		match_info.push_back(temp_info);
	}

	kdtree_release( kd_root );
	delete[] img_feat->descr;
	delete img_feat;
	free(nbrs);
	free(TestHSBowFeature.HSBowFV);


}

void  Image_COLOR_Match(IplImage *query,FILENAME input_file,vector<SimilarInfo>&match_info)
{
	//vector<SimilarInfo> match_info;
	HSBowFeature TestHSBowFeature;
	vector<int> Similar_Index;
	kd_node *root;
	root=Read_db_features(input_file.colorfeat_file);
	Extract_Query_Colorfeat(query,TestHSBowFeature);
	Match_Query_from_db(root,TestHSBowFeature,match_info,input_file.dbimagesname_file);

	//return match_info;

}

void  Image_BOW_Match(IplImage *query,FILENAME input_file,vector<SimilarInfo>&match_info)
{
	//vector<SimilarInfo> match_info;
	HSBowFeature TestHSBowFeature;
	vector<int> Similar_Index;
	kd_node *root=NULL;
	root=Read_db_features(input_file.bowfeat_filename);	
	Extract_Query_feat(query,input_file.Words_File,TestHSBowFeature);
	Match_Query_from_db(root,TestHSBowFeature,match_info,input_file.dbimagesname_file);

	//return match_info;

}

