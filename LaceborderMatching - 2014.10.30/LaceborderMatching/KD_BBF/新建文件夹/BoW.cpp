#include "Bow.h"

void Bow(Mat& HSDecription_grp, int Dim, int WordsNumber, float *Words,float *Norm_Hist)
{
	int i,j,k,l;
	int FeatureNumber = HSDecription_grp.rows;
	int *Hist;
	float CurFeature[128];

	Hist = (int *)calloc(WordsNumber,sizeof(int));

	for (j = 0; j < WordsNumber; j ++)
	{
		Hist[j] = Norm_Hist[j] = 0;
	}

	for (j = 0; j < FeatureNumber; j ++)
	{
		for (k = 0; k < Dim; k ++)
		{
			CurFeature[k] = HSDecription_grp.at<float>(j,k);
		}
		float Mindis = 9999.0;
		int Minindex;
		for (k = 0; k < WordsNumber; k ++)
		{
			float sum = 0,Curdis;
			for (l = 0; l < Dim; l ++)
			{
				if ((CurFeature[l] - Words[k * Dim + l]) > 0)
					sum = sum + (CurFeature[l] - Words[k * Dim + l]);
				else
					sum = sum + (-1.0)*(CurFeature[l] - Words[k * Dim + l]);
			}

			Curdis = sum/Dim;
			if (Curdis < Mindis)
			{
				Mindis = Curdis;
				Minindex = k;
			}
		}//End of Search Word
		Hist[Minindex] ++;
	}//End of Feature
	//Normaize Histgram
	for (l = 0; l < WordsNumber; l ++)
		Norm_Hist[l] = (float)(Hist[l])/FeatureNumber;

	free(Hist);
}