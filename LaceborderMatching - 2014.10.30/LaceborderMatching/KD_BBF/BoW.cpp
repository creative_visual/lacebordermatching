#include "Bow.h"

void Bow(Mat& HSDecription_grp, int Dim, int WordsNumber, float *Words,float *Norm_Hist)
{
	int i,j,k,l;
	int FeatureNumber = HSDecription_grp.rows;
	Mat VI=Mat::zeros(WordsNumber,Dim,CV_32FC1);
	Mat QueryDesc(WordsNumber,Dim,CV_32F,Words);
	Ptr<DescriptorMatcher> matcher=DescriptorMatcher::create("BruteForce");
//	FlannBasedMatcher matcher;
	vector<DMatch> matches;
	matches.clear();
	float sign;
	float cur_deal;
	matcher->match(HSDecription_grp,QueryDesc,matches );
	for (j = 0; j < FeatureNumber; j ++)
	{
		VI.row(matches[j].trainIdx)=VI.row(matches[j].trainIdx)+(HSDecription_grp.row(j)-QueryDesc.row(matches[j].trainIdx));
	}
	for (i = 0; i < WordsNumber;i ++)
	{
		for (j=0;j<Dim;j++)
		{
			if (VI.at<float>(i,j)<-0.0000001)
			{
				sign=-1.0;
				VI.at<float>(i,j)=sign*sqrt(abs(VI.at<float>(i,j)));

			}
			else
			{
				VI.at<float>(i,j)=sqrt(abs(VI.at<float>(i,j)));
			}

		}
	}

	normalize(VI,VI,1.0,0,NORM_L2);

	for (i=0;i<VI.rows;i++)
	{
		for(j=0;j<VI.cols;j++)
		{
			Norm_Hist[i*VI.cols+j]=VI.at<float>(i,j);

		}
	}
}
