//修改了ReadImageHSBowTFeatures函数
//修改了HSBowFeature结构体，在其中添加了index变量，表示该特征属于哪个图像

#include <ctime>
#include <iostream>
#include <time.h>
#include "surflib.h"
#include "kmeans.h"
#include "Proc_Function.h"
#include "standalone_image.h"
#include "gist.h"
#include "bbf.h"
#include "minpq.h"
#include "match.h"
#include "hesaff.h"
#include "BoW.h"
using namespace std;


int main()
{
	long start1,end1;
	double run_time1;
	start1=clock();
	int i;
	char image_direct[1024]={"F:\\customer0624"};
	char in[1024]={"F:\\customer0624\\5x.jpg"};
	char *curpath;
	curpath = getcwd(NULL, 0);
	string dir(image_direct);
	int pos=dir.find_last_of('\\');
	string dir_name(dir.substr(pos+1));
	char bowfeat[1024];
	sprintf(bowfeat,"%s\\bowfeat_%s.txt",curpath,dir_name.c_str());
	char words[1024];
	sprintf(words,"%s\\words_%s.txt",curpath,dir_name.c_str());
	char dbpath[1024];
	sprintf(dbpath,"%s\\dbpath_%s.txt",curpath,dir_name.c_str());
	char colorfeat[1024];
	sprintf(colorfeat,"%s\\colorfeat_%s.txt",curpath,dir_name.c_str());
	char hsfeatnum[1024];
	sprintf(hsfeatnum,"%s\\hsfeatnum%s.txt",curpath,dir_name.c_str());
	FILENAME input_file;
	strcpy(input_file.bowfeat_filename,bowfeat);
	strcpy(input_file.Words_File,words);
	strcpy(input_file.colorfeat_file,colorfeat);
	strcpy(input_file.dbimagesname_file,dbpath);
	strcpy(input_file.hs_num_one_img_file,hsfeatnum);

	IplImage *img;
	img=cvLoadImage(in);
	
	HSBowFeature TestHSBowFeature;
	vector<int> Similar_Index;
//	Extract_BOW_features(image_direct,input_file);
//	Extrace_COLOR_features(image_direct,input_file);
	end1=clock();
	run_time1=(double)(end1-start1)/CLOCKS_PER_SEC;
	printf("extract group images feature's run_time=%5.3f\n",run_time1);
	cout<<"\nFinish extract bow features.\n";
// 	char result_file[128]="G:\\cluster analysis all2.txt";
// 	freopen(result_file, "w", stdout);
// 	cout<<"query image"<<in<<endl;

	long start2,end2;
	double run_time2;
	start2=clock();
	vector<SimilarInfo> Similar1;
	vector<SimilarInfo> Similar2;
	Image_BOW_Match(img,input_file,Similar1);
//	Image_BOW_Match_reverse(img,input_file,Similar2);
//	Image_COLOR_Match(img,input_file,Similar1);
//	sort(Similar2, compare_SimilarInfo);
	sort(Similar1, compare_SimilarInfo);
	cout<<"\nFinish matching.\n";
	for (int i=0;i<Similar1.size();i++)
	{
		cout<<Similar1[i].matched_img_name<<"  ";
		cout<<Similar1[i].simil_ratio<<endl;
		if((i+1)%12==0)
			cout<<endl;
	}
// 	cout<<"\nFinish matching_reverse.\n";
// 	for (int i=0;i<Similar2.size();i++)
// 	{
// 		cout<<Similar2[i].matched_img_name<<endl;
// 		cout<<Similar2[i].simil_ratio<<endl;
// 	}
//	cvReleaseImage(&img);
	end2=clock();
	run_time2=(double)(end2-start2)/CLOCKS_PER_SEC;
	printf("query image feature extract run_time=%5.3f\n",run_time2);

	return 0;
}


