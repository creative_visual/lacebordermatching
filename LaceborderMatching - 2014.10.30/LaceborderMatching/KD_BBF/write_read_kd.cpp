#include "write_read_kd.h"
#include <fstream>


void Clear(kd_node *p)
{
	if (p->kd_left) Clear(p->kd_left);
	if (p->kd_right) Clear(p->kd_right);
	delete p;
}

void Save1(kd_node *p,long id,ofstream &fout,int& num,int dim)
{
	int i,j;
	num++;
	fout<<id<<"  "<<p->n<<"  "<<p->ki<<"  "<<p->kv<<"  "<<p->leaf<<endl;
	// shufei: bug when - id=40, p->n=3, p->leaf=1, no feature descriptor saved

	//if (p->n==1 || p->leaf==1) -- shufei: should be this? but even if this, still crashes
	/*if (p->leaf==1)   
	{
		for(i=0;i<p->n;i++)
		{
		   for (j=0;j<dim;j++)	
			  fout<<p->features[i].descr[j]<<"  ";
		   fout<<p->features[i].index<<endl;
		}

	}*/
	if (p->n==1)  // 
	{
		for (j=0;j<dim;j++)	
			fout<<p->features[0].descr[j]<<"  ";
		fout<<p->features[0].index<<endl;

	}

	if (p->kd_left)
		Save1(p->kd_left,id*2,fout,num,dim);
	if (p->kd_right) 
		Save1(p->kd_right,id*2+1,fout,num,dim);
}


bool Save(kd_node *root,char* filename,char* numfile)
{
	int node_num=0;
	ofstream fout(filename);
	ofstream fnum(numfile);
	if (fout.fail()) 
		return false;
	int dim=root->features[0].d;
	fout<<dim<<endl;
	if (root) 
		Save1(root,1,fout,node_num,dim);
	fnum<<node_num;
	fnum.close();
	fout.close();
	return true;
}



kd_node * Load(char* filename,char* numfile)
{
	kd_node *root=NULL;
	ifstream fin(filename);
	ifstream fnum(numfile);
	int n;
	kd_node *p;
	int i,j,s,t;
	int count;
	if (fnum.fail()) return NULL;
	if (!(fnum>>n)) return NULL;
	fnum.close();
	int d;
	if (fin.fail()) return NULL;
	fin>>d;
	count=0;
	for (i=0;i<n;i++)
	{
		int id,num,leaf,ki;
		float kv;
		if (!(fin>>id>>num>>ki>>kv>>leaf) || id<=0)  
			// todo: bug - leaf==1, but no BoW stored, shufei
			// happened when id=40
		{
			Clear(root);
			fin.close();
			return NULL;
		}
		if (root==0)
		{
			root=new kd_node;
			count++;
		}
		p=root;
		for (j=30;j>=0;j--)
			if (id & (1<<j))
				break;

		while (j--)
		{
			if (!(id & (1<<j)))
			{
				if (p->kd_left==0)
				{
					p->kd_left=new kd_node;
					//p->left->parent=p;
					count++;
				}
				p=p->kd_left;
			}
			else
			{
				if (p->kd_right==0)
				{
					p->kd_right=new kd_node;
					//p->right->parent=p;
					count++;
				}
				p=p->kd_right;
			}
		}
		float tmp;
		p->n=num;
		p->ki=ki;
		p->kv=kv;
		p->leaf=leaf;
		p->kd_left=NULL;
		p->kd_right=NULL;
		p->features=new struct feature[p->n];
		if(p->leaf==1)
		{
			p->features[0].descr=new double[d];
			for (t=0;t<d;t++)
			{
				fin>>tmp;
				p->features[0].descr[t]=tmp;
			}
			fin>>p->features[0].index;
		}
		/*if(p->leaf==1)
		{
			for(s=0;s<p->n;s++)
			{
				p->features[s].descr=new double[d];
				for (t=0;t<d;t++)
				{
					fin>>tmp;
					p->features[s].descr[t]=tmp;
				}
				fin>>p->features[s].index;
			}
		}*/
	}

	fin.close();
	if (count>n)
	{
		Clear(root);
		return NULL;
	}
	return root;
}



