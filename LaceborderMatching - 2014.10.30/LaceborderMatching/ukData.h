
// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the UKDATA_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// UKDATA_API functions as being imported from a DLL, wheras this DLL sees symbols
// defined with this macro as being exported.

#ifndef UKDATA_H
#define UKDATA_H

#ifdef __cplusplus
extern "C" 
{
#endif

#ifdef UKDATA_EXPORTS
#define UKDATA_API __declspec(dllexport)
#else
#define UKDATA_API __declspec(dllimport)
#endif

#pragma comment(lib,"ukData.lib") 
// 写入数据
// 输入参数 - cData 需要写入的数据
// 输入参数 - iLen 需要写入数据的长度 最多 512*256扇区 = 131072 字节
extern "C" _declspec(dllexport) BOOL _stdcall WriteData(char* cData, int iLen);

// 读取数据
// 输入参数 - cData 读取的数据 必须先申请内存
// 输入参数 - iLen 需要读取数据的长度 最多 512*256扇区 = 131072 字节
extern "C" _declspec(dllexport) BOOL _stdcall ReadData(char* cData, int iLen);

#ifdef __cplusplus
}
#endif
#endif
