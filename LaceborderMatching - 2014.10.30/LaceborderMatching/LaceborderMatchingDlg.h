
// LaceborderMatchingDlg.h : 头文件
//

#pragma once
#include <cv.h>
#include <cxcore.h>
#include <highgui.h>
#include "afxcmn.h"

#include <direct.h>
#include "shlwapi.h"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <ctime>
#include <iostream>
#include <time.h>
#include "KD_BBF/surflib.h"
//#include "KD_BBF/kmeans.h"
#include "KD_BBF/standalone_image.h"
#include "KD_BBF/Proc_Function.h"
#include "KD_BBF/gist.h"
#include "windows.h"

#include <iostream>
#include <fstream>
#include <shlwapi.h>

#include "KD_BBF/hesaff.h"
#include "KD_BBF/Bow.h"
//#include "KD_BBF/write_read_kd.h"
#include "KD_BBF/match.h"
#include "KD_BBF/Proc_Function.h"

#include <queue>

#include "PictureInfo.h"
#include "ShowPicture.h"
#include "SupplierInfo.h"
#include "Tip.h"
#include "ukData.h"


using namespace std;

typedef struct accountData 
{
	CString account;//用户名
	CString password;//密码
	bool Level;//级别
} ACCOUNT;

typedef struct DBData 
{
	vector<CString>SubFileName;
} DATA;

typedef struct MyDate 
{
	int Sum;//次数
	CString Name;
} MY_DATE;

// typedef struct MySimilarity_Info
// {
// 	float simil_ratio;
// 	CString matched_img_name;
// 
// }MySimilarInfo;

typedef struct My_HEDescrip
{
	vector<HEDescrip> feat;

}MyHEDescrip;

// CLaceborderMatchingDlg 对话框
class CLaceborderMatchingDlg : public CDialogEx
{
// 构造
public:
	CLaceborderMatchingDlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
	enum { IDD = IDD_LACEBORDERMATCHING_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持
	
// 实现
protected:
	HICON m_hIcon;


public:
	CString CurrentSelectedPicturePath;//当前点击选中的图片在数据库中的路径
	int CurrentSelectedPictureIndexNum;
	CString CurrentMenu;
	int CurrentDir;//导入多个文件夹，正在处理的文件夹
	vector<CString>  allfile;//导入多个文件夹
	vector<CString>  Supplierinfo;//保存供应商信息
	bool myFlag;

	void OnWritePictureInfo(vector<CString> & info);//将图片信息写入文件
	void OnReadPictureInfo(vector<CString> & info);//读取图片信息
	void GetImageDisplayData(IplImage* dspbuf, IplImage* &imagescale, int hpos, int vpos);
	void ShowPictureByKeyBoard(bool f);//键盘控制显示下一张或上一张图片
	void DeleteSetupPackage();//删除试用版安装包
	void MyDeleteFile(CString FilePath);//删除文件


protected:
	vector<ACCOUNT>  accounts;//记录每个账户的信息。
	IplImage* m_dspbuf;
	IplImage* m_imageBack;

	double m_rate_w,m_rate_h;//全局比例；与1600*838的比例

	CString CurrentPath;
// 	CString Account,Password;
// 	bool Level;//用户级别

	int CurrentAccount;//记录当前账户的accounts的序号

	
	CString CurrentPictureName,CurrentPicturePath;//存储导入待检测图片的文件名

	BOOL LogFlag;//标注是否登录

	CFont   font;

	int m_Width,m_Height;//记录全屏时显示界面长和宽

	CPoint CutLeftTopPoint,CutRightBottomPoint;//记录剪切的图片两个坐标
	BOOL LoadPictureSuccessFlag;//标记导入图片成功，用来解决点击图片打开时，鼠标左键弹起记录CutRightBottomPoint值得问题
	BOOL CutPictureFlag;//标记当前是否要裁剪图片
	BOOL MovePictureFlag;//鼠标按住移动标记
	int current_hpos,current_vpos;//当前导入的图像相对于显示区域的位移
	int manageAccountMenuNum;//标记账户管理时的序号
	bool levelChoose;
	bool deleteFlag;
	int manageDataBaseNum;//标记账户管理时的序号
	vector<CString> CurrentDataBasePicturePath;//保存当前选中的数据库目录下的所有图片文件名
	int DataBasePicture_w,DataBasePicture_h,DataBasePicture_x_num,DataBasePicture_y_num;//记录数据库显示图片缩略图的尺寸，一行一列显示图片数量
	int DataBasePicture_l,DataBasePicture_r,DataBasePicture_t,DataBasePicture_b;//图片显示区域坐标
	int DataBaseSubWindow_l,DataBaseSubWindow_r,DataBaseSubWindow_t,DataBaseSubWindow_b;//数据库显示窗口边界坐标
	int CurrentSubDataBasePageNum;//当前自目录数据库有多少页
	int CurrentSubDataBasePageIndex;//当前是第几页
	CString CurrentSelectedDataBasePath;//当前选中的数据库文件夹路径
	int singlePicture_w,singlePicture_h;

	CImageList m_imageList;
//	DATA DataBaseFileName[5];//5个大类目录名
	vector<CString> DataBaseFileName;//5个大类目录名

	BOOL NewDirFlag;//新建目录标记，新建的同时不打开目录下图片
	bool UpLoadPicureFlag;//区分是数据库导入单张图片还是匹配导入待检索图片
	bool showPageFlag;
	bool ShowingMatchingPicturesFlag;

	int k_t,k_b,k_l,k_r;//记录图像显示区域

	CPoint leftTop,RightButom;//记录裁剪区域坐标
	bool Texture_Colour_Flag;//纹理 颜色 搜索标志 
//	CPoint CurrentPoint;//记录当前鼠标点
	CString AntiCounterfeitingLabel;

//	int date[6];//保存搜索日期

	int  date[2];//保存搜索日期

	vector<int> DateNum;//记录索索记录的频率次数

	vector<float> SimilarityRate;

	CString CutPicturePath;//存储被剪切的文件路径

	BOOL CreatingDataBaseFlag;//正在创建数据库标记，正在创建时不可以进行其他操作
	BOOL LoadingPictureFlag;//正在文件夹图片，不可以进行其他操作
	bool ShowOriginalPictureFlag;

	void MyInitSet();//初始化设置
	void LogInBackGround();//登录背景
//	void GetImageDisplayData(IplImage* dspbuf, IplImage* &imagescale, int hpos, int vpos);
	void SetBackPic(CString pathname,int x,int y,int w,int h,int p_x,int p_y,bool f);
	void OnLoging();
	void SetPointColour(IplImage* dst,int x,int y,IplImage* src,int p_x,int p_y);
	void SetPointColour(IplImage* dst,int x,int y,int r,int g,int b);
	void LogSuccessfulBackGround();
	BOOL verifyAccount(CString Account,CString password);//验证账号
	void ShowText(int x,int y,BYTE R,BYTE G,BYTE B,LOGFONT lf,CString tip,bool f=true);
	void ShowMyText();
	void InitMenu();//初始化菜单按钮
	void InitEachSideOfMenu(int MenuNum);//初始化菜单按钮
	void ClickMenu(int num);//初始化菜单按钮
	void InitMenuBackground(int num);//初始化背景布局
	void MatchingBackground();//初始化图像匹配界面背景布局
	void LoadPicture();
	void ShowPicture();//显示当前导入的图片
	void ShowLoadedPicture();
	void ShowPictureCutLine();//显示裁剪线
	void MovedPicture(int d_x,int d_y);//显示移动后的图片
	void SearchByTexture();//以文理搜索
	void SearchByColour();//以颜色搜索
	void MovePic();//鼠标拖动
	void CutPic();//裁剪
	void InitPictureColour(IplImage* image,int r,int g,int b);//初始化图片颜色值
	void InitPictureColour(IplImage* image,int l,int r,int t,int b,int R,int G,int B);//初始化图片一个区域
	void CopyInHorizontal(IplImage* image,int l,int r,int t,int b,int x);//水平方向复制一段图片,横向复制范围包括两个端点，纵向不包括下边界
	void CopyInVertical(IplImage* image,int l,int r,int t,int b,int y);
	void CopyImageInHorizontalByMirror(IplImage* image,int l,int r,int t,int b,int x);//水平方向复制一段图片,镜面复制
	void CopyImageInHorizontal(IplImage* image,int l,int r,int t,int b,int x);//水平方向复制一段图片,平移复制
	void CopyImageInVerticalByMirror(IplImage* image,int l,int r,int t,int b,int y);//垂直方向复制一段图片,镜面复制
	void ManageAccountBackground();
	void ManageAccountClick(int num);//点击3个按钮
	void InitAcounts();//初始化账户
	void WriteCString(CString s,FILE *fp);//将字符串写入文件
	void ReadCString(CString &s,FILE *fp);//从文件中读取CString
	void WriteAccount(vector<ACCOUNT>  &accounts,char *filePath);//将当前所有账户写入user.acc
	void ReadAccount(vector<ACCOUNT>  &accounts,char *filePath);//从user.acc读入所有账户
	void changePassword();//修改密码
	void createAccount();//创建账户
	void manageAllAccount();//管理账户
	void manageAccountHide();//隐藏账户管理所有控件
	void SureManageAccount();//确定修改 账户
	void RadioChoose(int x,int y,int _x,int _y);//切换两个平行的按钮，f==true，前一个选中，否则后一个选中
	void SingleRadio(int x,int y,bool f);//单选按钮
	void ManageDataBaseClick(int num);//数据库管理 点击3个按钮 1,2,3
	void ManageDataBaseBackground();//初始化数据库管理界面背景布局
	void SubMenuLine(int l,int r,int t,int b);//数据库管理和账户管理的子菜单框边界
	void ShowCurrentDataBasePicture(CString path);//显示当前选中的数据库目录里面的搜有图片
	void GetAllFile(CString path,CString format,vector<CString> &CurrentDataBasePicturePath,bool f);//获取目录下的所有图片路径,f==true,每次清空容器
	void ClearCStringVector(vector<CString> &data);
	void GetDataBasePictureSize();//获取数据库显示缩略图的图片显示大小和显示数目
	void ShowEachPagePicture(int num);//显示每一页的图片
	void SetPictureInAPart(IplImage* image,int x,int y,int w,int h,CString pathname);//在一个指定区域显示缩略图
	void InitTree();//初始化树控件
	void ReadDataBaseInformation(char *filePath);//读取数据库信息
	void WriteDataBaseInformation(char *filePath);//读取数据库信息
	void CreateDataBaseDir();//初始化数据库目录。如果没有就创建
	void CreateNewDir(CString path);//新建一个目录
	void DeleteDir(CString path);//删除一个目录
	void GetCurrentDir(CString &path);//获取当前选中的目录的物理目录
	BOOL DirectoryEmpty(CString path,CString format);//判断目录下面有没有.jpg格式文件，没有就是空
	void UpLoadAllpicture();//导入一个文件夹下的所有图片
	void ChooseADir(CString &DirPath);//选择一个文件夹
	void transformPicture(vector<CString> &CurrentDataBasePicturePath);//将导入的图片全部转换为jpg存放到数据库
	void ChangeName(CString &Path,CString DirPath);//修改保存路径
	void RateOfAdvance(IplImage* image,int l,int r,int t,int b);//画一个进度条
	void RateOfAdvance(IplImage* image,int l,int r,int t,int b,float rate);//画一个进度条
	void LoadingPicture(CString &CurrentPath,CString DirPath);//移动图片
	void changePage(CPoint point);//显示前一页与后一页按钮
	void CurrentDataBasePictureFeature();//创建数据库特征
	void GetAllPicturePath(vector<CString>&AllPicturePath);//获取当前数据库所有文件
	void Extract_HS_features_Single(CString  PicturePath,int m_nFrameInd,int &DBnum_updated,queue<int>& images_leftout, int &count_leftout,PCA &sip/*,int *number_feat*/);//提取单个图片特征
	void GetAllDirPath(vector<CString>&AllDirPath);//获取当前数据库所有文件夹
	void CreateWords(char *WordsFile,vector<CString> &AllFeatureDATAfilename,vector<int> FeaturesNum,int &rate_num,float PictureNum);//为每个目录创建字典文件
	void Image_Matching();//匹配函数
	void Matching_Image_from_db(HSBowFeature TestHSBowFeature,vector <HSBowFeature> DBImageHSBowFeature, vector<MySimilarInfo>& match_info,CString DirPath);
	void GetCurrentDirAllPicturePath(CString CurrentSelectedDir,vector<CString>&AllPicturePath,int &FeatureCounts);//获取当前所选目录下所有图片文件文件，包括子目录下
	void ShowingMatchingPictures();//显示匹配结果
	void GetPointInPicture(int &x,int &y,CPoint &point);//获取裁剪区域两端点在原图的坐标
	void SureCutPicture();//确定裁剪图片
	int GetCurrentPicturePath(CString &path,CPoint point);//获取当前点击的图片的路径
	void OnReNamePicture(CString newName);//对当前选中的图片重命名

	void SearchFlashDrive();//检测U盘
	void WriteAntiCounterfeitingLabel();//写 防伪标识文件
	BOOL verifyDate();//验证统计信息的输入格式
	void WriteDateFile(vector<MY_DATE>&dateData,char *filePath);
	void ReadDateFile(vector<MY_DATE>&dateData,char*  filePath,bool flag=true);//默认清空不存在的记录
	void GetDatePicturePath();//获取搜索日期范围内的Date文件，并获取图片频率
	void ShowEachPictureName(int num);//显示每一页的图片 搜索频率和文件名
	void MakeFixLenString(CString &str, int len);

	void SaveReport();
	void CreateReport(CString path);
	void WriteReport(CString s,FILE *fp);
	void SaveEachPictureFeatureCounts(CString PicturePath,int num);//保存每个图片HS特征个数

	void DeleteSetUpFile();//删除安装信息
	void CreatePictureAddAndDeleteFlagFile();//创建数据库增减标记文件
	void DeletePictureAddAndDeleteFlagFile(CString DirPath);//删除数据库增减标记文件

	void CurrentPathFile();//记录当前软件运行的路径，以确保FeatureCounts记录的路径不会出错，主要用来限制用户安装软件后不可随意移动安装目录

	void MoveResourceForSetUp();//安装结束后，将当前目录的图片移到resource文件夹下
	BOOL PreTranslateMessage(MSG* pMsg);
	void ShowPageByKeyBoard(bool f);
	void ShowGoToPage();//显示 跳转到某一页
	void SetPageNum();
	void TestFlashDrive();

	void ShowOriginalPicture(CString path, CPoint point);//鼠标移动显示原图
	void GetPictureCounts(CString path,CString format,int &counts);//获取目录下的所有图片数目
	BOOL LoadingPictureCountsTest(CString path,int counts);//检测当前目录新增图片后，图片数量是否多于10000

	BOOL TipBeforeClose();//退出前的提示
	void UnLock(CString dirPath);
	void Lock(CString dirPath);
	void ReName(CString s,CString text);
	void UnRegisterDateLimited();//记录软件第一次运行的日期，限制试用期为一个月
	void InitDataBaseDir(int index);//初始化数据库目录

protected://记录坐标和长度的变量
	int MenuLenth[4];//四个菜单按钮的长度
	int MenuHeight;//菜单按钮的高度
	int MenuDistance;//两个按钮之间距离
	int Menu_x[4];//按钮的横坐标
	int Menu_y;//按钮的纵坐标

	CString leftTitlePath;
	int leftTitlePath_x,leftTitlePath_y,leftTitlePath_w,leftTitlePath_h;

	int BlueLine;//菜单栏下边的蓝色分界线

	CString MenuPath[11];//按钮的路径
	CString MenuName[4];//四个按钮名称

	int LoadPicture_x,LoadPicture_y;//图像匹配界面，导入图像的按钮坐标
	int LoadPicture_w,LoadPicture_h;//按钮长宽
	CString LoadPicture_path;

	int search_x,search_y;//图像匹配界面，搜索按钮坐标
	int search_w,search_h;//按钮长宽
	CString search_path;

	int ShowPicture_l,ShowPicture_r,ShowPicture_t,ShowPicture_b;//加载图片后的显示区域
	IplImage* LoadedPicture;//存储导入的图片

	int SearchByTexture_x,SearchByTexture_y,SearchByColour_x,SearchByColour_y;//搜索按纹理和颜色的坐标
	int Move_x,Move_y,Cut_x,Cut_y;//拖动和裁剪坐标
	int radio_w,radio_h;

	int ManageAccountSure_x,ManageAccountSure_y,ManageAccountSure_w,ManageAccountSure_h;//账户管理 确定修改

	int LevelTrue_x,LevelTrue_y,LevelFalse_x,LevelFalse_y;//级别坐标

	int UpLoadSinglePicture_x,UpLoadSinglePicture_y,UpLoadSinglePicture_w,UpLoadSinglePicture_h;//数据库界面，导入单个图片的按钮坐标
	int UpLoadAllPicture_x,UpLoadAllPicture_y,UpLoadAllPicture_w,UpLoadAllPicture_h;//数据库界面，批量图片的按钮坐标

	int GoBack_x,GoBack_y,GoBack_w,GoBack_h;//匹配显示界面 的 返回按钮
	int SureCut_x,SureCut_y,SureCut_w,SureCut_h;//确定裁剪按钮

	void InitSetValue();//初始化设置变量的值

	void DrawLineWindows();//数据库显示缩略图大小改变后，显示网格

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	virtual void OnOK();
	afx_msg void OnMouseHWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	CTreeCtrl m_tree;
	afx_msg void OnTvnEndlabeleditTreeDatabase(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMRClickTreeDatabase(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnAddsubclass();
	afx_msg void OnDeletesubclass();
	afx_msg void OnTvnSelchangedTreeDatabase(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnClose();
	afx_msg void OnEditPictureInfo();
	afx_msg void OnSavePictureAs();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnDeletePictureFromDataBase();
	afx_msg void OnCutPictureFromDataBase();
	afx_msg void OnPastePictureFromDataBase();
	afx_msg void OnCbnSelchangeComboGo();
	afx_msg void OnDropFiles(HDROP hDropInfo);
	afx_msg void OnDeAll();
};

int CALLBACK SetSelect_CB(HWND win, UINT msg, LPARAM param, LPARAM data) ;