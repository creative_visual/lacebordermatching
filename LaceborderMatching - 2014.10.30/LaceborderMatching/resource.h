//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by LaceborderMatching.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_LACEBORDERMATCHING_DIALOG   102
#define IDR_MAINFRAME                   128
#define IDC_CURSOR_HAND                 129
#define IDR_MENU_TREE                   130
#define IDI_ICON_DATABASE               131
#define IDD_DIALOG_PICTURE_INFO         132
#define IDD_DIALOG_SHOW_PIC             133
#define IDD_DIALOG1                     134
#define IDD_DIALOG_SUPPLIER             134
#define IDD_DIALOG2                     135
#define IDC_STATIC_BACKGROUND           1000
#define IDC_EDIT_ACCOUNT                1001
#define IDC_EDIT_PASSWORD               1002
#define IDC_EDIT_PASSWORD_SED           1004
#define IDC_COMBO_ACCOUNT               1005
#define IDC_TREE_DATABASE               1006
#define IDC_RADIO1                      1007
#define IDC_BUTTON1                     1008
#define IDC_EDIT1                       1009
#define IDC_EDIT2                       1010
#define IDC_EDIT3                       1011
#define IDC_EDIT4                       1012
#define IDC_EDIT5                       1013
#define IDC_EDIT6                       1014
#define IDC_EDIT7                       1015
#define IDC_EDIT8                       1016
#define IDC_EDIT9                       1017
#define IDC_EDIT10                      1018
#define IDC_EDIT11                      1019
#define IDC_SCROLLBAR1                  1020
#define IDC_EDIT12                      1020
#define IDC_SCROLLBAR2                  1021
#define IDC_EDIT13                      1021
#define IDC_STATIC_SHOW                 1022
#define IDC_EDIT14                      1022
#define IDC_EDIT_DATE                   1023
#define IDC_EDIT15                      1023
#define IDC_EDIT_DAT                    1024
#define IDC_STATIC_TIME                 1025
#define IDC_STATIC_TO                   1026
#define IDC_DATETIMEPICKER_FROM         1027
#define IDC_DATETIMEPICKER2             1028
#define IDC_DATETIMEPICKER_TO           1028
#define IDC_STATIC_PRO                  1029
#define IDC_CHECK1                      1030
#define IDC_LIST1                       1030
#define IDC_STATIC_All                  1031
#define IDC_STATIC_CUR                  1032
#define IDC_COMBO_GO                    1033
#define IDC_BUTTON2                     1034
#define ID_32771                        32771
#define ID_32772                        32772
#define ID_ADDSUBCLASS                  32773
#define ID_DELETESUBCLASS               32774
#define ID_32775                        32775
#define ID_32776                        32776
#define ID_32777                        32777
#define ID_32778                        32778
#define ID_32779                        32779
#define ID_32780                        32780
#define ID_32781                        32781
#define ID_32782                        32782
#define ID_DE_ALL                       32783

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        136
#define _APS_NEXT_COMMAND_VALUE         32784
#define _APS_NEXT_CONTROL_VALUE         1035
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
