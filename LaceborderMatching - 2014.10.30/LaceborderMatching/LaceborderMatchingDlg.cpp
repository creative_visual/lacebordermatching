
// LaceborderMatchingDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "LaceborderMatching.h"
#include "LaceborderMatchingDlg.h"
#include "afxdialogex.h"



#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//extern int feat_num_flag;
// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CLaceborderMatchingDlg 对话框


CString g_CutPicturePath;

CLaceborderMatchingDlg::CLaceborderMatchingDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CLaceborderMatchingDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

	m_dspbuf=NULL;
	m_imageBack=NULL;

	m_Width=0;
	m_Height=0;

	LogFlag=FALSE;

	LoadPictureSuccessFlag=FALSE;
	CutPictureFlag=FALSE;
	MovePictureFlag=TRUE;

	deleteFlag=false;
	NewDirFlag=FALSE;
	UpLoadPicureFlag=false;
	showPageFlag=true;

	ShowingMatchingPicturesFlag=false;
	CurrentSubDataBasePageIndex=0;
	CurrentSubDataBasePageNum=0;

	CutPicturePath="";

	CreatingDataBaseFlag=FALSE;
	LoadingPictureFlag=FALSE;
	ShowOriginalPictureFlag=false;
}

void CLaceborderMatchingDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TREE_DATABASE, m_tree);
}

BEGIN_MESSAGE_MAP(CLaceborderMatchingDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_LBUTTONDOWN()
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONUP()
	ON_WM_RBUTTONDOWN()
	ON_WM_MOUSEHWHEEL()
	ON_WM_MOUSEWHEEL()
	ON_NOTIFY(TVN_ENDLABELEDIT, IDC_TREE_DATABASE, &CLaceborderMatchingDlg::OnTvnEndlabeleditTreeDatabase)
	ON_NOTIFY(NM_RCLICK, IDC_TREE_DATABASE, &CLaceborderMatchingDlg::OnNMRClickTreeDatabase)
	ON_COMMAND(ID_ADDSUBCLASS, &CLaceborderMatchingDlg::OnAddsubclass)
	ON_COMMAND(ID_DELETESUBCLASS, &CLaceborderMatchingDlg::OnDeletesubclass)
	ON_NOTIFY(TVN_SELCHANGED, IDC_TREE_DATABASE, &CLaceborderMatchingDlg::OnTvnSelchangedTreeDatabase)
	ON_WM_CLOSE()
	ON_COMMAND(ID_32776, &CLaceborderMatchingDlg::OnEditPictureInfo)
	ON_COMMAND(ID_32777, &CLaceborderMatchingDlg::OnSavePictureAs)
	ON_WM_CTLCOLOR()
	ON_COMMAND(ID_32778, &CLaceborderMatchingDlg::OnDeletePictureFromDataBase)
	ON_COMMAND(ID_32780, &CLaceborderMatchingDlg::OnCutPictureFromDataBase)
	ON_COMMAND(ID_32781, &CLaceborderMatchingDlg::OnPastePictureFromDataBase)
	ON_CBN_SELCHANGE(IDC_COMBO_GO, &CLaceborderMatchingDlg::OnCbnSelchangeComboGo)
	ON_WM_DROPFILES()
	ON_COMMAND(ID_DE_ALL, &CLaceborderMatchingDlg::OnDeAll)
END_MESSAGE_MAP()


// CLaceborderMatchingDlg 消息处理程序

BOOL CLaceborderMatchingDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	theApp.pLaceborderMatchingDlg=this;

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码

	

	AntiCounterfeitingLabel="刻维花边匹配系统防伪标识码--2014--CogniVisual Technologies";
	

//	WriteAntiCounterfeitingLabel();
//////////////////////////////////////试用版 有效期至 2014.6.1/////////////////////////////////////
	SYSTEMTIME tmp_time;
	GetLocalTime(&tmp_time);
	CString AnalysisTime;
	AnalysisTime.Format("%4d%02d%02d",tmp_time.wYear,tmp_time.wMonth,tmp_time.wDay);

	int test_date;
	test_date=atoi(AnalysisTime);
	if (test_date>20140615)
	{
// 		AfxMessageBox("试用版截止到 2014年6月15日！！");
// 
// 		exit(-1);
	}
/////////////////////////////////////////////////////////////////////////////

	
	MyInitSet();

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}
void CLaceborderMatchingDlg::SearchFlashDrive()//检测U盘
{
	vector<CString> USBPath;
	CString tmp;
	CString FileName="....\\刻维花边匹配系统防伪标识码.ini";

	int i=0;
	char diskPath[5]={0};
	char c='C';
	for (i=0;i<24;i++)
	{
		sprintf( diskPath, "%c", c+i );
		strcat( diskPath, ":\\" );
		if (PathFileExists(diskPath))
		{
			if ( GetDriveType(diskPath)==DRIVE_REMOVABLE )
			{
				tmp.Format("%s",diskPath);
//				MessageBox(diskPath);
				USBPath.push_back(tmp);

			}
		}	
	}

	int num=USBPath.size();
	BOOL flag=FALSE;

	for (i=0;i<num;i++)
	{
		tmp=USBPath[i]+FileName;

		FILE *File=NULL;

		fopen_s(&File,tmp,"rb");//为读写建立新的二进制文件
		if (File)//打开成功
		{
			ReadCString(FileName,File);
			fclose(File);
		}
	
		File=NULL;
		if (FileName==AntiCounterfeitingLabel)
		{
			flag=TRUE;
			break;
		}
	}

	if (flag==FALSE)
	{
		AfxMessageBox("请插入带有 刻维花边匹配系统防伪标识码 的U盘！！ ");
		exit(-1);
	}

// 	char diskPath[5] = { 0 };
// 	DWORD allDisk = GetLogicalDrives(); //返回一个32位整数，将他转换成二进制后，表示磁盘,最低位为A盘
// 
// 	if (allDisk!=0)
// 	{
// 		for (int i=0;i<11;i++)     //假定最多有10个磁盘
// 		{
// 			if ((allDisk & 1)==1)
// 			{
// 				sprintf( diskPath, "%c", 'C'+i );
// 				strcat( diskPath, ":\\" );
// 
// 				if ( GetDriveType(diskPath)==DRIVE_REMOVABLE )
// 				{
// 					AfxMessageBox( "检测到U盘", MB_OK);
// 					AfxMessageBox( diskPath, MB_OK );
// 
// 					if( GetVolumeInformation(diskPath,0,0,0,0,0,0,0) ) //判断驱动是否准备就绪
// 					{
// 						AfxMessageBox( "U盘就绪", MB_OK);
// 						break;
// 					}
// 				}
// 			}
// 
// 			allDisk = allDisk>>1;
// 		}
// 	}
// 
// 	if ((allDisk & 1)!=1) //未检测到U盘
// 	{
// 		AfxMessageBox( "未检测到U盘,请插入U盘", MB_OK);
// 	}
}

void CLaceborderMatchingDlg::DeleteSetUpFile()//删除安装信息
{
	TCHAR DeskToppath[255];
	SHGetSpecialFolderPath(0,DeskToppath,CSIDL_DESKTOPDIRECTORY,0);//获取桌面路径

	CString path;
	path.Format(_T("%s"),DeskToppath);

	int pos =path.ReverseFind('\\')+1;
	path =path.Left(pos);

	CString newName=path+"CogniVisual Technologies.exe";
	CString NewNameForDelete=path+"CogniVisualTechnologies.exe";//为删除隐藏文件新命名的文件名
	path+="CogniVisual Technologies";

	

	CString setupexePath=path;

	if (PathFileExists(setupexePath))
	{
		TCHAR* filename =setupexePath.GetBuffer(setupexePath.GetLength()); 
		DeleteFile(filename);
	}
	if (PathFileExists(newName))
	{
 		TCHAR* filename =newName.GetBuffer(newName.GetLength()); 


		TCHAR* pOldName =filename; 
		TCHAR* pNewName =NewNameForDelete.GetBuffer(NewNameForDelete.GetLength()); 
		CFile::Rename(pOldName, pNewName);

// 		CString t="del /F "+NewNameForDelete;//包括隐藏、只读、系统文件
// 		CString cmd=_T("cmd /c ");
// 		cmd+=t; 
// 		WinExec((LPCTSTR)cmd,SW_HIDE);
		DeleteFile(pNewName);

//		DeleteFile(filename);
	}

}
void CLaceborderMatchingDlg::CurrentPathFile()//记录当前软件运行的路径，以确保FeatureCounts记录的路径不会出错，主要用来限制用户安装软件后不可随意移动安装目录
{
	TCHAR DeskToppath[255];
	SHGetSpecialFolderPath(0,DeskToppath,CSIDL_DESKTOPDIRECTORY,0);//获取桌面路径

	CString path;
	path.Format(_T("%s"),DeskToppath);

	int pos =path.ReverseFind('\\')+1;
	path =path.Left(pos);

	path+="CogniVisualTechnologiesSystemPath";//这个文件记录当前exe路径

	CString readingPath;
	FILE *File=NULL;

	if (PathFileExists(path))//存在则读取路径，判断是否与其相等
	{
		DeleteSetUpFile();//删除安装包文件

		fopen_s(&File,path,"rb");//为读写建立新的二进制文件
		if (File)//打开成功
		{
			ReadCString(readingPath,File);
			fclose(File);
		}
		if (readingPath!=CurrentPath)
		{
			AfxMessageBox("软件安装路径被移动，请将软件恢复到原安装目录，或者重新安装！！");
			exit(-1);
		}
		
	}
	else //路径不存在说明 安装后第一次使用，或者人为删除该文件
	{
		fopen_s(&File,path,"wb");//为读写建立新的二进制文件
		if (File)//打开成功
		{
			WriteCString(CurrentPath,File);		
		}

		fclose(File);
	}


	File=NULL;

	

}
void CLaceborderMatchingDlg::WriteAntiCounterfeitingLabel()//写 防伪标识文件
{
	FILE *File=NULL;

	fopen_s(&File,"F:\\刻维花边匹配系统防伪标识码.ini","wb");//为读写建立新的二进制文件
	if (File)//打开成功
	{
		WriteCString(AntiCounterfeitingLabel,File);		
	}

	fclose(File);
	File=NULL;
}
void CLaceborderMatchingDlg::InitSetValue()//初始化设置变量的值
{
	leftTitlePath=CurrentPath+"\\resource\\leftTitle.jpg";
	leftTitlePath_x=10;
	leftTitlePath_y=10;
	leftTitlePath_w=366;
	leftTitlePath_h=55;

	BlueLine=leftTitlePath_y+leftTitlePath_h+leftTitlePath_y;

	MenuLenth[0]=68;
	MenuLenth[1]=81;
	MenuLenth[2]=55;
	MenuLenth[3]=55;
	MenuHeight=34;

	MenuDistance=(int)((m_Width-800-MenuLenth[0]-MenuLenth[1]-MenuLenth[2]-MenuLenth[3]-42*4)/8.0);//分辨率横向至少 1225
	Menu_x[0]=400+21+MenuDistance;
	Menu_x[1]=400+21+MenuDistance+MenuLenth[0]+MenuDistance+21+21+MenuDistance;
	Menu_x[2]=m_Width-400-21-MenuDistance-MenuLenth[3]-MenuDistance-21-21-MenuDistance-MenuLenth[2];
	Menu_x[3]=m_Width-400-21-MenuDistance-MenuLenth[3];
	Menu_y=BlueLine-MenuHeight;
	MenuPath[0]=CurrentPath+"\\resource\\database01.jpg";
	MenuPath[1]=CurrentPath+"\\resource\\matching01.jpg";
	MenuPath[2]=CurrentPath+"\\resource\\statistics01.jpg";
	MenuPath[3]=CurrentPath+"\\resource\\account01.jpg";
	MenuPath[4]=CurrentPath+"\\resource\\database02.jpg";
	MenuPath[5]=CurrentPath+"\\resource\\matching02.jpg";
	MenuPath[6]=CurrentPath+"\\resource\\statistics02.jpg";;
	MenuPath[7]=CurrentPath+"\\resource\\account02.jpg";
	MenuPath[8]=CurrentPath+"\\resource\\title_left01.jpg";//灰色左半边
	MenuPath[9]=CurrentPath+"\\resource\\title_left02.jpg";//蓝色选中后的左半边
	MenuPath[10]=CurrentPath+"\\resource\\title_left03.jpg";//蓝色选中后的左半边无渐变

	MenuName[0]="database";//数据库管理
	MenuName[1]="matching";//图像匹配
	MenuName[2]="statistics";//统计信息
	MenuName[3]="account";//账户管理

	LoadPicture_x=290;
	LoadPicture_y=BlueLine+20;
	LoadPicture_w=79;
	LoadPicture_h=26;
	LoadPicture_path=CurrentPath+"\\resource\\loadPic.jpg";
	search_x=290;
	search_y=LoadPicture_y+30;
	search_w=79;
	search_h=26;
	search_path=CurrentPath+"\\resource\\search.jpg";

	ShowPicture_l=285;
	ShowPicture_r=m_Width-15;
	ShowPicture_t=search_y+35;
	ShowPicture_b=m_Height-43;

	LoadedPicture=NULL;//cvCreateImage(cvSize((ShowPicture_r-ShowPicture_l),(ShowPicture_b-ShowPicture_t)),8,4);

	SearchByTexture_x=430;
	SearchByTexture_y=BlueLine+50;
	SearchByColour_x=500;
	SearchByColour_y=BlueLine+50;//搜索按纹理和颜色的坐标
	Move_x=600;
	Move_y=BlueLine+50;
	Cut_x=670;
	Cut_y=BlueLine+50;
	radio_w=17;
	radio_h=17;

	SureCut_x=750;
	SureCut_y=BlueLine+44;
	SureCut_w=80;
	SureCut_h=26;//确定裁剪按钮


	ManageAccountSure_x=350;
	ManageAccountSure_w=93;
	ManageAccountSure_y=BlueLine+330;
	ManageAccountSure_h=25;

	LevelTrue_x=480;
	LevelTrue_y=BlueLine+260;
	LevelFalse_x=580;
	LevelFalse_y=BlueLine+260;

	DataBaseSubWindow_l=285;//左边缘线
	DataBaseSubWindow_r=m_Width-15;//右边缘线
	DataBaseSubWindow_t=BlueLine+15;//上边沿线
	DataBaseSubWindow_b=m_Height-33-10;//下边缘线

	singlePicture_w=150;//200  150
	singlePicture_h=120;//180  120

	GetDataBasePictureSize();//获取数据库显示缩略图的图片显示大小和显示数目
	
	InitTree();//初始化树控件
	CurrentSelectedDataBasePath=CurrentPath+"\\DataBase";

	UpLoadSinglePicture_x=290;
	UpLoadSinglePicture_y=BlueLine+50;
	UpLoadSinglePicture_w=94;
	UpLoadSinglePicture_h=25;//数据库界面，导入单个图片的按钮坐标
	UpLoadAllPicture_x=390;
	UpLoadAllPicture_y=BlueLine+50;
	UpLoadAllPicture_w=94;
	UpLoadAllPicture_h=25;//数据库界面，批量图片的按钮坐标

	manageDataBaseNum=1;

	GoBack_x=350;
	GoBack_y=BlueLine+35;
	GoBack_w=44;
	GoBack_h=25;//返回按钮



}
void CLaceborderMatchingDlg::GetDataBasePictureSize()//获取数据库显示缩略图的图片显示大小和显示数目
{
	
	DataBasePicture_x_num=(int)((float)(DataBaseSubWindow_r-DataBaseSubWindow_l)/(singlePicture_w+6));//长宽 160:90 四周各留 3像素
	DataBasePicture_y_num=(int)((float)(DataBaseSubWindow_b-20-DataBaseSubWindow_t-32)/(singlePicture_h+6));

	DataBasePicture_w=(int)((float)(DataBaseSubWindow_r-DataBaseSubWindow_l)/DataBasePicture_x_num);
	DataBasePicture_h=(int)((float)(DataBaseSubWindow_b-20-DataBaseSubWindow_t-32)/DataBasePicture_y_num);

	DataBasePicture_l=(DataBaseSubWindow_r+DataBaseSubWindow_l-DataBasePicture_w*DataBasePicture_x_num)/2;//数据库图片显示区域
	DataBasePicture_r=DataBasePicture_l+DataBasePicture_w*DataBasePicture_x_num;
	DataBasePicture_t=(DataBaseSubWindow_b-20+DataBaseSubWindow_t+32-DataBasePicture_h*DataBasePicture_y_num)/2;;
	DataBasePicture_b=DataBaseSubWindow_t+32+DataBasePicture_h*DataBasePicture_y_num;
}
void CLaceborderMatchingDlg::ReadDataBaseInformation(char *filePath)//读取数据库信息
{
	int n=5;
	int num;
	int i,j;
	CString tmp;

	FILE *DataBaseFile=NULL;

	fopen_s(&DataBaseFile,filePath,"rb");//为读写建立新的二进制文件
	if (DataBaseFile)//打开成功
	{
		while(!feof(DataBaseFile))//(feof返回1，说明到文件结尾)
		{
			fread(&num,sizeof(int),1,DataBaseFile);
			n=num;
			ClearCStringVector(DataBaseFileName);
			for (i=0;i<n;i++)
			{
				ReadCString(tmp,DataBaseFile);
				DataBaseFileName.push_back(tmp);

// 				if (num==0)
// 				{
// 					continue;
// 				}
// 				for (j=0;j<num;j++)
// 				{
// 					
// 					ReadCString(tmp,DataBaseFile);
// 					DataBaseFileName[i].SubFileName.push_back(tmp);
// 				}
				
			}
			ReadCString(tmp,DataBaseFile);
			if ("Adonis  2014"==tmp)
			{
				break;
			}
		}		
	}
	fclose(DataBaseFile);
	DataBaseFile=NULL;
}
void CLaceborderMatchingDlg::WriteDataBaseInformation(char *filePath)//读取数据库信息
{
	int n=(int)DataBaseFileName.size();
	int num;
	int i,j;
	
	FILE *DataBaseFile=NULL;

	fopen_s(&DataBaseFile,filePath,"wb");//为读写建立新的二进制文件
	if (DataBaseFile)//打开成功
	{
		num=(int)DataBaseFileName.size();
		fwrite(&num,sizeof(int),1,DataBaseFile);

		for (i=0;i<n;i++)
		{
			WriteCString(DataBaseFileName[i],DataBaseFile);
// 			num=(int)DataBaseFileName[i].SubFileName.size();
// 			fwrite(&num,sizeof(int),1,DataBaseFile);
// 			if (num>0)
// 			{
// 				for (j=0;j<num;j++)
// 				{
// 					WriteCString(DataBaseFileName[i].SubFileName[j],DataBaseFile);
// 				}
// 			}
		}		
	}
	//////////////////////////////////////////////////////////////////////////
	WriteCString("Adonis  2014",DataBaseFile);//账户结束标志 禁止用户注册这个用户名
	//////////////////////////////////////////////////////////
	fclose(DataBaseFile);
	DataBaseFile=NULL;
}

void CLaceborderMatchingDlg::CreateNewDir(CString path)//新建一个目录
{
	if(!PathFileExists(path))
	{
		_mkdir(path);
	}
}
void CLaceborderMatchingDlg::DeleteDir(CString path)//删除一个目录
{
	if(PathFileExists(path))
	{
		_rmdir(path);//删除一个目录，若成功则返回0，否则返回-1
	}
}
void CLaceborderMatchingDlg::CreateDataBaseDir()//初始化数据库目录。如果没有就创建
{
	CString path=CurrentPath+"\\DataBase";
	CreateNewDir(path);
	//试用版没有子目录
// 	path=CurrentPath+"\\DataBase\\大边";
// 	CreateNewDir(path);
// 	path=CurrentPath+"\\DataBase\\睫毛";
// 	CreateNewDir(path);
// 	path=CurrentPath+"\\DataBase\\面料";
// 	CreateNewDir(path);
// 	path=CurrentPath+"\\DataBase\\小边";
// 	CreateNewDir(path);
// 	path=CurrentPath+"\\DataBase\\其他";
// 	CreateNewDir(path);
}
void CLaceborderMatchingDlg::InitDataBaseDir(int index)//初始化数据库目录
{
	ClearCStringVector(DataBaseFileName);

	CString general[5]={"新建数据库1","新建数据库2","新建数据库3","新建数据库4","新建数据库5"};
	CString ditan[5]={"京式地毯","美术式地毯","仿古式地毯","彩花式地毯","其他"};
	CString bizhi[6]={"全纸壁纸","织物壁纸","天然材料壁纸","玻纤壁纸","塑料壁纸","其他"};
	CString fuzhuang[8]={"棉布","麻布","丝绸","呢绒","皮革","化纤","混纺","其他"};

	CString *hangye[4]={general,ditan,bizhi,fuzhuang};
	int num[4]={5,5,6,8};

	int i;
	for (i=0;i<num[index];i++)
	{
		DataBaseFileName.push_back(hangye[index][i]);
		CString path=CurrentPath+"\\DataBase\\"+hangye[index][i];
		CreateNewDir(path);
	}

}
void CLaceborderMatchingDlg::InitTree()//初始化树控件
{
	int index=3;

	CString title[4]={"数据库","地毯行业","壁纸行业","服装行业"};

	CreateDataBaseDir();//创建数据库目录
	//读取 数据库.bin
	CString Path=CurrentPath+"\\system\\DataBase.bin";//保存账户信息
	char *filePath;
	filePath=Path.GetBuffer(Path.GetLength());


	if (PathFileExists(Path))
	{
		ReadDataBaseInformation(filePath);//读取账户信息
	}
	else 
	{
		InitDataBaseDir(index);
		WriteDataBaseInformation(filePath);
	}

	

	//DeleteAllItems( );将删除所有结点。

	HTREEITEM hRoot;     // 树的根节点的句柄   
	HTREEITEM hCataItem; // 可表示任一分类节点的句柄   
	HTREEITEM hArtItem;  // 可表示任一子类节点的句柄   

	int x,y,w,h;
	x=12;
	y=BlueLine+47;
	w=250;
	h=m_Height-33-16-y;
	((CTreeCtrl *)GetDlgItem(IDC_TREE_DATABASE))->MoveWindow(x,y,w,h,TRUE);

	m_imageList.Create(32, 32, ILC_COLOR8|ILC_MASK, 3, 4);

	m_imageList.Add(AfxGetApp()->LoadIcon(IDI_ICON_DATABASE));// ico图标
	m_imageList.Add(AfxGetApp()->LoadIcon(IDI_ICON_DATABASE));
	m_imageList.Add(AfxGetApp()->LoadIcon(IDI_ICON_DATABASE));//图标个数随意加，多加几个也行



	m_tree.SetImageList(&m_imageList, TVSIL_NORMAL);   

	// 插入根节点   
//	hRoot = m_tree.InsertItem(_T("数据库"), 0, 0);
	hRoot = m_tree.InsertItem(_T(title[index]), 0, 0);

//	CString laceClass[5]={"大边","睫毛","面料","小边","其他"};
//	int SubClassNum[5]={3,5,7,5,3};//每个类子类个数
	CString tmp;

	int i,j;

	for (i=0;i<(int)DataBaseFileName.size();i++)
	{
		hCataItem = m_tree.InsertItem(DataBaseFileName[i], 1, 1, hRoot, TVI_LAST);
		m_tree.SetItemData(hCataItem, i);
// 		int n=(int)DataBaseFileName[i].SubFileName.size();
// 		if (n<1)
// 		{
// 			continue;
// 		}
// 		for (j=0;j<n;j++)
// 		{
// 			tmp=DataBaseFileName[i].SubFileName[j];
// 			hArtItem = m_tree.InsertItem(tmp, 2, 2, hCataItem, TVI_LAST); 
// 			m_tree.SetItemData(hArtItem, j);
// 		}  
	}

}
void CLaceborderMatchingDlg::InitAcounts()//初始化账户
{
	CString Path=CurrentPath+"\\system\\user.bin";//保存账户信息
	char *filePath;
	filePath=Path.GetBuffer(Path.GetLength());//保存 SaveImageData.txt的路径

	ACCOUNT tmp_account;

	if (PathFileExists(Path))
	{
		ReadAccount(accounts,filePath);//读取账户信息
	}
	else 
	{
		tmp_account.account="刻维科技";
		tmp_account.Level=true;
		tmp_account.password="123456";
		accounts.push_back(tmp_account);
////////////////////////////////////////////////////////////// 我测试专用账户 最后删除
// 		tmp_account.account="1";
// 		tmp_account.Level=true;
// 		tmp_account.password="1";
// 		accounts.push_back(tmp_account);
////////////////////////////////////////////////////////////////////
		WriteAccount(accounts,filePath);
	}

	int k=0;
}
void CLaceborderMatchingDlg::WriteAccount(vector<ACCOUNT>  &accounts,char *filePath)//将当前所有账户写入user.acc
{
	int n=(int)accounts.size();
	int i;

	FILE *accountFile=NULL;
	
	fopen_s(&accountFile,filePath,"wb");//为读写建立新的二进制文件
	if (accountFile)//打开成功
	{
		for (i=0;i<n;i++)
		{
			WriteCString(accounts[i].account,accountFile);
			WriteCString(accounts[i].password,accountFile);
			fwrite(&accounts[i].Level,sizeof(bool),1,accountFile);
		}		
	}
//////////////////////////////////////////////////////////////////////////
	WriteCString("Adonis  2014",accountFile);//账户结束标志 禁止用户注册这个用户名
//////////////////////////////////////////////////////////
	fclose(accountFile);
	accountFile=NULL;
}
void CLaceborderMatchingDlg::ReadAccount(vector<ACCOUNT>  &accounts,char *filePath)//从user.acc读入所有账户
{
	ACCOUNT tmp_account;

	FILE *accountFile=NULL;

	fopen_s(&accountFile,filePath,"rb");//为读写建立新的二进制文件
	if (accountFile)//打开成功
	{
		while(!feof(accountFile))//(feof返回1，说明到文件结尾)
		{
			ReadCString(tmp_account.account,accountFile);
			if ("Adonis  2014"==tmp_account.account)
			{
				break;
			}
			ReadCString(tmp_account.password,accountFile);
			fread(&tmp_account.Level,sizeof(bool),1,accountFile);

			accounts.push_back(tmp_account);
		}		
	}
	fclose(accountFile);
	accountFile=NULL;
}
void CLaceborderMatchingDlg::WriteCString(CString s,FILE *fp)
{
	char *a;
	int j=0;
	int k;//存储着数组长度

	s=s+"\n";//读取时每次读一行
	a=s.GetBuffer(s.GetLength());
	k=(int)strlen(a);
	char c;
	for (j=0;j<k;j++)
	{
///////////////////////加密/////////////////////////////
		if (j<(k-1))//加密 最后换行符不加密
		{
			c=a[k-2-j]-7;//加密
		}
		else
		{
			c=a[j];
		}
		
		fwrite(&c,sizeof(char),1,fp);
////////////////////////////////////////////////////////

// 		c=a[j];
// 		fwrite(&a[j],sizeof(char),1,fp);
		
	}
}
void CLaceborderMatchingDlg::ReadCString(CString &s,FILE *fp)
{
	char p[1000];
	fgets(p,1000,fp);
	CString str;
	str.Format(_T("%s"), p);
	s=str.Left(str.GetLength()-1);//为加密情况下直接读取
///////////////////////////////////////////////////解密处理
	s="";
	char c;
	int k=(int)strlen(p);
	int i;
	for (i=0;i<k;i++)
	{
		if (i<(k-1))
		{
			c=p[k-2-i]+7;
			s+=c;
		}
		
	}
//////////////////////////////////////////////
//	int k=0;
}
void CLaceborderMatchingDlg::MyInitSet()//初始化设置
{
	CString path; 
	GetModuleFileName(NULL,path.GetBufferSetLength(MAX_PATH+1),MAX_PATH); 
	path.ReleaseBuffer(); 
	int pos = path.ReverseFind('\\'); 
	CurrentPath = path.Left(pos);
	///////////////////////////////////////////////////////////
	path=CurrentPath+"\\system";
	CreateNewDir(path);
	path=CurrentPath+"\\resource";
	CreateNewDir(path);
//试用版目录
	path=CurrentPath+"\\tranning";//500张训练数据库
	CreateNewDir(path);
	path=CurrentPath+"\\testing";//测试数据集
	CreateNewDir(path);
	path=CurrentPath+"\\tranning\\tran";//500张训练数据库
	if (!PathFileExists(path+".{208D2C60-3AEA-1069-A2D7-08002B30309D}"))
	{
		CreateNewDir(path);
		Lock(path);
	}
		
	path=CurrentPath+"\\testing\\test";//测试数据集
	if (!PathFileExists(path+".{208D2C60-3AEA-1069-A2D7-08002B30309D}"))
	{
		CreateNewDir(path);
		Lock(path);
	}
	g_CutPicturePath=CurrentPath;
////////////////////////////////////////
	MoveResourceForSetUp();//安装结束后，将当前目录的图片移到resource文件夹下
//	SearchFlashDrive();//检测U盘

	CurrentPathFile();//确保软件不会被改变路径
	
	DeleteSetupPackage();//删除试用版安装包
	UnRegisterDateLimited();//试用期限制
	//////////////////////////////////////////////////////

///////////////////////////////分辨率检测/////////////////////////
	int iWidth = GetSystemMetrics(SM_CXSCREEN); 
	int iHeight = GetSystemMetrics(SM_CYSCREEN); 

	if (iWidth<1280||iHeight<720)
	{
		AfxMessageBox("当前系统分辨率小于 1280*720，请选择1280*720分辨率及以上的系统！！");
		exit(-1);
	}

//////////////////////////////////////////////////////////////
	ShowWindow(SW_MAXIMIZE);
	

	CRect rect;
	CvSize dst_cvsize;
	//////////////////////////////////////////////////针对xp1600*900分辨率，有垂直位移的问题/////////////////////////////////////////////////////////////////
	GetWindowRect(&rect);
	if (rect.bottom>863)
	{
		dst_cvsize.width=1600;
		dst_cvsize.height=838;
	}
	else
	{
		GetClientRect(&rect);

		dst_cvsize.width=rect.Width();
		dst_cvsize.height=rect.Height();
	}
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	m_Width=dst_cvsize.width;
	m_Height=dst_cvsize.height;

	cvReleaseImage(&m_dspbuf);
	m_dspbuf=cvCreateImage(dst_cvsize,8,4);
	GetDlgItem(IDC_STATIC_BACKGROUND)->SetWindowPos(0,0,0,dst_cvsize.width,dst_cvsize.height,SWP_SHOWWINDOW);
	CvSize size;
	size.height = m_dspbuf->height, size.width = m_dspbuf->width;
	cvReleaseImage(&m_imageBack);
	m_imageBack=  cvCreateImage(size, m_dspbuf->depth, 4);

	m_rate_w=rect.Width()/1600.00;
	m_rate_h=rect.Height()/838.00;
	if (m_rate_h>1)
	{
		m_rate_h=1;
	}
	if (m_rate_w>1)
	{
		m_rate_w=1;
	}



//	int size=140;
	font.CreatePointFont(140,_T("Verdana"),NULL);//设置输入框字体  登陆界面



	InitSetValue();//根据全屏后的尺寸计算坐标与长度
	InitAcounts();//初始化所有账户
	
	LogInBackGround();

//	::MoveFile((LPTSTR)"F:\\1.txt", (LPTSTR)"E:\\12.txt");剪切文件



//	LogSuccessfulBackGround();


/////////////////////////////////////////////创建畸形文件夹
// 	CString t="md F:\\abc..\\";
// 	CString cmd=_T("cmd /c ");
// 	cmd+=t; 
// 	WinExec((LPCTSTR)cmd,SW_HIDE);

	
// 	t="notepad F:\\abc..\\wwww.txt ";
// 	cmd=_T("cmd /c ");
// 	cmd+=t; 
// 	WinExec((LPCTSTR)cmd,SW_HIDE);//打开畸形文件夹里面的文件
// 
// 	t="RD F:\\abc..\\";
// 	cmd=_T("cmd /c ");
// 	cmd+=t; 
// 	WinExec((LPCTSTR)cmd,SW_HIDE);//删除畸形文件夹

}
void CLaceborderMatchingDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		if(nID == SC_RESTORE&&!IsIconic())//SC_MAXIMIZE 最大化ID； SC_MINIMIZE 最小化 ID ；SC_RESTORE 还原ID
		{
			//IsIconic() 返回值：如果窗口已图标化，返回值为非零；如果窗口未图标化，返回值为零
		}
		else
		CDialogEx::OnSysCommand(nID, lParam);
	}
	
	
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CLaceborderMatchingDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}

	if(m_dspbuf )
	{
		CPaintDC dc(GetDlgItem(IDC_STATIC_BACKGROUND));

		CDC dcMem;
		CBitmap bmpMem;
		dcMem.CreateCompatibleDC(&dc);
		bmpMem.CreateCompatibleBitmap(&dc, m_dspbuf->width, m_dspbuf->height);
		dcMem.SelectObject(&bmpMem);

		int linebyte;
		int w = m_dspbuf->width;
		int h = m_dspbuf->height;
		linebyte = m_dspbuf->widthStep;

		bmpMem.SetBitmapBits(linebyte * h, m_dspbuf->imageData);
		dc.BitBlt(0, 0, w,h, &dcMem, 0, 0, SRCCOPY);

		bmpMem.DeleteObject();
		dcMem.DeleteDC(); 
	}

	ShowMyText();
	if (ShowOriginalPictureFlag==false)
	{
//		ShowGoToPage();
	}
	
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CLaceborderMatchingDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CLaceborderMatchingDlg::LogInBackGround()
{
	int i,j;
	
	for(i = 0; i < m_imageBack->height; i++)
		for(j = 0; j < m_imageBack->width; j++)
		{
			SetPointColour(m_imageBack,j ,i,240,240,240);
// 			m_imageBack->imageData[ i * m_imageBack->widthStep + m_imageBack->nChannels * j + 2 ] =240;
// 			m_imageBack->imageData[ i * m_imageBack->widthStep + m_imageBack->nChannels * j + 1 ] =240;
// 			m_imageBack->imageData[ i * m_imageBack->widthStep + m_imageBack->nChannels * j  ] =240;
		}


		int x,y,w,h;

		CString pathname=CurrentPath+"\\resource\\title.jpg";
		x=m_Width/2-430;
		y=m_Height/2-40;
		w=398;
		h=80;
		SetBackPic(pathname,x,y,w,h,0,0,false);

		pathname=CurrentPath+"\\resource\\middle.jpg";
		x=m_Width/2;
		y=m_Height/2-143;
		w=81;
		h=285;
		SetBackPic(pathname,x,y,w,h,0,0,false);

		pathname=CurrentPath+"\\resource\\account.jpg";
		x=m_Width/2+55;
		y=m_Height/2-58;
		w=58;
		h=26;
		SetBackPic(pathname,x,y,w,h,0,0,false);

		pathname=CurrentPath+"\\resource\\password.jpg";
		x=m_Width/2+55;
		y=m_Height/2-7;
		w=58;
		h=26;
		SetBackPic(pathname,x,y,w,h,0,0,false);

		pathname=CurrentPath+"\\resource\\log.jpg";
		x=m_Width/2+150;
		y=m_Height/2+50;
		w=95;
		h=30;
		SetBackPic(pathname,x,y,w,h,0,0,false);

		pathname=CurrentPath+"\\resource\\bottom.jpg";
		y=m_Height-95;
		w=1600;
		h=95;
		if (m_Width/2>=800)
		{
			x=m_Width/2-800;
			SetBackPic(pathname,x,y,w,h,0,0,false);
		}
		else
		{
			x=800-m_Width/2;
			w=m_Width;
			SetBackPic(pathname,0,y,w,h,x,0,false);
		}


		CWnd* pWnd = GetDlgItem(IDC_EDIT_ACCOUNT);
		CWnd* pWnd1 = GetDlgItem(IDC_EDIT_PASSWORD);

		pWnd->SetWindowText("");
		pWnd1->SetWindowText("");

		CEdit *m_Edit=(CEdit *)GetDlgItem(IDC_EDIT_ACCOUNT);
		CEdit *m_Edit1=(CEdit *)GetDlgItem(IDC_EDIT_PASSWORD);
		m_Edit->SetFont(&font,FALSE);
		m_Edit1->SetFont(&font,FALSE);

		x=m_Width/2+115;
		y=m_Height/2-59;
		w=170;
		h=30;

		pWnd->MoveWindow(x,y,w,h,TRUE);  


		x=m_Width/2+115;
		y=m_Height/2-9;
		w=170;
		h=30;
		pWnd1->MoveWindow(x,y,w,h,TRUE); 

		pWnd->ShowWindow(SW_SHOWNA);//设置属性不可见
		pWnd1->ShowWindow(SW_SHOWNA);


		GetImageDisplayData(m_dspbuf, m_imageBack, 0, 0);
		Invalidate(FALSE);

}

void CLaceborderMatchingDlg::SetPointColour(IplImage* dst,int x,int y,IplImage* src,int p_x,int p_y)
{
	if (x<dst->width&&y<dst->height)
	{
		dst->imageData[ y* dst->widthStep + dst->nChannels * x + 2 ] =src->imageData[p_y * src->widthStep + src->nChannels * p_x + 2 ];
		dst->imageData[ y* dst->widthStep + dst->nChannels * x + 1 ] =src->imageData[p_y * src->widthStep + src->nChannels * p_x + 1 ];
		dst->imageData[ y* dst->widthStep + dst->nChannels *x  ] =src->imageData[p_y * src->widthStep + src->nChannels * p_x  ];
	}

}
void CLaceborderMatchingDlg::SetPointColour(IplImage* dst,int x,int y,int r,int g,int b)
{
	dst->imageData[ y* dst->widthStep + dst->nChannels * x + 2 ] =(BYTE)r;
	dst->imageData[ y* dst->widthStep + dst->nChannels * x + 1 ] =(BYTE)g;
	dst->imageData[ y* dst->widthStep + dst->nChannels *x  ] =(BYTE)b;
}
void CLaceborderMatchingDlg::SetBackPic(CString pathname,int x,int y,int w,int h,int p_x,int p_y,bool f)//f==false原图，f==true镜像对称
{
	char *path = (LPSTR)(LPCTSTR)pathname;
	IplImage* temp = cvLoadImage(path,CV_LOAD_IMAGE_COLOR);

	if (!temp)
	{
		return;
	}

	int i,j,i1,j1,i2,j2;

	if (f==false)
	{
		for(i =0; i <h; i++)
			for(j =0; j < w; j++)
			{
				i1=y+i;
				j1=x+j;
				i2=p_y+i;
				j2=p_x+j;
				SetPointColour(m_imageBack,j1,i1,temp,j2,i2);
			}
	}
	else if (f==true)//右边镜像对称
	{
		for(i =0; i <h; i++)
			for(j =0; j < w; j++)
			{
				i1=y+i;
				j1=x-j;
				i2=p_y+i;
				j2=p_x+j;
				SetPointColour(m_imageBack,j1,i1,temp,j2,i2);
			}
	}
	cvReleaseImage(&temp);
}

void CLaceborderMatchingDlg::GetImageDisplayData(IplImage* dspbuf, IplImage* &imagescale, int hpos, int vpos)
{
	int i,j,i2,j2;
	int linebyte = dspbuf->widthStep;
	int linebytesclae = imagescale->widthStep;
	char *temp_dsp = dspbuf->imageData, *temp_scale = imagescale->imageData;
	UINT *dsp = (UINT *)temp_dsp, *scale = (UINT *)temp_scale;

	if( imagescale->width <= dspbuf->width && imagescale->height <= dspbuf->height )
	{
		int x = (dspbuf->width-imagescale->width)/2, y = (dspbuf->height-imagescale->height)/2;
		for( i = y, i2=0; i < imagescale->height+y, i2<imagescale->height; i++,i2++ )
			for( j = x, j2=0; j < imagescale->width+x, j2<imagescale->width; j++,j2++ )
			{
				//if( i < imagescale->height && j < imagescale->width )
				{
					dsp[i * dspbuf->width + j] = scale[(i2+vpos) * imagescale->width + (j2+hpos)];
				}
			}
	}

	if( imagescale->width <= dspbuf->width && imagescale->height > dspbuf->height )
	{
		int x = (dspbuf->width-imagescale->width)/2, y = 0;
		for( i = 0; i < dspbuf->height; i++)
			for( j = x, j2=0; j < imagescale->width+x, j2<imagescale->width; j++,j2++ )
			{
				//if( i < imagescale->height && j < imagescale->width )
				{
					dsp[i * dspbuf->width + j] = scale[(i+vpos) * imagescale->width + (j2+hpos)];
				}
			}
	}

	if( imagescale->width > dspbuf->width && imagescale->height <= dspbuf->height )
	{
		int x = 0, y = (dspbuf->height-imagescale->height)/2;
		for( i = y, i2=0; i < imagescale->height+y, i2<imagescale->height; i++,i2++ )
			for( j = 0; j < dspbuf->width; j++ )
			{
				//if( i < imagescale->height && j < imagescale->width )
				{
					dsp[i * dspbuf->width + j] = scale[(i2+vpos) * imagescale->width + (j+hpos)];
				}
			}
	}

	if( imagescale->width > dspbuf->width && imagescale->height > dspbuf->height )
	{
		DWORD oldtime = GetTickCount();
		for( i = 0; i < dspbuf->height; i++)
			for( j = 0; j < dspbuf->width; j++ )
			{
				if( i < imagescale->height && j < imagescale->width )
				{
					dsp[i * dspbuf->width + j] = scale[(i+vpos) * imagescale->width + (j+hpos)];
				}
			}
			DWORD time = GetTickCount() - oldtime;
			int kkk = 0;
	}
	temp_dsp = NULL, temp_scale = NULL;
	dsp = NULL,scale = NULL;
}

void CLaceborderMatchingDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	int l,r,t,b;
	int i;

	if (LogFlag==FALSE)
	{
		l=m_Width/2+150;
		t=m_Height/2+50;
		r=95+l;
		b=30+t;
		if (point.x>l&&point.x<r&&point.y>t&&point.y<b)
		{
			OnLoging();
		}
	}

	if (LogFlag==TRUE)
	{
		if (CreatingDataBaseFlag==TRUE)
		{
			AfxMessageBox("正在创建数据库，为确保创建成功请勿切换其他功能！！");
			return;
		}
		
		if (LoadingPictureFlag==TRUE)
		{
			AfxMessageBox("正在导入图片，为确保系统稳定请勿切换其他功能！！");
			return;
		}

		l=m_Width-70;
		t=BlueLine-21;
		r=m_Width-20;
		b=BlueLine;
		if (point.x>l&&point.x<r&&point.y>t&&point.y<b)//注销登录
		{
			((CTreeCtrl *)GetDlgItem(IDC_TREE_DATABASE))->ShowWindow(SW_HIDE);
			manageAccountHide();

			ShowingMatchingPicturesFlag=false;
			DataBaseSubWindow_t=k_t;
			DataBaseSubWindow_b=k_b;
			DataBaseSubWindow_r=k_r;
			DataBaseSubWindow_l=k_l;
			GetDataBasePictureSize();

			CWnd* pWnd= GetDlgItem(IDC_DATETIMEPICKER_FROM);//前一个日期
			pWnd->ShowWindow(SW_HIDE);

			pWnd= GetDlgItem(IDC_DATETIMEPICKER_TO);
			pWnd->ShowWindow(SW_HIDE);

			pWnd= GetDlgItem(IDC_STATIC_TIME);
			pWnd->ShowWindow(SW_HIDE);
			pWnd= GetDlgItem(IDC_STATIC_TO);
			pWnd->ShowWindow(SW_HIDE);
			pWnd= GetDlgItem(IDC_STATIC_PRO);//新密码
			pWnd->ShowWindow(SW_HIDE);

			pWnd= GetDlgItem(IDC_COMBO_GO);//新密码
			pWnd->ShowWindow(SW_HIDE);

			LogFlag=FALSE;
			LoadPictureSuccessFlag=FALSE;
			CurrentMenu="";
			cvReleaseImage(&LoadedPicture);
			LoadedPicture=NULL;
			LogInBackGround();
		}


//		for (i=0;i<4;i++)
		for (i=0;i<2;i++)//试用版 统计信息 与 账号管理 不可用
		{
			l=Menu_x[i];
			t=Menu_y;
			r=MenuLenth[i]+l;
			b=MenuHeight+t;
			if (point.x>l&&point.x<r&&point.y>t&&point.y<b)
			{
				manageAccountHide();

				LogSuccessfulBackGround();
				InitMenu();
				showPageFlag=false;
				ClickMenu(i+1);
				GetImageDisplayData(m_dspbuf, m_imageBack, 0, 0);				
				Invalidate(FALSE);
			}
		}

		if (CurrentMenu==MenuName[1])
		{					
			if (ShowingMatchingPicturesFlag==true)
			{
				changePage(point);

				l=GoBack_x;
				t=GoBack_y;
				r=GoBack_w+l;
				b=GoBack_h+t;
				if (point.x>l&&point.x<r&&point.y>t&&point.y<b)//返回
				{
					manageAccountHide();

					LogSuccessfulBackGround();
					InitMenu();
					ClickMenu(2);
					GetImageDisplayData(m_dspbuf, m_imageBack, 0, 0);
					ShowLoadedPicture();//如果已导入图片，会自动显示
					Invalidate(FALSE);
				}

				l=450;
				t=BlueLine+35;
				r=94+l;
				b=25+t;
				if (point.x>l&&point.x<r&&point.y>t&&point.y<b)//保存报告
				{
					SaveReport();

				}
//				return;

	
			}
			else if (ShowingMatchingPicturesFlag==false)
			{
				ShowLoadedPicture();//如果已导入图片，会自动显示

				l=search_x;
				t=search_y;
				r=search_w+l;
				b=search_h+t;
				if (point.x>l&&point.x<r&&point.y>t&&point.y<b)
				{
					if (LoadedPicture)
					{
//						ShowingMatchingPicturesFlag=true;
						Image_Matching();//匹配函数

						
					}
				}

				l=LoadPicture_x;
				t=LoadPicture_y;
				r=LoadPicture_w+l;
				b=LoadPicture_h+t;
				if (point.x>l&&point.x<r&&point.y>t&&point.y<b)
				{

					LoadPictureSuccessFlag=FALSE;
//					CutPictureFlag=FALSE;//每次打开图片，默认不裁剪图片
					CString prePath=CurrentPicturePath;
					LoadPicture();
//					CurrentPicturePath="C:\\Users\\Administrator\\Desktop\\2\\3.{208D2C60-3AEA-1069-A2D7-08002B30309D}\\2\\1.jpg";
					if (CurrentPicturePath =="")//取消导入
					{
						if (LoadedPicture)//前一幅图片还在
						{
							CurrentPicturePath=prePath;
							LoadPictureSuccessFlag=TRUE;
// 							MovePictureFlag=TRUE;
// 							CutPictureFlag=FALSE;//每次打开图片，默认不裁剪图片
						}
					}
					else
					{
						ShowPicture();

						MovePictureFlag=TRUE;
						CutPictureFlag=FALSE;
						MovePic();
						GetImageDisplayData(m_dspbuf, m_imageBack, 0, 0);

						ShowLoadedPicture();
						Invalidate(FALSE);
					}

				}



				l=SearchByTexture_x;
				t=SearchByTexture_y;
				r=radio_w+l;
				b=radio_h+t;
				if (point.x>l&&point.x<r&&point.y>t&&point.y<b)
				{
					SearchByTexture();
					GetImageDisplayData(m_dspbuf, m_imageBack, 0, 0);
					//				ShowPicture();
					//				MovedPicture(0,0);
					ShowLoadedPicture();
					Invalidate(FALSE);
				}
/*				试用版 颜色搜索不可用
				l=SearchByColour_x;
				t=SearchByColour_y;
				r=radio_w+l;
				b=radio_h+t;
				if (point.x>l&&point.x<r&&point.y>t&&point.y<b)
				{
					SearchByColour();
					GetImageDisplayData(m_dspbuf, m_imageBack, 0, 0);
					//				MovedPicture(0,0);
					// 				ShowPicture();
					ShowLoadedPicture();
					Invalidate(FALSE);
				}*/


				l=Move_x;
				t=Move_y;
				r=radio_w+l;
				b=radio_h+t;
				if (point.x>l&&point.x<r&&point.y>t&&point.y<b)
				{
					MovePictureFlag=TRUE;
					CutPictureFlag=FALSE;
					MovePic();
					GetImageDisplayData(m_dspbuf, m_imageBack, 0, 0);
					//				MovedPicture(0,0);
					//				ShowPicture();
					ShowLoadedPicture();
					Invalidate(FALSE);
				}
				l=Cut_x;
				t=Cut_y;
				r=radio_w+l;
				b=radio_h+t;
				if (point.x>l&&point.x<r&&point.y>t&&point.y<b)
				{
					CutPictureFlag=TRUE;
					MovePictureFlag=FALSE;
					CutPic();
					GetImageDisplayData(m_dspbuf, m_imageBack, 0, 0);
					//				MovedPicture(0,0);
					//				ShowPicture();
					ShowLoadedPicture();
					Invalidate(FALSE);
				}

				if (LoadedPicture&&CutPictureFlag==TRUE&&MovePictureFlag==FALSE)
				{
					l=SureCut_x;
					t=SureCut_y;
					r=SureCut_w+l;
					b=SureCut_h+t;
					if (point.x>l&&point.x<r&&point.y>t&&point.y<b)
					{
						SureCutPicture();//确定裁剪图片
						ShowPicture();
					}
				}
			}
		}

		if ((CurrentMenu==MenuName[0]&&manageDataBaseNum==1)||CurrentMenu==MenuName[2]||ShowingMatchingPicturesFlag==true)
		{
			int k=GetCurrentPicturePath(CurrentSelectedPicturePath,point);
			if (k>=0&&k<(int)CurrentDataBasePicturePath.size())
			{
				CShowPicture newPicture;
				newPicture.DoModal();
			}
		}
		

		if (CurrentMenu==MenuName[0])//数据库管理
		{
			if (manageDataBaseNum==1)
			{
				changePage(point);

				
			}
			else if (manageDataBaseNum==2)
			{				
				l= UpLoadSinglePicture_x;
				t=UpLoadSinglePicture_y;
				r= UpLoadSinglePicture_w+l;
				b=UpLoadSinglePicture_h+t;
				if (point.x>l&&point.x<r&&point.y>t&&point.y<b)//导入单张图片
				{
					if (TRUE==LoadingPictureCountsTest(CurrentSelectedDataBasePath,1))
					{
						MessageBox("该目录最大存储500张图片！");
						return;
					}

					///////////////////////////////////////////////////////////解决由导入文件夹所有文件切换到导入一张图片四周残留//////////////////////////
					int k=DataBaseSubWindow_t;
					DataBaseSubWindow_t+=30;
					GetDataBasePictureSize();
					InitPictureColour(m_dspbuf,DataBasePicture_l,DataBasePicture_r+1,DataBasePicture_t,DataBasePicture_b+1,240,240,240);
					DataBaseSubWindow_t=k;
					GetDataBasePictureSize();
					///////////////////////////////////////////////////////////////////////
					LoadPicture();
					if (CurrentPicturePath!="")
					{
						UpLoadPicureFlag=true;
						ShowPicture();
						UpLoadPicureFlag=false;
						cvReleaseImage(&LoadedPicture);
						LoadedPicture=NULL;
						LoadPictureSuccessFlag=FALSE;//解决图像匹配界面显示的问题
						///////////////////////////////////////////////////////////////////////
						ClearCStringVector(Supplierinfo);

						LoadingPicture(CurrentPicturePath,CurrentSelectedDataBasePath);//移动图片

						CurrentSelectedPicturePath=CurrentPicturePath;
						OnEditPictureInfo();

						CreatePictureAddAndDeleteFlagFile();//创建数据库增减标记文件
					}
					else
					{
						
						ManageDataBaseClick(2);//点击3个子菜单
						GetImageDisplayData(m_dspbuf, m_imageBack, 0, 0);
						Invalidate(FALSE);
// 						InitPictureColour(m_dspbuf,ShowPicture_l+1,ShowPicture_r,ShowPicture_t,ShowPicture_b,240,240,240);
// 						Invalidate(FALSE);
					}
					
//					return;
				}

				l=UpLoadAllPicture_x;
				t=UpLoadAllPicture_y;
				r=UpLoadAllPicture_w+l;
				b=UpLoadAllPicture_h+t;
				if (point.x>l&&point.x<r&&point.y>t&&point.y<b)//导入一个文件夹下的所有图片
				{
					showPageFlag=false;
					LoadingPictureFlag=TRUE;
					 ((CTreeCtrl*)GetDlgItem(IDC_TREE_DATABASE))->EnableWindow(FALSE);
					UpLoadAllpicture();//导入一个文件夹下的所有图片

				
					showPageFlag=true;
					 ((CTreeCtrl*)GetDlgItem(IDC_TREE_DATABASE))->EnableWindow(TRUE);
					LoadingPictureFlag=FALSE;
				}

				changePage(point);



				
			}
			else if (manageDataBaseNum==3)
			{
				ClearCStringVector(CurrentDataBasePicturePath);

				l= UpLoadSinglePicture_x;
				t=UpLoadSinglePicture_y;
				r= UpLoadSinglePicture_w+l;
				b=UpLoadSinglePicture_h+t;
				if (point.x>l&&point.x<r&&point.y>t&&point.y<b)//创建数据库
				{
					CurrentDataBasePictureFeature();//创建数据库特征
				}
			}


			int x[3]={330,430,530};

			int lenth[3]={93,93,93};

			for (i=0;i<3;i++)
			{
				l= x[i];
				t=BlueLine+15;
				r= lenth[i]+l;
				b=31+t;
				if (point.x>l&&point.x<r&&point.y>t&&point.y<b)
				{
					if (i>0&&accounts[CurrentAccount].Level==false)
					{
						AfxMessageBox("操作员无权限！");
						return;
					}
					ManageDataBaseClick(i+1);//点击3个子菜单
					GetImageDisplayData(m_dspbuf, m_imageBack, 0, 0);
//					DrawLineWindows();
					Invalidate(FALSE);
				}
			}
		
		}

		if (CurrentMenu==MenuName[2])//统计信息
		{
			l= 1000;
			t=BlueLine+18;
			r= 93+l;
			b=25+t;
			if (point.x>l&&point.x<r&&point.y>t&&point.y<b)
			{
				
				//验证日期格式输入
				BOOL f=verifyDate();//验证统计信息的输入格式
				if (f==FALSE)
				{
					MessageBox("起始时间、终止时间输入不正确！");
				}
				else
				{
					//搜索对应日期的文件 
					GetDatePicturePath();//获取搜索日期范围内的Date文件，并获取图片频率



					InitPictureColour(m_dspbuf,DataBasePicture_l,DataBasePicture_r+1,DataBasePicture_t,DataBasePicture_b+1,240,240,240);

					/////////////////////////////////////////////////////////////////////////////////////////////////////////
					if (CurrentDataBasePicturePath.size()>0)
					{
						showPageFlag=true;
						CurrentSubDataBasePageNum=((int)CurrentDataBasePicturePath.size()-1)/(DataBasePicture_x_num*DataBasePicture_y_num)+1;
						CurrentSubDataBasePageIndex=1;
						SetPageNum();
						ShowEachPagePicture(CurrentSubDataBasePageIndex);

						CString path_=CurrentPath+"\\resource\\next_page.jpg";
						SetBackPic(path_,DataBaseSubWindow_l*0.75+DataBaseSubWindow_r*0.25,DataBaseSubWindow_b-20,25,18,0,4,true);//前一页
						path_=CurrentPath+"\\resource\\next_page.jpg";
						SetBackPic(path_,DataBaseSubWindow_l*0.25+DataBaseSubWindow_r*0.75,DataBaseSubWindow_b-20,25,18,0,4,false);//后一页

						
					}
					else
					{
						showPageFlag=false;
						CurrentSubDataBasePageIndex=0;
						CurrentSubDataBasePageNum=0;
						InitPictureColour(m_imageBack,DataBasePicture_l,DataBasePicture_r+1,DataBasePicture_t,DataBasePicture_b+1,240,240,240);
					}

					

					if (CurrentSubDataBasePageIndex>0)
					{
						DrawLineWindows();
					}
					
					//显示文件

					GetImageDisplayData(m_dspbuf, m_imageBack, 0, 0);

					Invalidate(FALSE);

					

				}
								
			}
			else
			{
				changePage(point);
			}

			
		}


		if (CurrentMenu==MenuName[3])//账户管理
		{
			int x[3]={330,421,538};
			
			int lenth[3]={51,77,64};

			for (i=0;i<3;i++)
			{
				l= x[i];
				t=BlueLine+15;
				r= lenth[i]+l;
				b=31+t;
				if (point.x>l&&point.x<r&&point.y>t&&point.y<b)
				{
					if (i>0&&accounts[CurrentAccount].Level==false)
					{
						AfxMessageBox("操作员无权限！");
						return;
					}
					ManageAccountClick(i+1);//点击3个子菜单
					GetImageDisplayData(m_dspbuf, m_imageBack, 0, 0);
					Invalidate(FALSE);
				}
			}

			l=ManageAccountSure_x;
			t=ManageAccountSure_y;
			r=ManageAccountSure_w+l;
			b=ManageAccountSure_h+t;
			if (point.x>l&&point.x<r&&point.y>t&&point.y<b)
			{
				SureManageAccount();
			}

			if (manageAccountMenuNum==2||manageAccountMenuNum==3)
			{
				l=LevelTrue_x;
				t=LevelTrue_y;
				r=radio_w+l;
				b=radio_h+t;
				if (point.x>l&&point.x<r&&point.y>t&&point.y<b)
				{
					levelChoose=true;
					RadioChoose(LevelTrue_x,LevelTrue_y,LevelFalse_x,LevelFalse_y);
					GetImageDisplayData(m_dspbuf, m_imageBack, 0, 0);
					Invalidate(FALSE);
				}

				l=LevelFalse_x;
				t=LevelFalse_y;
				r=radio_w+l;
				b=radio_h+t;
				if (point.x>l&&point.x<r&&point.y>t&&point.y<b)
				{
					levelChoose=false;
					RadioChoose(LevelFalse_x,LevelFalse_y,LevelTrue_x,LevelTrue_y);
					GetImageDisplayData(m_dspbuf, m_imageBack, 0, 0);
					Invalidate(FALSE);
				}
			}
			if (manageAccountMenuNum==2)
			{
				l=480;
				t=BlueLine+180;
				r=radio_w+l;
				b=radio_h+t;
				if (point.x>l&&point.x<r&&point.y>t&&point.y<b)
				{
					if (deleteFlag==true)
					{
						deleteFlag=false;
					}
					else if (deleteFlag==false)
					{
						deleteFlag=true;
					}
					
					SingleRadio(l,t,deleteFlag);
					GetImageDisplayData(m_dspbuf, m_imageBack, 0, 0);
					Invalidate(FALSE);
				}
			}
		}
		
	}
	
	CDialogEx::OnLButtonDown(nFlags, point);
}


void CLaceborderMatchingDlg::GetDatePicturePath()//获取搜索日期范围内的Date文件，并获取图片频率
{
	vector<CString>datePath;

	CString tmp=CurrentPath+"\\system\\*.Date";

	WIN32_FIND_DATA FindFileData;
	HANDLE hFind;
	hFind = FindFirstFile(tmp, &FindFileData);
	if (hFind == INVALID_HANDLE_VALUE) 
	{
		MessageBox("无搜索记录！！");
		return;
	}
	else 
	{
		do 
		{
			CString tmp_name=FindFileData.cFileName;
			tmp_name=tmp_name.Left(10);

			int tmp_n;

			CString tmp_s=tmp_name.Left(4);

			tmp_name=tmp_name.Right(5);
			tmp_s+=tmp_name.Left(2);
			tmp_s+=tmp_name.Right(2);
			tmp_n=atoi(tmp_s);

			if (tmp_n>=date[0]&&tmp_n<=date[1])
			{
				tmp_name=FindFileData.cFileName;
				datePath.push_back(tmp_name);
			}
// 			int k=0;
// 			bool f=false;

// 			if (tmp_n[0]<date[0]||tmp_n[0]>date[3])
// 			{
// 				f=true;
// 			}
// 			else if (tmp_n[0]==date[0]||tmp_n[0]==date[3])
// 			{
// 				if (tmp_n[1]<date[1]||tmp_n[1]>date[4])
// 				{
// 					f=true;
// 				}
// 				else if (tmp_n[1]==date[1]||tmp_n[1]==date[4])
// 				{
// 					if (tmp_n[2]<date[2]||tmp_n[2]>date[5])
// 					{
// 						f=true;
// 					}
// 				}
// 			}
			

// 			if (f==false)
// 			{
// 				tmp_name=FindFileData.cFileName;
// 				datePath.push_back(tmp_name);
// 			}
		} 
		while (FindNextFile(hFind, &FindFileData) != 0);
	}

	if (datePath.size()<1)
	{
		MessageBox("无搜索记录！！");
		ClearCStringVector(CurrentDataBasePicturePath);//将保存搜索的图片路径

		return;
	}

	int i;
	CString Path;//保存日期文件
	char *filePath;

	MY_DATE tmp_date;
	vector<MY_DATE>dateData;//日期文件信息

	vector<MY_DATE>tmp_dateData;//日期文件信息

	for (int i1=0;i1<(int)datePath.size();i1++)
	{
		Path=CurrentPath+"\\system\\"+datePath[i1];
		filePath=Path.GetBuffer(Path.GetLength());

		ReadDateFile(tmp_dateData,filePath);//读取日期文件
		int num=(int)tmp_dateData.size();
		for (i=0;i<num;i++)
		{
			bool f=false;
			for (int j=0;j<(int)dateData.size();j++)//判断新读入的文件 在原来的总的文件中是否已经有记录
			{
				CString s1=tmp_dateData[i].Name;
				if (dateData[j].Name==s1)
				{
					dateData[j].Sum+=tmp_dateData[i].Sum;
					f=true;
					break;
				}
			}
			if (f==false)
			{
				tmp_date.Name=tmp_dateData[i].Name;//如果没有记录
				tmp_date.Sum=tmp_dateData[i].Sum;
				dateData.push_back(tmp_date);
			}
		}
	}

	if (!DateNum.empty())
	{
		DateNum.clear();
		vector<int>().swap(DateNum);
	}
	ClearCStringVector(CurrentDataBasePicturePath);//将保存搜索的图片路径
///////////////////////////////////对搜索次数排序/////////////////////////////////////////////////////////////////////////
	int c,d;
	int swap;
	CString  pathname;

	int n=(int)dateData.size();

	for (c = 0 ; c < ( n - 1 ); c++)
	{
		for (d = 0 ; d < n - c - 1; d++)
		{
			if (dateData[d].Sum < dateData[d+1].Sum) /* For decreasing order use < */
			{
				swap       = dateData[d].Sum;
				dateData[d].Sum   = dateData[d+1].Sum;
				dateData[d+1].Sum= swap;

				pathname =dateData[d].Name;
				dateData[d].Name = dateData[d+1].Name;
				dateData[d+1].Name = pathname;
			}
		}
	}
///////////////////////////////////////////////////////////////////////////////////////////////////////////

	for (i=0;i<(int)dateData.size();i++)
	{
		CString s=dateData[i].Name;
		if (PathFileExists(s))
		{
			CurrentDataBasePicturePath.push_back(s);
		}
		

		int n=dateData[i].Sum;
		DateNum.push_back(n);//保存搜索频率次数
	}

	if (!tmp_dateData.empty())
	{
		tmp_dateData.clear();
		vector<MY_DATE>().swap(tmp_dateData);
	}

	if (!dateData.empty())
	{
		dateData.clear();
		vector<MY_DATE>().swap(dateData);
	}

	

	ClearCStringVector(datePath);

	
	
}

void CLaceborderMatchingDlg::ShowingMatchingPictures()//显示匹配结果
{
	int l,r,t,b;

	((CTreeCtrl *)GetDlgItem(IDC_TREE_DATABASE))->ShowWindow(SW_HIDE);//隐藏导航栏
// 	InitPictureColour(m_imageBack,600,m_Width-15,BlueLine+15,m_Height-33-10,0,240,0);
// 	InitPictureColour(m_imageBack,285,599,BlueLine+15,m_Height-33-10,240,240,240);
	InitPictureColour(m_imageBack,285,m_Width-15,BlueLine+15,m_Height-33-10,240,240,240);//刷新匹配结果显示区域
	CopyImageInHorizontal(m_imageBack,270,555,BlueLine+5,m_Height-33,0);//重新绘制画面替换原先的导航栏区域

//	InitPictureColour(m_imageBack,285,599,BlueLine+70,m_Height-33-10,0,240,0);

	l=285;
	r=599;
	t=BlueLine+70;
	b=m_Height-33-10;

	l=15;
	r=599;
	t=BlueLine+70;
	b=m_Height/2;

	SetPictureInAPart(m_imageBack,l,t,r-l,b-t,CurrentPicturePath);//将当前带匹配图片显示

	

	CString path;

	path=CurrentPath+"\\resource\\GoBack.jpg";
//	SetBackPic(path,350,(BlueLine+35),44,25,0,0,false);
	SetBackPic(path,GoBack_x,GoBack_y,GoBack_w,GoBack_h,0,0,false);//显示返回按钮
	path=CurrentPath+"\\resource\\saveReport.jpg";
	SetBackPic(path,450,(BlueLine+35),94,25,0,0,false);//显示保存报告按钮

/////////////////////////  下面一段 初始化 匹配结果显示区域 //////////////////////////////////////////////////////////////////	
	l=600;
	r=m_Width-15;
	t=BlueLine+15;
	b=m_Height-33-10;

	path=CurrentPath+"\\resource\\manageaccount_Bottom.jpg";
	SetBackPic(path,l,b-16,16,16,0,0,false);//左下角
	SetBackPic(path,r,b-16,16,16,0,0,true);//右下角

	CopyInHorizontal(m_imageBack,(l+16),(r-15),(b-16),b,(l+15));

	CopyImageInVerticalByMirror(m_imageBack,l,r+1,(b-16),b,t);//垂直方向复制一段图片,镜面复制

	CopyInVertical(m_imageBack,l,(l+15),(t+15),(b-16),(b-15));
	CopyInVertical(m_imageBack,(r-16),r,(t+15),(b-16),(b-15));


	///////////////////////////////////////////////////////////改变图片显示区域与单元格大小//////////////////////////
	l=602;
	k_t=DataBaseSubWindow_t;
	DataBaseSubWindow_t=t-30;
	k_b=DataBaseSubWindow_b;
	DataBaseSubWindow_b=b;
	k_r=DataBaseSubWindow_r;
	DataBaseSubWindow_r=r;
	k_l=DataBaseSubWindow_l;
	DataBaseSubWindow_l=l;

	GetDataBasePictureSize();//根据分辨率调整匹配结果的每个单元格大小
	InitPictureColour(m_dspbuf,DataBasePicture_l,DataBasePicture_r+1,DataBasePicture_t,DataBasePicture_b+1,240,240,240);

	CurrentSubDataBasePageNum=((int)CurrentDataBasePicturePath.size()-1)/(DataBasePicture_x_num*DataBasePicture_y_num)+1;//计算当前匹配结果需要显示的页数

	/////////////////////////////////////////////////////////////////////////////////////////////////////////
	if (CurrentDataBasePicturePath.size()>0)
	{
		SetPageNum();//设置快速跳转页面的页面序号

		CurrentSubDataBasePageIndex=1;
		ShowEachPagePicture(CurrentSubDataBasePageIndex);//显示当前是第几页

		CString path_=CurrentPath+"\\resource\\next_page.jpg";
		SetBackPic(path_,DataBaseSubWindow_l*0.75+DataBaseSubWindow_r*0.25,DataBaseSubWindow_b-20,25,18,0,4,true);//显示 前一页按钮
		path_=CurrentPath+"\\resource\\next_page.jpg";
		SetBackPic(path_,DataBaseSubWindow_l*0.25+DataBaseSubWindow_r*0.75,DataBaseSubWindow_b-20,25,18,0,4,false);//后一页 按钮
	}
	else
	{
		InitPictureColour(m_imageBack,DataBasePicture_l,DataBasePicture_r+1,DataBasePicture_t,DataBasePicture_b+1,240,240,240);
	}

	

	DrawLineWindows();//显示 每个单元格网格线条
// 	DataBaseSubWindow_t=k_t;
// 	DataBaseSubWindow_b=k_b;
// 	DataBaseSubWindow_r=k_r;
// 	DataBaseSubWindow_l=k_l;
// 	GetDataBasePictureSize();
}
void CLaceborderMatchingDlg::Image_Matching()//匹配函数
{
	CString eignfilePath=CurrentPath+"\\eignfile.txt";
	char *eignfile=(LPSTR)(LPCTSTR)eignfilePath;

	GetCurrentDir(CurrentSelectedDataBasePath);//获取界面左侧目录结构，当前选中的数据库目录

	if (CurrentSelectedDataBasePath==(CurrentPath+"\\DataBase"))
	{
		MessageBox("请选择子目录，如果想在所有数据库中搜索，请购买正式版！！");
		return;
	}
	BOOL f=DirectoryEmpty(CurrentSelectedDataBasePath,"jpg");//判断当前数据库目录下是否存在jpg文件，不存在则认为是空

	if (!PathFileExists(CurrentSelectedDataBasePath+"\\bowfeat_xbfile_kane_x.txt")&&f==FALSE)//不为空的情况下
	{
		ShowingMatchingPicturesFlag=false;//非空情况下 ，对应bow文件不存在则说明未更新数据库
	//		AfxMessageBox("当前目录未创建数据库，请先为当前目录创建数据库！！");
		AfxMessageBox("当前目录图片未更新，请先更新数据库！！");
		return;
	}

///////////////////////////////////////显示进度条区域/////////////////////////////////////////////////////////////////////
	int l=835;
	int r=m_Width-30;
	int t=UpLoadAllPicture_y-6;
	int b=UpLoadAllPicture_y+UpLoadAllPicture_h-7;

	RateOfAdvance(m_dspbuf,l,r,t,b);
	CRect tmp;
	tmp.left=l-2;
	tmp.left=300;
	tmp.right=r+2;
	tmp.top=t-2;
	tmp.bottom=b+2;
	InvalidateRect(tmp,TRUE);
	OnPaint();
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

	


	vector<CString>AllDirPath;

	GetAllDirPath(AllDirPath);//获取当前数据库所有文件目录的全路径
	int DirNum=(int)AllDirPath.size();//保存目录个数 包括当前目录和所有子目录

	int i;
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	bool flag=false;
	for (i=0;i<DirNum;i++)
	{
		if (PathFileExists(CurrentSelectedDataBasePath+"\\AddAndDelete"))//判断当前选择的数据库目录是增减文件
		{
			flag=true;//	AddAndDelete文件存在则说明上次创建数据库后有增减文件
			break;
		}
	}

	if (flag==true)
	{
		ShowingMatchingPicturesFlag=false;
//		AfxMessageBox("当前目录或者子目录图片有更新，请先为当前目录重新创建数据库！！");
		AfxMessageBox("当前目录或者子目录图片有更新，请先为当前目录更新数据库！！");

		return;
	}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	
	IplImage *img;
	img=cvLoadImage(CurrentPicturePath);//加载当前导入的带匹配图片

	CString tmp_name,tmp_name2,WordsFileName;
	char *WordsFile;

	char *bowfeat_filename;
	char *bowtree_num_file;

	
	float rate;
	vector<MySimilarInfo> match_info;//保存匹配度和对应文件路径
	match_info.clear();
	for (i=0;i<DirNum;i++)//每个目录下面匹配一次啊
	{
		CurrentSelectedDataBasePath=AllDirPath[i];

		if (Texture_Colour_Flag==true)//纹理  根据纹理还是颜色选择项 选择加载的特征文件
		{
			tmp_name= CurrentSelectedDataBasePath+"\\bowfeat_xbfile_kane_x.txt";
			bowfeat_filename = (LPSTR)(LPCTSTR)tmp_name;
			// 	tmp_name2= CurrentSelectedDataBasePath+"\\bowfeatnum_xbfile_kane_x.txt";
			// 	bowtree_num_file = (LPSTR)(LPCTSTR)tmp_name2;

			WordsFileName=CurrentSelectedDataBasePath+"\\words_xbfile_kane_x.txt";
			WordsFile = (LPSTR)(LPCTSTR)WordsFileName;
		}
		else if (Texture_Colour_Flag==false)//颜色
		{
			tmp_name= CurrentSelectedDataBasePath+"\\colorfeat_file.txt";
			bowfeat_filename = (LPSTR)(LPCTSTR)tmp_name;
		}

		HSBowFeature TestHSBowFeature;
	//	kd_node *root=NULL;
		
		//	root=Read_db_features(bowfeat_filename,bowtree_num_file);
		if (!PathFileExists(CurrentSelectedDataBasePath+"\\FeatureCounts.Date"))// FeatureCounts.Date 文件记录的是 每个目录下面的每张图片的文件名和对应的特征数目
		{
			continue;//如果该文件不存在，则说明该目录没有生成过每个图片的特征文件
		}
//		root=Read_db_features(bowfeat_filename);
//		HSBowFeature TestHSBowFeature;
		vector <HSBowFeature> DBImageHSBowFeature;
		DBImageHSBowFeature.clear();
		ReadImageHSBowTFeatures(bowfeat_filename,DBImageHSBowFeature);

// 		int tmpN=DBImageHSBowFeature.size();
// 		if (DBImageHSBowFeature[tmpN-1].index==DBImageHSBowFeature[tmpN-2].index)
// 		{
// 			DBImageHSBowFeature.pop_back();
// 		}

		if (Texture_Colour_Flag==true)//纹理
		{
			Extract_Query_feat(img,WordsFile,TestHSBowFeature,eignfile);
		}
		else if (Texture_Colour_Flag==false)//颜色
		{
			Extract_Query_Colorfeat(img,TestHSBowFeature);
		}

		Matching_Image_from_db(TestHSBowFeature,DBImageHSBowFeature,match_info,CurrentSelectedDataBasePath);
//		ImageBruteMatch(TestHSBowFeature,DBImageHSBowFeature,match_info,input_file.dbimagesname_file);
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//sort(match_info, compare_SimilarInfo);
		//排序

		int c,d;
		float swap;
		CString  pathname;

		int n=(int)match_info.size();
		//冒泡排序算法
		for (c = 0 ; c < ( n - 1 ); c++)//每次都会对匹配结果进行排序 将匹配度最高的排在最前面
		{
			for (d = 0 ; d < n - c - 1; d++)
			{
				if (match_info[d].simil_ratio < match_info[d+1].simil_ratio) /* For decreasing order use < */
				{
					swap       = match_info[d].simil_ratio;
					match_info[d].simil_ratio   = match_info[d+1].simil_ratio;
					match_info[d+1].simil_ratio= swap;

					pathname =match_info[d].matched_img_name;
					match_info[d].matched_img_name = match_info[d+1].matched_img_name;
					match_info[d+1].matched_img_name = pathname;
				}
			}
		}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


		rate=(float)(i+1)/DirNum;
		RateOfAdvance(m_dspbuf,l,r,t,b,rate);
		InvalidateRect(tmp,FALSE);
		OnPaint();

	}
    cvReleaseImage(&img);
	
	

	
////////////////////////////////////////////////////////////////////////////////////////
// 	float rate=0.3333;
// 	RateOfAdvance(m_dspbuf,l,r,t,b,rate);
// 	InvalidateRect(tmp,FALSE);
// 	OnPaint();
////////////////////////////////////////////////////////////////////////////////////////////
	
	
	
	



//	cvReleaseImage(&img);

/////////////////////////匹配进度///////////////////////////////////////////////////////////////////
	rate=1;
	RateOfAdvance(m_dspbuf,l,r,t,b,rate);
	InvalidateRect(tmp,FALSE);
	OnPaint();
//////////////////////////////////////////////////////////////////////////////////////////////


	//匹配结束后 调用显示界面
//	int i;
	CString tmp_string;
	ClearCStringVector(CurrentDataBasePicturePath);
	float tmp_f;
	if (!SimilarityRate.empty())//清空容器
	{
		SimilarityRate.clear();
		vector<float>().swap(SimilarityRate);
	}

	int Num=(int)match_info.size();
	if (Num>30)
	{
		Num=30;
	}
	for (i=0;i<Num;i++)//选择前 30个匹配结果  文件名保存在 CurrentDataBasePicturePath 匹配度保存在 SimilarityRate
	{
		tmp_string=match_info[i].matched_img_name;
		CurrentDataBasePicturePath.push_back(tmp_string);
		tmp_f=match_info[i].simil_ratio;
		SimilarityRate.push_back(tmp_f);

		CreateNewDir(CurrentPath+"\\TmpData");
		CString pictureName;
		int pos = match_info[i].matched_img_name.ReverseFind('\\')+1; 
		pictureName = match_info[i].matched_img_name.Right(match_info[i].matched_img_name.GetLength()-pos);
		pictureName=CurrentPath+"\\TmpData\\"+pictureName;
		CopyFile( match_info[i].matched_img_name,pictureName,TRUE);

	}

	ShowingMatchingPicturesFlag=true;
	ShowingMatchingPictures();//显示匹配结果
	GetImageDisplayData(m_dspbuf, m_imageBack, 0, 0);//跟新当前显示界面
	Invalidate(FALSE);
	







//////////////////////保存搜索结果图片///////////////////////////////
	//读取当前时间
	SYSTEMTIME tmp_time;
	GetLocalTime(&tmp_time);
	CString AnalysisTime;
	AnalysisTime.Format("%4d-%02d-%02d",tmp_time.wYear,tmp_time.wMonth,tmp_time.wDay);
	
	CString Path=CurrentPath+"\\system\\"+AnalysisTime+".Date";//保存日期文件
	char *filePath;
	filePath=Path.GetBuffer(Path.GetLength());//

	MY_DATE tmp_date;
	vector<MY_DATE>dateData;//日期文件信息

	if (PathFileExists(Path))
	{
		ReadDateFile(dateData,filePath);//读取日期文件
		int num=(int)CurrentDataBasePicturePath.size();
		for (i=0;i<num;i++)
		{
			bool f=false;
			for (int j=0;j<(int)dateData.size();j++)
			{
				if (CurrentDataBasePicturePath[i]==dateData[j].Name)//如果 CurrentDataBasePicturePath[i]记录的数据库文件之前被检索过，就把之前的检索记录增1
				{
					dateData[j].Sum++;
					f=true;
					break;
				}
			}
			if (f==false)//否则就将这次的检索记录新增在后面
			{
				tmp_date.Name=CurrentDataBasePicturePath[i];
				tmp_date.Sum=1;
				dateData.push_back(tmp_date);
			}
		}
	}
	else //如果不存在 则说明是软件第一次检索
	{
		int num=(int)CurrentDataBasePicturePath.size();
		for (i=0;i<num;i++)
		{
			tmp_date.Name=CurrentDataBasePicturePath[i];
			tmp_date.Sum=1;
			dateData.push_back(tmp_date);
		}
	}

	WriteDateFile(dateData,filePath);//更新日期文件

///////////////////////////////////////////////////
	int kkk=1;

}
void CLaceborderMatchingDlg::WriteDateFile(vector<MY_DATE>&dateData,char *filePath)
{
	int n=(int)dateData.size();
	int i;

	FILE *File=NULL;

	fopen_s(&File,filePath,"wb");//为读写建立新的二进制文件
	if (File)//打开成功
	{
		for (i=0;i<n;i++)
		{
			WriteCString(dateData[i].Name,File);
			fwrite(&dateData[i].Sum,sizeof(int),1,File);
		}		
	}
	//////////////////////////////////////////////////////////////////////////
	WriteCString("Adonis  2014",File);//账户结束标志 禁止用户注册这个用户名
	//////////////////////////////////////////////////////////
	fclose(File);
	File=NULL;
}
void CLaceborderMatchingDlg::ReadDateFile(vector<MY_DATE>&dateData,char*  filePath,bool flag)//从user.acc读入所有账户
{
	if (!dateData.empty())
	{
		dateData.clear();
		vector<MY_DATE>().swap(dateData);
	}

	MY_DATE tmp_date;

	FILE *File=NULL;

	fopen_s(&File,filePath,"rb");//为读写建立新的二进制文件
	if (File)//打开成功
	{
		while(!feof(File))//(feof返回1，说明到文件结尾)
		{
			ReadCString(tmp_date.Name,File);
			if ("Adonis  2014"==tmp_date.Name)
			{
				break;
			}
			fread(&tmp_date.Sum,sizeof(int),1,File);

			if (flag==true)
			{
				if (PathFileExists(tmp_date.Name))
				{
					dateData.push_back(tmp_date);
				}
			}
			else
			{
				dateData.push_back(tmp_date);
			}
			
			
		}		
	}
	fclose(File);
	File=NULL;
}
void CLaceborderMatchingDlg::Matching_Image_from_db(HSBowFeature TestHSBowFeature,vector <HSBowFeature> DBImageHSBowFeature, vector<MySimilarInfo>& match_info,CString DirPath)
{
	int i,j;
	float *rate_info=new float[DBImageHSBowFeature.size()];
	float *asend_rate=new float[DBImageHSBowFeature.size()];
	for(i=0;i<DBImageHSBowFeature.size();i++)
	{
		rate_info[i]=vector_correlation(TestHSBowFeature.HSBowFV,DBImageHSBowFeature[i].HSBowFV,TestHSBowFeature.HSBowFVSize);
	}

	int *asending_index=new int[DBImageHSBowFeature.size()];
	find_first_thirty(rate_info,DBImageHSBowFeature.size(),asend_rate,asending_index);

// 	struct feature *img_feat = new struct feature;
// 	img_feat->d=TestHSBowFeature.HSBowFVSize;
// 	img_feat->descr = new double[img_feat->d];
// 	img_feat->index=0;
// 	for (i=0;i<img_feat->d;i++)
// 	{
// 		img_feat->descr[i]=TestHSBowFeature.HSBowFV[i]; 
// 	}




	vector<CString>all_img_name;

////////////////////////////////////////////////////////////////////////////////////////////////
	vector<MY_DATE>dateData;//日期文件信息

	CString FeatureCountsPath=DirPath+"\\FeatureCounts.Date";//保存特征数文件
	char *filePath;
	filePath=FeatureCountsPath.GetBuffer(FeatureCountsPath.GetLength());

	ClearCStringVector(all_img_name);
//	FeatureCounts=0;

	if (PathFileExists(filePath))
	{
		ReadDateFile(dateData,filePath);//将每个图片的文件全路径和对应的特征数保存在dateData

		for (int j=0;j<(int)dateData.size();j++)
		{
			CString path1=dateData[j].Name;
			all_img_name.push_back(path1);//只记录有特征文件的图片路径，新增的不会被加入，已经删除的图片必须把FeatureCounts.Date记录删除
		}
	}

	if (!dateData.empty())
	{
		dateData.clear();
		vector<MY_DATE>().swap(dateData);
	}
////////////////////////////////////////////////////////////////////////////////////////////////

	int num=K;
	num=(int)all_img_name.size();

	if (num>30)
	{
		num=30;
	}

//	match_info.clear();
	for( i = 0; i < num; i++ )
	{
		MySimilarInfo temp_info;
		//		cout<<DBImageHSBowFeature[asending_index[i]].index<<"  ";
		temp_info.matched_img_name=all_img_name[DBImageHSBowFeature[asending_index[i]].index];
		temp_info.simil_ratio=asend_rate[i];
		match_info.push_back(temp_info);
	}
	delete[] rate_info;
	delete[] asend_rate;
	delete[] asending_index;
	for(i=0;i<DBImageHSBowFeature.size();i++)
		free(DBImageHSBowFeature[i].HSBowFV);
	free(TestHSBowFeature.HSBowFV);



	
// 	struct feature** nbrs;
// 	int k;
// 	k = kdtree_bbf_knn( kd_root, img_feat, num, &nbrs, KDTREE_BBF_MAX_NN_CHKS );
// 
// 	for( i = 0; i < num; i++ )
// 	{
// 		MySimilarInfo temp_info;
// 		temp_info.matched_img_name=all_img_name[nbrs[i]->index];
// 		float *nbrs_tmp=(float *)calloc(TestHSBowFeature.HSBowFVSize,sizeof(float));
// 		for (j=0;j<TestHSBowFeature.HSBowFVSize;j++)
// 			nbrs_tmp[j]=nbrs[i]->descr[j];
// 		temp_info.simil_ratio=vector_correlation(TestHSBowFeature.HSBowFV,nbrs_tmp,TestHSBowFeature.HSBowFVSize);//error
// 		match_info.push_back(temp_info);
// 	}
// 
// 	kdtree_release( kd_root );
// 	delete[] img_feat->descr;
// 	delete img_feat;
// 	free(nbrs);
// 	free(TestHSBowFeature.HSBowFV);


}

void CLaceborderMatchingDlg::changePage(CPoint point)//显示前一页与后一页按钮
{
	if ((int)CurrentDataBasePicturePath.size()<1)
	{
		return;
	}
	int l,r,t,b;
	l=DataBaseSubWindow_l*0.75+DataBaseSubWindow_r*0.25-25;
	t=DataBaseSubWindow_b-21;
	r=l+25;
	b=20+t;
	if (point.x>l&&point.x<r&&point.y>t&&point.y<b)//前一页
	{
		CurrentSubDataBasePageIndex--;
		if (CurrentSubDataBasePageIndex<1)
		{
			CurrentSubDataBasePageIndex=1;
		}
		ShowEachPagePicture(CurrentSubDataBasePageIndex);
		DrawLineWindows();
		GetImageDisplayData(m_dspbuf, m_imageBack, 0, 0);
	}
	l=DataBaseSubWindow_l*0.25+DataBaseSubWindow_r*0.75;
	t=DataBaseSubWindow_b-21;
	r=l+25;
	b=20+t;
	if (point.x>l&&point.x<r&&point.y>t&&point.y<b)//后一页
	{
		CurrentSubDataBasePageIndex++;
		if (CurrentSubDataBasePageIndex>=CurrentSubDataBasePageNum)
		{
			CurrentSubDataBasePageIndex=CurrentSubDataBasePageNum;
		}
		ShowEachPagePicture(CurrentSubDataBasePageIndex);
		DrawLineWindows();
		GetImageDisplayData(m_dspbuf, m_imageBack, 0, 0);
	}

	
	
	CRect tmp;
	tmp.left=DataBasePicture_l;
	tmp.right=DataBasePicture_r;
	tmp.top=DataBasePicture_t;
	tmp.bottom=DataBasePicture_b+30;

	InvalidateRect(tmp,FALSE);
}
void CLaceborderMatchingDlg::UpLoadAllpicture()//导入一个文件夹下的所有图片
{
	int k=DataBaseSubWindow_t;
	DataBaseSubWindow_t+=30;
	GetDataBasePictureSize();
	InitPictureColour(m_dspbuf,DataBasePicture_l,DataBasePicture_r+1,DataBasePicture_t,DataBasePicture_b+1,240,240,240);
//	GetImageDisplayData(m_dspbuf, m_imageBack, 0, 0);
	
	UnLock(CurrentPath+"\\tranning\\tran.{208D2C60-3AEA-1069-A2D7-08002B30309D}");
	CString dirPath;
	ChooseADir(dirPath);

// 	if (dirPath!=(CurrentPath+"\\tranning\\tran"))
// 	{
// 		AfxMessageBox("试用版只能从tranning文件夹下的tran文件夹导入图片！！");
// 		if (!PathFileExists(CurrentPath+"\\tranning\\tran.{208D2C60-3AEA-1069-A2D7-08002B30309D}"))
// 		{
// 			Lock(CurrentPath+"\\tranning\\tran");
// 		}
// //		return;
// 		dirPath="";
// 	}
	if (dirPath==(CurrentPath+"\\tranning\\tran"))
	{
		dirPath=CurrentPath+"\\tranning\\tran.{208D2C60-3AEA-1069-A2D7-08002B30309D}";
	}
	if (!PathFileExists(CurrentPath+"\\tranning\\tran.{208D2C60-3AEA-1069-A2D7-08002B30309D}"))
	{
		Lock(CurrentPath+"\\tranning\\tran");
	}
//	dirPath=CurrentPath+"\\tranning\\tran.{208D2C60-3AEA-1069-A2D7-08002B30309D}";
////////////////////////////////////////////////////////
	CFileFind finder;
//	vector<CString>  allfile;
	ClearCStringVector(allfile);

	BOOL bworking =(BOOL) finder.FindFile(dirPath+"\\*.*");   //查询该文件夹下的所有图片
	while (bworking)
	{
		bworking =(BOOL)  finder.FindNextFile();
		CString fileName=finder.GetFileName();
		if (!finder.IsDots()&&finder.IsDirectory())
		{
			fileName=dirPath+"\\"+fileName;
			BOOL f=DirectoryEmpty(fileName,"jpg");

			if (f==TRUE)//为空
			{
				f=DirectoryEmpty(fileName,"jpeg");

				if (f==TRUE)//为空
				{
					f=DirectoryEmpty(fileName,"png");

					if (f==TRUE)//为空
					{
						f=DirectoryEmpty(fileName,"bmp");
					}
				}
			}
			
			if (f==FALSE)//不为空
			{
				allfile.push_back(fileName);       //将文件夹名保留
			}
			
		}
	}

	if (allfile.empty())
	{
		allfile.push_back(dirPath);
	}
//////////////////////////////////////////////////////

//	ShowCurrentDataBasePicture(dirPath);
///////////////////////////////////////////////////////////////////////////////////////////////////

	if (!PathFileExists(dirPath))
	{
		DataBaseSubWindow_t=k;
		GetDataBasePictureSize();

		//取消选择目录的情况
		ManageDataBaseClick(2);//点击3个子菜单
		GetImageDisplayData(m_dspbuf, m_imageBack, 0, 0);
		Invalidate(FALSE);

		if (!PathFileExists(CurrentPath+"\\tranning\\tran.{208D2C60-3AEA-1069-A2D7-08002B30309D}"))
		{
			Lock(CurrentPath+"\\tranning\\tran");
		}

		return;
	}

	int DirNum=0;
	///////////////////////////////////////////获取本次导入图片的数量//////////////////////////////////

	int PictureCounts=0;
	int tmp_count=0;

	for (DirNum=0;DirNum<(int)allfile.size();DirNum++)
	{
		dirPath=allfile[DirNum];
		CString format[4]={"jpg","jpeg","bmp","png"};

		int i=0;
		for (i=0;i<4;i++)
		{
			GetPictureCounts(dirPath,format[i],tmp_count);//获取所有 图片数量
			PictureCounts+=tmp_count;

			MSG msg;
			while(::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
			{
				::TranslateMessage(&msg);
				::DispatchMessage(&msg);
			}
		}

		MSG msg;
		while(::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			::TranslateMessage(&msg);
			::DispatchMessage(&msg);
		}
	}

	if (TRUE==LoadingPictureCountsTest(CurrentSelectedDataBasePath,PictureCounts))
	{
		MessageBox("该目录最大存储500张图片！");

		DataBaseSubWindow_t=k;
		GetDataBasePictureSize();

		//取消选择目录的情况
		ManageDataBaseClick(2);//点击3个子菜单
		GetImageDisplayData(m_dspbuf, m_imageBack, 0, 0);
		Invalidate(FALSE);

		if (!PathFileExists(CurrentPath+"\\tranning\\tran.{208D2C60-3AEA-1069-A2D7-08002B30309D}"))
		{
			Lock(CurrentPath+"\\tranning\\tran");
		}

		return;
	}
	/////////////////////////////////////////////////////////////////////////////
	for (DirNum=0;DirNum<(int)allfile.size();DirNum++)
	{
		MSG msg;
		while(::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			::TranslateMessage(&msg);
			::DispatchMessage(&msg);
		}

		dirPath=allfile[DirNum];

		CurrentDir=DirNum+1;
		ClearCStringVector(Supplierinfo);

		CSupplierInfo SupplierInfo;
		SupplierInfo.DoModal();

		CRect tmp;
		tmp.left=DataBasePicture_l;
		tmp.right=DataBasePicture_r+1;
		tmp.top=DataBasePicture_t;
//		tmp.bottom=DataBasePicture_b+1;
		tmp.bottom=m_Height;
		InvalidateRect(tmp,FALSE);

		CString format[4]={"jpg","jpeg","bmp","png"};
		ClearCStringVector(CurrentDataBasePicturePath);
		int i=0;
		for (i=0;i<4;i++)
		{
			MSG msg;
			while(::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
			{
				::TranslateMessage(&msg);
				::DispatchMessage(&msg);
			}

			GetAllFile(dirPath,format[i],CurrentDataBasePicturePath,false);//获取所有 jpg文件
		}

		int kkk=((int)CurrentDataBasePicturePath.size()-1)/(DataBasePicture_x_num*DataBasePicture_y_num)+1;
		//格式转换到数据库
		CurrentSubDataBasePageNum=kkk;
		transformPicture(CurrentDataBasePicturePath);//将导入的图片全部转换为jpg存放到数据库
		CurrentSubDataBasePageNum=kkk;//转移过程中会改变GetAllFile获取文件个数对文件编号命名时CurrentSubDataBasePageNum会被改变

		if (CurrentDataBasePicturePath.size()>0)
		{
			showPageFlag=true;
			CurrentSubDataBasePageIndex=1;
			ShowEachPagePicture(CurrentSubDataBasePageIndex);

			CString path_=CurrentPath+"\\resource\\next_page.jpg";
			SetBackPic(path_,DataBaseSubWindow_l*0.75+DataBaseSubWindow_r*0.25,DataBaseSubWindow_b-20,25,18,0,4,true);//前一页
			path_=CurrentPath+"\\resource\\next_page.jpg";
			SetBackPic(path_,DataBaseSubWindow_l*0.25+DataBaseSubWindow_r*0.75,DataBaseSubWindow_b-20,25,18,0,4,false);//后一页


			DrawLineWindows();
			GetImageDisplayData(m_dspbuf, m_imageBack, 0, 0);
			//	CRect tmp;
			tmp.left=DataBasePicture_l;
			tmp.right=DataBasePicture_r+1;
			tmp.top=DataBasePicture_t;
//			tmp.bottom=DataBasePicture_b+1;
			tmp.bottom=m_Height;
			InvalidateRect(tmp,FALSE);

		}
		else
		{
			InitPictureColour(m_dspbuf,DataBasePicture_l,DataBasePicture_r+1,DataBasePicture_t,DataBasePicture_b+1,240,240,240);
		}
	}


//////////////////////////////////////////////////////////////////////////////////////////////////////////
	CreatePictureAddAndDeleteFlagFile();//创建数据库增减标记文件

	DataBaseSubWindow_t=k;
	GetDataBasePictureSize();
}
void CLaceborderMatchingDlg::RateOfAdvance(IplImage* image,int l,int r,int t,int b)//画一个进度条
{
	int i,j;
	for (i=l;i<=r;i++)
	{
		for (j=t;j<=b;j++)
		{
			if (i==l||i==r||j==t||j==b)
			{
				SetPointColour(image,i ,j,0,0,0);
			}
		}
	}
}
void CLaceborderMatchingDlg::RateOfAdvance(IplImage* image,int l,int r,int t,int b,float rate)//画一个进度条
{
	int i,j;
	int k=(int)((r-l)*rate);
	int g;
	for (i=l+1;i<(l+k);i++)
	{
		g=180;
		for (j=t+1;j<b;j++)
		{
			if (j<=(t+b)/2)
			{
				g=g+5;
			}
			else
			{
				g=g-5;
			}
			SetPointColour(image,i ,j,0,g,0);
		}
	}
}

void CLaceborderMatchingDlg::transformPicture(vector<CString> &CurrentDataBasePicturePath)//将导入的图片全部转换为jpg存放到数据库
{
	int i,n;
// 	char *path;
// 	IplImage* temp=NULL;

	int l=UpLoadAllPicture_x+UpLoadAllPicture_w+5;
	int r=m_Width-30;
	int t=UpLoadAllPicture_y;
	int b=UpLoadAllPicture_y+UpLoadAllPicture_h-1;

	RateOfAdvance(m_dspbuf,l,r,t,b);
	CRect tmp;
	tmp.left=l-2;
//	tmp.right=r+2;
// 	tmp.top=t-2;
// 	tmp.bottom=b+2;
	tmp.right=m_Width;
	tmp.top=0;
	tmp.bottom=m_Height;

	InvalidateRect(tmp,TRUE);
	OnPaint();


	n=(int)CurrentDataBasePicturePath.size();
	if (n>0)
	{
		for (i=0;i<n;i++)
		{
			MSG msg;
			while(::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
			{
				::TranslateMessage(&msg);
				::DispatchMessage(&msg);
			}

			RateOfAdvance(m_dspbuf,l,r,t,b,(float)(i+1)/n);
			InvalidateRect(tmp,FALSE);
//			Invalidate(FALSE);
			OnPaint();

			LoadingPicture(CurrentDataBasePicturePath[i],CurrentSelectedDataBasePath);
			// 		path = (LPSTR)(LPCTSTR)CurrentDataBasePicturePath[i];
			// 		cvReleaseImage(&temp);
			// 		temp = cvLoadImage(path,CV_LOAD_IMAGE_COLOR);
			// 		if (!temp)
			// 		{
			// 			continue;
			// 		}
			// 		ChangeName(CurrentDataBasePicturePath[i],CurrentSelectedDataBasePath);
			// 		cvSaveImage(CurrentDataBasePicturePath[i],temp);
		}
	}
	else
	{
		InitPictureColour(m_dspbuf,l,r+1,t,b+1,240,240,240);
		InvalidateRect(tmp,FALSE);
		OnPaint();

		MessageBox("当前选择的文件夹没有图片！！");
	}
	
}
void CLaceborderMatchingDlg::LoadingPicture(CString &CurrentPath,CString DirPath)//移动图片
{
	/////////////////////////////////直接复制///////////////////////////////////////
	CString pictureName;
	int pos = CurrentPath.ReverseFind('\\')+1; 
	pictureName = CurrentPath.Right(CurrentPath.GetLength()-pos);
	CString tmps1=pictureName;
	pictureName=DirPath+"\\"+pictureName;
	CString tmpS=CurrentPath.Left(pos-1);
	if (tmpS.Right(13)=="tranning\\tran"||tmpS.Right(12)=="testing\\test")
	{
		tmpS=tmpS+".{208D2C60-3AEA-1069-A2D7-08002B30309D}\\"+tmps1;
//		CopyFile(tmpS,pictureName,TRUE);
		CurrentPath=tmpS;
	}

	CopyFile(CurrentPath,pictureName,TRUE);
	



	///////////////////////////////////////////////////////////////////////////


	char *path;
	IplImage* temp=NULL;
	path = (LPSTR)(LPCTSTR)CurrentPath;
	cvReleaseImage(&temp);
	temp = cvLoadImage(path,CV_LOAD_IMAGE_COLOR);
	if (!temp)
	{
		return;
	}
	ChangeName(CurrentPath,DirPath);
//	cvSaveImage(CurrentPath,temp);
	cvReleaseImage(&temp);

	////////////////////////////////////////////////////////////////////////////////////////
//	CurrentPath=pictureName+".jpg";
	TCHAR* pOldName =pictureName.GetBuffer(pictureName.GetLength()); 
	TCHAR* pNewName =CurrentPath.GetBuffer(CurrentPath.GetLength()); 
	CFile::Rename(pOldName, pNewName);

////////////////////////////////////////////////////////////////////////////////////////////
	CString infoPath=CurrentPath+"Info.dat";

	int n=(int)Supplierinfo.size();
	if (n>0)//供应商信息界面 按确定 而不是直接关闭对话框的情况
	{
		int i;

		FILE *File=NULL;

		fopen_s(&File,infoPath,"wb");//为读写建立新的二进制文件
		if (File)//打开成功
		{
			for (i=1;i<n;i++)//读取的修改后的文件名不再保存
			{
				WriteCString(Supplierinfo[i],File);
			}		
		}
		//////////////////////////////////////////////////////////////////////////
		WriteCString("Adonis  2014",File);//账户结束标志 禁止用户注册这个用户名
		//////////////////////////////////////////////////////////
		fclose(File);
		File=NULL;
	}
	
///////////////////////////////////////////////////////////////////////////////////////////
}

void CLaceborderMatchingDlg::ChangeName(CString &Path,CString DirPath)//修改保存路径
{
	vector<CString> tmpPath;
	CString tmp_s;

	int kkk=CurrentSubDataBasePageNum;
	GetAllFile(DirPath,"jpg",tmpPath,true);//获取所有 jpg文件
	CurrentSubDataBasePageNum=kkk;
	ClearCStringVector(tmpPath);

	int k=(int)tmpPath.size();
//	tmp_s.Format("%05d",k);
	tmp_s.Format("%09d",k);
	HTREEITEM hItem = m_tree.GetSelectedItem();
	if (!hItem)
	{
		AfxMessageBox("请先选择将要导入的数据库目录！");
	}
	else
	{
		if (hItem==m_tree.GetRootItem())
		{
			Path="DataBase";
		}
		else
		{
			CString s;
			s = m_tree.GetItemText(hItem);
			hItem=m_tree.GetParentItem(hItem);
			if (hItem==m_tree.GetRootItem())
			{
				Path="DataBase_"+s;
			}
			else
			{
				Path=s;
				s = m_tree.GetItemText(hItem);
				s=s+"_"+Path;
				Path="DataBase_"+s;
			}
		}	
	}
	CString tmp_Path=DirPath+"\\"+Path+tmp_s+".jpg";
	while(PathFileExists(tmp_Path))
	{
		k++;
//		tmp_s.Format("%05d",k);
		tmp_s.Format("%09d",k);
		tmp_Path=DirPath+"\\"+Path+tmp_s+".jpg";
	}

	Path=tmp_Path;
	
}
int CALLBACK SetSelect_CB(HWND win, UINT msg, LPARAM param, LPARAM data) 
{
	if ( msg == BFFM_INITIALIZED ) {
		::SendMessage(win, BFFM_SETSELECTION, TRUE, (LPARAM)(LPCTSTR)(g_CutPicturePath+"\\tranning\\"));
	}
	return(0);
}
void CLaceborderMatchingDlg::ChooseADir(CString &DirPath)//选择一个文件夹
{
	BROWSEINFO bInfo;
	ZeroMemory(&bInfo, sizeof(bInfo));
	bInfo.hwndOwner =GetSafeHwnd();
	bInfo.lpszTitle = _T("请选择含有原始图像的目录: ");
	bInfo.ulFlags = BIF_RETURNONLYFSDIRS | BIF_EDITBOX;// BIF_BROWSEINCLUDEFILES
	bInfo.lpfn=SetSelect_CB;

	LPITEMIDLIST lpDlist; //用来保存返回信息的IDList
	lpDlist = SHBrowseForFolder(&bInfo) ; //显示选择对话框
	if(lpDlist != NULL) //用户按了确定按钮
	{
		TCHAR chPath[MAX_PATH]; //用来存储路径的字符串
		SHGetPathFromIDList(lpDlist, chPath);//把项目标识列表转化成字符串
	//*pathname = chPath; //将TCHAR类型的字符串转换为CString类型的字符串

		DirPath.Format(_T("%s"),chPath);
	}
}

void CLaceborderMatchingDlg::OnLButtonUp(UINT nFlags, CPoint point)
{
// 	CutLeftTopPoint.x=ShowPicture_l;
// 	CutLeftTopPoint.y=ShowPicture_t;
// 	CutRightBottomPoint.x=ShowPicture_l;
// 	CutRightBottomPoint.y=ShowPicture_t;

	CDialogEx::OnLButtonUp(nFlags, point);
}
void CLaceborderMatchingDlg::OnMouseMove(UINT nFlags, CPoint point)
{
	
// 	if (LogFlag==TRUE&&(CurrentMenu==MenuName[0]||ShowingMatchingPicturesFlag==true))
// 	{
// 		CurrentPoint=point;
// 	}
	if (LogFlag==TRUE&&CurrentMenu==MenuName[1]&&ShowingMatchingPicturesFlag==true)
	{
		CurrentSelectedPicturePath="";
		int k=GetCurrentPicturePath(CurrentSelectedPicturePath,point);

		ShowOriginalPicture(CurrentSelectedPicturePath,point);

		ShowOriginalPictureFlag=true;
		CRect tmp;
		tmp.left=15;
		tmp.right=599;
		tmp.top=m_Height/2;
		tmp.bottom=m_Height;
		InvalidateRect(tmp,FALSE);

		tmp.left=(DataBasePicture_l+DataBasePicture_r)*0.5-50;
		tmp.right=m_Width;
		tmp.top=m_Height-33-30;
		tmp.bottom=m_Height;
		InvalidateRect(tmp,FALSE);
		ShowOriginalPictureFlag=false;
	}

	int l,r,t,b;
	if (LogFlag==TRUE&&CurrentMenu==MenuName[1]&&ShowingMatchingPicturesFlag==false)
	{
		l=ShowPicture_l;
		t=ShowPicture_t;
		r=ShowPicture_r;
		b=ShowPicture_b;
		if (point.x>l&&point.x<r&&point.y>t&&point.y<b)
		{
			if (nFlags==MK_LBUTTON&&LoadPictureSuccessFlag==TRUE&&MovePictureFlag==TRUE)
			{
				MovedPicture(point.x-CutLeftTopPoint.x,point.y-CutLeftTopPoint.y);
				ShowLoadedPicture();
				Invalidate(FALSE);
			}

			if (nFlags==MK_LBUTTON&&LoadPictureSuccessFlag==TRUE&&CutPictureFlag==TRUE)//裁剪
			{
				CutRightBottomPoint=point;
				ShowLoadedPicture();
				ShowPictureCutLine();
				CRect tmp;
				tmp.left=ShowPicture_l;
				tmp.right=m_Width;
				tmp.top=0;
				tmp.bottom=m_Height;
//				Invalidate(FALSE);
				InvalidateRect(tmp,FALSE);
			}
			else
			{
				CutLeftTopPoint=point;
			}
			
			if (MovePictureFlag==TRUE&&LoadPictureSuccessFlag==TRUE)
			{
				SetClassLong(this->GetSafeHwnd(),GCL_HCURSOR , (LONG)LoadCursor(AfxGetInstanceHandle() , MAKEINTRESOURCE(IDC_CURSOR_HAND)));//切换手型指针
			}
			
		}
		else
		{
			SetClassLong(this->GetSafeHwnd(),GCL_HCURSOR , (LONG)LoadCursor(NULL , IDC_ARROW));//标准箭头
		}


		
// 		if (LoadedPicture)//测试当前坐标对图片坐标的转换
// 		{
// 			int x,y;
// 			GetPointInPicture(x,y,point);
// 
// 			CString Tip;
// 			LOGFONT   lf;   
// 			memset(&lf,   0,   sizeof(LOGFONT));   
// 			lf.lfHeight   =20;         //字体的高
// 			lf.lfWidth    =8;
// 			lstrcpy(lf.lfFaceName, _T("Verdana"));
// 			CString s;
// 			s.Format("%d",x);
// 			Tip="x= "+s;
// 			s.Format("%d",y);
// 			Tip+="  y= "+s;
// 			ShowText(point.x+5,point.y+5,0,0,0,lf,Tip);
// 		}
		
	}

	CDialogEx::OnMouseMove(nFlags, point);
}
void CLaceborderMatchingDlg::ShowOriginalPicture(CString path, CPoint point)
{


	char *pathname = (LPSTR)(LPCTSTR)path;
	IplImage* temp = cvLoadImage(pathname,CV_LOAD_IMAGE_COLOR);

	double rate;
	int tmp_w;
	int tmp_h;
	int middle_x;
	int middle_y;

	if (!temp)
	{
		InitPictureColour(m_dspbuf,15,599,m_Height/2,m_Height-33-10 ,240,240,240);
		
		return;
	}
	int w=DataBasePicture_w;
	int h=DataBasePicture_h;

	int x;
	int y;
	x=(point.x-DataBasePicture_l)%DataBasePicture_w;
	y=(point.y-DataBasePicture_t)%DataBasePicture_h;

	x=x-w/2;
	y=y-h/2;//距离中心点的距离
	w=w-6;
	h=h-6;

	tmp_w=temp->width;
	tmp_h=temp->height;
	middle_x=tmp_w/2;
	middle_y=tmp_h/2;


	//默认长宽都小于显示区域
	rate=(double)(temp->height)/h;

	if (tmp_h>h)
	{
		rate=(double)(temp->height)/h;
		tmp_h=h;
		tmp_w=(int)(temp->width/rate);
	}
	if (tmp_w>w)
	{
		rate=(double)(temp->width)/w;
		tmp_h=(int)(temp->height/rate);
		tmp_w=w;
	}


	if (rate<1)
	{
		rate=1;
	}

	x=x*rate+temp->width/2;
	y=y*rate+temp->height/2;

// 	t=BlueLine+70;
// 	b=m_Height-33-10;
// 
// 	l=15;
// 	r=599;
// 	t=BlueLine+70;
// 	b=m_Height/2;

	tmp_h=m_Height-33-10 -m_Height/2-5;
	tmp_w=599-15;

	int i,j,i1,j1,i2,j2;
	for(i =0; i <tmp_w; i++)
		for(j =0; j <tmp_h; j++)
		{
			i1=15+i;
			j1=m_Height/2+5+j;
			j2=x-tmp_w/2+i;
			i2=y-tmp_h/2+j;
			if (j2>=0&&j2<temp->width&&i2>=0&&i2<temp->height)
			{
				SetPointColour(m_dspbuf,i1,j1,temp,j2,i2);
			}
			else
			{
				SetPointColour(m_dspbuf,i1,j1,240,240,240);
			}
			
		}
	cvReleaseImage(&temp);

}
void CLaceborderMatchingDlg::OnRButtonDown(UINT nFlags, CPoint point)
{
//	((CTreeCtrl *)GetDlgItem(IDC_TREE_DATABASE))->EnableWindow(TRUE);

	int l,r,t,b;
	if (LogFlag==TRUE&&CurrentMenu==MenuName[1])
	{
		l=ShowPicture_l;
		t=ShowPicture_t;
		r=ShowPicture_r;
		b=ShowPicture_b;
		if (point.x>l&&point.x<r&&point.y>t&&point.y<b)
		{
			if (LoadPictureSuccessFlag==TRUE&&CutPictureFlag==TRUE)
			{
				ShowLoadedPicture();
				Invalidate(FALSE);
				//右击取消剪切数据
				CutLeftTopPoint.x=ShowPicture_l;
				CutLeftTopPoint.y=ShowPicture_t;
				CutRightBottomPoint.x=ShowPicture_l;
				CutRightBottomPoint.y=ShowPicture_t;
			}			
		}
	}
	if (LogFlag==TRUE&&accounts[CurrentAccount].Level==true)//数据库管理
	{
		if ((manageDataBaseNum==1&&CurrentMenu==MenuName[0])||CurrentMenu==MenuName[2]||ShowingMatchingPicturesFlag==true)
		{
			int k=GetCurrentPicturePath(CurrentSelectedPicturePath,point);
			CMenu Menu; 

			if (Menu.LoadMenu(IDR_MENU_TREE))  
			{   
				CMenu* pSubMenu = Menu.GetSubMenu(1); 

				if (k>=0&&k<=(int)CurrentDataBasePicturePath.size())
				{
					if (CurrentMenu==MenuName[2]||ShowingMatchingPicturesFlag==true)
					{
						pSubMenu->EnableMenuItem(ID_32778,MF_DISABLED|MF_GRAYED);//灰化禁用
						pSubMenu->EnableMenuItem(ID_32780,MF_DISABLED|MF_GRAYED);//灰化禁用
						pSubMenu->EnableMenuItem(ID_32781,MF_DISABLED|MF_GRAYED);//灰化禁用		
					}
				}
				else //if (0==(int)CurrentDataBasePicturePath.size())//为空
				{
					pSubMenu->EnableMenuItem(ID_32776,MF_DISABLED|MF_GRAYED);//灰化禁用
					pSubMenu->EnableMenuItem(ID_32777,MF_DISABLED|MF_GRAYED);//灰化禁用	
					pSubMenu->EnableMenuItem(ID_32778,MF_DISABLED|MF_GRAYED);//灰化禁用
					pSubMenu->EnableMenuItem(ID_32780,MF_DISABLED|MF_GRAYED);//灰化禁用

				}
				if (CutPicturePath=="")
				{
					pSubMenu->EnableMenuItem(ID_32781,MF_DISABLED|MF_GRAYED);//灰化禁用	  粘贴
				}

				if (pSubMenu!=NULL)   
				{       
					pSubMenu->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x+3, point.y+20, this);   
				}  
			}
			
			

//			MessageBox(CurrentSelectedPicturePath);
		}
	}

	CDialogEx::OnRButtonDown(nFlags, point);
}

void CLaceborderMatchingDlg::LoadPicture()
{
	CString tmppath;
	CString picturename;
	char *pictureName;

	if (CurrentMenu==MenuName[0])//数据库管理
	{
		UnLock(CurrentPath+"\\tranning\\tran.{208D2C60-3AEA-1069-A2D7-08002B30309D}");

		picturename=CurrentPath+"\\tranning\\tran\\";
		pictureName=(LPSTR)(LPCTSTR)picturename;
	}
	else if (CurrentMenu==MenuName[1])
	{
		UnLock(CurrentPath+"\\testing\\test.{208D2C60-3AEA-1069-A2D7-08002B30309D}");
		UnLock(CurrentPath+"\\tranning\\tran.{208D2C60-3AEA-1069-A2D7-08002B30309D}");
		picturename=CurrentPath+"\\testing\\test\\";
		pictureName=(LPSTR)(LPCTSTR)picturename;
	}
	CFileDialog fdlg(TRUE, NULL,pictureName/*NULL*/, OFN_NOCHANGEDIR);

	if( fdlg.DoModal() == IDOK )
	{
		CurrentPicturePath = fdlg.GetPathName();
		CurrentPictureName = fdlg.GetFileName();

// 		tmppath=CurrentPicturePath.Left(CurrentPicturePath.GetLength()-CurrentPictureName.GetLength());
// 
// 		CurrentPicturePath=tmppath.Left(tmppath.GetLength()-1)+".{208D2C60-3AEA-1069-A2D7-08002B30309D}\\"+CurrentPictureName;
// 		if (CurrentMenu==MenuName[0])//数据库管理
// 		{
// 			if (tmppath!=(CurrentPath+"\\tranning\\tran\\"))
// 			{
// 				AfxMessageBox("试用版只能从tranning文件夹下的tran文件夹导入图片！！");
// 				CurrentPicturePath ="";
// 				CurrentPictureName ="";
// 			}
// 		}
// 		else if (CurrentMenu==MenuName[1])
// 		{
// 			if ((tmppath!=(CurrentPath+"\\tranning\\tran\\"))&&(tmppath!=(CurrentPath+"\\testing\\test\\")))
// 			{
// 				AfxMessageBox("试用版只能从\ntranning文件夹下的tran文件夹\n或者\ntesting文件夹下的test文件夹\n选择图片！！");
// 				CurrentPicturePath ="";
// 				CurrentPictureName ="";
// 			}
// 		}

	}
	else
	{
		CurrentPicturePath ="";
		CurrentPictureName ="";
	}

	CString path1=CurrentPicturePath.Left(CurrentPicturePath.GetLength()-1-CurrentPictureName.GetLength());

	if (path1.Right(13)=="tranning\\tran"||path1.Right(12)=="testing\\test")
	{
		path1=path1+".{208D2C60-3AEA-1069-A2D7-08002B30309D}\\"+CurrentPictureName;
		//		CopyFile(tmpS,pictureName,TRUE);
		CurrentPicturePath=path1;
	}
	
	if (!PathFileExists(CurrentPath+"\\testing\\test.{208D2C60-3AEA-1069-A2D7-08002B30309D}"))
	{
		Lock(CurrentPath+"\\testing\\test");
	}
	if (!PathFileExists(CurrentPath+"\\tranning\\tran.{208D2C60-3AEA-1069-A2D7-08002B30309D}"))
	{
		Lock(CurrentPath+"\\tranning\\tran");
	}
}
void CLaceborderMatchingDlg::ShowPicture()
{
	int i,j;
	char *path = (LPSTR)(LPCTSTR)CurrentPicturePath;
	IplImage* temp = cvLoadImage(path,CV_LOAD_IMAGE_COLOR);

	if (!temp)
	{
		return;
	}
	IplImage* image=cvCreateImage(cvSize(temp->width,temp->height),8,4);
	for(i =0; i <temp->height; i++)
		for(j =0; j <temp->width; j++)
		{
			SetPointColour(image,j,i,temp,j,i);
		}
	cvReleaseImage(&temp);

	cvReleaseImage(&LoadedPicture);
	LoadedPicture=cvCreateImage(cvSize((ShowPicture_r-ShowPicture_l),(ShowPicture_b-ShowPicture_t)),8,4);
	current_hpos=0;
	current_vpos=0;
	if (UpLoadPicureFlag==false)
	{
		GetImageDisplayData(LoadedPicture, image, 0, 0);
	}
	else if (UpLoadPicureFlag==true)//数据库导入单张图片
	{
		if (LoadedPicture->width>image->width&&LoadedPicture->height>image->height)
		{
			GetImageDisplayData(LoadedPicture, image, 0, 0);
		}
		else
		{
			SetPictureInAPart(LoadedPicture,0,0,LoadedPicture->width,LoadedPicture->height,CurrentPicturePath);
		}
	}
	
	cvReleaseImage(&image);

	ShowLoadedPicture();
	
	LoadPictureSuccessFlag=TRUE;


	Invalidate(FALSE);
}
void CLaceborderMatchingDlg::ShowLoadedPicture()
{
	if (!LoadedPicture)
	{
		return;
	}
	int i,j;
	for (i=ShowPicture_l;i<ShowPicture_r;i++)
	{
		for (j=ShowPicture_t;j<ShowPicture_b;j++)
		{
			//显示图片区域轮廓
// 			if (i==ShowPicture_l||i==ShowPicture_r||j==ShowPicture_t||j==ShowPicture_b)
// 			{
// 				SetPointColour(m_dspbuf,i,j,0,255,0);
// 			}
			SetPointColour(m_dspbuf,i,j,LoadedPicture,i-ShowPicture_l,j-ShowPicture_t);
		}
	}
}
void CLaceborderMatchingDlg::MovedPicture(int d_x,int d_y)
{
	int i,j;
	char *path = (LPSTR)(LPCTSTR)CurrentPicturePath;
	IplImage* temp = cvLoadImage(path,CV_LOAD_IMAGE_COLOR);

	if (!temp)
	{
		return;
	}
	IplImage* image=cvCreateImage(cvSize(temp->width,temp->height),8,4);
	for(i =0; i <temp->height; i++)
		for(j =0; j <temp->width; j++)
		{
			SetPointColour(image,j,i,temp,j,i);
		}
		cvReleaseImage(&temp);

		cvReleaseImage(&LoadedPicture);
		LoadedPicture=cvCreateImage(cvSize((ShowPicture_r-ShowPicture_l),(ShowPicture_b-ShowPicture_t)),8,4);
		if (image->width>LoadedPicture->width)
		{
			current_hpos=current_hpos-d_x;
			if (current_hpos>(image->width-LoadedPicture->width))
			{
				current_hpos=(image->width-LoadedPicture->width);
			}
		}
		if (image->height>LoadedPicture->height)
		{
			current_vpos=current_vpos-d_y;
			if (current_vpos>(image->height-LoadedPicture->height))
			{
				current_vpos=(image->height-LoadedPicture->height);
			}
		}
		if (current_hpos<0)
		{
			current_hpos=0;
		}
		
		if (current_vpos<0)
		{
			current_vpos=0;
		}
		
		GetImageDisplayData(LoadedPicture, image, current_hpos, current_vpos);
		cvReleaseImage(&image);

		LoadPictureSuccessFlag=TRUE;


		Invalidate(FALSE);
}
void CLaceborderMatchingDlg::ShowPictureCutLine()//显示裁剪线
{
	int i,j;
	int l,r,t,b;
	l=CutLeftTopPoint.x;
	t=CutLeftTopPoint.y;
	r=CutRightBottomPoint.x;
	b=CutRightBottomPoint.y;

	for (i=l;i<=r;i++)
	{
		for (j=t;j<=b;j++)
		{
			if (i==l||i==r||j==t||j==b)
			{
				SetPointColour(m_dspbuf,i,j,0,255,0);
			}
		}
	}

	leftTop=CutLeftTopPoint;
	RightButom=CutRightBottomPoint;
}
void CLaceborderMatchingDlg::OnLoging()
{
	CString Account,p;
	GetDlgItemText(IDC_EDIT_ACCOUNT, Account);
	
	GetDlgItemText(IDC_EDIT_PASSWORD, p);
	
	BOOL f=verifyAccount(Account,p);
	if (f==TRUE)
	{
		LogSuccessfulBackGround();
		////////////////////////////////////////////////////菜单选项栏按钮
		InitMenu();
		ClickMenu(2);//默认选择 检索

		GetImageDisplayData(m_dspbuf, m_imageBack, 0, 0);
	}
	else
	{
		AfxMessageBox("账号和密码不匹配！");
		return;
	}

	((CTreeCtrl *)GetDlgItem(IDC_TREE_DATABASE))->ShowWindow(SW_SHOW);
	

	CWnd* pWnd = GetDlgItem(IDC_EDIT_ACCOUNT);
	CWnd* pWnd1 = GetDlgItem(IDC_EDIT_PASSWORD);

	pWnd->ShowWindow(SW_HIDE);//设置属性不可见
	pWnd1->ShowWindow(SW_HIDE);

	Invalidate(FALSE);

	LogFlag=TRUE;//解决 Invalidate(FALSE);调用 OnTvnSelchangedTreeDatabase(NMHDR *pNMHDR, LRESULT *pResult)自动选中根节点的问题
}

BOOL CLaceborderMatchingDlg::verifyAccount(CString Account,CString password)//验证账号
{
	int n=(int)accounts.size();
	int i;
	for (i=0;i<n;i++)
	{
		if (Account==accounts[i].account&&password==accounts[i].password)
		{
			CurrentAccount=i;
			return TRUE;
		}
	}

	return FALSE;

}
BOOL CLaceborderMatchingDlg::verifyDate()//验证统计信息的输入格式
{
	CString AnalysisTime;
	SYSTEMTIME tmp_time;

	((CDateTimeCtrl*)GetDlgItem(IDC_DATETIMEPICKER_FROM))->GetTime(&tmp_time);
	AnalysisTime.Format("%4d%02d%02d",tmp_time.wYear,tmp_time.wMonth,tmp_time.wDay);
	date[0]=atoi(AnalysisTime);

	((CDateTimeCtrl*)GetDlgItem(IDC_DATETIMEPICKER_TO))->GetTime(&tmp_time);
	AnalysisTime.Format("%4d%02d%02d",tmp_time.wYear,tmp_time.wMonth,tmp_time.wDay);
	date[1]=atoi(AnalysisTime);

	if (date[1]<date[0])
	{
		return FALSE;
	}



// 	CString Date[2];
// 	GetDlgItemText(IDC_EDIT_DAT, Date[0]);
// 	GetDlgItemText(IDC_EDIT_DATE, Date[1]);
// 
// 	int i,j;
// 	for (i=0;i<2;i++)
// 	{
// 		int n=Date[i].GetLength();
// 		if (n!=10)//标准 日期格式 2014-05-21  10个字符；
// 		{
// 			return FALSE;
// 		}
// 		char s;
// 
// 		for (j=0;j<10;j++)
// 		{
// 			s=Date[i].GetAt(j);
// 			if (j==4||j==7)
// 			{
// 				if (s!='-')
// 				{
// 					return FALSE;
// 				}
// 			}
// 			else if (!(s>='0'&&s<='9'))
// 			{
// 				return FALSE;
// 			}
// 		}
// 	}
// 
// 	CString tmp;
// 	tmp=Date[0].Left(4);
// 	Date[0]=Date[0].Right(5);
// 	tmp+=Date[0].Left(2);
// 	tmp+=Date[0].Right(2);
// 	date[0]=atoi(tmp);
// 
// 	tmp=Date[1].Left(4);
// 	Date[1]=Date[1].Right(5);
// 	tmp+=Date[1].Left(2);
// 	tmp+=Date[1].Right(2);
// 	date[1]=atoi(tmp);

// 	tmp=Date[0].Left(4);
// 	date[0]=atoi(tmp);
// 	tmp=Date[0].Right(5);
// 	tmp=tmp.Left(2);
// 	date[1]=atoi(tmp);
// 	tmp=Date[0].Right(2);
// 	date[2]=atoi(tmp);
// 
// 	tmp=Date[1].Left(4);
// 	date[3]=atoi(tmp);
// 	tmp=Date[1].Right(5);
// 	tmp=tmp.Left(2);
// 	date[4]=atoi(tmp);
// 	tmp=Date[1].Right(2);
// 	date[5]=atoi(tmp);


	return TRUE;

}

void CLaceborderMatchingDlg::InitEachSideOfMenu(int MenuNum)//初始化菜单按钮
{
	CString pathname;
	int x,y,w,h;
	int right_x;
	int i,j;

	if (MenuNum==0)
	{
		pathname=MenuPath[8];//MenuNum=0，表示灰色。其他表示蓝色，选中下的按钮
		x=400;
		w=21;
		right_x=m_Width-x;
	}
	else
	{
		if (MenuNum==4)
		{
			pathname=MenuPath[10];
		}
		else
		{
			pathname=MenuPath[9];
		}
		
		x=Menu_x[MenuNum-1]-MenuDistance-21;
		w=18;
		right_x=Menu_x[MenuNum-1]+MenuLenth[MenuNum-1]+MenuDistance+21;
	}
	y=Menu_y;
	h=MenuHeight;

	SetBackPic(pathname,x,y,w,h,0,0,false);
	SetBackPic(pathname,right_x,y,w,h,0,0,true);
// 	for (i=(right_x-w);i<right_x;i++)
// 	{
// 		for (j=49;j<83;j++)
// 		{
// 			SetPointColour(m_imageBack,i,j,m_imageBack,(right_x-i+x),j);
// 		}
// 	}

	if (MenuNum==1)
	{
		SetBackPic(MenuPath[10],x,y,w,h,0,0,false);
	}
	if (MenuNum==4)
	{
		SetBackPic(MenuPath[9],x,y,w,h,0,0,false);
	}

	for (i=x+w;i<(right_x-w+1);i++)
	{
		for (j=Menu_y;j<BlueLine;j++)
		{
			SetPointColour(m_imageBack,i,j,m_imageBack,x+w-1,j);
		}
	}
}
void CLaceborderMatchingDlg::InitMenu()//初始化菜单按钮
{
	InitEachSideOfMenu(0);//初始化菜单两边
	int i=0;
	for (i=0;i<4;i++)
	{
		// 		pathname=MenuPath[i];
		// 		x=Menu_x[i];
		// 		y=Menu_y;
		// 		w=MenuLenth[i];
		// 		h=MenuHeight;
		//		SetBackPic(pathname,x,y,w,h,0,0);
		SetBackPic(MenuPath[i],Menu_x[i],Menu_y,MenuLenth[i],MenuHeight,0,0,false);
//		SetBackPic(MenuPath[i+4],Menu_x[i],Menu_y,MenuLenth[i],MenuHeight,0,0);
	}
//	ClickMenu(4);
}
void CLaceborderMatchingDlg::ClickMenu(int num)//初始化菜单按钮
{
	((CTreeCtrl *)GetDlgItem(IDC_TREE_DATABASE))->ShowWindow(SW_SHOW);
	CWnd* pWnd= GetDlgItem(IDC_DATETIMEPICKER_FROM);//前一个日期
	pWnd->ShowWindow(SW_HIDE);

	pWnd= GetDlgItem(IDC_DATETIMEPICKER_TO);//新密码
	pWnd->ShowWindow(SW_HIDE);

	pWnd= GetDlgItem(IDC_STATIC_TIME);//新密码
	pWnd->ShowWindow(SW_HIDE);
	pWnd= GetDlgItem(IDC_STATIC_TO);//新密码
	pWnd->ShowWindow(SW_HIDE);
	pWnd= GetDlgItem(IDC_STATIC_PRO);//进度显示
	pWnd->ShowWindow(SW_HIDE);

	pWnd= GetDlgItem(IDC_COMBO_GO);
	pWnd->ShowWindow(SW_HIDE);
	((CComboBox* )GetDlgItem(IDC_COMBO_GO))->ResetContent();
	((CComboBox* )GetDlgItem(IDC_COMBO_GO))->SetWindowText("");

	if (num>0&&num<5)
	{
		CurrentMenu=MenuName[num-1];//标记当前菜单选项
	}
	InitEachSideOfMenu(num);//初始化菜单两边
	SetBackPic(MenuPath[num+3],Menu_x[num-1],Menu_y,MenuLenth[num-1],MenuHeight,0,0,false);
	InitMenuBackground(num);
}
void CLaceborderMatchingDlg::InitMenuBackground(int num)//初始化背景布局
{
	if (ShowingMatchingPicturesFlag==true)//从匹配结果界面跳转到其他
	{
		DataBaseSubWindow_t=k_t;
		DataBaseSubWindow_b=k_b;
		DataBaseSubWindow_r=k_r;
		DataBaseSubWindow_l=k_l;
		GetDataBasePictureSize();
	}		
	ShowingMatchingPicturesFlag=false;

	if (num==1)//数据库管理
	{
		ManageDataBaseBackground();
	}
	else if (num==2)//图像匹配
	{
		MatchingBackground();
	}
	else if (num==3)//统计信息
	{
		SubMenuLine(DataBaseSubWindow_l,DataBaseSubWindow_r,DataBaseSubWindow_t,DataBaseSubWindow_b);
		CString path;
		path=CurrentPath+"\\resource\\sure.jpg";
		SetBackPic(path,1000,BlueLine+18,93,25,0,0,false);//标题栏左边

		CWnd* pWnd= GetDlgItem(IDC_DATETIMEPICKER_FROM);//前一个日期
		pWnd->MoveWindow(450,BlueLine+21, 207,20,TRUE);
		pWnd->SetWindowText("");
		pWnd->ShowWindow(SW_SHOWNA);

// 		CEdit *m_Edit=(CEdit *)GetDlgItem(IDC_DATETIMEPICKER_FROM);
// 		m_Edit->SetFont(&font,FALSE);

		pWnd= GetDlgItem(IDC_DATETIMEPICKER_TO);//新密码
		pWnd->SetWindowText("");
		pWnd->MoveWindow(720,BlueLine+21, 207,20,TRUE);
		pWnd->ShowWindow(SW_SHOWNA);
// 		m_Edit=(CEdit *)GetDlgItem(IDC_DATETIMEPICKER_TO);
// 		m_Edit->SetFont(&font,FALSE);

		CRect rt;  
		pWnd= GetDlgItem(IDC_STATIC_TIME);//新密码
		pWnd->GetWindowRect(&rt);  
		pWnd->MoveWindow(350, BlueLine+24,rt.Width(),rt.Height(),TRUE);  
		pWnd->ShowWindow(SW_SHOWNA);
		pWnd= GetDlgItem(IDC_STATIC_TO);//新密码
		pWnd->GetWindowRect(&rt);  
		pWnd->MoveWindow(678, BlueLine+22,rt.Width(),rt.Height(),TRUE); 
		pWnd->ShowWindow(SW_SHOWNA);
//		MatchingBackground();


		showPageFlag=false;
		ClearCStringVector(CurrentDataBasePicturePath);
		CurrentSubDataBasePageIndex=0;
		CurrentSubDataBasePageNum=0;
	}
	else if (num==4)//账户管理
	{
		ManageAccountBackground();
	}
}
void CLaceborderMatchingDlg::ManageDataBaseBackground()//初始化数据库管理界面背景布局
{
	SubMenuLine(DataBaseSubWindow_l,DataBaseSubWindow_r,DataBaseSubWindow_t,DataBaseSubWindow_b);
	ManageDataBaseClick(manageDataBaseNum);
}
void CLaceborderMatchingDlg::SubMenuLine(int l,int r,int t,int b)//数据库管理和账户管理的子菜单框边界
{
	CString path;
	path=CurrentPath+"\\resource\\manageaccount_top.jpg";
	SetBackPic(path,l,t,10,31,0,0,false);//标题栏左边
	SetBackPic(path,r,t,10,31,0,0,true);//标题栏右边
	path=CurrentPath+"\\resource\\manageaccount_Bottom.jpg";
	SetBackPic(path,l,b-16,16,16,0,0,false);//左下角
	SetBackPic(path,r,b-16,16,16,0,0,true);//右下角

	CopyInHorizontal(m_imageBack,(l+10),(r-9),t,(t+31),(l+9));
	CopyInHorizontal(m_imageBack,(l+16),(r-15),(b-16),b,(l+15));
	CopyInVertical(m_imageBack,l,(l+15),(t+31),(b-16),(b-15));
	CopyInVertical(m_imageBack,(r-16),r,(t+31),(b-16),(b-15));
}
void CLaceborderMatchingDlg::MatchingBackground()//初始化图像匹配界面背景布局
{
	CString path;
//	int i,j;

	SetBackPic(LoadPicture_path,LoadPicture_x,LoadPicture_y,LoadPicture_w,LoadPicture_h,0,0,false);
	SetBackPic(search_path,search_x,search_y,search_w,search_h,8,2,false);



	path=CurrentPath+"\\resource\\testSet.jpg";
	SetBackPic(path,410,(BlueLine+15),81,23,0,0,false);
	path=CurrentPath+"\\resource\\testSet_left.jpg";
	SetBackPic(path,410,search_y+search_h-17,19,17,0,0,false);

	CopyImageInHorizontalByMirror(m_imageBack,410,425,(BlueLine+15),(BlueLine+15+24),(m_Width-20-15));//镜面复制
	CopyImageInHorizontalByMirror(m_imageBack,410,425,(search_y+search_h-17),(search_y+search_h),(m_Width-20-15));//镜面复制
// 	for (i=(m_Width-20-15);i<=(m_Width-20);i++)
// 	{
// 		for (j=170;j<193;j++)
// 		{
// 			SetPointColour(m_imageBack,i,j,m_imageBack,410+m_Width-20-i,j);
// 		}
// 		for (j=228;j<245;j++)
// 		{
// 			SetPointColour(m_imageBack,i,j,m_imageBack,410+m_Width-20-i,j);
// 		}
// 	}
	CopyInHorizontal(m_imageBack,489,(m_Width-20-14),(BlueLine+15),(BlueLine+15+24),490);
	CopyInHorizontal(m_imageBack,429,(m_Width-20-14),(search_y+search_h-17),(search_y+search_h),428);
// 	for (i=489;i<(m_Width-20-14);i++)
// 	{
// 		for (j=170;j<193;j++)
// 		{
// 			SetPointColour(m_imageBack,i,j,m_imageBack,490,j);
// 		}
// 	}
// 	for (i=429;i<(m_Width-20-15);i++)
// 	{
// 		for (j=228;j<245;j++)
// 		{
// 			SetPointColour(m_imageBack,i,j,m_imageBack,428,j);
// 		}
// 	}
	
	CopyInVertical(m_imageBack,410,415,(BlueLine+15+23),(search_y+search_h-17),(search_y+search_h-16));
	CopyInVertical(m_imageBack,(m_Width-20-14),(m_Width-20),(BlueLine+15+23),(search_y+search_h-17),(search_y+search_h-16));
// 	for (j=193;j<228;j++)
// 	{
// 		for (i=410;i<415;i++)
// 		{
// 			SetPointColour(m_imageBack,i,j,m_imageBack,i,228);
// 		}
// 		for (i=(m_Width-20-5);i<(m_Width-19);i++)
// 		{
// 			SetPointColour(m_imageBack,i,j,m_imageBack,i,228);
// 		}
// 	}

	SearchByTexture();
	MovePic();

	path=CurrentPath+"\\resource\\SureCut.jpg";
	SetBackPic(path,SureCut_x,SureCut_y,SureCut_w,SureCut_h,0,0,false);
}
void CLaceborderMatchingDlg::ManageAccountBackground()//初始化账户管理界面背景布局
{
	int l,r,t,b;
	l=285;//左边缘线
	r=m_Width-15;//右边缘线
	t=BlueLine+15;//上边沿线
	b=BlueLine+366;//下边缘线
	SubMenuLine(l,r,t,b);

// 	CString path;
// 	path=CurrentPath+"\\resource\\manageaccount_top.jpg";
// 	SetBackPic(path,290,BlueLine+15,10,31,0,0,false);
// 	SetBackPic(path,m_Width-20,BlueLine+15,10,31,0,0,true);
// 	path=CurrentPath+"\\resource\\manageaccount_Bottom.jpg";
// 	SetBackPic(path,290,BlueLine+350,16,16,0,0,false);
// 	SetBackPic(path,m_Width-20,BlueLine+350,16,16,0,0,true);
// 
// 	CopyInHorizontal(m_imageBack,300,(m_Width-20-9),(BlueLine+15),(BlueLine+15+31),299);
// 	CopyInHorizontal(m_imageBack,306,(m_Width-20-15),(BlueLine+350),(BlueLine+366),305);
// 	CopyInVertical(m_imageBack,290,305,(BlueLine+15+31),(BlueLine+350),(BlueLine+351));
// 	CopyInVertical(m_imageBack,(m_Width-20-16),(m_Width-20),(BlueLine+15+31),(BlueLine+350),(BlueLine+351));

	CString path=CurrentPath+"\\resource\\sure.jpg";
	SetBackPic(path,ManageAccountSure_x,ManageAccountSure_y,ManageAccountSure_w,ManageAccountSure_h,0,0,false);//确定修改

	ManageAccountClick(1);

}
void CLaceborderMatchingDlg::ManageDataBaseClick(int num)//数据库管理 点击3个按钮 1,2,3
{
	CWnd*pWnd= GetDlgItem(IDC_STATIC_PRO);//进度显示
	pWnd->ShowWindow(SW_HIDE);

	CString path[6];
	CString path_;
	int i;

	manageDataBaseNum=num;

	path[0]=CurrentPath+"\\resource\\databaseView01.jpg";
	path[1]=CurrentPath+"\\resource\\databaseView02.jpg";
	path[2]=CurrentPath+"\\resource\\pictureLoading01.jpg";
	path[3]=CurrentPath+"\\resource\\pictureLoading02.jpg";
	path[4]=CurrentPath+"\\resource\\databaseCreate01.jpg";
	path[5]=CurrentPath+"\\resource\\databaseCreate02.jpg";

	int x[3]={330,430,530};
	int y=BlueLine+16;

	int lenth[3]={93,93,93};

	int push_x[3]={0,0,0};//3个按钮按下去的偏移量

	for (i=0;i<3;i++)
	{
		if ((i+1)==num)
		{
			SetBackPic(path[2*i+1],x[i],y,lenth[i],30,0,1,false);
		}
		else 
		{
			SetBackPic(path[2*i],x[i],y,lenth[i],30,0,0,false);
		}
	}

	
	if (num==1)//数据库浏览
	{
//		ShowCurrentDataBasePicture("F:\\xiaobian_images");
		InitPictureColour(m_imageBack,DataBasePicture_l,DataBasePicture_r+1,DataBasePicture_t,DataBasePicture_b+1,240,240,240);
		ShowCurrentDataBasePicture(CurrentSelectedDataBasePath);
		DrawLineWindows();
	}
	else if (num==2)//图片导入
	{
		ClearCStringVector(CurrentDataBasePicturePath);//解决误点翻页按钮

		showPageFlag=false;
		InitPictureColour(m_imageBack,DataBasePicture_l,DataBasePicture_r+1,DataBasePicture_t,m_Height-33-11,240,240,240);
		path_=CurrentPath+"\\resource\\UpLoadSinglePicture.jpg";
		SetBackPic(path_,UpLoadSinglePicture_x,UpLoadSinglePicture_y,UpLoadSinglePicture_w,UpLoadSinglePicture_h,0,0,false);
		path_=CurrentPath+"\\resource\\UpLoadAllPicture.jpg";
		SetBackPic(path_,UpLoadAllPicture_x,UpLoadAllPicture_y,UpLoadAllPicture_w,UpLoadAllPicture_h,0,0,false);
	}
	else if (num==3)//创建数据库
	{
		ClearCStringVector(CurrentDataBasePicturePath);//解决误点翻页按钮

		InitPictureColour(m_imageBack,DataBasePicture_l,DataBasePicture_r+1,DataBasePicture_t,m_Height-33-11,240,240,240);

		path_=CurrentPath+"\\resource\\DataBase_Update.jpg";//原先的创建数据库
		SetBackPic(path_,UpLoadSinglePicture_x,UpLoadSinglePicture_y,UpLoadSinglePicture_w,UpLoadSinglePicture_h,0,0,false);
// 		path_=CurrentPath+"\\resource\\DataBase_Update.jpg";
// 		SetBackPic(path_,UpLoadAllPicture_x,UpLoadAllPicture_y,UpLoadAllPicture_w,UpLoadAllPicture_h,0,0,false);

//		CurrentDataBasePictureFeature();//创建数据库特征

		

	}
}
void CLaceborderMatchingDlg::CurrentDataBasePictureFeature()//创建数据库特征
{
	CreatingDataBaseFlag=TRUE;//开始创建

	CWnd*pWnd= GetDlgItem(IDC_STATIC_PRO);//进度信息  显示进度百分比数据


	int rate_num=0;//用来计算进度百分比的变量

	int i,j;
	vector<CString>AllPicturePath;//存储当前选中的数据库目录以及其子目录下的所有 jpg格式图片的全路径
	vector<CString>AllDirPath;//存储当前选中的目录，以及其所有的子目录全路径
	vector<int>DirPicturenum;//每个目录下图片个数；
	int PictureNum,DirNum;
	int DBnum;

	char *file_temp_name;


	FILE *read_feat;

	///////////////////////////////////////////////////////////////////  创建所有图片 特征文件  
	GetAllPicturePath(AllPicturePath);//获取当前数据库选择的目录及子目录下的所有jpg图片文件的全路径
	PictureNum=(int)AllPicturePath.size();//保存图片总数

	if (PictureNum<=0)
	{
		AfxMessageBox("         当前选择的目录没有图片！！         ");
		CreatingDataBaseFlag=FALSE;//标记 创建过程结束
		return;
	}
	GetAllDirPath(AllDirPath);//获取当前数据库所有目录全路径
	DirNum=(int)AllDirPath.size();//保存所有目录个数

	for (i=0;i<DirNum;i++)//循环统计每个目录下jpg文件数目
	{
		vector<CString>tmp;
		GetAllFile(AllDirPath[i],"jpg",tmp,true);//获取所有 jpg文件
		DirPicturenum.push_back((int)tmp.size());//保存每个目录下的jpg文件数目
		ClearCStringVector(tmp);//清空容器
	}


	/////////////////////////////////////////////////////////////////////////////////
	int l=UpLoadAllPicture_x+UpLoadAllPicture_w+5;//进度条显示区域 左边界
	int r=m_Width-30;//进度条显示区域 右边界
	int t=UpLoadAllPicture_y;//进度条显示区域 上边界
	int b=UpLoadAllPicture_y+UpLoadAllPicture_h-1;//进度条显示区域 下边界

	InitPictureColour(m_dspbuf,l,r,t,b,240,240,240);//进度条显示区域 初始化，去除上一次进度信息的影响

	RateOfAdvance(m_dspbuf,l,r,t,b);//显示进度显示区域矩形框 边框颜色为 黑色
	CRect tmp;
	tmp.left=l-2;
	tmp.right=r+2;
	tmp.top=t-2;
	tmp.bottom=b+2;
	InvalidateRect(tmp,TRUE);
	OnPaint();//刷新进度显示区域界面

	pWnd->MoveWindow(l+10,t+5, 207,15,TRUE);//移动进度显示 百分比数据控件 到进度显示区域
	pWnd->SetWindowText("");//清空百分比数据
	pWnd->ShowWindow(SW_SHOWNA);//显示百分比数据static控件


	///////////////////////////////   以下未注释的相关变量与代码 与 单独的 匹配算法源代码 基本一致/////////////////////////////////////////////////  


	int DBnum_updated=PictureNum;
	queue<int> images_leftout;
	int count_leftout=0;

//	int *number_feat=new int[PictureNum];
	int all_feat_num=0;

	CString eignfilePath=CurrentPath+"\\eignfile.txt";
//	CreateNewDir(eignfilePath);	
	char *fan_hsf=(LPSTR)(LPCTSTR)eignfilePath;
	///////////////////////////////////////////////
	//		PCA ANALYSIS
//	char fan_hsf[1024]="F:\\eignfile.txt";
	PCA sip=read_pca(fan_hsf);



//	cout<<sip.eigenvectors.rows<<" "<<sip.eigenvectors.cols<<endl;
	///////////////////////////////////////////////////////////

	for (i=0;i<PictureNum;i++)//根据所有图片数目 循环对每个图片生成 特征文件
	{
		if (!PathFileExists(AllPicturePath[i]+"_Features.txt"))//如果该图片的特征文件已经存在 则跳过
		{
			//调用生成特征文件
			Extract_HS_features_Single(AllPicturePath[i],i,DBnum_updated,images_leftout, count_leftout,sip/*,number_feat*/);
		}



		rate_num++;//每处理一张图片 百分比变量增 1

		float rate=(float)4*PictureNum; // 整个创建过程 分为 4部分： 创建特征文件， 创建字典文件，生成bow文件，生成 颜色特征文件
		rate=((float)rate_num)/rate;//计算百分比
		RateOfAdvance(m_dspbuf,l,r,t,b,rate);//显示进度 
		InvalidateRect(tmp,FALSE);
		OnPaint();//刷新进度画面
		CString tmp_pro;
		tmp_pro.Format("%d",(int)(rate*100));
		tmp_pro="完成 "+tmp_pro+"  %";
		pWnd->SetWindowText(tmp_pro);//显示百分比进度

		//防止循环中界面出现假死现象
		MSG msg;
		while(::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			::TranslateMessage(&msg);
			::DispatchMessage(&msg);
		}

	}
	while(!images_leftout.empty())
	{
		int erase_ind = images_leftout.front();
		//			AllImagefilename.erase(AllImagefilename.begin()+erase_ind);
		AllPicturePath.erase(AllPicturePath.begin()+erase_ind);
		images_leftout.pop();
	}



	////////////////////////////////////////////////////////////////////  创建当前目录与其子目录的 字典文件
	ClearCStringVector(AllPicturePath);//清空容器


	int m_it,Dim,WordsNumber;
	float *Words=NULL;

	////////////////////////////////////////////////////
	// 	AllPicturePath[0]="F:\\HS features5";
	// 	k=1;
	////////////////////////////////////////////////
	char *bowfeat_filename;
	char *bowtree_num_file;
	char *hs_num_one_img_file;
	char *colorfeat_file;


	CString tmp_name,tmp_name2,colorfeat;
	int i7;
	char *WordsFile;
	CString s1;
	vector<CString> AllFeatureDATAfilename;//保存当前目录下的所有图片特征文件的全路径
	HSBowFeature CurBowFeat;


	int tmp_num=0;	
	int FeatureCounts;//记录所有特征数

	for (i7=0;i7<DirNum;i7++)
	{
		//防止循环中出现假死
		MSG msg;
		while(::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			::TranslateMessage(&msg);
			::DispatchMessage(&msg);
		}

		//调用生成特征文件

		FeatureCounts=0;//记录所有特征数
		/*CString */s1= AllDirPath[i7]+"\\words_xbfile_kane_x.txt";//字典文件
		/*char **/WordsFile = (LPSTR)(LPCTSTR)s1;
		tmp_name2=AllDirPath[i7]+"\\hs_num_one_img_file.txt";//
		hs_num_one_img_file=(LPSTR)(LPCTSTR)tmp_name2;
		colorfeat=AllDirPath[i7]+"\\colorfeat_file.txt";
		colorfeat_file=(LPSTR)(LPCTSTR)colorfeat;

		ClearCStringVector(AllFeatureDATAfilename);//清空容器
		//		GetCurrentDirAllPicturePath(AllDirPath[i7],AllFeatureDATAfilename,FeatureCounts);//读取图片的功能已不重要，关键是为了读取FeatureCounts

		GetAllFile(AllDirPath[i7],"jpg",AllFeatureDATAfilename,true);//每次清空容器  获取当前目录下的所有图片文件
		//////////////////////////////////////获取字典/////////////////////////////////////////////////////////////////////////
		vector<MY_DATE>dateData;//日期文件信息

		CString FeatureCountsPath=AllDirPath[i7]+"\\FeatureCounts.Date";//保存有每个图片特征数的文件
		char *filePath;
		filePath=FeatureCountsPath.GetBuffer(FeatureCountsPath.GetLength());

		vector<int> FeaturesNum;
		ClearCStringVector(AllFeatureDATAfilename);
		FeatureCounts=0;

		if (PathFileExists(filePath))
		{
			ReadDateFile(dateData,filePath);//将 FeatureCounts.Date文件保存的每张图片的全路径和特征数 保存在 dateData结构体中

			for (int j=0;j<(int)dateData.size();j++)
			{
				FeatureCounts+=dateData[j].Sum;//记录当前目录下所有图片特征总和
				int tmp_n=dateData[j].Sum;
				CString path1=dateData[j].Name;
				FeaturesNum.push_back(tmp_n);//保存每张图片的特征数目 顺序与AllFeatureDATAfilename的图片一致
				AllFeatureDATAfilename.push_back(path1);//只记录有特征文件的图片路径，新增的不会被加入，已经删除的图片必须把FeatureCounts.Date记录删除
			}
		}

		if (!dateData.empty())
		{
			dateData.clear();//清空数据
			vector<MY_DATE>().swap(dateData);
		}
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////
		int i;
		int k=AllFeatureDATAfilename.size();
		DBnum=(int)AllFeatureDATAfilename.size();


		FILE *out_bow;


		for (i=0;i<k;i++)
		{
			AllFeatureDATAfilename[i]+="_Features.txt";//由文件名获取对应的特征文件
		}
		//////////////////////////////////////////////////////////////////////////////////////////
		// 		all_feat_num=0;
		// 		for (i=0;i<k;i++)
		// 		{
		// 			all_feat_num+=number_feat[tmp_num+i];
		// 			
		// 		}
		// 		tmp_num+=DirPicturenum[i7];
		//	cout<<all_feat_num<<endl;
		/////////////////////////////////////////////获取所有特征值////////
		all_feat_num=FeatureCounts;//当前目录下面所有图片的特征总和
		////////////////////////////////////////////////////

		if ((!PathFileExists(AllDirPath[i7]+"\\AddAndDelete"))&&PathFileExists(AllDirPath[i7]+"\\bowfeat_xbfile_kane_x.txt"))//bow文件存在说明生成过bow文件，AddAndDelete文件不存在说明从上次创建到这次没有增减图片
		{
			rate_num+=DBnum;//bow进度
			rate_num+=DBnum;//颜色进度
			float rate=(float)3*PictureNum;
			rate=((float)(rate_num))/rate;
			RateOfAdvance(m_dspbuf,l,r,t,b,rate);
			InvalidateRect(tmp,FALSE);
			OnPaint();

			CString tmp_pro;
			tmp_pro.Format("%d",(int)(rate*100));
			tmp_pro="完成 "+tmp_pro+"  %";
			pWnd->SetWindowText(tmp_pro);



			continue;
		}//不满足条件 则说明 是第一次创建或者数据库有新增图片

		if (DBnum<1)
		{
			// 			float rate=(float)(PictureNum+DirNum);
			// 			rate=((float)(i7+1+PictureNum))/rate;
			// 			RateOfAdvance(m_dspbuf,l,r,t,b,rate);
			// 			InvalidateRect(tmp,FALSE);
			// 			OnPaint();
			continue;
		}

// 		ofstream fnum;
// 		fnum.open(hs_num_one_img_file);
// 		if (!fnum)
// 		{
// 			MessageBox("Can't open feat num file in proc_func ling 2621.\n");
// 			//			cout<<"Can't open feat num file in proc_func ling 243.\n";
// 			//			exit(-1);
// 			CreatingDataBaseFlag=FALSE;
// 			return;
// 		}
// 
// 		fnum<<all_feat_num;
// 
// 		fnum.close();

		/////////////////////////////////////////////////////////////////////////////////
		CreateWords(WordsFile,AllFeatureDATAfilename,FeaturesNum,rate_num,PictureNum);//创建字典文件


		VlGMM *gmm=Read_Into_GMM(WordsFile,Dim,WordsNumber);
		int FV_DIM=2 * Dim * WordsNumber;

//		Words = ReadintoWords(WordsFile,Dim,WordsNumber);


		tmp_name= AllDirPath[i7]+"\\bowfeat_xbfile_kane_x.txt";
		bowfeat_filename = (LPSTR)(LPCTSTR)tmp_name;
		// 		tmp_name2= AllDirPath[i7]+"\\bowfeatnum_xbfile_kane_x.txt";
		// 		bowtree_num_file = (LPSTR)(LPCTSTR)tmp_name2;

		DBnum=(int)AllFeatureDATAfilename.size();

		out_bow=fopen(bowfeat_filename,"w");

		for (m_it=0;m_it<DBnum;m_it++)
		{			
			file_temp_name = (LPSTR)(LPCTSTR)AllFeatureDATAfilename[m_it];

			read_feat=fopen(file_temp_name,"r");

			if(!read_feat)
				continue;  // no feature file exist for this one.
			int index;
			fscanf(read_feat,"%d",&index);
			float *local_feat=new float[FeaturesNum[m_it]*HS_DIM];
//			Mat CurHSfeat(FeaturesNum[m_it],HS_DIM,CV_32FC1);
			int s=0;
			while(!feof(read_feat))
			{
				for (j=0;j<HS_DIM;j++)  
					fscanf(read_feat,"%f",&local_feat[s*HS_DIM+j]);
				s++;			
			}

			HSBowFeature CurBowFeat;
			CurBowFeat.index=/*index*/m_it;
			CurBowFeat.HSBowFVSize=FV_DIM;
			CurBowFeat.HSBowFV=Fisher_Vector_Exp(local_feat,gmm,HS_DIM,FeaturesNum[m_it]);
			fprintf(out_bow,"%d ",CurBowFeat.index);
			fprintf(out_bow,"%d ",CurBowFeat.HSBowFVSize);
			for (j=0;j<CurBowFeat.HSBowFVSize;j++)
				fprintf(out_bow,"%f ",CurBowFeat.HSBowFV[j]);
			fprintf(out_bow,"\n");	

			delete[] CurBowFeat.HSBowFV;
			delete[] local_feat;
			fclose(read_feat);
// 			Bow(CurHSfeat,Dim, WordsNumber, Words,CurBowFeat.HSBowFV);
// 			AllBowFeat.push_back(CurBowFeat);
// 			fclose(read_feat);
			float rate=(float)4*PictureNum;//这一段跟新进度信息
			rate=((float)(rate_num))/rate;
			RateOfAdvance(m_dspbuf,l,r,t,b,rate);
			InvalidateRect(tmp,FALSE);
			OnPaint();

			CString tmp_pro;
			tmp_pro.Format("%d",(int)(rate*100));
			tmp_pro="完成 "+tmp_pro+"  %";
			pWnd->SetWindowText(tmp_pro);

			rate_num++;

			//防止循环中假死
			MSG msg;
			while(::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
			{
				::TranslateMessage(&msg);
				::DispatchMessage(&msg);
			}


		}
		free(Words);

		fclose(out_bow);





		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////创建颜色特征/////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		if (DBnum>0)
		{
			vector<COLOR_FEAT> AllcolorFeat;
			for (int m_nFrameInd = 0; m_nFrameInd < DBnum; m_nFrameInd ++)
			{
				COLOR_FEAT cur_colfeat;
				// 			char input_img_name[128];
				// 			strcpy(input_img_name,AllImagefilename[m_nFrameInd].c_str());
				// 			cout<<input_img_name<<endl;
				CString path=AllFeatureDATAfilename[m_nFrameInd].Left(AllFeatureDATAfilename[m_nFrameInd].GetLength()-13);
				IplImage *img = cvLoadImage(path/*AllFeatureDATAfilename[m_nFrameInd]*/);
				Extract_Color_Hist(img,cur_colfeat.hist);
				cur_colfeat.index=m_nFrameInd;
				AllcolorFeat.push_back(cur_colfeat);
				cvReleaseImage(&img);



				//跟新进度信息
				float rate=(float)4*PictureNum;
				rate=((float)(rate_num))/rate;
				RateOfAdvance(m_dspbuf,l,r,t,b,rate);
				InvalidateRect(tmp,FALSE);
				OnPaint();

				CString tmp_pro;
				tmp_pro.Format("%d",(int)(rate*100));
				tmp_pro="完成 "+tmp_pro+"  %";
				pWnd->SetWindowText(tmp_pro);

				rate_num++;


				MSG msg;
				while(::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
				{
					::TranslateMessage(&msg);
					::DispatchMessage(&msg);
				}
			}

			out_bow=fopen(colorfeat_file,"w");
			for(i=0;i<AllcolorFeat.size()-1;i++)
			{
				fprintf(out_bow,"%d ",AllcolorFeat[i].index);
				fprintf(out_bow,"%d ",AllcolorFeat[i].hist.cols);
				for (j=0;j<AllcolorFeat[i].hist.cols;j++)
					fprintf(out_bow,"%f ",AllcolorFeat[i].hist.at<float>(j));
				fprintf(out_bow,"\n");	
			}
			fprintf(out_bow,"%d ",AllcolorFeat[i].index);
			fprintf(out_bow,"%d ",AllcolorFeat[i].hist.cols);
			for (j=0;j<AllcolorFeat[i].hist.cols-1;j++)
				fprintf(out_bow,"%f ",AllcolorFeat[i].hist.at<float>(j));
			fprintf(out_bow,"%f",AllcolorFeat[i].hist.at<float>(j));

			fclose(out_bow);
		}

		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////





		// 		float rate=(float)(PictureNum+DirNum);
		// 		rate=((float)(i7+1+PictureNum))/rate;
		// 		RateOfAdvance(m_dspbuf,l,r,t,b,rate);
		// 		InvalidateRect(tmp,FALSE);
		// 		OnPaint();
		DeletePictureAddAndDeleteFlagFile(AllDirPath[i7]);//删除数据库增减标记文件 AddAndDelete
	}


	pWnd->SetWindowText("完成 100  %");

	RateOfAdvance(m_dspbuf,l,r,t,b,1);//进度条达到100%
	InvalidateRect(tmp,FALSE);
	OnPaint();


//	delete []number_feat;


	//	MessageBox("                      创建完成！                ");
	MessageBox("                      更新完成！                ");

	pWnd->ShowWindow(SW_HIDE);//进度信息隐藏

	CreatingDataBaseFlag=FALSE;//创建结束

	int kk=1;
}
//void CLaceborderMatchingDlg::CreateWords(CString  DirPath)//为每个目录创建字典文件
void CLaceborderMatchingDlg::CreateWords(char *WordsFile,vector<CString> &AllFeatureDATAfilename,vector<int> FeaturesNum,int &rate_num,float PictureNum)//为每个目录创建字典文件
{
	//vector<string> AllFeatureDATAfilename;
	int DBnum=0;
	int m_it,i,j,t,s;
	i=0;t=0;
	int ii=0;
	int iter=0;
	int i_beg=0;
	int feat_num=0;
	int group_num;
	DBnum=AllFeatureDATAfilename.size();

	int all_hs_num=0;
	for(i=0;i<DBnum;i++)
		all_hs_num+=FeaturesNum[i];


	float *featdata=new float[all_hs_num*HS_DIM];
	for (m_it=0;m_it<DBnum;m_it++)
	{
		ifstream read_img_feat;
		read_img_feat.open(AllFeatureDATAfilename[m_it]);
		if (!read_img_feat)
		{
			cout<<"Can't open feat file in proc_func line 375.\n";
			exit(-1);
		}
		int index;
		read_img_feat>>index;
		while (!read_img_feat.eof())
		{
			for (s=0;s<HS_DIM;s++)	
			{
				read_img_feat>>featdata[t*HS_DIM+s];
			}
			t++;

		}

	}
	VlGMM *gmm=kane_gmm_new(HS_DIM,nClusters);

	double LL=kane_gmm_cluster(gmm,featdata,all_hs_num);
	delete[] featdata;
	FILE * f1;
	f1=fopen(WordsFile,"w");
	//	f1=fopen("F:\\fv_kane_gmm.txt","w");

	fprintf(f1,"%d\n%d\n",nClusters,HS_DIM);

	for (i=0;i<nClusters;i++)
		fprintf(f1,"%f ",gmm->priors[i]);
	fprintf(f1,"\n");

	for (i=0;i<nClusters;i++)
	{
		for (j=0;j<HS_DIM;j++)
			fprintf(f1,"%f ",gmm->means[i*HS_DIM+j]);
		fprintf(f1,"\n");
	}

	for (i=0;i<nClusters-1;i++)
	{
		for (j=0;j<HS_DIM;j++)
			fprintf(f1,"%f ",gmm->covariances[i*HS_DIM+j]);
		fprintf(f1,"\n");

		MSG msg;
		while(::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			::TranslateMessage(&msg);
			::DispatchMessage(&msg);
		}
	}
	for (j=0;j<HS_DIM-1;j++)
		fprintf(f1,"%f ",gmm->covariances[i*HS_DIM+j]);
	fprintf(f1,"%f",gmm->covariances[i*HS_DIM+j]);
	fclose(f1);


// 	do 
// 	{
// 		MSG msg;
// 		while(::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
// 		{
// 			::TranslateMessage(&msg);
// 			::DispatchMessage(&msg);
// 		}
// 
// 		CWnd*pWnd= GetDlgItem(IDC_STATIC_PRO);//进度信息
// 		rate_num++;
// 
// 		int l=UpLoadAllPicture_x+UpLoadAllPicture_w+5;
// 		int r=m_Width-30;
// 		int t=UpLoadAllPicture_y;
// 		int b=UpLoadAllPicture_y+UpLoadAllPicture_h-1;
// 
// 		float rate=(float)4*PictureNum;
// 		rate=((float)(rate_num))/rate;
// 		RateOfAdvance(m_dspbuf,l,r,t,b,rate);
// 		CRect tmp;
// 		tmp.left=l-2;
// 		tmp.right=r+2;
// 		tmp.top=t-2;
// 		tmp.bottom=b+2;
// 		InvalidateRect(tmp,FALSE);
// 		OnPaint();
// 
// 		CString tmp_pro;
// 		tmp_pro.Format("%d",(int)(rate*100));
// 		tmp_pro="完成 "+tmp_pro+"  %";
// 		pWnd->SetWindowText(tmp_pro);
// 
// 
// 
// 	} while (iter<DBnum);

}
void CLaceborderMatchingDlg::Extract_HS_features_Single(CString  PicturePath,int m_nFrameInd,int &DBnum_updated,queue<int>& images_leftout, int &count_leftout,PCA &sip/*,int *number_feat*/)//提取单个图片特征
{
	// 	if (PathFileExists(path))
	// 	{
	// 		MessageBox("hello");
	// 	}
	//	feat_num_flag=0;
	vector <HEDescrip> HSDecription_grp;

	//	IplImage* img = cvLoadImage(PicturePath,CV_LOAD_IMAGE_COLOR);

	IplImage *in_load = cvLoadImage(PicturePath,CV_LOAD_IMAGE_COLOR);//根据每幅图片的路径加载图片
	IplImage *img1;

	////////////////////////////////////////////////////////////////////////////////之后路径会改成图片同一级目录路径/////////////
	CString s;

	s=PicturePath+"_Features.txt";//获取对应的特征文件
	char *hs_file = (LPSTR)(LPCTSTR)s;

	int in_size=in_load->width*in_load->height;
	int rel_flag=0;
	if (in_size<90000)
		img1=in_load;
	else
	{
		img1=crop_image(in_load);
		rel_flag=1;
	}
	Mat img(img1);

	int firstlevel_size=img.rows*img.cols;
	if (firstlevel_size>400000)
	{
		do
		{
			// 				img = gaussianBlur(img, 3.2);
			// 				img = kane_halfImage(img);
			pyrDown( img, img, Size( img.cols/2, img.rows/2 ) );
			firstlevel_size=img.rows*img.cols;
		}while(firstlevel_size>400000);

	}
	//		char hs_file[1024];
	//	strcpy(hs_file,AllFeatureDATAfilename[m_nFrameInd].c_str());


	Mat second_down;
	pyrDown( img, second_down, Size( img.cols/2, img.rows/2 ) );

	vector<KeyPoint> keypoints;//构造2个专门由点组成的点向量用来存储特征点
	if (img.rows<100||img.cols<100)
		dense_sampling_detect(img,keypoints,0);
	else
		dense_sampling_detect(img,keypoints,1);

	SurfDescriptorExtractor extractor;//定义描述子对象
	Mat descriptors;//存放特征向量的矩阵
	extractor.compute(img,keypoints,descriptors);

	Mat eig=sip.project(descriptors);
	if(eig.rows)
		SaveHSfeatures(eig,hs_file,m_nFrameInd,0);
// 	if(descriptors.rows)
// 		SaveHSfeatures(descriptors,hs_file,m_nFrameInd,0);

	vector<KeyPoint> keypoints1;//构造2个专门由点组成的点向量用来存储特征点
	dense_sampling_detect(second_down,keypoints1,1);
	SurfDescriptorExtractor extractor1;//定义描述子对象
	Mat descriptors1;//存放特征向量的矩阵
	extractor1.compute(second_down,keypoints1,descriptors1);
	Mat eig1;
	if (descriptors1.rows>0)
	{
		eig1=sip.project(descriptors1);
		if(eig1.rows)
			SaveHSfeatures(eig1,hs_file,m_nFrameInd,1);
	}
// 	if(descriptors1.rows)
// 		SaveHSfeatures(descriptors1,hs_file,m_nFrameInd,1);


	int NewSFNum=eig1.rows+eig.rows;
	cvReleaseImage(&in_load);
	if (rel_flag)
	{
		cvReleaseImage(&img1);
	}

	if(NewSFNum==0)
	{
		printf("no features: %s\n", hs_file);
		images_leftout.push(m_nFrameInd-count_leftout++);
		DBnum_updated--;
	}


	//////////////////////////////////////////////////////////////////////////////////



	//保存每个图片的 特征个数
	int FeatureCounts=NewSFNum;
	if (FeatureCounts>0)//特征数不为0才写入记录
	{
		SaveEachPictureFeatureCounts(PicturePath,FeatureCounts);//保存每个图片HS特征个数

	}

}
void CLaceborderMatchingDlg::GetAllPicturePath(vector<CString>&AllPicturePath)//获取当前数据库所有文件
{
	vector<CString>AllDirPath;//保存所有目录
	CString DirPath;
//	CString laceClass[5]={"大边","睫毛","面料","小边","其他"};

	int i,j;

	HTREEITEM hItem = m_tree.GetSelectedItem();//获取导航栏树结构控件当前选中的节点

	if (hItem==m_tree.GetRootItem())//当前选中的是整个数据库
	{
		DirPath=CurrentPath+"\\DataBase";
		AllDirPath.push_back(DirPath);//数据库 根目录
		for (i=0;i<(int)DataBaseFileName.size();i++)//依次将5个第二级目录以及第三级子目录存入
		{
			DirPath=CurrentPath+"\\DataBase\\"+DataBaseFileName[i];
			AllDirPath.push_back(DirPath);			
		}
	}
	else if (m_tree.GetParentItem(hItem)==m_tree.GetRootItem())//第二级目录
	{
		i=m_tree.GetItemData(hItem);

		DirPath=CurrentPath+"\\DataBase\\"+DataBaseFileName[i];
		AllDirPath.push_back(DirPath);
	}

	j=(int)AllDirPath.size();
	for (i=0;i<j;i++)//循环对每个目录下查找所有的jpg文件
	{
		GetAllFile(AllDirPath[i],"jpg",AllPicturePath,false);//获取所有 jpg文件
	}
	ClearCStringVector(AllDirPath);

}
void CLaceborderMatchingDlg::GetAllDirPath(vector<CString>&AllDirPath)//获取当前数据库所有文件夹
{
//	vector<CString>AllDirPath;
	CString DirPath;
//	CString laceClass[5]={"大边","睫毛","面料","小边","其他"};

	int i,j;

	HTREEITEM hItem = m_tree.GetSelectedItem();

	if (hItem==m_tree.GetRootItem())//当前选中的是整个数据库
	{
		DirPath=CurrentPath+"\\DataBase";
		AllDirPath.push_back(DirPath);
		for (i=0;i<(int)DataBaseFileName.size();i++)
		{
			DirPath=CurrentPath+"\\DataBase\\"+DataBaseFileName[i];
			AllDirPath.push_back(DirPath);
// 			if ((int)DataBaseFileName[i].SubFileName.size()>0)
// 			{
// 				for (j=0;j<(int)DataBaseFileName[i].SubFileName.size();j++)
// 				{
// 					DirPath=CurrentPath+"\\DataBase\\"+laceClass[i]+"\\"+DataBaseFileName[i].SubFileName[j];
// 					AllDirPath.push_back(DirPath);
// 				}
// 			}

		}
	}
	else if (m_tree.GetParentItem(hItem)==m_tree.GetRootItem())//第二级目录
	{
		i=m_tree.GetItemData(hItem);

		DirPath=CurrentPath+"\\DataBase\\"+DataBaseFileName[i];
		AllDirPath.push_back(DirPath);

// 		if ((int)DataBaseFileName[i].SubFileName.size()>0)
// 		{
// 			for (j=0;j<(int)DataBaseFileName[i].SubFileName.size();j++)
// 			{
// 				DirPath=CurrentPath+"\\DataBase\\"+laceClass[i]+"\\"+DataBaseFileName[i].SubFileName[j];
// 				AllDirPath.push_back(DirPath);
// 			}
// 		}
	}
// 	else
// 	{
// 		j=m_tree.GetItemData(hItem);
// 		hItem=m_tree.GetParentItem(hItem);
// 		i=m_tree.GetItemData(hItem);
// 
// 		DirPath=CurrentPath+"\\DataBase\\"+laceClass[i]+"\\"+DataBaseFileName[i].SubFileName[j];
// 		AllDirPath.push_back(DirPath);
// 	}

}
void CLaceborderMatchingDlg::GetCurrentDirAllPicturePath(CString CurrentSelectedDir,vector<CString>&AllPicturePath,int &FeatureCounts)//获取当前所选目录下所有图片文件文件，包括子目录下
{
	vector<CString>AllDirPath;
	CString DirPath;
//	CString laceClass[5]={"大边","睫毛","面料","小边","其他"};

	int i,j;


	DirPath=CurrentPath+"\\DataBase";

	if (CurrentSelectedDir==DirPath)//当前选中的是整个数据库
	{
		
		AllDirPath.push_back(DirPath);
		for (i=0;i<(int)DataBaseFileName.size();i++)
		{
			DirPath=CurrentPath+"\\DataBase\\"+DataBaseFileName[i];
			AllDirPath.push_back(DirPath);
// 			if ((int)DataBaseFileName[i].SubFileName.size()>0)
// 			{
// 				for (j=0;j<(int)DataBaseFileName[i].SubFileName.size();j++)
// 				{
// 					DirPath=CurrentPath+"\\DataBase\\"+laceClass[i]+"\\"+DataBaseFileName[i].SubFileName[j];
// 					AllDirPath.push_back(DirPath);
// 				}
// 			}

		}
	}
	else
	{
		DirPath=CurrentPath+"\\DataBase\\"+DataBaseFileName[i];
		AllDirPath.push_back(DirPath);

// 		bool f=false;
// 		for (i=0;i<5;i++)
// 		{
// 			DirPath=CurrentPath+"\\DataBase\\"+laceClass[i];
// 			if (CurrentSelectedDir==DirPath)//第二级目录
// 			{
// 
// 				DirPath=CurrentPath+"\\DataBase\\"+laceClass[i];
// 				AllDirPath.push_back(DirPath);
// 
// 				if ((int)DataBaseFileName[i].SubFileName.size()>0)
// 				{
// 					for (j=0;j<(int)DataBaseFileName[i].SubFileName.size();j++)
// 					{
// 						DirPath=CurrentPath+"\\DataBase\\"+laceClass[i]+"\\"+DataBaseFileName[i].SubFileName[j];
// 						AllDirPath.push_back(DirPath);
// 					}
// 				}
// 				f=true;
// 				break;
// 			}
// 		}
// 		
// 		if (f==false)
// 		{
// 			DirPath=CurrentSelectedDir;
// 			AllDirPath.push_back(DirPath);
// 		}
	}
		

	j=(int)AllDirPath.size();
	for (i=0;i<j;i++)
	{
		GetAllFile(AllDirPath[i],"jpg",AllPicturePath,false);//获取所有 jpg文件

		//获取每个目录下的FeatureCounts;相加

		vector<MY_DATE>dateData;//日期文件信息

		CString FeatureCountsPath=AllDirPath[i]+"\\FeatureCounts.Date";//保存特征数文件
		char *filePath;
		filePath=FeatureCountsPath.GetBuffer(FeatureCountsPath.GetLength());

		if (PathFileExists(filePath))
		{
			ReadDateFile(dateData,filePath);

			for (int j=0;j<(int)dateData.size();j++)
			{
				FeatureCounts+=dateData[j].Sum;
			}
		}

		if (!dateData.empty())
		{
			dateData.clear();
			vector<MY_DATE>().swap(dateData);
		}
		
	}
	ClearCStringVector(AllDirPath);

}

void CLaceborderMatchingDlg::ShowCurrentDataBasePicture(CString path)//显示当前选中的数据库目录里面的所有图片
{
	if (!PathFileExists(path))
	{
		path=path.Right(path.GetLength()-CurrentPath.GetLength()-1);
//		MessageBox(path+"   \n不存在！");
		return;
	}
	GetAllFile(path,"jpg",CurrentDataBasePicturePath,true);//获取所有 jpg文件
	
	if (CurrentDataBasePicturePath.size()>0)
	{
		
		CurrentSubDataBasePageIndex=1;
		ShowEachPagePicture(CurrentSubDataBasePageIndex);

		CString path_=CurrentPath+"\\resource\\next_page.jpg";
		SetBackPic(path_,DataBaseSubWindow_l*0.75+DataBaseSubWindow_r*0.25,DataBaseSubWindow_b-20,25,18,0,4,true);//前一页
		path_=CurrentPath+"\\resource\\next_page.jpg";
		SetBackPic(path_,DataBaseSubWindow_l*0.25+DataBaseSubWindow_r*0.75,DataBaseSubWindow_b-20,25,18,0,4,false);//后一页


		
	}
	else
	{
		CurrentSubDataBasePageIndex=0;
		CurrentSubDataBasePageNum=0;
		InitPictureColour(m_imageBack,DataBasePicture_l,DataBasePicture_r+1,DataBasePicture_t,DataBasePicture_b+1,240,240,240);
	}
	SetPageNum();

	
//	((CTreeCtrl *)GetDlgItem(IDC_TREE_DATABASE))->SetFocus();

// 	CRect tmp;
// 	tmp.left=DataBasePicture_l;
// 	tmp.right=DataBasePicture_r;
// 	tmp.top=DataBasePicture_t;
// 	tmp.bottom=DataBasePicture_b;
// 
// 	InvalidateRect(tmp,FALSE);
	int k=0;

}
void CLaceborderMatchingDlg::ShowEachPagePicture(int num)//显示每一页的图片
{
	int i,n;
	int x,y;
	int k;
	if (num>0)
	{
		k=(num-1)*DataBasePicture_x_num*DataBasePicture_y_num;
	}
	else if (num==0)
	{
		k=0;
	}
	if (CurrentSubDataBasePageNum>num)
	{
		n=DataBasePicture_x_num*DataBasePicture_y_num;
	}
	else if (CurrentSubDataBasePageNum==num)
	{
		
		n=(int)CurrentDataBasePicturePath.size()-k;
	}

	InitPictureColour(m_imageBack,DataBasePicture_l,DataBasePicture_r,DataBasePicture_t,DataBasePicture_b,240,240,240);
	for (i=0;i<n;i++)
	{
		
		x=i%DataBasePicture_x_num*DataBasePicture_w+DataBasePicture_l;
		y=i/DataBasePicture_x_num*DataBasePicture_h+DataBasePicture_t;
		if ((i+k)<(int)CurrentDataBasePicturePath.size())
		{
			int d_lenth=0;
			if (CurrentMenu==MenuName[2]||ShowingMatchingPicturesFlag==true)
			{
				d_lenth=30;
				y+=d_lenth;
				
			}
			else
			{
				d_lenth=10;
				y+=d_lenth;
			}
			SetPictureInAPart(m_imageBack,x,y,DataBasePicture_w,DataBasePicture_h-d_lenth,CurrentDataBasePicturePath[i+k]);

// 			if (CurrentMenu==MenuName[2]/*||ShowingMatchingPicturesFlag==true*/)
// 			{
// 				CString Tip;
// 				LOGFONT   lf;   
// 				memset(&lf,   0,   sizeof(LOGFONT));   
// 				lf.lfHeight   =20;         //字体的高
// 				lf.lfWidth    =8;
// 				lstrcpy(lf.lfFaceName, _T("Verdana"));
// 
// 				Tip.Format("%d",DateNum[i+k]);
// 				Tip="总共被检索："+Tip;
// 				Tip=Tip+" 次";
// 				ShowText(x+1,y+1,0,255,0,lf,Tip,false);
// 			}
			
		}
		
	}

	ShowGoToPage();
}

int CLaceborderMatchingDlg::GetCurrentPicturePath(CString &path,CPoint point)//获取当前点击的图片的路径
{
	int i,n;
	int x,y,r,b;
	int k;

	int index=-1;

	if (CurrentSubDataBasePageIndex>0)
	{
		k=(CurrentSubDataBasePageIndex-1)*DataBasePicture_x_num*DataBasePicture_y_num;
	}
	else if (CurrentSubDataBasePageIndex==0)
	{
		k=0;
	}
	if (CurrentSubDataBasePageNum>CurrentSubDataBasePageIndex)
	{
		n=DataBasePicture_x_num*DataBasePicture_y_num;
	}
	else if (CurrentSubDataBasePageNum==CurrentSubDataBasePageIndex)
	{

		n=(int)CurrentDataBasePicturePath.size()-k;
	}

	for (i=0;i<n;i++)
	{

		x=i%DataBasePicture_x_num*DataBasePicture_w+DataBasePicture_l;
		y=i/DataBasePicture_x_num*DataBasePicture_h+DataBasePicture_t;
		r=x+DataBasePicture_w;
		b=y+DataBasePicture_h;

		if (point.x>x&&point.x<r&&point.y>y&&point.y<b)
		{
			if ((i+k)<(int)CurrentDataBasePicturePath.size())
			{
				index=i+k;
				path=CurrentDataBasePicturePath[index];
				
				break;
			}
			else
			{
				path="";
				index=-1;
			}
		}
		

	}

	CurrentSelectedPictureIndexNum=index;
	return index;
}

void CLaceborderMatchingDlg::SetPictureInAPart(IplImage* image,int x,int y,int w,int h,CString pathname)//在一个指定区域显示缩略图
{
	char *path = (LPSTR)(LPCTSTR)pathname;
	IplImage* temp = cvLoadImage(path,CV_LOAD_IMAGE_COLOR);

	if (!temp)
	{
		return;
	}

	x=x+w/2;
	y=y+h/2;
	w=w-6;
	h=h-6;

	double rate;
	int tmp_w=temp->width;
	int tmp_h=temp->height;
	int middle_x=tmp_w/2;
	int middle_y=tmp_h/2;

//默认长宽都小于显示区域
	rate=(double)(temp->height)/h;

	if (tmp_h>h)
	{
		rate=(double)(temp->height)/h;
		tmp_h=h;
		tmp_w=(int)(temp->width/rate);
	}
	if (tmp_w>w)
	{
		rate=(double)(temp->width)/w;
		tmp_h=(int)(temp->height/rate);
		tmp_w=w;
	}


	if (rate<1)
	{
		rate=1;
	}

	int i,j,i1,j1,i2,j2;
	for(i =0; i <tmp_h; i++)
		for(j =0; j <tmp_w; j++)
		{
			i1=y-tmp_h/2+i;
			j1=x-tmp_w/2+j;
			i2=(int)(rate*i);
			j2=(int)(rate*j);
			SetPointColour(image,j1,i1,temp,j2,i2);
		}
	cvReleaseImage(&temp);
}
void CLaceborderMatchingDlg::GetAllFile(CString path,CString format,vector<CString> &CurrentDataBasePicturePath,bool f)//获取目录下的所有图片路径
{
	CString tmp=path+"\\*."+format;
	if (f==true)
	{
		ClearCStringVector(CurrentDataBasePicturePath);
	}


	WIN32_FIND_DATA FindFileData;
	HANDLE hFind;
	hFind = FindFirstFile(tmp, &FindFileData);
	if (hFind == INVALID_HANDLE_VALUE) 
	{
//		MessageBox("该目录下无图片！！");
		return;
	}
	else 
	{
		do 
		{
			tmp=path+"\\"+FindFileData.cFileName;
			CurrentDataBasePicturePath.push_back(tmp);

			MSG msg;
			while(::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
			{
				::TranslateMessage(&msg);
				::DispatchMessage(&msg);
			}
		} 
		while (FindNextFile(hFind, &FindFileData) != 0);
	}

	CurrentSubDataBasePageNum=((int)CurrentDataBasePicturePath.size()-1)/(DataBasePicture_x_num*DataBasePicture_y_num)+1;
}

void CLaceborderMatchingDlg::ClearCStringVector(vector<CString> &data)
{
	if (!data.empty())
	{
		data.clear();
		vector<CString>().swap(data);
	}
}
void CLaceborderMatchingDlg::ManageAccountClick(int num)//点击3个按钮 1,2,3
{
	CString path[6];
	int i;

	manageAccountMenuNum=num;

	path[0]=CurrentPath+"\\resource\\changePassword01.jpg";
	path[1]=CurrentPath+"\\resource\\changePassword02.jpg";
	path[2]=CurrentPath+"\\resource\\manageaccount01.jpg";
	path[3]=CurrentPath+"\\resource\\manageaccount02.jpg";
	path[4]=CurrentPath+"\\resource\\createaccount01.jpg";
	path[5]=CurrentPath+"\\resource\\createaccount02.jpg";

	int x[3]={330,421,538};
	int y=BlueLine+15;

	int lenth[3]={51,77,64};

	int push_x[3]={22,8,15};//3个按钮按下去的偏移量

	for (i=0;i<3;i++)
	{
		if ((i+1)==num)
		{
			SetBackPic(path[2*i+1],x[i],y,lenth[i],31,push_x[i],0,false);
		}
		else 
		{
			SetBackPic(path[2*i],x[i],y,lenth[i],31,0,0,false);
		}
	}

	manageAccountHide();

	if (num==1)//修改密码
	{
		changePassword();
		CopyInHorizontal(m_imageBack,480,497,BlueLine+180,BlueLine+197,500);
		CopyInHorizontal(m_imageBack,LevelFalse_x,LevelFalse_x+17,LevelFalse_y,LevelFalse_y+17,LevelFalse_x+20);
		CopyInHorizontal(m_imageBack,LevelTrue_x,LevelTrue_x+17,LevelTrue_y,LevelTrue_y+17,LevelTrue_x+20);
	}
	else if (num==2)//管理当前账户
	{
		manageAllAccount();
		SingleRadio(480,BlueLine+180,deleteFlag);
		levelChoose=false;
		RadioChoose(LevelFalse_x,LevelFalse_y,LevelTrue_x,LevelTrue_y);
		GetImageDisplayData(m_dspbuf, m_imageBack, 0, 0);
		Invalidate(FALSE);
	}
	else if (num==3)//创建账户
	{
		CopyInHorizontal(m_imageBack,480,497,BlueLine+180,BlueLine+197,500);
		createAccount();
		levelChoose=false;
		RadioChoose(LevelFalse_x,LevelFalse_y,LevelTrue_x,LevelTrue_y);
		GetImageDisplayData(m_dspbuf, m_imageBack, 0, 0);
		Invalidate(FALSE);
	}
}
void CLaceborderMatchingDlg::changePassword()//修改密码
{
	CWnd* pWnd= GetDlgItem(IDC_EDIT_PASSWORD);//旧密码
	pWnd->MoveWindow(470,BlueLine+150, 200,30,TRUE);
	pWnd->SetWindowText("");
	pWnd->ShowWindow(SW_SHOWNA);

	CEdit *m_Edit=(CEdit *)GetDlgItem(IDC_EDIT_PASSWORD);
	m_Edit->SetFont(&font,FALSE);

	pWnd= GetDlgItem(IDC_EDIT_PASSWORD_SED);//新密码
	pWnd->SetWindowText("");
	pWnd->MoveWindow(470,BlueLine+200, 200,30,TRUE);
	pWnd->ShowWindow(SW_SHOWNA);
	m_Edit=(CEdit *)GetDlgItem(IDC_EDIT_PASSWORD_SED);
	m_Edit->SetFont(&font,FALSE);
}
void CLaceborderMatchingDlg::manageAllAccount()//管理账户
{
	CWnd* pWnd= GetDlgItem(IDC_COMBO_ACCOUNT);//旧密码
	pWnd->MoveWindow(470,BlueLine+100, 300,500,TRUE);
	pWnd->SetWindowText("");
	pWnd->ShowWindow(SW_SHOWNA);

	int j=(int)accounts.size();

	((CComboBox*)GetDlgItem(IDC_COMBO_ACCOUNT))->ResetContent();
	for (int i=0;i<j;i++)
	{
		CString s=accounts[i].account+"        /";
		if (accounts[i].Level==false)
		{
			s+="操作员";

		}
		else
		{
			s+="管理员";
		}
		((CComboBox*)GetDlgItem(IDC_COMBO_ACCOUNT))->InsertString(i,s);
	}

}
void CLaceborderMatchingDlg::createAccount()//创建账户
{
	CWnd* pWnd= GetDlgItem(IDC_EDIT_PASSWORD);//密码
	pWnd->MoveWindow(470,BlueLine+150, 200,30,TRUE);
	pWnd->SetWindowText("");
	pWnd->ShowWindow(SW_SHOWNA);

	CEdit *m_Edit=(CEdit *)GetDlgItem(IDC_EDIT_PASSWORD);
	m_Edit->SetFont(&font,FALSE);

	pWnd= GetDlgItem(IDC_EDIT_PASSWORD_SED);//第二次密码
	pWnd->SetWindowText("");
	pWnd->MoveWindow(470,BlueLine+200, 200,30,TRUE);
	pWnd->ShowWindow(SW_SHOWNA);
	m_Edit=(CEdit *)GetDlgItem(IDC_EDIT_PASSWORD_SED);
	m_Edit->SetFont(&font,FALSE);

	pWnd= GetDlgItem(IDC_EDIT_ACCOUNT);//账户名
	pWnd->MoveWindow(470,BlueLine+100, 200,30,TRUE);
	pWnd->SetWindowText("");
	pWnd->ShowWindow(SW_SHOWNA);

	m_Edit=(CEdit *)GetDlgItem(IDC_EDIT_ACCOUNT);
	m_Edit->SetFont(&font,FALSE);
}

void CLaceborderMatchingDlg::manageAccountHide()//隐藏账户管理所有控件
{
	CWnd* pWnd= GetDlgItem(IDC_EDIT_ACCOUNT);//密码
	pWnd->ShowWindow(SW_HIDE);
	pWnd= GetDlgItem(IDC_EDIT_PASSWORD);//密码
	pWnd->ShowWindow(SW_HIDE);
	pWnd= GetDlgItem(IDC_EDIT_PASSWORD_SED);//密码
	pWnd->ShowWindow(SW_HIDE);
	pWnd= GetDlgItem(IDC_COMBO_ACCOUNT);//密码
	pWnd->ShowWindow(SW_HIDE);

}
void CLaceborderMatchingDlg::SureManageAccount()//确定修改 账户
{
	bool manageSuccessFlag=false;

	CString Account,p,p1;
	int i;

	if (manageAccountMenuNum==1)//修改密码
	{
		GetDlgItemText(IDC_EDIT_PASSWORD, p);
		GetDlgItemText(IDC_EDIT_PASSWORD_SED, p1);
		if (p==""&&p1=="")
		{
			AfxMessageBox("                密码输入不能为空！                 ");
			manageSuccessFlag=false;
		}
		else if (p==p1)
		{
			int len=p.GetLength();
			bool f=false;
			for (int l=0;l<len;l++)
			{
				char c=p.GetAt(l);
				if (!((c>='a'&&c<='z')||(c>='0'&&c<='9')||(c>='A'&&c<='Z')))
				{
					f=true;
					break;
				}

			}
			if (f==false)
			{
				accounts[CurrentAccount].password=p;
				MessageBox("                         密码修改成功 !                   ");
				manageSuccessFlag=true;
			}
			else
			{
				MessageBox("                         密码不可以输入除 英文字母与数字 之外的特殊字符 !                   ");
				manageSuccessFlag=false;
			}
			
		}
		else 
		{
			AfxMessageBox("两次密码输入不一致！");
			manageSuccessFlag=false;
		}
		
		
	}
	else if (manageAccountMenuNum==2)//管理现有账户
	{
		i=((CComboBox*)GetDlgItem(IDC_COMBO_ACCOUNT))->GetCurSel();
		if (i<0)
		{
			MessageBox("     请先选择一个账户！！     ");
			manageSuccessFlag=false;
		}
		else if (i==0)
		{
			AfxMessageBox("该用户是系统保留账户，不可以修改！");
			manageSuccessFlag=false;
		} 
		else if (deleteFlag==true)
		{
			if (accounts[i].account==accounts[CurrentAccount].account)
			{
				MessageBox("不可以删除当前登录账号！！");
				
			}
			else
			{
				myFlag=false;

				CTip tip;
				tip.DoModal();

				if (myFlag==true)
				{
					int j=(int)accounts.size();
					for (int i1=i;i1<j-1;i1++)
					{
						accounts[i1].account=accounts[i1+1].account;
						accounts[i1].password=accounts[i1+1].password;
						accounts[i1].Level=accounts[i1+1].Level;
					}
					accounts.pop_back();
					manageSuccessFlag=true;
					MessageBox("                   删除账户成功    ！！          ");
				}
				else
				{
					manageSuccessFlag=false;
				}
				
			}
			
		}
		else
		{
			if (levelChoose==true)
			{
				accounts[i].Level=true;
			}
			else
			{
				accounts[i].Level=false;
			}
			manageSuccessFlag=true;
			MessageBox("                   修改成功    ！！            ");
		}
		
		int j=(int)accounts.size();

		((CComboBox*)GetDlgItem(IDC_COMBO_ACCOUNT))->ResetContent();
		for ( i=0;i<j;i++)
		{
			CString s=accounts[i].account+"        /";
			if (accounts[i].Level==false)
			{
				s+="操作员";

			}
			else
			{
				s+="管理员";
			}
			((CComboBox*)GetDlgItem(IDC_COMBO_ACCOUNT))->InsertString(i,s);
		}
	}
	else if (manageAccountMenuNum==3)//创建账户
	{
		GetDlgItemText(IDC_EDIT_ACCOUNT, Account);
		GetDlgItemText(IDC_EDIT_PASSWORD, p);
		GetDlgItemText(IDC_EDIT_PASSWORD_SED, p1);
		if (Account=="")
		{
			AfxMessageBox("   用户名不能为空！   ");
			manageSuccessFlag=false;
		}
		else if (p==""&&p1=="")
		{
			AfxMessageBox("   密码输入不能为空！   ");
			manageSuccessFlag=false;
		}
		else if (p==p1)
		{
			manageSuccessFlag=true;
			int j=(int)accounts.size();
			for (int i1=0;i1<j;i1++)
			{
				if (accounts[i1].account==Account)
				{
					AfxMessageBox("   该用户名已经存在，请使用其他用户名！！");
					manageSuccessFlag=false;
				}
			}
			if (manageSuccessFlag==true)
			{
				if (Account=="Adonis  2014")
				{
					AfxMessageBox("   该用户名被系统保留，请使用其他用户名！！");
					manageSuccessFlag=false;
				}
				else 
				{
					ACCOUNT tmp;
					tmp.account=Account;
					tmp.password=p;
					if (levelChoose==true)
					{
						tmp.Level=true;
					}
					else
					{
						tmp.Level=false;
					}
					accounts.push_back(tmp);
					MessageBox("       账户创建成功 !       ");
				}	
			}			
		}
		else 
		{
			AfxMessageBox("两次密码输入不一致！");
			manageSuccessFlag=false;
		}
	}

	CWnd* pWnd= GetDlgItem(IDC_EDIT_PASSWORD);
	pWnd->SetWindowText("");
	pWnd= GetDlgItem(IDC_EDIT_PASSWORD_SED);
	pWnd->SetWindowText("");
	pWnd= GetDlgItem(IDC_EDIT_ACCOUNT);
	pWnd->SetWindowText("");

	if (manageSuccessFlag==false)
	{
		return;
	}
	CString Path=CurrentPath+"\\system\\user.bin";//保存账户信息
	char *filePath;
	filePath=Path.GetBuffer(Path.GetLength());//保存 SaveImageData.txt的路径

	WriteAccount(accounts,filePath);

}
void CLaceborderMatchingDlg::SearchByTexture()//以文理搜索
{
	Texture_Colour_Flag=true;

	CString path;
	path=CurrentPath+"\\resource\\radio02.jpg";
	SetBackPic(path,SearchByTexture_x,SearchByTexture_y,radio_w,radio_h,0,0,false);
	path=CurrentPath+"\\resource\\radio01.jpg";
	SetBackPic(path,SearchByColour_x,SearchByColour_y,radio_w,radio_h,0,0,false);
}
void CLaceborderMatchingDlg::SearchByColour()//以颜色搜索
{
	Texture_Colour_Flag=false;
	CString path;
	path=CurrentPath+"\\resource\\radio01.jpg";
	SetBackPic(path,SearchByTexture_x,SearchByTexture_y,radio_w,radio_h,0,0,false);
	path=CurrentPath+"\\resource\\radio02.jpg";
	SetBackPic(path,SearchByColour_x,SearchByColour_y,radio_w,radio_h,0,0,false);
}
void CLaceborderMatchingDlg::MovePic()//鼠标拖动
{
	CString path;
	path=CurrentPath+"\\resource\\radio02.jpg";
	SetBackPic(path,Move_x,Move_y,radio_w,radio_h,0,0,false);
	path=CurrentPath+"\\resource\\radio01.jpg";
	SetBackPic(path,Cut_x,Cut_y,radio_w,radio_h,0,0,false);
}
void CLaceborderMatchingDlg::CutPic()//裁剪
{
	CString path;
	path=CurrentPath+"\\resource\\radio01.jpg";
	SetBackPic(path,Move_x,Move_y,radio_w,radio_h,0,0,false);
	path=CurrentPath+"\\resource\\radio02.jpg";
	SetBackPic(path,Cut_x,Cut_y,radio_w,radio_h,0,0,false);
}
void CLaceborderMatchingDlg::RadioChoose(int x,int y,int _x,int _y)//切换两个平行的按钮，f==true，前一个选中，否则后一个选中
{
	CString path,_path;
// 	if (f==true)
// 	{
// 		_path=CurrentPath+"\\resource\\radio01.jpg";
// 		path=CurrentPath+"\\resource\\radio02.jpg";
// 	}
// 	else
// 	{
		path=CurrentPath+"\\resource\\radio01.jpg";
		_path=CurrentPath+"\\resource\\radio02.jpg";
//	}

	SetBackPic(_path,x,y,radio_w,radio_h,0,0,false);
	SetBackPic(path,_x,_y,radio_w,radio_h,0,0,false);
}
void CLaceborderMatchingDlg::SingleRadio(int x,int y,bool f)//单选按钮
{
	CString path;
	if (f==true)
	{
		path=CurrentPath+"\\resource\\radio02.jpg";
	}
	else
	{
		path=CurrentPath+"\\resource\\radio01.jpg";
	}
	SetBackPic(path,x,y,radio_w,radio_h,0,0,false);
}

void CLaceborderMatchingDlg::InitPictureColour(IplImage* image,int r,int g,int b)//初始化图片颜色值
{
	int i,j;
	for(i = 0; i < image->height; i++)
		for(j = 0; j < image->width; j++)
		{
			SetPointColour(image,j ,i,r,g,b);
		}
}
void CLaceborderMatchingDlg::InitPictureColour(IplImage* image,int l,int r,int t,int b,int R,int G,int B)//初始化图片一个区域
{
	int i,j;
	for(i = t; i <b; i++)
		for(j = l; j <r; j++)
		{
			SetPointColour(image,j ,i,R,G,B);
		}
}
void CLaceborderMatchingDlg::CopyInHorizontal(IplImage* image,int l,int r,int t,int b,int x)//水平方向复制一段图片,横向复制范围包括两个端点，纵向不包括下边界
{
	int i,j;

	for (i=l;i<=r;i++)
	{
		for (j=t;j<b;j++)
		{
			SetPointColour(image,i,j,image,x,j);
		}
	}
}
void CLaceborderMatchingDlg::CopyInVertical(IplImage* image,int l,int r,int t,int b,int y)//垂直方向复制一段图片,纵向复制范围包括两个端点
{
	int i,j;

	for (i=l;i<=r;i++)
	{
		for (j=t;j<=b;j++)
		{
			SetPointColour(image,i,j,image,i,y);
		}
	}
}
void CLaceborderMatchingDlg::CopyImageInHorizontalByMirror(IplImage* image,int l,int r,int t,int b,int x)//水平方向复制一段图片,镜面复制
{
	int i,j;

	for (i=0;i<=(r-l);i++)
	{
		for (j=t;j<b;j++)
		{
			SetPointColour(image,(x+i),j,image,(r-i),j);
		}
	}
}
void CLaceborderMatchingDlg::CopyImageInHorizontal(IplImage* image,int l,int r,int t,int b,int x)//水平方向复制一段图片,平移复制
{
	int i,j;

	for (i=0;i<=(r-l);i++)
	{
		for (j=t;j<b;j++)
		{
			SetPointColour(image,(x+i),j,image,(l+i),j);
		}
	}
}
void CLaceborderMatchingDlg::CopyImageInVerticalByMirror(IplImage* image,int l,int r,int t,int b,int y)//垂直方向复制一段图片,镜面复制
{
	int i,j;

	for (i=l;i<r;i++)
	{
		for (j=0;j<=(b-t);j++)
		{
			SetPointColour(image,i,(y+j),image,i,(b-j));
		}
	}
}

void CLaceborderMatchingDlg::LogSuccessfulBackGround()
{
	int i,j;
	int x,y,w,h;
	CString pathname;

	InitPictureColour(m_imageBack,240,240,240);//初始化背景色
	SetBackPic(leftTitlePath,leftTitlePath_x,leftTitlePath_y,leftTitlePath_w,leftTitlePath_h,0,0,false);//左上角大标题

	for (i=0;i<m_Width;i++)
	{
		for (j=BlueLine;j<(BlueLine+3);j++)
		{
			SetPointColour(m_imageBack,i,j,58,120,179);//菜单下蓝色线
		}		
//		SetPointColour(m_imageBack,i,113,90,90,90);		//蓝线下面的黑线
	}
	//账户与注销
//	Level=false;
	if (accounts[CurrentAccount].Level==true)
	{
		pathname=CurrentPath+"\\resource\\Level_true.jpg";
	}
	else if (accounts[CurrentAccount].Level==false)
	{
		pathname=CurrentPath+"\\resource\\Level_false.jpg";
	}
	
	x=m_Width-360;
	y=BlueLine-18;
	w=18;
	h=18;
	SetBackPic(pathname,x,y,w,h,0,0,false);

	pathname=CurrentPath+"\\resource\\LogOut.jpg";

	x=m_Width-70;
	y=BlueLine-21;
	w=21;
	h=21;
	SetBackPic(pathname,x,y,w,h,0,0,false);

/////////////////////////////////////////////////欢迎您
	pathname=CurrentPath+"\\resource\\welcome.jpg";
	x=m_Width-406;
	y=0;
	w=106;
	h=31;
	SetBackPic(pathname,x,y,w,h,0,0,false);

	pathname=CurrentPath+"\\resource\\Rightwelcome.jpg";
	x=m_Width-200;
	y=0;
	w=159;
	h=31;
	SetBackPic(pathname,x,y,w,h,0,0,false);

	CopyInHorizontal(m_imageBack,(m_Width-300),(m_Width-200),0,31,(m_Width-301));
	CopyInHorizontal(m_imageBack,(m_Width-41),m_Width,0,31,(m_Width-301));
// 	for (j=0;j<31;j++)
// 	{
// 		for (i=(m_Width-300);i<(m_Width-200);i++)
// 		{
// 			SetPointColour(m_imageBack,i,j,m_imageBack,(m_Width-301),j);
// 		}
// 		for (i=(m_Width-41);i<m_Width;i++)
// 		{
// 			SetPointColour(m_imageBack,i,j,m_imageBack,(m_Width-301),j);
// 		}
// 	}

	//当前路径提示	
// 	pathname=CurrentPath+"\\resource\\currentPath.jpg";
// 	x=275;
// 	y=116;
// 	w=10;
// 	h=33;
// 	SetBackPic(pathname,x,y,w,h,0,0,false);
// 
// 	for (i=(m_Width-20);i<(m_Width-10);i++)
// 	{
// 		for (j=116;j<199;j++)
// 		{
// 			SetPointColour(m_imageBack,i,j,m_imageBack,284,j);
// 		}
// 	}
// 	for (i=285;i<(m_Width-20);i++)
// 	{
// 		for (j=116;j<199;j++)
// 		{
// 			SetPointColour(m_imageBack,i,j,m_imageBack,284,j);
// 		}
// 	}
	/////////////////////////////////////////////////////////底栏显示
	pathname=CurrentPath+"\\resource\\copyright.jpg";
	x=0;
	y=m_Height-33;
	w=277;
	h=33;
	SetBackPic(pathname,x,y,w,h,0,0,false);

	pathname=CurrentPath+"\\resource\\bottomright.jpg";
	x=m_Width-162;
	y=m_Height-33;
	w=162;
	h=33;
	SetBackPic(pathname,x,y,w,h,0,0,false);

	CopyInHorizontal(m_imageBack,276,x,y,m_Height,275);
// 	for (i=276;i<=x;i++)
// 	{
// 		for (j=y;j<m_Height;j++)
// 		{
// 			SetPointColour(m_imageBack,i,j,m_imageBack,275,j);
// 		}
// 	}
///////////////////////////////////////////////////////////////////////////菜单导航   宽 270
	pathname=CurrentPath+"\\resource\\menuIco.jpg";
	x=5;
	y=BlueLine+5;
	w=125;
	h=47;
	SetBackPic(pathname,x,y,w,h,0,0,false);
/////////////////////////菜单导航右上半
	CopyImageInHorizontalByMirror(m_imageBack,2,27,y,(y+h),245);
// 	for (i=245;i<270;i++)
// 	{
// 		for (j=y;j<(y+h);j++)
// 		{
// 			SetPointColour(m_imageBack,i,j,m_imageBack,(272-i),j);
// 		}
// 	}
///////////////////////	菜单导航上边中间部分
	CopyInHorizontal(m_imageBack,130,244,y,(y+h),129);
// 	for (i=130;i<245;i++)
// 	{
// 		for (j=y;j<(y+h);j++)
// 		{
// 			SetPointColour(m_imageBack,i,j,m_imageBack,129,j);
// 		}
// 	}
///////////////////////
	pathname=CurrentPath+"\\resource\\rightbottom.jpg";
	x=245;
	y=m_Height-33-25;
	w=25;
	h=25;
	SetBackPic(pathname,x,y,w,h,0,0,false);
	x=27;
	SetBackPic(pathname,x,y,w,h,0,0,true);

	CopyInHorizontal(m_imageBack,27,244,y,(m_Height-33),26);
	CopyInVertical(m_imageBack,4,269,(BlueLine+51),(m_Height-33-25),(BlueLine+51));
// 	for (i=2;i<27;i++)//菜单栏左下角
// 	{
// 		for (j=y;j<(m_Height-33);j++)
// 		{
// 			SetPointColour(m_imageBack,i,j,m_imageBack,(272-i),j);
// 		}
// 	}

	//中间部分
// 	for (i=27;i<245;i++)
// 	{
// 		for (j=y;j<(m_Height-33);j++)
// 		{
// 			SetPointColour(m_imageBack,i,j,m_imageBack,26,j);
// 		}
// 	}

// 	for (i=4;i<270;i++)
// 	{
// 		for (j=(BlueLine+51);j<(m_Height-33-25);j++)
// 		{
// 			SetPointColour(m_imageBack,i,j,m_imageBack,i,(BlueLine+51));
// 		}
// 	}
	/////////////////////////////////////////右边框的左下角
	CopyImageInHorizontalByMirror(m_imageBack,245,269,(m_Height-33-25),(m_Height-33),271);//镜面复制
// 	for (i=271;i<296;i++)
// 	{
// 		for (j=(m_Height-33-25);j<(m_Height-33);j++)
// 		{
// 			SetPointColour(m_imageBack,i,j,m_imageBack,(540-i),j);
// 		}
// 	}
	/////////////////////////右边框 右下角
	CopyImageInHorizontal(m_imageBack,245,270,(m_Height-33-25),(m_Height-33),m_Width-25);//平移复制

// 	for (i=0;i<25;i++)
// 	{
// 		for (j=(m_Height-33-25);j<(m_Height-33);j++)
// 		{
// 			SetPointColour(m_imageBack,(m_Width-i),j,m_imageBack,(270-i),j);
// 		}
// 	}

	/////////////////////////右边框 左上角
	CopyImageInVerticalByMirror(m_imageBack,271,296,(m_Height-33-25),(m_Height-33-2),(BlueLine+5));

// 	for (i=271;i<296;i++)
// 	{
// 		for (j=160;j<185;j++)
// 		{
// 			SetPointColour(m_imageBack,i,j,m_imageBack,i,(m_Height-33-j+159));
// 		}
// 	}
	/////////////////////////右边框 右上角	
	CopyImageInHorizontalByMirror(m_imageBack,271,296,(BlueLine+5),(BlueLine+30),m_Width-26);//镜面复制
// 	for (i=(m_Width-25);i<m_Width;i++)
// 	{
// 		for (j=160;j<185;j++)
// 		{
// 			SetPointColour(m_imageBack,i,j,m_imageBack,i,(m_Height-33-j+159));
// 		}
// 	}
	//////////////////////// 上下边框
	CopyInHorizontal(m_imageBack,296,(m_Width-25),(BlueLine+5),(BlueLine+30),295);
	CopyInHorizontal(m_imageBack,296,(m_Width-25),(m_Height-33-25),(m_Height-33),295);
// 	for (i=296;i<(m_Width-24);i++)
// 	{
// 		for (j=160;j<185;j++)
// 		{
// 			SetPointColour(m_imageBack,i,j,m_imageBack,295,j);
// 		}
// 
// 		for (j=(m_Height-33-25);j<(m_Height-33);j++)
// 		{
// 			SetPointColour(m_imageBack,i,j,m_imageBack,295,j);
// 		}
// 	}
	/////////////////////////////左右边框
	CopyInVertical(m_imageBack,270,m_Width-1,(BlueLine+29),(m_Height-33-25),(BlueLine+28));
// 	for (j=185;j<(m_Height-33-25);j++)
// 	{
// 		for (i=270;i<m_Width;i++)
// 		{
// 			SetPointColour(m_imageBack,i,j,m_imageBack,i,184);
// 		}
// 	}

	

	////////////////////////////////////////////////////菜单选项栏按钮
// 	pathname=CurrentPath+"\\resource\\01.jpg";
// 	x=400;
// 	y=49;
// 	w=21;
// 	h=34;
// 	SetBackPic(pathname,x,y,w,h,0,0);
// 
// 	for (i=(m_Width-421);i<(m_Width-400);i++)
// 	{
// 		for (j=49;j<83;j++)
// 		{
// 			SetPointColour(m_imageBack,i,j,m_imageBack,(m_Width-i),j);
// 		}
// 	}
// 
// 	for (i=421;i<(m_Width-420);i++)
// 	{
// 		for (j=49;j<83;j++)
// 		{
// 			SetPointColour(m_imageBack,i,j,m_imageBack,420,j);
// 		}
// 	}
/////////////////////////////菜单按钮

// 	int lenth=(int)((m_Width-800-81-68-55-55-42*4)/8);//分辨率横向至少 1225
// ///////////////////////////
// 	pathname=CurrentPath+"\\resource\\database01.jpg";
// 	x=400+21+lenth;
// 	y=49;
// 	w=68;
// 	h=34;
// 	SetBackPic(pathname,x,y,w,h,0,0);
// 
// 	pathname=CurrentPath+"\\resource\\matching01.jpg";
// 	x=400+21+lenth+68+lenth+21+21+lenth;
// 	y=49;
// 	w=81;
// 	h=34;
// 	SetBackPic(pathname,x,y,w,h,0,0);
// 
// 	pathname=CurrentPath+"\\resource\\statistics01.jpg";
// 	x=m_Width-400-21-lenth-55-lenth-21-21-lenth-55;
// 	y=49;
// 	w=55;
// 	h=34;
// 	SetBackPic(pathname,x,y,w,h,0,0);
// 
// 	pathname=CurrentPath+"\\resource\\account01.jpg";
// 	x=m_Width-400-21-lenth-55;
// 	y=49;
// 	w=55;
// 	h=34;
// 	SetBackPic(pathname,x,y,w,h,0,0);
	
//	Invalidate(FALSE);
}

void CLaceborderMatchingDlg::ShowText(int x,int y,BYTE R,BYTE G,BYTE B,LOGFONT lf,CString tip,bool f)
{
	CDC *pDC=GetDC();
	if (f==false)
	{
		pDC->SetBkColor(RGB(255,255,255));//设置背景色
	}
	else
	{
		pDC->SetBkMode(TRANSPARENT);
	}
	
	
	CFont font2;
	font2.CreateFontIndirect(&lf);
	pDC->SelectObject(&font2);
	pDC->SetTextColor(RGB(R,G,B));

	pDC->TextOut(x,y,tip);
	ReleaseDC (pDC);
}
void CLaceborderMatchingDlg::ShowMyText()//显示文本和提示信息
{
	CString Tip;
	LOGFONT   lf;   
	memset(&lf,   0,   sizeof(LOGFONT));   
	lf.lfHeight   =20;         //字体的高
	lf.lfWidth    =8;
	//	lf.lfWeight   = 700;         //粗体
	lstrcpy(lf.lfFaceName, _T("Verdana"));

	if (LogFlag==TRUE)
	{
// 		Tip="管理员";
// 		ShowText(200,90,0,0,0,lf,Tip);
		if (accounts[CurrentAccount].Level==true)
		{
			Tip="管理员 ";
			ShowText(m_Width-350+50,2,255,0,0,lf,Tip);//管理员为 红色
			Tip=accounts[CurrentAccount].account;
			lf.lfWeight   = 700;
			ShowText(m_Width-330,BlueLine-21,58,120,179,lf,Tip);//管理员为 红色
			lf.lfWeight   = 300;
		}
		else if (accounts[CurrentAccount].Level==false)
		{
			Tip="操作员 ";
			ShowText(m_Width-350+50,2,255,255,255,lf,Tip);//管理员为 红色
			Tip=accounts[CurrentAccount].account;
			ShowText(m_Width-330,BlueLine-21,0,0,0,lf,Tip);//管理员为 红色
		}
		Tip="注销";
		ShowText(m_Width-47,BlueLine-22,0,0,0,lf,Tip);//管理员为 红色

		if (CurrentMenu==MenuName[1]&&ShowingMatchingPicturesFlag==false)
		{
			Tip="纹理";
			ShowText(SearchByTexture_x+radio_w,SearchByTexture_y-3,0,0,0,lf,Tip);
			Tip="颜色";
			ShowText(SearchByColour_x+radio_w,SearchByColour_y-3,0,0,0,lf,Tip);
			Tip="移动";
			ShowText(Move_x+radio_w,Move_y-3,0,0,0,lf,Tip);
			Tip="裁剪";
			ShowText(Cut_x+radio_w,Cut_y-3,0,0,0,lf,Tip);
		}

		if (CurrentMenu==MenuName[3])//账户管理
		{
			if (manageAccountMenuNum==1)//修改密码
			{
				Tip="用    户    名 :  "+accounts[CurrentAccount].account;
				ShowText(350,BlueLine+100,0,0,0,lf,Tip);
				Tip="新    密    码 :";
				ShowText(350,BlueLine+150,0,0,0,lf,Tip);
				Tip="再次输入密码 :";
				ShowText(350,BlueLine+200,0,0,0,lf,Tip);

			}
			if (manageAccountMenuNum==2)//管理账户
			{
				Tip="现  有  用  户 :  ";
				ShowText(350,BlueLine+100,0,0,0,lf,Tip);
				Tip="级  别  修  改 :";
				ShowText(350,BlueLine+260,0,0,0,lf,Tip);
				Tip="删除选中用户 :";
				ShowText(350,BlueLine+180,0,0,0,lf,Tip);
				Tip="管理员";
				ShowText(500,BlueLine+260,0,0,0,lf,Tip);
				Tip="操作员";
				ShowText(600,BlueLine+260,0,0,0,lf,Tip);
			}

			if (manageAccountMenuNum==3)//创建
			{
				Tip="新  用  户  名 :  ";
				ShowText(350,BlueLine+100,0,0,0,lf,Tip);
				Tip="密          码 :";
				ShowText(350,BlueLine+150,0,0,0,lf,Tip);
				Tip="再次输入密码 :";
				ShowText(350,BlueLine+200,0,0,0,lf,Tip);
				Tip="设  置  级  别 :";
				ShowText(350,BlueLine+260,0,0,0,lf,Tip);
				Tip="管理员";
				ShowText(500,BlueLine+260,0,0,0,lf,Tip);
				Tip="操作员";
				ShowText(600,BlueLine+260,0,0,0,lf,Tip);
			}
		}

		if (CurrentMenu==MenuName[0]||ShowingMatchingPicturesFlag==true||CurrentMenu==MenuName[2])//数据库管理
		{
			if ((manageDataBaseNum==1||CurrentMenu==MenuName[2]||manageDataBaseNum==2&&showPageFlag==true)||ShowingMatchingPicturesFlag==true)
			{
				CString s;
				s.Format("%d",CurrentSubDataBasePageIndex);
				Tip="第  "+s;
				s.Format("%d",CurrentSubDataBasePageNum);
				Tip=Tip+" / "+s+" 页";

				int k=(DataBasePicture_l+DataBasePicture_r)*0.5-50;
				ShowText(k,m_Height-33-30,0,0,0,lf,Tip);
				

			}
			
		}
	}

	if (ShowOriginalPictureFlag==false)
	{
		ShowEachPictureName(CurrentSubDataBasePageIndex);
	}
	
}

void CLaceborderMatchingDlg::OnOK()//输入账号回车
{
	if (LogFlag==FALSE)
	{
		OnLoging();
	}
}


void CLaceborderMatchingDlg::OnMouseHWheel(UINT nFlags, short zDelta, CPoint pt)
{
	// 此功能要求 Windows Vista 或更高版本。
	// _WIN32_WINNT 符号必须 >= 0x0600。
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	CDialogEx::OnMouseHWheel(nFlags, zDelta, pt);
}


BOOL CLaceborderMatchingDlg::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	//滚轮控制数据库显示的图片的每个长宽
// 	if (zDelta>0)
// 	{
// 		singlePicture_w-=100;
// 		singlePicture_h=0.9*singlePicture_w;
// 		if (singlePicture_w<100)
// 		{
// 			singlePicture_w=100;
// 		}
// 		if (singlePicture_h<90)
// 		{
// 			singlePicture_h=90;
// 		}
// 		if (singlePicture_h>(DataBasePicture_b-DataBasePicture_t)/2)
// 		{
// 			singlePicture_h=(DataBasePicture_b-DataBasePicture_t)/2-6;
// 		}
// 	} 
// 	else
// 	{
// 		singlePicture_w+=100;
// 		singlePicture_h=0.9*singlePicture_w;
// 		if (singlePicture_w>(DataBasePicture_r-DataBasePicture_l)/3)
// 		{
// 			singlePicture_w=(DataBasePicture_r-DataBasePicture_l)/3-6;
// 		}
// 		if (singlePicture_h>(DataBasePicture_b-DataBasePicture_t)/2)
// 		{
// 			singlePicture_h=(DataBasePicture_b-DataBasePicture_t)/2-6;
// 		}
// 	}
// 
// 	GetDataBasePictureSize();
// 	DrawLineWindows();
//  	Invalidate(FALSE);


	if (zDelta<0&&(CurrentSubDataBasePageIndex<CurrentSubDataBasePageNum))
	{
		CurrentSubDataBasePageIndex++;
	}
	else if (zDelta>0&&(CurrentSubDataBasePageIndex>1))
	{
		CurrentSubDataBasePageIndex--;		
	}
	ShowEachPagePicture(CurrentSubDataBasePageIndex);
	GetImageDisplayData(m_dspbuf, m_imageBack, 0, 0);
	Invalidate(FALSE);
	return CDialogEx::OnMouseWheel(nFlags, zDelta, pt);
}






////////////////////////////////////////////////////////////////////////////////////////////////////可删
void CLaceborderMatchingDlg::DrawLineWindows()
{
//	GetImageDisplayData(m_dspbuf, m_imageBack, 0, 0);
	int i,j;
	int l,r,t,b;
	l=DataBasePicture_l;
	t=DataBasePicture_t;
	r=DataBasePicture_r;
	b=DataBasePicture_b;

	for (i=l;i<=r;i++)
	{
		for (j=t;j<=b;j++)
		{
			if (((i-l)%DataBasePicture_w==0)||((j-t)%DataBasePicture_h==0))
			{
//				SetPointColour(m_dspbuf,i,j,255,0,0);
				SetPointColour(m_imageBack,i,j,255,0,0);
			}
			if (i==DataBasePicture_l||i==DataBasePicture_r||j==DataBasePicture_t||j==DataBasePicture_b)
			{
//				SetPointColour(m_dspbuf,i,j,0,255,0);
				SetPointColour(m_imageBack,i,j,0,255,0);
			}

		}
	}
}


void CLaceborderMatchingDlg::OnTvnEndlabeleditTreeDatabase(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMTVDISPINFO pTVDispInfo = reinterpret_cast<LPNMTVDISPINFO>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码
	//修改数据库文件夹名

	TV_DISPINFO* pTVDI=(TV_DISPINFO*)pNMHDR;

	HTREEITEM hItem = m_tree.GetSelectedItem();  
	CString s = m_tree.GetItemText(hItem);
	CString text=pTVDI->item.pszText;

////////////////////////////////////////////////////////////  数据库 根目录与5大目录不可以修改
	if (hItem==m_tree.GetRootItem())
	{
		return;
	}
	HTREEITEM parent = m_tree.GetParentItem(hItem);
// 	if (parent==m_tree.GetRootItem())
// 	{
// 		return;
// 	}
/////////////////////////////////////////////////////////////
	if (text=="")
	{
		text=s;
	}

	int n=m_tree.GetItemData(parent);
	int kk=m_tree.GetItemData(hItem);

	int k=(int)DataBaseFileName.size();
	int i=0;
	if (k>0)
	{
		for (i=0;i<k;i++)
		{
			if ((kk!=i)&&(text==DataBaseFileName[i]))
			{
				MessageBox("该目录名已存在！");
				return;
			}
		}
	}
//	hItem = m_tree.GetSelectedItem(); 
	k=m_tree.GetItemData(pTVDI->item.hItem);
	s=DataBaseFileName[kk];
	DataBaseFileName[kk]=text;

	m_tree.SetItemText(pTVDI->item.hItem,text);

//////////////////////////////////数据库 目录文件夹名的更改////////////////////////////////////////////////	/
//	CString laceClass[5]={"大边","睫毛","面料","小边","其他"};

	s=CurrentPath+"\\DataBase\\"+/*laceClass[n]+"\\"+*/s;
	TCHAR* pOldName =s.GetBuffer(s.GetLength()); 
	text=CurrentPath+"\\DataBase\\"+/*laceClass[n]+"\\"+*/text;
	TCHAR* pNewName =text.GetBuffer(text.GetLength()); 
	CFile::Rename(pOldName, pNewName);

	GetCurrentDir(CurrentSelectedDataBasePath);
/////////////////////////////////////////////////////////////////////////////////////////

	*pResult = 0;
}


void CLaceborderMatchingDlg::OnNMRClickTreeDatabase(NMHDR *pNMHDR, LRESULT *pResult)//右键树形控件
{
	// TODO: 在此添加控件通知处理程序代码
	HTREEITEM hItem = m_tree.GetSelectedItem(); 
	HTREEITEM root=m_tree.GetRootItem();//得到根结点

	CPoint pt(0, 0);  
	::GetCursorPos(&pt); 
 	m_tree.ScreenToClient(&pt);
 	UINT nFlags=MK_RBUTTON;//右键按下
 	HTREEITEM hNewSel = m_tree.HitTest(pt, &nFlags);    // Set the selection to the item that the mouse was over when the user right clicked 
	
	if (NULL == hNewSel)  
	{   
		m_tree.SelectItem(NULL); 
		m_tree.SelectItem(root);
	}
//	else if (hItem !=hNewSel)  
	else if (hItem ==hNewSel)  
	{   
 //		m_tree.SelectItem(hNewSel);//右键与左键选中的不一样时，以右键为准
 //		m_tree.SetFocus(); 

		::GetCursorPos(&pt);

		if (hNewSel!=root)//当前节点不是根节点
		{
			hItem=m_tree.GetParentItem(hNewSel);//得到父结点。
		}
		CMenu Menu; 
		if (Menu.LoadMenu(IDR_MENU_TREE))  
		{   
			CMenu* pSubMenu = Menu.GetSubMenu(0); 
			if (hNewSel==root)
			{
				pSubMenu->EnableMenuItem(ID_DELETESUBCLASS,MF_DISABLED|MF_GRAYED);//灰化禁用
//				pSubMenu->EnableMenuItem(ID_ADDSUBCLASS,MF_DISABLED|MF_GRAYED);//灰化禁用
			}
			else
			{
//				pSubMenu->EnableMenuItem(ID_DELETESUBCLASS,MF_DISABLED|MF_GRAYED);//灰化禁用
				pSubMenu->EnableMenuItem(ID_ADDSUBCLASS,MF_DISABLED|MF_GRAYED);//灰化禁用
// 				if (hItem==root)//五个大类之一
// 				{
// 					pSubMenu->EnableMenuItem(ID_DELETESUBCLASS,MF_DISABLED|MF_GRAYED);//灰化禁用
// 				}
// 				else//子类
// 				{
// 					pSubMenu->EnableMenuItem(ID_ADDSUBCLASS,MF_DISABLED|MF_GRAYED);//灰化禁用
// 				}
			}

			if (pSubMenu!=NULL)   
			{       
				pSubMenu->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, pt.x, pt.y, this);   
			}  
		}
	}



	*pResult = 0;

// 	CString s = m_tree.GetItemText(hNewSel);
// 	LOGFONT   lf;   
// 	memset(&lf,   0,   sizeof(LOGFONT));   
// 	lf.lfHeight   =20;         //字体的高
// 	lf.lfWidth    =8;
// 	lstrcpy(lf.lfFaceName, _T("Verdana"));
// 	ShowText(100,m_Height-100,58,120,179,lf,s);
}


void CLaceborderMatchingDlg::OnAddsubclass()//添加子类
{
	HTREEITEM hItem = m_tree.GetSelectedItem();
	int n=m_tree.GetItemData(hItem);
	int k=(int)DataBaseFileName.size();
// 	if (k>4)
// 	{
// 		MessageBox("试用版 目录增加限制在5个！");
// 		return;
// 	}
	CString s;
	s.Format("%d",k);
	s="新子类  "+s;
	int i=0;
	if (k>0)
	{
		bool f=false;
		int kk=0;
		while(f==false)
		{
			f=true;
			s.Format("%d",(k+kk));
			s="新子类  "+s;

			for (i=0;i<k;i++)
			{
				if (s==DataBaseFileName[i])
				{
					kk++;
					f=false;
					break;
				}
			}
		}
		
	}
	DataBaseFileName.push_back(s);
	 k=(int)DataBaseFileName.size()-1;
	hItem=m_tree.InsertItem(s, 2, 2, hItem, TVI_LAST); 
	m_tree.SetItemData(hItem, k);

///////////////////////////////////先创建目录在选中当前新建的节点/////////////////////////////////////////////
	//新建数据库目录下文件夹
	CString path;
	GetCurrentDir(path);
	path=path+"\\"+s;
	CreateNewDir(path);

//	NewDirFlag=TRUE;
	m_tree.SelectItem(hItem);
	NewDirFlag=FALSE;//该变量已经失去作用，原来思路是先选中节点在创建目录，但是选中当前节点打开目录下的文件要求该目录必须存在，所以需要该变量标注这是第一次创建，就不打开目录下文件
/////////////////////////////////////先选中新建的节点，再创建新目录，在选中当前的节点，查看新目录 这个思路未能成功在创建后就打开显示目录下图片/////////////////////////////////////////////

// 	NewDirFlag=TRUE;
// 	m_tree.SelectItem(hItem);
// 	NewDirFlag=FALSE;
// 
// 	//新建数据库目录下文件夹
// 	CString path;
// 	GetCurrentDir(path);
// 	CreateNewDir(path);
// 	m_tree.SelectItem(hItem);
}


void CLaceborderMatchingDlg::OnDeletesubclass()
{
	HTREEITEM hItem = m_tree.GetSelectedItem();

	HTREEITEM parent = m_tree.GetParentItem(hItem);

	int n=m_tree.GetItemData(parent);
	int k=m_tree.GetItemData(hItem);

	//删除数据库目录下文件夹与文件
	CString path;
	GetCurrentDir(path);


	BOOL f=DirectoryEmpty(path,"jpg");
	if (f==TRUE)
	{
		int i;

		CString s = m_tree.GetItemText(hItem);
		for (i=0;i<(int)DataBaseFileName.size();i++)
		{
			if (s==DataBaseFileName[i])
			{
				k=i;
				break;
			}
		}

		for (i=k;i<((int)DataBaseFileName.size()-1);i++)
		{
			DataBaseFileName[i]=DataBaseFileName[i+1];
		}
		DataBaseFileName.pop_back();
		DeleteDir(path);
		m_tree.DeleteItem(hItem);//用于删除某一结点
	}
	else
	{
		MessageBox("该目录非空，不能删除！");
	}


	
}
BOOL CLaceborderMatchingDlg::DirectoryEmpty(CString path,CString format)//判断目录下面有没有.jpg格式文件，没有就是空
{
	CString tmp=path+"\\*."+format;

	WIN32_FIND_DATA FindFileData;
	HANDLE hFind;
	hFind = FindFirstFile(tmp, &FindFileData);
	if (hFind == INVALID_HANDLE_VALUE) 
	{
		return TRUE;//为空
	}
	else 
	{
		return FALSE;//不为空
	}
}

void CLaceborderMatchingDlg::GetCurrentDir(CString &path)//获取当前选中的目录的物理目录
{
	HTREEITEM hItem = m_tree.GetSelectedItem();
	CString s = m_tree.GetItemText(hItem);
	if (hItem!=m_tree.GetRootItem()&&hItem)
	{
		path="\\"+s;
// 		hItem = m_tree.GetParentItem(hItem);//得到父结点
// 
// 		if (hItem!=m_tree.GetRootItem())
// 		{
// 			s = m_tree.GetItemText(hItem);
// 			s="\\"+s+path;
// 		}
// 		else if (hItem==m_tree.GetRootItem())
// 		{
// 			s=path;
// 		}
		s=path;
	}		
	else if (hItem==m_tree.GetRootItem())
	{
		s="";
	}

	path=CurrentPath+"\\DataBase"+s;
}
void CLaceborderMatchingDlg::OnTvnSelchangedTreeDatabase(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMTREEVIEW pNMTreeView = reinterpret_cast<LPNMTREEVIEW>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码
	if (LogFlag==FALSE||LoadingPictureFlag==TRUE)
	{
		return;
	}
	HTREEITEM hItem = m_tree.GetSelectedItem();
	CString s = m_tree.GetItemText(hItem);
	if (hItem!=m_tree.GetRootItem()&&hItem)
	{
		CurrentSelectedDataBasePath="\\"+s;
// 		hItem = m_tree.GetParentItem(hItem);//得到父结点
// 
// 		if (hItem!=m_tree.GetRootItem())
// 		{
// 			s = m_tree.GetItemText(hItem);
// 			s="\\"+s+CurrentSelectedDataBasePath;
// 		}
// 		else if (hItem==m_tree.GetRootItem())
// 		{
// 			s=CurrentSelectedDataBasePath;
// 		}
		s=CurrentSelectedDataBasePath;

//		MessageBox("当前选中 "+CurrentSelectedDataBasePath);
	}		
	else if (hItem==m_tree.GetRootItem())
	{
		s="";
	}
	
	CurrentSelectedDataBasePath=CurrentPath+"\\DataBase"+s;

	GetCurrentDir(CurrentSelectedDataBasePath);
	if (CurrentMenu==MenuName[0]&&manageDataBaseNum==1&&NewDirFlag==FALSE)
	{
		ShowCurrentDataBasePicture(CurrentSelectedDataBasePath);
		DrawLineWindows();
		GetImageDisplayData(m_dspbuf, m_imageBack, 0, 0);
		CRect tmp;
		tmp.left=DataBasePicture_l;
		tmp.right=DataBasePicture_r;
//		tmp.top=DataBasePicture_t;
//		tmp.bottom=DataBasePicture_b;
		tmp.top=0;
		tmp.bottom=m_Height;
		InvalidateRect(tmp,FALSE);
	}
// 	else 
// 	{
// 		GetCurrentDir(CurrentSelectedDataBasePath);
// 	}

	*pResult = 0;
}


void CLaceborderMatchingDlg::OnClose()
{
	if (CreatingDataBaseFlag==TRUE)
	{
		AfxMessageBox("正在创建数据库，为确保创建成功请勿退出软件！！");
		return;
	}

	// TODO: 在此添加消息处理程序代码和/或调用默认值
	CString Path=CurrentPath+"\\system\\DataBase.bin";//保存账户信息
	char *filePath;
	filePath=Path.GetBuffer(Path.GetLength());

	WriteDataBaseInformation(filePath);


	DeleteSetUpFile();//删除安装信息

// 	if (accounts[CurrentAccount].Level==true)
// 	{
// 		if (TRUE==TipBeforeClose())
// 		{
// 			return;
// 		}
// 		//退出前的提示
// 	}
	
	CDialogEx::OnClose();
}

void CLaceborderMatchingDlg::GetPointInPicture(int &x,int &y,CPoint &point)//获取裁剪区域两端点在原图的坐标
{
	int H,W;//屏幕显示的图片左上角坐标
	int h,w;//有效区长宽
	int picture_h,picture_w;
	char *path = (LPSTR)(LPCTSTR)CurrentPicturePath;
	IplImage* temp = cvLoadImage(path,CV_LOAD_IMAGE_COLOR);

	picture_h=temp->height;
	picture_w=temp->width;
	cvReleaseImage(&temp);

	w=ShowPicture_r-ShowPicture_l;
	h=ShowPicture_b-ShowPicture_t;


	if (picture_h<h)
	{
		if (picture_w<w)
		{
			H=ShowPicture_t+(h-picture_h)/2;
			W=ShowPicture_l+(w-picture_w)/2;

		} 
		else
		{
			H=ShowPicture_t+(h-picture_h)/2;
			W=ShowPicture_l;
		}
	} 
	else
	{
		if (picture_w<w)
		{
			H=ShowPicture_t;
			W=ShowPicture_l+(w-picture_w)/2;

		} 
		else
		{
			H=ShowPicture_t;
			W=ShowPicture_l;
	
		}
	}

	x=point.x-W+current_hpos;
	y=point.y-H+current_vpos;

	if (x<0)
	{
		x=0;
	}
	if (x>picture_w)
	{
		x=picture_w-1;
	}
	if (y<0)
	{
		y=0;
	}
	if (y>picture_h)
	{
		y=picture_h-1;
	}
	
}

void CLaceborderMatchingDlg::SureCutPicture()//确定裁剪图片
{
	int l,r,t,b;
// 	GetPointInPicture(l,t,CutLeftTopPoint);
// 	GetPointInPicture(r,b,CutRightBottomPoint);

	GetPointInPicture(l,t,leftTop);
	GetPointInPicture(r,b,RightButom);
	if (leftTop==RightButom)
	{
		return;
	}
	IplImage* image=cvCreateImage(cvSize(r-l+1,b-t+1),8,3);

	char *path = (LPSTR)(LPCTSTR)CurrentPicturePath;
	IplImage* temp = cvLoadImage(path,CV_LOAD_IMAGE_COLOR);

	int i,j;
	for (i=0;i<image->width;i++)
	{
		for (j=0;j<image->height;j++)
		{
			SetPointColour(image,i,j,temp,i+l,j+t);
		}		
	}

	CurrentPicturePath=CurrentPath+"\\Cuted.jpg";
	cvSaveImage(CurrentPicturePath,image);
	cvReleaseImage(&image);
	cvReleaseImage(&temp);

	leftTop.x=0;
	leftTop.y=0;
	RightButom=leftTop;
}

void CLaceborderMatchingDlg::OnEditPictureInfo()
{

	CPictureInfo EditingPicture;
	EditingPicture.DoModal();
	// TODO: 在此添加命令处理程序代码
}

void CLaceborderMatchingDlg::OnWritePictureInfo(vector<CString> & info)
{
	//重命名  info第一个值  修改保存当前所有图片的容器的变量  改完名字后立即改保存信息的文件名 即infoPath
	CString picturename;
	CString tmpPath;
	picturename=CurrentSelectedPicturePath;
	int pos =picturename.ReverseFind('\\')+1;
	tmpPath=picturename.Left(pos);
	picturename =picturename.Right(picturename.GetLength()-pos);
	picturename=picturename.Left(picturename.GetLength()-4);//文件格式不可以改

	if (picturename!=info[0])//不等于则 说明已经更名
	{
		if (PathFileExists(CurrentSelectedPicturePath+"Info.dat"))//对之前的保存图片信息的文件删除
		{
			CString tmp=CurrentSelectedPicturePath+"Info.dat";
			TCHAR* filename =tmp.GetBuffer(tmp.GetLength()); 
			DeleteFile(filename);
		}
		CurrentSelectedPicturePath=tmpPath+info[0]+".jpg";
		OnReNamePicture(CurrentSelectedPicturePath);//对当前选中的图片重命名
	}



	CString infoPath=CurrentSelectedPicturePath+"Info.dat";

	int n=(int)info.size();
	int i;

	FILE *File=NULL;

	fopen_s(&File,infoPath,"wb");//为读写建立新的二进制文件
	if (File)//打开成功
	{
		for (i=1;i<n;i++)//读取的修改后的文件名不再保存
		{
			WriteCString(info[i],File);
		}		
	}
	//////////////////////////////////////////////////////////////////////////
	WriteCString("Adonis  2014",File);//账户结束标志 禁止用户注册这个用户名
	//////////////////////////////////////////////////////////
	fclose(File);
	File=NULL;

	ClearCStringVector(info);
}

void CLaceborderMatchingDlg::OnReadPictureInfo(vector<CString> & info)
{
	CString infoPath=CurrentSelectedPicturePath+"Info.dat";
	CString tmp;

	FILE *File=NULL;

	if (PathFileExists(infoPath))
	{
		fopen_s(&File,infoPath,"rb");//为读写建立新的二进制文件
		if (File)//打开成功
		{
			while(!feof(File))//(feof返回1，说明到文件结尾)
			{
				ReadCString(tmp,File);
				if ("Adonis  2014"==tmp)
				{
					break;
				}
				info.push_back(tmp);
			}	
			fclose(File);
		}
	}
	
	File=NULL;
}

void CLaceborderMatchingDlg::OnReNamePicture(CString newName)//对当前选中的图片重命名
{	
	CString path=CurrentDataBasePicturePath[CurrentSelectedPictureIndexNum];//当前被改名的图片路径

	/////////////////////////////////调用这个函数 说明确定会改名  那么保存这张图片feature数的文件就必须改名/////////////////////////////////////////////////////////
	CString DirPath;
	GetCurrentDir(DirPath);//获取当前选中的目录
	CString FilePath=DirPath+"\\FeatureCounts.Date";

	if (PathFileExists(FilePath))
	{
		char *filePath;
		filePath=FilePath.GetBuffer(FilePath.GetLength());
		vector<MY_DATE>dateData;//日期文件信息

		ReadDateFile(dateData,filePath);//读取日期文件

		for (int j=0;j<(int)dateData.size();j++)
		{
			if (path==dateData[j].Name)
			{
				dateData[j].Name=newName;
				break;
			}
		}

		WriteDateFile(dateData,filePath);//更新日期文件

		if (!dateData.empty())
		{
			dateData.clear();
			vector<MY_DATE>().swap(dateData);
		}

	}
	/////////////////////////////////////////////////////////////////////////////////////////

	CurrentDataBasePicturePath[CurrentSelectedPictureIndexNum]=newName;

	CString s=path;
	TCHAR* pOldName =s.GetBuffer(s.GetLength()); 
	CString text=newName;
	TCHAR* pNewName =text.GetBuffer(text.GetLength()); 
	CFile::Rename(pOldName, pNewName);

	path=path+"_Features.txt";
	if (PathFileExists(path))
	{
		pOldName =path.GetBuffer(path.GetLength()); 
		text=newName+"_Features.txt";
		pNewName =text.GetBuffer(text.GetLength()); 
		CFile::Rename(pOldName, pNewName);
	}
	

}

void CLaceborderMatchingDlg::OnSavePictureAs()//导出图片另存为
{

	TCHAR DeskToppath[255];
	SHGetSpecialFolderPath(0,DeskToppath,CSIDL_DESKTOPDIRECTORY,0);//获取桌面路径


	CString picturename=CurrentSelectedPicturePath;
	int pos =picturename.ReverseFind('\\')+1;
	picturename =picturename.Right(picturename.GetLength()-pos);

	char *pictureName=(LPSTR)(LPCTSTR)picturename;;
	strcat(DeskToppath,"\\");
	strcat(DeskToppath,pictureName);

	CFileDialog fd(FALSE,NULL, DeskToppath, OFN_NOCHANGEDIR);//参数3 表示默认的文件名（路径）
	fd.m_ofn.lpstrFilter = "(*.jpg) \0*.jpg\0\0";
	fd.m_ofn.lpstrDefExt = "jpg";

	CString Newpath;

	if( IDOK == fd.DoModal() )
	{
		Newpath = fd.GetPathName();

		char *path;
		IplImage* temp=NULL;
		path = (LPSTR)(LPCTSTR)CurrentSelectedPicturePath;
		cvReleaseImage(&temp);
		temp = cvLoadImage(path,CV_LOAD_IMAGE_COLOR);
		if (!temp)
		{
			return;
		}
		if (Newpath.Right(4)!=".jpg")
		{
			Newpath+=".jpg";
		}
		cvSaveImage(Newpath,temp);
		cvReleaseImage(&temp);
	}
	else
	{
		return;
	}
}
void CLaceborderMatchingDlg::OnDeletePictureFromDataBase()//从数据库删除图片
{
	// TODO: 在此添加命令处理程序代码
	CString picturename=CurrentSelectedPicturePath;
	TCHAR* filename =picturename.GetBuffer(picturename.GetLength()); 
	DeleteFile(filename);

	picturename=CurrentSelectedPicturePath+"_Features.txt";//特征文件
	if (PathFileExists(picturename))
	{
		filename =picturename.GetBuffer(picturename.GetLength()); 
		DeleteFile(filename);


//		特征文件存在 则FeatureCounts.Date文件必存在 特征文件必须修改
		CString DirPath;
		GetCurrentDir(DirPath);//获取当前选中的目录
		CString FilePath=DirPath+"\\FeatureCounts.Date";

		if (PathFileExists(FilePath))
		{
			char *filePath;
			filePath=FilePath.GetBuffer(FilePath.GetLength());
			vector<MY_DATE>dateData;//日期文件信息

			ReadDateFile(dateData,filePath,false);//读取再立即写，相当于删除不存在的文件名记录

			int pictureIndex;

			for (int j=0;j<(int)dateData.size();j++)
			{
				if (CurrentSelectedPicturePath==dateData[j].Name)
				{
					pictureIndex=j;
					break;
				}
			}
			for (int j=pictureIndex;j<((int)dateData.size()-1);j++)
			{
				dateData[j].Name=dateData[j+1].Name;
				dateData[j].Sum=dateData[j+1].Sum;
			}
			dateData.pop_back();

			WriteDateFile(dateData,filePath);//更新日期文件

			if (!dateData.empty())
			{
				dateData.clear();
				vector<MY_DATE>().swap(dateData);
			}

		}

	}
	

	picturename=CurrentSelectedPicturePath+"Info.dat";//特征文件
	if (PathFileExists(picturename))
	{
		filename =picturename.GetBuffer(picturename.GetLength()); 
		DeleteFile(filename);
	}


	//当前目录 ，上一级目录，以及根目录 图片增减文件修改
	CreatePictureAddAndDeleteFlagFile();//创建数据库增减标记文件
	//删除保存特征个数文件记录


	//重新显示当前数据界面
	ShowCurrentDataBasePicture(CurrentSelectedDataBasePath);
	DrawLineWindows();
	GetImageDisplayData(m_dspbuf, m_imageBack, 0, 0);
	CRect tmp;
	tmp.left=DataBasePicture_l;
	tmp.right=DataBasePicture_r;

	tmp.top=0;
	tmp.bottom=m_Height;
	InvalidateRect(tmp,FALSE);
	
}


void CLaceborderMatchingDlg::OnCutPictureFromDataBase()//剪切
{
	CutPicturePath=CurrentSelectedPicturePath;
}


void CLaceborderMatchingDlg::OnPastePictureFromDataBase()//粘贴
{
	if (CutPicturePath!="")
	{
		CString CurrentDBPath=CurrentSelectedDataBasePath;
		//	int pos =CurrentDBPath.ReverseFind('\\')+1;
		CurrentDBPath =CurrentSelectedDataBasePath+"\\";//选择粘贴当前目录 含有"\\"

		//将剪切 路径下的文件盒对应的特征文件移动过来
		CString picturename=CutPicturePath;
		int pos =picturename.ReverseFind('\\')+1;
		picturename =picturename.Right(picturename.GetLength()-pos);//获取剪切的图片名

		picturename=CurrentDBPath+picturename;//新路径

		MoveFile(CutPicturePath, picturename);

		CString src,dst;

		src=CutPicturePath+"_Features.txt";//特征文件
		if (PathFileExists(src))
		{
			dst=picturename+"_Features.txt";
			MoveFile(src, dst);

			MY_DATE  tmpdateData;
			CString DirPath;
			CString FilePath;
//读取原来目录下的FeatureCounts.Date文件，并将原来的信息删除

			pos =src.ReverseFind('\\');
			DirPath =src.Left(pos);//获取目录名
			FilePath=DirPath+"\\FeatureCounts.Date";//原路径特征文件存在，则FeatureCounts.Date文件必存在

			char *filePath;
			filePath=FilePath.GetBuffer(FilePath.GetLength());
			vector<MY_DATE>dateData;//日期文件信息

			ReadDateFile(dateData,filePath,false);//读取的时候，如果路径表示的文件已经不存在，则不会加入容器，相当于删除

			int pictureIndex;

			for (int j=0;j<(int)dateData.size();j++)
			{
				if (CurrentSelectedPicturePath==dateData[j].Name)
				{
					tmpdateData.Name=dateData[j].Name;
					tmpdateData.Sum=dateData[j].Sum;
					pictureIndex=j;
					break;
				}
			}
			for (int j=pictureIndex;j<((int)dateData.size()-1);j++)
			{
				dateData[j].Name=dateData[j+1].Name;
				dateData[j].Sum=dateData[j+1].Sum;
			}
			dateData.pop_back();

			WriteDateFile(dateData,filePath);//更新日期文件

			if (!dateData.empty())
			{
				dateData.clear();
				vector<MY_DATE>().swap(dateData);
			}
//修改当前目录下的FeatureCounts.Date文件

			GetCurrentDir(DirPath);//获取当前选中的目录
			FilePath=DirPath+"\\FeatureCounts.Date";

	//			vector<MY_DATE>dateData;//日期文件信息
//			char *filePath;
			filePath=FilePath.GetBuffer(FilePath.GetLength());

			if (PathFileExists(FilePath))
			{
				ReadDateFile(dateData,filePath);//读取日期文件
			}
			dateData.push_back(tmpdateData);
			
			WriteDateFile(dateData,filePath);//更新日期文件

			if (!dateData.empty())
			{
				dateData.clear();
				vector<MY_DATE>().swap(dateData);
			}

///////////////////////////////////////////////////////////////////////
		}

		src=CutPicturePath+"Info.dat";//特征文件
		if (PathFileExists(src))
		{
			dst=picturename+"Info.dat";
			MoveFile(src, dst);
		}


		//修改原路径的 增减文件
		CString DirPath;

		pos =src.ReverseFind('\\');
		DirPath =src.Left(pos);//获取剪切的图片名

		CString FilePath=DirPath+"\\AddAndDelete";

		if (!PathFileExists(FilePath))
		{
			char *filePath;
			filePath=FilePath.GetBuffer(FilePath.GetLength());
			FILE *File=NULL;

			fopen_s(&File,filePath,"wb");//为读写建立新的二进制文件
			if (File)//打开成功
			{
				fclose(File);		
			}

		}


		//修改当前路径的 增减文件
		CreatePictureAddAndDeleteFlagFile();//创建数据库增减标记文件
		//将保存图片特征个数的文件读取，写到新的目录


		CutPicturePath="";//清空剪切
		//重新显示当前数据界面

		ShowCurrentDataBasePicture(CurrentSelectedDataBasePath);
		DrawLineWindows();
		GetImageDisplayData(m_dspbuf, m_imageBack, 0, 0);
		CRect tmp;
		tmp.left=DataBasePicture_l;
		tmp.right=DataBasePicture_r;
		tmp.top=0;
		tmp.bottom=m_Height;
		InvalidateRect(tmp,FALSE);
	}


}


HBRUSH CLaceborderMatchingDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  在此更改 DC 的任何特性
	if (nCtlColor == CTLCOLOR_STATIC)//控件属性设置  Simple为 true
	{
		pDC->SetTextColor(RGB(0,0,0));
		pDC->SetBkMode(TRANSPARENT);
//		pDC->SetBkColor(RGB(0,0,0));
	}
	// TODO:  如果默认的不是所需画笔，则返回另一个画笔
	return hbr;
}


void CLaceborderMatchingDlg::ShowEachPictureName(int num)//显示每一页的图片 搜索频率和文件名
{
	int i,n;
	int x,y;
	int k;
	if (num>0)
	{
		k=(num-1)*DataBasePicture_x_num*DataBasePicture_y_num;
	}
	else if (num==0)
	{
		k=0;
	}
	if (CurrentSubDataBasePageNum>num)
	{
		n=DataBasePicture_x_num*DataBasePicture_y_num;
	}
	else if (CurrentSubDataBasePageNum==num)
	{

		n=(int)CurrentDataBasePicturePath.size()-k;
	}

	for (i=0;i<n;i++)
	{

		x=i%DataBasePicture_x_num*DataBasePicture_w+DataBasePicture_l;
		y=i/DataBasePicture_x_num*DataBasePicture_h+DataBasePicture_t;
		if ((i+k)<(int)CurrentDataBasePicturePath.size())
		{		
			CString Tip;
			LOGFONT   lf;   
			memset(&lf,   0,   sizeof(LOGFONT));   
			lf.lfHeight   =12;         //字体的高
			lf.lfWidth    =5;
			lstrcpy(lf.lfFaceName, _T("Verdana"));

			if (CurrentMenu==MenuName[2]||ShowingMatchingPicturesFlag==true||(manageDataBaseNum==1&&CurrentMenu==MenuName[0]))
			{
				Tip=CurrentDataBasePicturePath[i+k];
				int pos =Tip.ReverseFind('\\')+1;
				Tip =Tip.Right(Tip.GetLength()-pos);//获取图片名

				ShowText(x+3,y+2,0,0,0,lf,Tip,false);
			}

			if (CurrentMenu==MenuName[2])
			{

				lf.lfHeight   =18;         //字体的高
				lf.lfWidth    =8;

				Tip.Format("%d",DateNum[i+k]);
				
				Tip="总共被检索："+Tip;
				Tip=Tip+" 次";
				ShowText(x+3,y+15,0,0,0,lf,Tip,false);
			}

			if (ShowingMatchingPicturesFlag==true)
			{
				
				lf.lfHeight   =18;         //字体的高
				lf.lfWidth    =8;

				Tip.Format("%f",100*SimilarityRate[i+k]);
				MakeFixLenString(Tip,2);
				Tip="匹配度："+Tip;
				Tip=Tip+" %";
//				ShowText(x+3,y+15,0,0,0,lf,Tip,false);
			}

		}

	}
}
void CLaceborderMatchingDlg::MakeFixLenString(CString &str, int len)
{
	CString tmp = str;

	int p,l;
	p = tmp.Find(".");
	tmp.Delete(0, p+1);
	l = tmp.GetLength();

	if( l < len )
		str += "0";
	else
	{
		str=str.Left(p+2+1);
	}
}

void CLaceborderMatchingDlg::SaveReport()
{
	TCHAR DeskToppath[255];
	SHGetSpecialFolderPath(0,DeskToppath,CSIDL_DESKTOPDIRECTORY,0);//获取桌面路径

	char *pictureName=(LPSTR)(LPCTSTR)CurrentPictureName;;
	strcat(DeskToppath,"\\");
	strcat(DeskToppath,pictureName);
	strcat(DeskToppath,"_Report.doc");

	CFileDialog fd(FALSE,NULL, DeskToppath, OFN_NOCHANGEDIR);//参数3 表示默认的文件名（路径）
	fd.m_ofn.lpstrFilter = "(*.doc) \0*.doc\0\0";
	fd.m_ofn.lpstrDefExt = "doc";

	if( IDOK == fd.DoModal() )
	{
		CString path = fd.GetPathName();
		CreateReport(path);
	}
	else
	{
		return;
	}
}

void CLaceborderMatchingDlg::CreateReport(CString path)
{
	FILE *fp;
	char *p1;

	p1=path.GetBuffer(path.GetLength());//保存 报告的路径

	fopen_s(&fp,p1,"w");

	if (!fp)
	{
		MessageBox("请确认文件是否正在被其他程序使用或者文件已损坏！！");
		return;
	}

	CString s;



	WriteReport("<html xmlns=\"http://www.w3.org/1999/xhtml\"><body>",fp);

	s="<p><a style=\"text-indent:2em;font-size:1em;font-weight: 900;color:black;\">当前检测文件名：</a><a>"+CurrentPictureName+"</a></p>";//文件名
// 	WriteReport(s,fp);
// 	s=CurrentPictureName;
// 	s="<a>"+s+"</a></p>";
	WriteReport(s,fp);


	WriteReport("<br />",fp);
	WriteReport("<br />",fp);

	WriteReport("<br />",fp);
	WriteReport("<br />",fp);



	int n=(int)SimilarityRate.size();
	s.Format("%d",n);
	s="前 "+s+" 幅匹配结果：";
	WriteReport("\n<p><a style=\"font-size:1em;font-weight: 900;color:black;\">"+s+"</a></p>",fp);


	CString Tip;

	s="";
	for (int l1=0;l1<n;l1++)
	{
		s.Format("%d",l1);
		s="[ "+s+" ]  ";

		Tip=CurrentDataBasePicturePath[l1];
		int pos =Tip.ReverseFind('\\')+1;
		Tip =Tip.Right(Tip.GetLength()-pos);//获取图片名

		s+=Tip;

		Tip.Format("%f",100*SimilarityRate[l1]);
		MakeFixLenString(Tip,2);
		Tip="匹配度："+Tip;
		Tip=Tip+" %";
		Tip="  "+Tip;
		s+=Tip;
		WriteReport("\n<p><a style=\"font-size:1em;font-weight: 900;color:black;\">"+s+"</a></p>",fp);

// 		Tip.Format("%f",100*SimilarityRate[l1]);
// 		MakeFixLenString(Tip,2);
// 		Tip="匹配度："+Tip;
//		s="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+Tip+" %";
// 		s="";
// 		for (int l2=0;l2<7;l2++)
// 		{
// 			s="&nbsp;"+s;
// 		}
// 		s=s+Tip+" %";
//		WriteReport("\n<p><a style=\"font-size:1em;font-weight: 900;color:black;\">"+s+"</a></p>",fp);

	}

	

	WriteReport("\n<p>.................................................................................................</p>\n",fp);

	WriteReport("\n</body></html>\n",fp);

	fclose(fp);









// 
// 	WriteReport("<html xmlns=\"http://www.w3.org/1999/xhtml\"><body>",fp);
// 
// 
// 	s="<p><a style=\"text-indent:2em;font-size:1em;font-weight: 900;color:black;\">当前检测文件名：</a>";//文件名
// 	WriteReport(s,fp);
// 	s=CurrentPictureName;
// 	int l=5;
// 
// 
// 	for (int l1=0;l1<l;l1++)
// 	{
// 		s+="&nbsp;";
// 	}
// 	s="<a>"+s+"</a></p>";
// 	WriteReport(s,fp);
// 
// 
// 	WriteReport("<br />",fp);
// 	WriteReport("<br />",fp);
// 
// 
// 
// 	int n=(int)SimilarityRate.size();
// 	s.Format("%d",n);
// 	s="前 "+s+" 幅匹配结果：";
// 	WriteReport("\n<p><a style=\"font-size:1em;font-weight: 900;color:black;\">"+s+"</a></p>",fp);
// 
// 	CString Tip;
// 	int l1;
// 
// 	for (l1=0;l1<n;l1++)
// 	{
// // 		s.Format("%d",l1);
// // 		s="[ "+s+" ]  ";
// // 
// // 		Tip=CurrentDataBasePicturePath[l1];
// // 		int pos =Tip.ReverseFind('\\')+1;
// // 		Tip =Tip.Right(Tip.GetLength()-pos);//获取图片名
// // 
// // 		s+=Tip;
// // 		WriteReport("\n<p><a style=\"font-size:1em;font-weight: 900;color:black;\">"+s+"</a></p>",fp);
// // 
// // 		Tip.Format("%f",100*SimilarityRate[l1]);
// // 		MakeFixLenString(Tip,2);
// // 		Tip="匹配度："+Tip;
// // 		s=Tip+" %";
// // 		WriteReport("\n<p><a style=\"font-size:1em;font-weight: 900;color:black;\">"+s+"</a></p>",fp);
// 
// // 		WriteReport("\n<p><a style=\"font-size:1em;font-weight: 900;color:black;\">搜索条件：图片数：</a>",fp);
// // 		s.Format("%d",1);
// // 		s="<a>"+s+"</a></p>";
// // 		WriteReport(s,fp);
// 	}
// 
// 
// 
// 
// 
// 	WriteReport("\n</body></html>\n",fp);
// 
// 	fclose(fp);

}
void CLaceborderMatchingDlg::WriteReport(CString s,FILE *fp)
{
	char *a;
	int j=0;
	int k;//存储着大于相似阈值的图片序号

	s=s+"\n";
	a=s.GetBuffer(s.GetLength());
	k=(int)strlen(a);
	for (j=0;j<k;j++)
	{
		fwrite(&a[j],sizeof(char),1,fp);
	}
}

void CLaceborderMatchingDlg::SaveEachPictureFeatureCounts(CString PicturePath,int FeatureCounts)//保存每个图片HS特征个数
{
	//如果当前图片所在目录下已存在 记录特征个数的文件，就读取这个文件 看是否有当前图片的记录，没有的话就记录

	CString DirPath=PicturePath;
	int pos =PicturePath.ReverseFind('\\');
	DirPath =DirPath.Left(pos);//获取图片所在目录 

	CString FeatureCountsPath=DirPath+"\\FeatureCounts.Date";//保存特征数的文件
	char *filePath;
	filePath=FeatureCountsPath.GetBuffer(FeatureCountsPath.GetLength());


	MY_DATE tmp_date;
	vector<MY_DATE>dateData;//日期文件信息

	if (PathFileExists(filePath))
	{
		ReadDateFile(dateData,filePath);//读取日期文件
		bool f=false;
		for (int j=0;j<(int)dateData.size();j++)
		{
			if (PicturePath==dateData[j].Name)//当前图片特征记录已经存在 则跟新最新特征记录
			{
				dateData[j].Sum=FeatureCounts;
				f=true;
				break;
			}
		}
		if (f==false)//当前图片之前没有记录，则新增记录
		{
			tmp_date.Name=PicturePath;
			tmp_date.Sum=FeatureCounts;
			dateData.push_back(tmp_date);
		}
	}
	else //特征记录文件不存在则是第一次创建，将所有图片特征都记录
	{
		tmp_date.Name=PicturePath;
		tmp_date.Sum=FeatureCounts;
		dateData.push_back(tmp_date);
	}

	WriteDateFile(dateData,filePath);//将特征记录保存为文件

	if (!dateData.empty())
	{
		dateData.clear();
		vector<MY_DATE>().swap(dateData);
	}

}

void CLaceborderMatchingDlg::CreatePictureAddAndDeleteFlagFile()//创建数据库增减标记文件
{
	CString DirPath;
	GetCurrentDir(DirPath);//获取当前选中的目录
	CString FilePath=DirPath+"\\AddAndDelete";

	if (!PathFileExists(FilePath))
	{
		char *filePath;
		filePath=FilePath.GetBuffer(FilePath.GetLength());
		FILE *File=NULL;

		fopen_s(&File,filePath,"wb");//为读写建立新的二进制文件
		if (File)//打开成功
		{
			fclose(File);		
		}
		
	}
}

void CLaceborderMatchingDlg::DeletePictureAddAndDeleteFlagFile(CString DirPath)//删除数据库增减标记文件
{
	CString FilePath=DirPath+"\\AddAndDelete";

	if (PathFileExists(FilePath))
	{		
		TCHAR* filename =FilePath.GetBuffer(FilePath.GetLength()); 
		DeleteFile(filename);
	}
}

void CLaceborderMatchingDlg::MoveResourceForSetUp()//安装结束后，将当前目录的图片移到resource文件夹下
{
	CString PictureName[57]=
	{
		"account.jpg",
		"account01.jpg",
		"account02.jpg",
		"bottom.jpg",
		"bottomright.jpg",
		"changePassword01.jpg",
		"changePassword02.jpg",
		"copyright.jpg",
		"createaccount01.jpg",
		"createaccount02.jpg",
		"currentPath.jpg",
		"DataBase_Create.jpg",
		"DataBase_Update.jpg",
		"database01.jpg",
		"database02.jpg",
		"databaseCreate01.jpg",
		"databaseCreate02.jpg",
		"databaseView01.jpg",
		"databaseView02.jpg",
		"GoBack.jpg",
		"leftTitle.jpg",
		"Level_false.jpg",
		"Level_true.jpg",
		"loadPic.jpg",
		"log.jpg",
		"LogOut.jpg",
		"manageaccount_Bottom.jpg",
		"manageaccount_top.jpg",
		"manageaccount01.jpg",
		"manageaccount02.jpg",
		"matching01.jpg",
		"matching02.jpg",
		"menuIco.jpg",
		"middle.jpg",
		"next_page.jpg",
		"password.jpg",
		"pictureLoading01.jpg",
		"pictureLoading02.jpg",
		"radio01.jpg",
		"radio02.jpg",
		"rightbottom.jpg",
		"Rightwelcome.jpg",
		"saveReport.jpg",
		"search.jpg",
		"statistics01.jpg",
		"statistics02.jpg",
		"sure.jpg",
		"SureCut.jpg",
		"testSet.jpg",
		"testSet_left.jpg",
		"title.jpg",
		"title_left01.jpg",
		"title_left02.jpg",
		"title_left03.jpg",
		"UpLoadAllPicture.jpg",
		"UpLoadSinglePicture.jpg",
		"welcome.jpg"
	};

	CString srcPath,dstPath;

	srcPath=CurrentPath+"\\";
	dstPath=CurrentPath+"\\resource\\";

	int i;
	for (i=0;i<57;i++)
	{
		if (PathFileExists(dstPath+PictureName[i]))
		{
			if (PathFileExists(srcPath+PictureName[i]))
			{
				CString FilePath=srcPath+PictureName[i];
				TCHAR* filename =FilePath.GetBuffer(FilePath.GetLength()); 
				DeleteFile(filename);
			}
		}
		else if (!PathFileExists(dstPath+PictureName[i]))
		{
			if (PathFileExists(srcPath+PictureName[i]))
			{
				CString src=srcPath+PictureName[i];

				CString dst=dstPath+PictureName[i];

				MoveFile(src, dst);
			}
		}
	}

////////训练库//////////////////////////////////////////////////////

	srcPath=CurrentPath+"\\";
	dstPath=CurrentPath+"\\tranning\\tran.{208D2C60-3AEA-1069-A2D7-08002B30309D}\\";

	CString tmpPic;
	for (i=0;i<500;i++)
	{
		tmpPic.Format("%03d",i+1);
		tmpPic="0002_tran_n"+tmpPic+".jpg";
		if (PathFileExists(dstPath+tmpPic))
		{
			if (PathFileExists(srcPath+tmpPic))
			{
				CString FilePath=srcPath+tmpPic;
				TCHAR* filename =FilePath.GetBuffer(FilePath.GetLength()); 
				DeleteFile(filename);
			}
		}
		else if (!PathFileExists(dstPath+tmpPic))
		{
			if (PathFileExists(srcPath+tmpPic))
			{
				CString src=srcPath+tmpPic;

				CString dst=dstPath+tmpPic;

				MoveFile(src, dst);
			}
		}
	}

///////////////////////////////////////////////
	srcPath=CurrentPath+"\\";
	dstPath=CurrentPath+"\\testing\\test.{208D2C60-3AEA-1069-A2D7-08002B30309D}\\";

	for (i=0;i<20;i++)
	{
		tmpPic.Format("%03d",i+1);
		tmpPic="0001_test_n"+tmpPic+".jpg";
		if (PathFileExists(dstPath+tmpPic))
		{
			if (PathFileExists(srcPath+tmpPic))
			{
				CString FilePath=srcPath+tmpPic;
				TCHAR* filename =FilePath.GetBuffer(FilePath.GetLength()); 
				DeleteFile(filename);
			}
		}
		else if (!PathFileExists(dstPath+tmpPic))
		{
			if (PathFileExists(srcPath+tmpPic))
			{
				CString src=srcPath+tmpPic;

				CString dst=dstPath+tmpPic;

				MoveFile(src, dst);
			}
		}
	}
}


BOOL CLaceborderMatchingDlg::PreTranslateMessage(MSG* pMsg)
{
//	TestFlashDrive();


	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_LEFT)
	{
		ShowPageByKeyBoard(false);//上一张
	}
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_RIGHT)
	{
		ShowPageByKeyBoard(true);//上一张
	}
		
	return CDialog::PreTranslateMessage(pMsg);
}
void CLaceborderMatchingDlg::TestFlashDrive()
{
	CString text;

	char *cData;
	cData=new char[100];

	int iLen=100;

	BOOL f=ReadData(cData, iLen);

	text.Format(_T("%s"), cData);
	text=text.Left(text.GetLength()-1);//为加密情况下直接读取

// 	if (f==TRUE)
// 	{
// 		MessageBox(text);
// 	}
// 	else
// 	{
// 		MessageBox("读取失败！！");
// 	}

	text="";
	char c;
	int k=(int)strlen(cData);
	int i;
	for (i=0;i<k;i++)
	{
		if (i<(k-1))
		{
			c=cData[k-2-i]+7;
			text+=c;
		}

	}


	delete []cData;

	if (f==TRUE)
	{
		
		if (text!=AntiCounterfeitingLabel)
		{
			AfxMessageBox("请插入带有 刻维花边匹配系统防伪标识码 的U盘！！ ");
			exit(-1);
		}
	}
	else
	{
		AfxMessageBox("请插入带有 刻维花边匹配系统防伪标识码 的U盘！！ ");
		exit(-1);
	}
}
void CLaceborderMatchingDlg::ShowPictureByKeyBoard(bool f)
{
	if (f==true)//下一张
	{
		CurrentSelectedPictureIndexNum++;
	}
	else if (f==false)//上一张
	{
		CurrentSelectedPictureIndexNum--;
	}
	if (CurrentSelectedPictureIndexNum<0)
	{
		CurrentSelectedPictureIndexNum=0;
	}
	if (CurrentSelectedPictureIndexNum>=(int)CurrentDataBasePicturePath.size())
	{
		CurrentSelectedPictureIndexNum=(int)CurrentDataBasePicturePath.size()-1;
	}

	CurrentSelectedPicturePath=CurrentDataBasePicturePath[CurrentSelectedPictureIndexNum];
	CShowPicture newPicture;
	newPicture.DoModal();
}

void CLaceborderMatchingDlg::ShowPageByKeyBoard(bool f)
{
	if (CurrentMenu==MenuName[2]||ShowingMatchingPicturesFlag==true||(manageDataBaseNum==1&&CurrentMenu==MenuName[0]))
	{
		((CComboBox*)GetDlgItem(IDC_COMBO_ACCOUNT))->SetFocus();//防止属性控件影响 按键

		if (f==true)//下一张
		{
			CurrentSubDataBasePageIndex++;
		}
		else if (f==false)//上一张
		{
			CurrentSubDataBasePageIndex--;
		}
		if (CurrentSubDataBasePageIndex<1)
		{
			CurrentSubDataBasePageIndex=1;
		}
		if (CurrentSubDataBasePageIndex>=CurrentSubDataBasePageNum)
		{
			CurrentSubDataBasePageIndex=CurrentSubDataBasePageNum;
		}

		ShowEachPagePicture(CurrentSubDataBasePageIndex);
		DrawLineWindows();
		GetImageDisplayData(m_dspbuf, m_imageBack, 0, 0);

		CRect tmp;
		tmp.left=DataBasePicture_l;
		tmp.right=DataBasePicture_r;
		tmp.top=DataBasePicture_t;
		tmp.bottom=DataBasePicture_b+30;

		InvalidateRect(tmp,FALSE);
	}
	
}

void CLaceborderMatchingDlg::ShowGoToPage()
{
	if (CurrentMenu==MenuName[2]||ShowingMatchingPicturesFlag==true||(manageDataBaseNum==1&&CurrentMenu==MenuName[0]))
	{
		if (CurrentSubDataBasePageNum>1)
		{
			((CComboBox*)GetDlgItem(IDC_COMBO_GO))->MoveWindow(DataBasePicture_l+2,m_Height-33-30, 70,16,TRUE);
			((CComboBox*)GetDlgItem(IDC_COMBO_GO))->ShowWindow(SW_SHOWNA);
		}
		

	}
	else
	{
		((CComboBox*)GetDlgItem(IDC_COMBO_GO))->ShowWindow(SW_HIDE);
	}
}
void CLaceborderMatchingDlg::SetPageNum()
{
	int i;
	CString num;
	((CComboBox* )GetDlgItem(IDC_COMBO_GO))->ResetContent();
	((CComboBox* )GetDlgItem(IDC_COMBO_GO))->SetWindowText("");
	for (i=0;i<CurrentSubDataBasePageNum;i++)
	{
		num.Format("%d",(i+1));
		((CComboBox* )GetDlgItem(IDC_COMBO_GO))->InsertString(i,num);
	}
}


void CLaceborderMatchingDlg::OnCbnSelchangeComboGo()
{
	int i =((CComboBox* )GetDlgItem(IDC_COMBO_GO))->GetCurSel();
//	CString num;
/*	((CComboBox* )GetDlgItem(IDC_COMBO_GO))->GetText(i, num);*/
// 	num.Format("%d",(i+1));
// 	MessageBox(num);

	CurrentSubDataBasePageIndex=i+1;

	if (CurrentSubDataBasePageIndex<1)
	{
		CurrentSubDataBasePageIndex=1;
	}
	if (CurrentSubDataBasePageIndex>=CurrentSubDataBasePageNum)
	{
		CurrentSubDataBasePageIndex=CurrentSubDataBasePageNum;
	}

	ShowEachPagePicture(CurrentSubDataBasePageIndex);
	DrawLineWindows();
	GetImageDisplayData(m_dspbuf, m_imageBack, 0, 0);

	CRect tmp;
	tmp.left=DataBasePicture_l;
	tmp.right=DataBasePicture_r;
	tmp.top=DataBasePicture_t;
	tmp.bottom=DataBasePicture_b+30;

	InvalidateRect(tmp,FALSE);
}


void CLaceborderMatchingDlg::OnDropFiles(HDROP hDropInfo)
{
	if (CurrentMenu==MenuName[0]&&manageDataBaseNum==1)//数据库管理
	{
		TCHAR  szFileName[1024];
		::ZeroMemory(szFileName,1024);
		::DragQueryFile(hDropInfo,0,szFileName,1023);//(在完成)一个成功拖放操作后获取被拖放文件的名称等信息
		//参数4存储拖拽文件名称缓冲区的大小，即lpszFile指针所指缓冲区的字符数。
		::DragFinish(hDropInfo); //释放shell为传递文件名而开辟的内存空间。

		CString filename;
		filename.Format(_T("%s"),szFileName);
		CurrentPicturePath=filename;
		if (filename.Right(3)!="jpg"&&filename.Right(3)!="png"&&filename.Right(3)!="bmp"&&filename.Right(4)!="jpeg")
		{
			MessageBox("请选择图片文件！");
			CDialogEx::OnDropFiles(hDropInfo);
			return;
		}

		if (TRUE==LoadingPictureCountsTest(CurrentSelectedDataBasePath,1))
		{
			MessageBox("该目录最大存储500张图片！");
			CDialogEx::OnDropFiles(hDropInfo);
			return;
		}
		
		if (CurrentPicturePath!="")
		{
			if (IDOK==MessageBox("是否将"+filename+"上传至 数据库"+CurrentSelectedDataBasePath.Right(CurrentSelectedDataBasePath.GetLength()-CurrentPath.GetLength()-9),"上传图片",MB_OKCANCEL|MB_ICONQUESTION))
			{
				ClearCStringVector(Supplierinfo);
				LoadingPicture(CurrentPicturePath,CurrentSelectedDataBasePath);//移动图片

				CurrentSelectedPicturePath=CurrentPicturePath;
				OnEditPictureInfo();

				CreatePictureAddAndDeleteFlagFile();//创建数据库增减标记文件


				//重新显示当前数据界面
				ShowCurrentDataBasePicture(CurrentSelectedDataBasePath);
				DrawLineWindows();
				GetImageDisplayData(m_dspbuf, m_imageBack, 0, 0);
				CRect tmp;
				tmp.left=DataBasePicture_l;
				tmp.right=DataBasePicture_r;

				tmp.top=0;
				tmp.bottom=m_Height;
				InvalidateRect(tmp,FALSE);
			}
			
		}	
	}

	CDialogEx::OnDropFiles(hDropInfo);
}


void CLaceborderMatchingDlg::OnDeAll()//删除目录下所有文件
{
	HTREEITEM hItem = m_tree.GetSelectedItem();
//	HTREEITEM parent = m_tree.GetParentItem(hItem);

// 	int n=m_tree.GetItemData(parent);
// 	int k=m_tree.GetItemData(hItem);

	//删除数据库目录下文件夹与文件
	CString path;
	GetCurrentDir(path);

	/////////////////////////删除子类下所有文件/////////////////////////////////////
	WIN32_FIND_DATA FindFileData;
	HANDLE hFind;
	hFind = FindFirstFile(path+"\\*.*", &FindFileData);
	if (hFind == INVALID_HANDLE_VALUE) 
	{
		return;
	}
	else 
	{
		do 
		{
			CString picturename=path+"\\"+FindFileData.cFileName;
			TCHAR* filename =picturename.GetBuffer(picturename.GetLength()); 
			DeleteFile(filename);

			MSG msg;
			while(::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
			{
				::TranslateMessage(&msg);
				::DispatchMessage(&msg);
			}
		} 
		while (FindNextFile(hFind, &FindFileData) != 0);
	}

	///////////////////////////////////////////////////////////
	if (CurrentMenu==MenuName[0]&&manageDataBaseNum==1)//数据库管理
	{
		//重新显示当前数据界面
		ShowCurrentDataBasePicture(CurrentSelectedDataBasePath);
		DrawLineWindows();
		GetImageDisplayData(m_dspbuf, m_imageBack, 0, 0);
		CRect tmp;
		tmp.left=DataBasePicture_l;
		tmp.right=DataBasePicture_r;

		tmp.top=0;
		tmp.bottom=m_Height;
		InvalidateRect(tmp,FALSE);
	}
	
}

void CLaceborderMatchingDlg::GetPictureCounts(CString path,CString format,int &counts)//获取目录下的所有图片数目
{
	CString tmp=path+"\\*."+format;

	counts=0;

	WIN32_FIND_DATA FindFileData;
	HANDLE hFind;
	hFind = FindFirstFile(tmp, &FindFileData);
	if (hFind == INVALID_HANDLE_VALUE) 
	{
		return;
	}
	else 
	{
		do 
		{
			counts++;

			MSG msg;
			while(::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
			{
				::TranslateMessage(&msg);
				::DispatchMessage(&msg);
			}
		} 
		while (FindNextFile(hFind, &FindFileData) != 0);
	}
}

BOOL CLaceborderMatchingDlg::LoadingPictureCountsTest(CString path,int counts)//检测当前目录新增图片后，图片数量是否多于10000
{
	CString tmp=path+"\\*.jpg";

	int tmp_counts=0;

	GetPictureCounts(path,"jpg",tmp_counts);

	if ((tmp_counts+counts)>500)
	{
		return TRUE;//超过了10000
	}
	else
	{
		return FALSE;
	}
}

BOOL CLaceborderMatchingDlg::TipBeforeClose()//退出前的提示
{
	CString DirPath;
	CString laceClass[5]={"大边","睫毛","面料","小边","其他"};

	int i,j;

	CString tip="";

	if (PathFileExists(CurrentPath+"\\DataBase\\AddAndDelete"))//根目录
	{
		tip="数据库\n";
	}
	else
	{
		for (i=0;i<(int)DataBaseFileName.size();i++)
		{

			DirPath=CurrentPath+"\\DataBase\\"+DataBaseFileName[i];

			if (PathFileExists(DirPath+"\\AddAndDelete"))//子目录
			{
				tip=tip+"数据库\\"+DataBaseFileName[i]+"\n";
			}
// 			else if ((int)DataBaseFileName[i].SubFileName.size()>0)
// 			{
// 				for (j=0;j<(int)DataBaseFileName[i].SubFileName.size();j++)
// 				{
// 					if (PathFileExists(CurrentPath+"\\DataBase\\"+laceClass[i]+"\\"+DataBaseFileName[i].SubFileName[j]+"\\AddAndDelete"))//子目录
// 					{
// 						tip=tip+"数据库\\"+laceClass[i]+"\\"+DataBaseFileName[i].SubFileName[j]+"\n";
// 					}			
// 				}
// 			}

		}
	}

	if (tip.GetLength()>0)
	{
		if (IDOK==MessageBox(tip+"目录图片有更新，是否更新数据库？","数据库更新",MB_OKCANCEL|MB_ICONQUESTION))
		{
			manageAccountHide();
			LogSuccessfulBackGround();
			InitMenu();
			showPageFlag=false;
			ClickMenu(1);
			GetImageDisplayData(m_dspbuf, m_imageBack, 0, 0);				
			Invalidate(FALSE);
			ManageDataBaseClick(3);//点击3个子菜单
			GetImageDisplayData(m_dspbuf, m_imageBack, 0, 0);
			Invalidate(FALSE);

			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	return FALSE;

}

void CLaceborderMatchingDlg::UnLock(CString dirPath)
{	
	CString s=dirPath.Left(dirPath.GetLength()-39);
	ReName(dirPath,s);
}


void CLaceborderMatchingDlg::Lock(CString dirPath)
{
	ReName(dirPath,dirPath+".{208D2C60-3AEA-1069-A2D7-08002B30309D}");
}
void CLaceborderMatchingDlg::ReName(CString s,CString text)
{
	TCHAR* pOldName =s.GetBuffer(s.GetLength()); 
	TCHAR* pNewName =text.GetBuffer(text.GetLength()); 
	CFile::Rename(pOldName, pNewName);
}

void CLaceborderMatchingDlg::UnRegisterDateLimited()//记录软件第一次运行的日期，限制试用期为一个月
{
	TCHAR DeskToppath[255];
	SHGetSpecialFolderPath(0,DeskToppath,CSIDL_DESKTOPDIRECTORY,0);//获取桌面路径

	CString path;
	path.Format(_T("%s"),DeskToppath);

	int pos =path.ReverseFind('\\')+1;
	path =path.Left(pos);

	path+="CogniVisualInfo.Date";//这个文件记录日期

	CString readingPath;
	FILE *File=NULL;

	SYSTEMTIME tmp_time;
	GetLocalTime(&tmp_time);
	SYSTEMTIME Reg_time;

	if (PathFileExists(path))//存在则读取路径，判断是否与其相等
	{
		fopen_s(&File,path,"rb");//为读写建立新的二进制文件
		if (File)//打开成功
		{
//			ReadCString(readingPath,File);
			fread(&Reg_time,sizeof(SYSTEMTIME),1,File);
			fclose(File);
		}

		COleDateTime dTimeF(tmp_time); 				
		COleDateTime dTimeS(Reg_time); 

		COleDateTimeSpan dTimeSpan = dTimeF - dTimeS; 
		int nSecnonSpan = dTimeSpan.GetDays();

		if (nSecnonSpan<0||nSecnonSpan>91)
		{
			AfxMessageBox("试用期已过！！");
			exit(-1);
		}
		else
		{
			CString tmp;
			tmp.Format("%d",91-nSecnonSpan);
			MessageBox("试用期还剩 "+tmp+" 天！！");
		}

	}
	else //路径不存在说明 安装后第一次使用，或者人为删除该文件
	{
		fopen_s(&File,path,"wb");//为读写建立新的二进制文件
		if (File)//打开成功
		{
			fwrite(&tmp_time,sizeof(SYSTEMTIME),1,File);
//			WriteCString(CurrentPath,File);		
		}

		fclose(File);

		MessageBox("试用期还剩 91 天！！");
	}


	File=NULL;



}

void CLaceborderMatchingDlg::DeleteSetupPackage()//删除试用版安装包
{
	TCHAR DeskToppath[255];
	SHGetSpecialFolderPath(0,DeskToppath,CSIDL_DESKTOPDIRECTORY,0);//获取桌面路径

	CString path;
	path.Format(_T("%s"),DeskToppath);

	int pos =path.ReverseFind('\\')+1;
	path =path.Left(pos);

	path+="CogniVisualSetupPackagePath";//这个文件记录当前安装包exe路径

	CString SetupPackagePath;
	FILE *File=NULL;

	if (PathFileExists(path))//存在则读取路径，判断是否与其相等
	{
		fopen_s(&File,path,"rb");//为读写建立新的二进制文件
		if (File)//打开成功
		{
			ReadCString(SetupPackagePath,File);
			fclose(File);
		}
	}
	File=NULL;

	MyDeleteFile(path);

	MyDeleteFile(SetupPackagePath+"\\安装.exe");
	MyDeleteFile(SetupPackagePath+"\\Package.{208D2C60-3AEA-1069-A2D7-08002B30309D}\\package.exe");
	DeleteDir(SetupPackagePath+"\\Package.{208D2C60-3AEA-1069-A2D7-08002B30309D}");
}

void CLaceborderMatchingDlg::MyDeleteFile(CString FilePath)//删除文件
{
	if (PathFileExists(FilePath))
	{		
		TCHAR* filename =FilePath.GetBuffer(FilePath.GetLength()); 
		DeleteFile(filename);
	}
}