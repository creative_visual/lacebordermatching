// ShowPicture.cpp : 实现文件
//

#include "stdafx.h"
#include "LaceborderMatching.h"
#include "ShowPicture.h"
#include "afxdialogex.h"


// CShowPicture 对话框

IMPLEMENT_DYNAMIC(CShowPicture, CDialogEx)

CShowPicture::CShowPicture(CWnd* pParent /*=NULL*/)
	: CDialogEx(CShowPicture::IDD, pParent)
{
	m_dspbuf=NULL;
	m_image=NULL;
	m_hpos=0;
	m_vpos=0;
}

CShowPicture::~CShowPicture()
{
}

void CShowPicture::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_SCROLLBAR2, m_SvBar);
	DDX_Control(pDX, IDC_SCROLLBAR1, m_SBar);
}


BEGIN_MESSAGE_MAP(CShowPicture, CDialogEx)
	ON_WM_PAINT()
	ON_WM_VSCROLL()
	ON_WM_HSCROLL()
END_MESSAGE_MAP()

BOOL CShowPicture::OnInitDialog()
{
	CDialogEx::OnInitDialog();


	// TODO: 在此添加额外的初始化代码
	if (theApp.pLaceborderMatchingDlg)
	{
		int iWidth = GetSystemMetrics(SM_CXSCREEN); 
		int iHeight = GetSystemMetrics(SM_CYSCREEN); 

		IplImage * image=cvLoadImage(theApp.pLaceborderMatchingDlg->CurrentSelectedPicturePath);

		if (!image)
		{
			return FALSE;
		}
		int w,h;

		h=image->height;
		w=image->width;
		CvSize size;
		size.height =h, size.width =w;
		cvReleaseImage(&m_image);
		m_image=  cvCreateImage(size, 8, 4);

		int i,j;
		for(i = 0; i < image->height; i++)
			for(j = 0; j < image->width; j++)
			{
				m_image->imageData[ i * m_image->widthStep + m_image->nChannels * j + 2 ] = image->imageData[ i * image->widthStep + image->nChannels * j + 2 ];
				m_image->imageData[ i * m_image->widthStep + m_image->nChannels * j + 1 ] = image->imageData[ i * image->widthStep + image->nChannels * j + 1 ];
				m_image->imageData[ i * m_image->widthStep + m_image->nChannels * j + 0 ] = image->imageData[ i * image->widthStep + image->nChannels * j + 0 ];

			}
		cvReleaseImage(&image);

		int window_w,window_h;

		window_w=w;
		window_h=h;

		if (h>(iHeight-80))
		{
			window_h=iHeight-80;
			h=iHeight-80-15;//滚动条
			GetDlgItem(IDC_SCROLLBAR2)->ShowWindow(SW_SHOW);
		}
		if (w>(iWidth-20))
		{
			window_w=iWidth-20;
			w=iWidth-20-15;//滚动条
			GetDlgItem(IDC_SCROLLBAR1)->ShowWindow(SW_SHOW);
		}
		
		cvReleaseImage(&m_dspbuf);
		size.height =h, size.width =w;
		m_dspbuf=  cvCreateImage(size, 8, 4);
		GetDlgItem(IDC_STATIC_SHOW)->MoveWindow(0,0,w,h,TRUE);

	

		window_w=window_w+6;//窗口比客户区宽6，高32
		window_h=window_h+32;

		GetDlgItem(IDC_SCROLLBAR2)->MoveWindow(window_w-15-6,0,15,window_h-30,TRUE);//垂直滚动
		GetDlgItem(IDC_SCROLLBAR1)->MoveWindow(0,window_h-30-15,window_w-6-15,15,TRUE);//水平滚动

		this->MoveWindow((iWidth-window_w)/2,(iHeight-window_h)/2,window_w,window_h,TRUE);	
		theApp.pLaceborderMatchingDlg->GetImageDisplayData(m_dspbuf,m_image,0, 0);

		CString fileName=theApp.pLaceborderMatchingDlg->CurrentSelectedPicturePath;
		int pos =fileName.ReverseFind('\\')+1;
		fileName =fileName.Right(fileName.GetLength()-pos);//获取 图片名
		this->SetWindowTextA(fileName);
	}

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

// CShowPicture 消息处理程序


void CShowPicture::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 在此处添加消息处理程序代码
	// 不为绘图消息调用 CDialogEx::OnPaint()

	if(m_dspbuf )
	{
		CPaintDC dc(GetDlgItem(IDC_STATIC_SHOW));

		CDC dcMem;
		CBitmap bmpMem;
		dcMem.CreateCompatibleDC(&dc);
		bmpMem.CreateCompatibleBitmap(&dc, m_dspbuf->width, m_dspbuf->height);
		dcMem.SelectObject(&bmpMem);

		int linebyte;
		int w = m_dspbuf->width;
		int h = m_dspbuf->height;
		linebyte = m_dspbuf->widthStep;

		bmpMem.SetBitmapBits(linebyte * h, m_dspbuf->imageData);
		dc.BitBlt(0, 0, w,h, &dcMem, 0, 0, SRCCOPY);

		bmpMem.DeleteObject();
		dcMem.DeleteDC(); 
	}
}


void CShowPicture::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	if (m_image)
	{
		int t=m_image->height-m_dspbuf->height;
		if (t>0)
		{
			m_SvBar.SetScrollRange(0,100);//水平滑动条范围设置为 0 到100
			//m_SBar.SetScrollPos(0); //水平滑动条初始位置设置为 0 
			int position=m_SvBar.GetScrollPos();

			switch (nSBCode)
			{
			case SB_LINELEFT://向左滚动一行 
				position--; 
				break; 
			case SB_LINERIGHT://向右滚动一行 
				position++; 
				break; 
			case SB_PAGELEFT: //向左滚动一页 
				position-=10; 
				break; 
			case SB_PAGERIGHT: //向右滚动一页 
				position+=10; 
				break; 
			case SB_THUMBTRACK://拖动滑动块 
				position=nPos;//这里的nPos 就是函数 OnHScroll()的第 2 个参数 
				break; 
			case SB_LEFT://移动到最左边 
				position=0; 
				break; 
			case SB_RIGHT://移动到最右边 
				position=100; 
				break; 

			} 
			if(position>100) position=100; 
			if(position<0) position=0; 
			m_SvBar.SetScrollPos(position);//根据position 的值来设定滑动块的位置
			//		UpdateData(FALSE);
			m_vpos=position*t/100;

			theApp.pLaceborderMatchingDlg->GetImageDisplayData(m_dspbuf, m_image, m_hpos, m_vpos);
			Invalidate(FALSE);
		} 
	} 

	CDialogEx::OnVScroll(nSBCode, nPos, pScrollBar);
}


void CShowPicture::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	if (m_image)
	{
		
		int t=m_image->width-m_dspbuf->width;

		if (t>0)
		{
			m_SBar.SetScrollRange(0,100);//水平滑动条范围设置为 0 到100
			//m_SBar.SetScrollPos(0); //水平滑动条初始位置设置为 0 
			int position=m_SBar.GetScrollPos();

			switch (nSBCode)
			{
			case SB_LINELEFT://向左滚动一行 
				position--; 
				break; 
			case SB_LINERIGHT://向右滚动一行 
				position++; 
				break; 
			case SB_PAGELEFT: //向左滚动一页 
				position-=10; 
				break; 
			case SB_PAGERIGHT: //向右滚动一页 
				position+=10; 
				break; 
			case SB_THUMBTRACK://拖动滑动块 
				position=nPos;//这里的nPos 就是函数 OnHScroll()的第 2 个参数 
				break; 
			case SB_LEFT://移动到最左边 
				position=0; 
				break; 
			case SB_RIGHT://移动到最右边 
				position=100; 
				break; 
			} 
			if(position>100) position=100; 
			if(position<0) position=0; 
			m_SBar.SetScrollPos(position);//根据position 的值来设定滑动块的位置

			m_hpos=position*t/100;
			
			theApp.pLaceborderMatchingDlg->GetImageDisplayData(m_dspbuf, m_image, m_hpos,m_vpos );
			Invalidate(FALSE);

		} 
	} 

	CDialogEx::OnHScroll(nSBCode, nPos, pScrollBar);
}
BOOL CShowPicture::PreTranslateMessage(MSG* pMsg)
{
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_LEFT)
	{
		this->OnOK();
		theApp.pLaceborderMatchingDlg->ShowPictureByKeyBoard(false);//上一张
	}
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_RIGHT)
	{
		this->OnOK();
		theApp.pLaceborderMatchingDlg->ShowPictureByKeyBoard(true);//上一张
	}

	return CDialog::PreTranslateMessage(pMsg);
}