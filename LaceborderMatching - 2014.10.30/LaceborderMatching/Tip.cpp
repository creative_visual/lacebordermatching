// Tip.cpp : 实现文件
//

#include "stdafx.h"
#include "LaceborderMatching.h"
#include "Tip.h"
#include "afxdialogex.h"


// CTip 对话框

IMPLEMENT_DYNAMIC(CTip, CDialogEx)

CTip::CTip(CWnd* pParent /*=NULL*/)
	: CDialogEx(CTip::IDD, pParent)
{

}

CTip::~CTip()
{
}

void CTip::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CTip, CDialogEx)
	ON_BN_CLICKED(IDC_BUTTON1, &CTip::OnSure)
	ON_BN_CLICKED(IDC_BUTTON2, &CTip::OnCancle)
END_MESSAGE_MAP()


// CTip 消息处理程序


void CTip::OnSure()
{
	// TODO: 在此添加控件通知处理程序代码
	theApp.pLaceborderMatchingDlg->myFlag=true;
	this->OnOK();
}


void CTip::OnCancle()
{
	// TODO: 在此添加控件通知处理程序代码
	this->OnOK();
}
