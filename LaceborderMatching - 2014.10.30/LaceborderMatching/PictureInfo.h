#pragma once
#include "afxwin.h"


// CPictureInfo 对话框

class CPictureInfo : public CDialogEx
{
	DECLARE_DYNAMIC(CPictureInfo)

public:
	CPictureInfo(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CPictureInfo();
	virtual BOOL OnInitDialog();

// 对话框数据
	enum { IDD = IDD_DIALOG_PICTURE_INFO };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	void OnReadPictureInfo();

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnSure();
	afx_msg void OnReName();
	CEdit m_edit;
};
