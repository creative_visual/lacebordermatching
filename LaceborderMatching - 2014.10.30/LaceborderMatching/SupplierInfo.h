#pragma once


// CSupplierInfo 对话框

class CSupplierInfo : public CDialogEx
{
	DECLARE_DYNAMIC(CSupplierInfo)

public:
	CSupplierInfo(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CSupplierInfo();
	virtual BOOL OnInitDialog();

// 对话框数据
	enum { IDD = IDD_DIALOG1 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnGetSupplierInfo();
};
