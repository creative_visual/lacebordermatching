#ifndef COUNT_UTILS_H
#define COUNT_UTILS_H

#define clip(x,a,b) max(a,min(x,b))
#define DIFF(x, y) (((x)>(y))?((x)-(y)):((y)-(x)))
#define ABS(x) ((x<0)?-(x):(x))
#define MAXA(x,y) ((ABS(x)>ABS(y))?(x):(y))
#define MIN2(x, y) (((x)<(y))?(x):(y))
#define MAX2(x, y) (((x)>(y))?(x):(y))

#ifndef min
#define min(a,b)            (((a) < (b)) ? (a) : (b))
#endif

#ifndef max
#define max(a,b)			(((a) < (b)) ? (b) : (a))
#endif

#define CLAMP(x, t1, t2)	(((x) < (t1))? (t1): ((x) > (t2))? (t2): (x))
#define SIGN(A)				(((A)>=0.0f)?1.0f:-1.0f)

int define_detection_roi(int& g_count_line_y, int& top_line_y, int& btm_line_y, int& g_max_face_size, int frame_w, int frame_h);

#endif // ~COUNT_UTILS_H