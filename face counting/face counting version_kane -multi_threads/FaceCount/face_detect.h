#ifndef FACE_DETECT_H
#define FACE_DETECT_H

#include <opencv2/opencv.hpp>
#include <vector>

//#include "LBPFaceTypes.h"
// #pragma comment( lib, "libjasper.lib" )
// #pragma comment( lib, "libjpeg.lib" )
// #pragma comment( lib, "libpng.lib" )
// #pragma comment( lib, "libtiff.lib" )
// #pragma comment( lib, "zlib.lib" )
// 
// #pragma comment( lib,"opencv_highgui246.lib" )
// #pragma comment( lib,"opencv_core246.lib" )
// #pragma comment( lib,"opencv_imgproc246.lib" )
// #pragma comment( lib,"opencv_objdetect246.lib" )
// #pragma comment( lib,"opencv_video246.lib" )
//#pragma comment( lib,"opencv_ffmpeg246.lib" )

using namespace std;
using namespace cv;


#define  DETECTED        0
#define  TRACKDETECTED   -1

struct rect
{
	int xmin;
	int ymin;
	int xmax;
	int ymax;
};

struct target_info
{
	rect pos;
	int label;
	int is_cross;
	int btrack;
};

struct frame_info
{
	vector<target_info> targets;
};

struct TargetInfo
{
	int label;
	vector<rect> pos;
	int begin,end;
};

// struct frame_info
// {
// 	vector<rect> targets;
// 	vector<int> label;
// };

void detect_using_cascades(string cascade_path, string video_name, string back_ground_path, string dir_out);
void decrypt_stage_threshold(CvHaarClassifierCascade *classifier, double delta);
void encode_stage_threshold(CvHaarClassifierCascade *classifier, double delta);
double get_rect_overlap(rect* R1,rect* R2);
void combine_two_faces(rect *dst, rect *src);
void faces_from_cascades(vector<vector<rect>> &cascade_faces, vector<rect> &faces/*, vector<int> &cascade_flag*/);
void detect( Mat& img, CascadeClassifier& cascade, int span_cb, int span_cr, unsigned char &mincb, unsigned char &mincr, vector<rect> &faces_target);
void detect( Mat& img, CascadeClassifier& cascade, vector<rect> &faces_target);
double face_area_rate(Mat skin, int x, int y, int l);
void draw_comb_target(Mat image, vector<rect> &faces);
void draw_line(Mat image, int *points);
void Mat2IplImage(Mat &mat, IplImage *image);
void track_faces(vector<rect> &pre_faces, vector<rect> &cur_faces, vector<int> &pre_idx, vector<int> &cur_idx, int &max_idx);
void get_detected_face_pixels(Mat frame, vector<rect> &faces, vector<Mat> &face_pixels);
void track_faces(vector<Mat> &pre_target, Mat cur_frame, int search_radius, vector<rect> &pre_faces, vector<rect> &tracked);
void set_target_label(vector<Mat> &pre_pixels, Mat cur_frame, vector<rect> &cur_faces, frame_info &pre_info, frame_info &cur_info, int &max_label);
void draw_video_targets(string video_path, string out_path, Mat background, vector<frame_info> &video_info);
void make_targets_successive(vector<Mat> &frames, vector<frame_info> &vtargets_info);
void save_video_frames(string video_path, double begin_rate, double end_rate, string out_dir);
int get_cross_flag(rect target, int count_line_y);
int get_cross_flag(target_info target, int count_line_y);

void set_target_label_y(vector<Mat> &pre_pixels, Mat cur_frame, vector<rect> &cur_faces, frame_info &pre_info, frame_info &cur_info, int &max_label);
int is_passed(int label, int pre_idx, int cur_idx, int count_line_y);
int is_target(Mat diff, rect r);
void diff_width_background(Mat frame, Mat background, Mat &diff);
void erase_background_target(Mat &diff, frame_info &cur_frame);
//void ExtractFaceFeatures(Mat img_gray, int max_obj_num,vector <ObjRect> &FaceFeatures);

void copy_roi_pxl(Mat image, int x, int y, int w, int h, Mat &roi);
double track_face(Mat pre_frame, rect pre_face, int search_radius, Mat cur_frame, rect &tracked);
void set_target_label(Mat pre_frame, Mat cur_frame, vector<rect> &cur_faces, frame_info &pre_info, frame_info &cur_info, int &max_label);
void set_label(Mat pre_frame, Mat cur_frame, vector<rect> &cur_faces, frame_info &pre_info, frame_info &cur_info, int &max_label);
int binsight(int width, int height, rect target);
void erase_background_target(Mat bggray, Mat frame, string cascade_path, frame_info &cur_frame);
void detect_gray( Mat gray, CascadeClassifier& cascade, vector<rect> &faces_target );
void erase_false_negative(vector<frame_info> &video_info);
void get_path_files(string path, vector<string> &files);
void generate_video(string pic_dir, string out_path);
int det_pass_line(target_info target);
int bpass_line(target_info target);
int bneed_track(target_info target);
double search_face(Mat pre_frame, rect pre_face, int search_radius, Mat cur_frame, rect &tracked);

void diff_width_background(string video_path, string bg_path, string out_path);
void avg_image(string pic_dir, string avg_img);
int is_software_valid();
void g_GetImgFullScaleDspData(IplImage *dspbuf, IplImage *origin);
void draw_count_info(string video_path, vector<frame_info> &video_info);
#endif