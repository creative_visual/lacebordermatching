
#include <opencv2/opencv.hpp>
#include <vector>

using namespace std;
using namespace cv;

#define GLCM_DIS 3  //灰度共生矩阵的统计距离
#define GLCM_CLASS 16 //计算灰度共生矩阵的图像灰度值等级化
#define GLCM_ANGLE_HORIZATION 0  //水平
#define GLCM_ANGLE_VERTICAL   1	 //垂直
#define GLCM_ANGLE_DIGONAL    2  //对角

int calGLCM(IplImage* bWavelet,int angleDirection,double* featureVector);
double get_avg_gray(IplImage *gray);
double get_avg_gray(Mat gray);

double get_cov(IplImage *gray1, IplImage *gray2);
double get_cov(IplImage *gray1, IplImage *gray2, double avg1, double avg2);
double get_cov(Mat gray1, Mat gray2, double avg1, double avg2);  // 协方差
double get_cov(Mat gray1, Mat gray2);

double get_dev(IplImage *gray, double avg);
double get_dev(Mat gray, double avg);  // 方差

double get_correlation(Mat gray1, Mat gray2, int stdl);
double get_correlation(IplImage *image1, IplImage *image2, int stdl);

double vector_distance(double *features1, double *features2, int length);
void normalise_features(vector<double *> &features, int len);

double diff_images(Mat gray1, Mat gray2, int stdl);
double diff_images2(Mat& pre_gray, Mat& cur_gray, CvRect& pre_roi, CvRect& cur_roi);
