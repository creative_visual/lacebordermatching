
// FaceCountDlg.h : 头文件
//

#pragma once

#include <opencv2/opencv.hpp>
#include <string>
using namespace std;

// CFaceCountDlg 对话框
class CFaceCountDlg : public CDialogEx
{
// 构造
public:
	CFaceCountDlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
	enum { IDD = IDD_FACECOUNT_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButtonOpen();
	afx_msg void OnBnClickedButtonStart();
	afx_msg void OnBnClickedButtonParams();
	void UpdateParams();
	void MoveParamsCtrl();
	void DisplayImage(UINT nid, CRect dsprect, IplImage *dspbuf);

private:
	string m_video_path;

	int m_minfacesize;
	int m_maxfacesize;
	int m_count_line;
	int m_interval;
	int m_avgdiff;
	double m_zoomrate;
	double m_scalefactor;
	int m_minneighbors;
	int m_detflag;

	int m_openflag;

	CRect m_dsprect;
	IplImage *m_dspbuf;
	CWinThread *m_detthr, *m_drawthr;
public:
	afx_msg void OnBnClickedButtonRestart();
	afx_msg void OnBnClickedButtonParamedit();
// 	afx_msg void OnEnKillfocusEditMinsize();
// 	afx_msg void OnEnKillfocusEditMaxsize();
// 	afx_msg void OnEnKillfocusEditCountline();
};
