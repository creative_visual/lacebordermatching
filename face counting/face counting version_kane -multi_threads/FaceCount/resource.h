//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by FaceCount.rc
//
#define IDD_FACECOUNT_DIALOG            102
#define IDR_MAINFRAME                   128
#define IDC_EDIT_VIDEOPATH              1002
#define IDC_BUTTON_OPEN                 1003
#define IDC_EDIT_MINSIZE                1004
#define IDC_EDIT_MAXSIZE                1005
#define IDC_EDIT_COUNTLINE              1006
#define IDC_STATIC_COMPLETERATE         1007
#define IDC_BUTTON_START                1008
#define IDC_EDIT_INTERVAL               1009
#define IDC_EDIT_AVGDIFF                1010
#define IDC_EDIT_ZOOMRATE               1011
#define IDC_EDIT_SCALEFACTOR            1012
#define IDC_EDIT_MINNEIGHBORS           1013
#define IDC_EDIT_DETFLAG                1014
#define IDC_BUTTON_PARAMS               1015
#define IDC_STATIC_INTERVAL             1016
#define IDC_STATIC_AVGDIFF              1017
#define IDC_STATIC_ZOOMRATE             1018
#define IDC_STATIC_SCALEFACTOR          1019
#define IDC_STATIC_NEIGHBORS            1020
#define IDC_STATIC_DETFLAG              1021
#define IDC_IMAGE                       1022
#define IDC_BUTTON_RESTART              1023
#define IDC_BUTTON1                     1024
#define IDC_BUTTON_PARAMEDIT            1024

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1025
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
