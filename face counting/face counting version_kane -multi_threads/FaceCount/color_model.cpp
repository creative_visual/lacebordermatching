// color hist gram.cpp : 定义控制台应用程序的入口点。
//
#include "stdafx.h"
#include "color_model.h"
#include "fstream"
using namespace std;


Mat filt_hist(CvHistogram *hist,int size)
{
	int i,j;
	Mat color_feat(1,size,CV_32F,cvScalar(0));
	Mat tmp(1,size+4,CV_32F,cvScalar(0));
	tmp.at<float>(0)=cvQueryHistValue_1D(hist,size-2);
	tmp.at<float>(1)=cvQueryHistValue_1D(hist,size-1);
	for (i=2;i<size+2;i++)
	{
		tmp.at<float>(i)=cvQueryHistValue_1D(hist,i-2);
	}
	tmp.at<float>(i)=cvQueryHistValue_1D(hist,0);
	tmp.at<float>(i+1)=cvQueryHistValue_1D(hist,1);
	j=2;
	for (i=0;i<size;i++)
	{
		float bin_val0 = tmp.at<float>(j-2);
		float bin_val1 = tmp.at<float>(j-1);
		float bin_val2 = tmp.at<float>(j);
		float bin_val3 = tmp.at<float>(j+1);
		float bin_val4 = tmp.at<float>(j+2);
		color_feat.at<float>(i)=bin_val0+bin_val4+4*(bin_val1+bin_val3)+6*bin_val2;
		j++;
	}
	cvReleaseHist(&hist);
	return color_feat;

}


int get_locat_index(float point_val,int bins,int bin_size,int beg_val,int end_val)
{
	//	cout<<point_val<<" ";
	int index_val;
	int i;
	vector<int>bin_index;
	for (i=0;i<bins;i++)
	{
		bin_index.push_back(i);
	}
	int m_it;
	for (m_it=0;m_it<bins;m_it++)
	{
		if ((point_val>=(beg_val+m_it*bin_size))&&(point_val<(beg_val+(m_it+1)*bin_size)))
			index_val=m_it;
	}
	if (abs(point_val-end_val)<1e-10)
		index_val=0;

	return index_val;
}

void Project_location_to_binsindex(IplImage* h_plane,IplImage* s_plane,IplImage* v_plane,Rect roirect,vector<int>& project_loc)
{
	int i,j;

	int bins1=4;
	int bins2=4;
	int bins3=4;
	vector<int>h_pro;
	vector<int>s_pro;
	vector<int>v_pro;

	int width=h_plane->width;
	int height=h_plane->height;
	int step=h_plane->widthStep;
	float* data0=(float *)h_plane->imageData;
	float* data1=(float *)s_plane->imageData;
	float* data2=(float *)v_plane->imageData;
	for (i=roirect.y;i<roirect.height+roirect.y;i++)
	{
		for (j=roirect.x;j<roirect.width+roirect.x;j++)
		{
			float point_val=((float*)(h_plane->imageData+i*step))[j];
			int index=get_locat_index(point_val,bins1,90,0,360);

			float point_val1=((float*)(s_plane->imageData+i*step))[j];
			int index1=get_locat_index(point_val1,bins2,250,0,1000);

			float point_val2=((float*)(v_plane->imageData+i*step))[j];
			int index2=get_locat_index(point_val2,bins3,25,0,100);


			//calculate according to the rules: real_index=index*100+index1*10+index2-(index*68+index1*6)
			int real_index=index<<4|index1<<2|index2;      
			project_loc.push_back(real_index);
			//			cout<<real_index<<" ";

		}
		//		cout<<"line: "<<i<<endl;
	} 

}

// void Project_location_to_binsindex(IplImage* h_plane,IplImage* s_plane,IplImage* v_plane,Rect roirect,vector<int>& project_loc)
// {
// 	int i,j;
// 	
// 	int bins1=8;
// 	int bins2=8;
// 	int bins3=4;
// 	vector<int>h_pro;
// 	vector<int>s_pro;
// 	vector<int>v_pro;
// 	
// 	cvSetImageROI(h_plane,roirect);
// 	cvSetImageROI(s_plane,roirect);
// 	cvSetImageROI(v_plane,roirect);
// 	IplImage *h_copy=cvCreateImage(cvSize(roirect.width,roirect.height),h_plane->depth,h_plane->nChannels);
// 	IplImage *s_copy=cvCreateImage(cvSize(roirect.width,roirect.height),s_plane->depth,s_plane->nChannels);
// 	IplImage *v_copy=cvCreateImage(cvSize(roirect.width,roirect.height),v_plane->depth,v_plane->nChannels);
// 	cvCopy(h_plane,h_copy);
// 	cvCopy(s_plane,s_copy);
// 	cvCopy(v_plane,v_copy);
// 	int width=h_copy->width;
// 	int height=h_copy->height;
// 	int step=h_copy->widthStep;
// 	float* data0=(float *)h_copy->imageData;
// 	float* data1=(float *)s_copy->imageData;
// 	float* data2=(float *)v_copy->imageData;
// 	for (i=0;i<height;i++)
// 	{
// 		for (j=0;j<width;j++)
// 		{
// 			float point_val=((float*)(h_copy->imageData+i*step))[j];
// 			int index=get_locat_index(point_val,bins1,45,0,360);
// 
// 			float point_val1=((float*)(s_copy->imageData+i*step))[j];
// 			int index1=get_locat_index(point_val1,bins2,125,0,1000);
// 
// 			float point_val2=((float*)(v_copy->imageData+i*step))[j];
// 			int index2=get_locat_index(point_val2,bins3,25,0,100);
// 
// 
// 			//calculate according to the rules: real_index=index*100+index1*10+index2-(index*68+index1*6)
// 			int real_index=index*32+index1*4+index2;      
// 			project_loc.push_back(real_index);
// 
// 		}
// 		//		cout<<"line: "<<i<<endl;
// 	} 
// 	cvResetImageROI(h_plane);
// 	cvResetImageROI(s_plane);
// 	cvResetImageROI(v_plane);
// 	cvReleaseImage(&h_copy);
// 	cvReleaseImage(&s_copy);
// 	cvReleaseImage(&v_copy);
// 
// }

int Kronecker_delta_function(int x,int c)
{
	int cal;
	if (x==c)
		cal=1;
	else
		cal=0;
	return cal;
}



void generate_color_prob_model(vector<int> project_loc,vector<float>&color_porb_distub,Rect roi_rect)
{
	float a=(roi_rect.width*roi_rect.width+roi_rect.height*roi_rect.height);

	CvPoint rect_center;
	rect_center.x=roi_rect.x+roi_rect.width/2;
	rect_center.y=roi_rect.y+roi_rect.height/2;
	int i,j,k;
	float prob_val;
	float norm_factor=0.0;
	int hist_itr;
	vector<float>all_kernel_val;
	for (i=roi_rect.y;i<roi_rect.height;i++)
	{
		for (j=roi_rect.x;j<roi_rect.width;j++)
		{
			CvPoint point_inrect=cvPoint(j,i);
			float dis=(point_inrect.x-rect_center.x)*(point_inrect.x-rect_center.x)+(point_inrect.y-rect_center.y)*(point_inrect.y-rect_center.y);
			float kernal_para=dis/a;
			float kernal_result;
			if (kernal_para<1.0)
				kernal_result=1-kernal_para;
			else
				kernal_result=0.0;
			all_kernel_val.push_back(kernal_result);
			norm_factor+=kernal_result;
		}

	}
	int it_m;
	for (i=0;i<64;i++)
	{
		color_porb_distub.push_back(0);
	}
	float all_pro=0.0;
	for (k=0;k<64;k++)
	{
		for (i=roi_rect.y;i<roi_rect.height;i++)
		{
			for (j=roi_rect.x;j<roi_rect.width;j++)
			{
				int delta=Kronecker_delta_function(project_loc[i*roi_rect.width+j],k);
				if(delta)
					color_porb_distub[k]+=all_kernel_val[i*roi_rect.width+j]*(float)delta;

			}

		}
		color_porb_distub[k]=color_porb_distub[k]/norm_factor;
		//		all_pro+=color_porb_distub[k];
	}
	//	cout<<all_pro<<" ";


}



float Bhattacharyya_distance(vector<float>s1,vector<float>s2)
{
	int N=s1.size();
	int i;
	float ou_dis=0.0;
	for (i=0;i<N;i++)
	{
		ou_dis+=sqrt(s1[i]*s2[i]);
	}

	return ou_dis;
}


void Draw_Bow_Hist( vector<float>feat)
{
	int hist_height=100;    
	int bins =feat.size();    
	int scale = 2;   //直方图的宽度  

	Mat hist_img = Mat::zeros(hist_height,bins*scale, CV_8UC3); //创建一个直方图图像并初始化为0   
	for(int i=0;i<bins;i++)    
	{    
		float bin_val = feat[i]; // 第i灰度级上的数      
		int intensity = cvRound(bin_val*hist_height*10);  //要绘制的高度    
		//填充第i灰度级的数据  
		rectangle(hist_img,Point(i*scale,hist_height-1),    
			Point((i+1)*scale - 1, hist_height - intensity),    
			CV_RGB(255,255,255));    
	}  

	imshow( " Histogram1", hist_img ); 
	cvWaitKey(0);


}