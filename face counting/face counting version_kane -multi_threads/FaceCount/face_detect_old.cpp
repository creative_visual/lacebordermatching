
#include "StdAfx.h"
#include "face_detect.h"
#include "MyCascadeClassifier.h"
//#include "LBPFaceDetect.h"
//#include "FaceCountDlg.h"
#include <Windows.h>
#include <opencv2\legacy\legacy.hpp>
#include <io.h>
#include <iomanip>
#include "texture.h"


#define   NOTPASS		111
#define   PASSING		112
#define   PASSED		113
#define   LOSEPASSED    114
#define   COUNTED		115

#define   MATCHTHRE     55
#define   AVGDIFFT      55

int g_min_face_size = 75, g_max_face_size = 175;
int g_count_line_y = 540;
int g_successive_frames = 10, g_max_track_num = 8;
vector<frame_info> video_target_info;
vector<int> g_bcounted(1,0);
// int top_line_y = g_count_line_y - g_max_face_size ;
// int btm_line_y = g_count_line_y + g_max_face_size * 1.2;
double corr_t = 0.05;
int g_interval = 0;
int g_avgdiff = 55;
double g_zoomrate = 0.8;
double g_scalefactor = 1.1;
int g_minneighbors = 2;
int g_detflag = 0;
string g_video_path;
int g_draw_fidx = 0;

UINT g_pro_info_id;
HWND g_hinfo, g_hstart, g_hopen, g_hwnd;
CRect g_rect;
IplImage *g_dspbuf;
DWORD g_dettime = 0, g_tracktime = 0;
CRITICAL_SECTION gSection;
volatile int g_bdetect = 0, g_bdraw = 0;
// CFaceCountDlg *g_app;

void detect_using_cascades(string cascade_path, string video_name, string back_ground_path, string dir_out)
{
	int span_cb = 37, span_cr = 42;
	unsigned char mincb,mincr;

	vector<rect> faces;
	//cout<<1<<endl;
	cv::VideoCapture capture(video_name.c_str());
	cv::Mat frame; // 现在的视频帧

	//cout<<2<<endl;
	int frameRate = capture.get(CV_CAP_PROP_FPS);
	int framenum = capture.get(CV_CAP_PROP_FRAME_COUNT);
	int width = (int)capture.get(CV_CAP_PROP_FRAME_WIDTH);
	int height = (int)capture.get(CV_CAP_PROP_FRAME_HEIGHT);
	cv::Size frameSize = cv::Size(width,height);

	CvSize frame_size = cvSize((int)capture.get(CV_CAP_PROP_FRAME_WIDTH), (int)capture.get(CV_CAP_PROP_FRAME_HEIGHT));

	video_target_info.clear();
	g_bcounted.resize(1,0);
	g_draw_fidx = 0;

	//cout<<4<<endl;
	int frame_idx = 0, max_label = 0;
	//vector<Mat> pre_faces_pixels;
	vector<target_info> pre_faces;
 	//vector<rect> pre_faces;
	frame_info pre_faces_info;
	vector<Mat> successive_frames(g_successive_frames);

	Mat bg,bgg, pre_gray;
// 	bg = imread(back_ground_path);
// 	cvtColor( bg, bgg, CV_BGR2GRAY );

	int top_line_y = g_count_line_y - g_max_face_size ;
	int btm_line_y = g_count_line_y + g_max_face_size * 1.2;

	DWORD time = GetTickCount();
	while( true )
	{
		if( !g_bdetect )
		{
			video_target_info.clear();
			g_bcounted.resize(1,0);
			g_draw_fidx = 0;
			break;
		}

		if (!capture.read(frame) )
			break;

		top_line_y = g_count_line_y - g_max_face_size ;
		btm_line_y = g_count_line_y + g_max_face_size * 1.2;

		int detx = 0, dety = top_line_y;
		int detw = frameSize.width, deth = btm_line_y - top_line_y + 1;
		Mat det_frame(deth, detw, CV_8UC1);

		int zoomw = width * g_zoomrate, zoomh = height * g_zoomrate;
		int zdetx = 0, zdety = top_line_y * g_zoomrate;
		int zdetw = zoomw, zdeth = (btm_line_y - top_line_y + 1) * g_zoomrate;
		Mat det_zframe(zdeth, zdetw, CV_8UC1);

		vector<rect> faces;
		MyCascadeClassifier cascade;
		cascade.load(cascade_path.c_str());
		CvHaarClassifierCascade *classifier = cascade.get_classifier_info();
		decrypt_stage_threshold(classifier, 3);

		Mat frame_gray, zoom_gray;
		cvtColor( frame, frame_gray, CV_BGR2GRAY );
// 		if( g_zoomrate < 1.0 )
// 			resize(frame_gray, zoom_gray, cvSize(zoomw, zoomh));

		double r = 1.0 / g_zoomrate;

		frame_info cur_info;
		int det_flag = g_interval == 0 ? 1 : (frame_idx+1) % (g_interval+1);

		DWORD dt = GetTickCount();
		if( det_flag )
		{
			if( g_zoomrate < 1.0 )
			{
				copy_roi_pxl(frame_gray, detx, dety, detw, deth, det_frame);
				resize(det_frame, det_zframe, cvSize(zdetw, zdeth));
				//copy_roi_pxl(zoom_gray, zdetx, zdety, zdetw, zdeth, det_zframe);
				detect_gray(det_zframe, cascade, faces);
				//imwrite("det_zframe.bmp", det_zframe);
			}
			else
			{
				copy_roi_pxl(frame_gray, detx, dety, detw, deth, det_frame);
				detect_gray(det_frame, cascade, faces);
			}
			
			for(int k = 0; k < faces.size(); k++)
			{
				if( g_zoomrate < 1.0 )
				{
					faces[k].xmin = faces[k].xmin * r;
					faces[k].ymin = faces[k].ymin * r;
					faces[k].xmax = faces[k].xmax * r;

					int l = faces[k].xmax - faces[k].xmin;
					faces[k].ymax = faces[k].ymin + l;
				}
				
				faces[k].ymin += top_line_y;
				faces[k].ymax += top_line_y;
			}
		}
		else
		{
			for(int k = 0; k < pre_faces_info.targets.size(); k++)
			{
				int flag = pre_faces_info.targets[k].btrack;
				if( flag == TRACKDETECTED || flag == DETECTED )
					faces.push_back(pre_faces_info.targets[k].pos);
			}
		}
		DWORD dtp = GetTickCount() - dt;
		g_dettime += dtp;

		DWORD tt = GetTickCount();

		set_target_label(pre_gray, frame_gray, faces, pre_faces_info, cur_info, max_label);

		pre_faces_info = cur_info;
		
		//EnterCriticalSection(&gSection); 
		video_target_info.push_back(cur_info);
		erase_false_negative(video_target_info);

		while(g_bcounted.size() < max_label + 1)
			g_bcounted.push_back(0);
		//LeaveCriticalSection(&gSection);

		pre_gray = frame_gray;
		DWORD ttp = GetTickCount() - tt;
		g_tracktime += ttp;
		//frame_gray.copyTo(pre_gray);

		frame_idx++;
		double complete_rate = frame_idx / (double)framenum;
// 		char cinfo[128];
// 		sprintf(cinfo,"正在统计:  %.6f%%",complete_rate*100);
// 		::SetWindowText(g_hinfo, cinfo);
	}
	DWORD dt = GetTickCount() - time;  // 8 * 60s
	capture.release();
// 	cout<<  dt/1000<<endl; 
// 
// 	cout<<endl;
// 	cout<<"检测完成!!!"<<endl;

	//g_bcounted.resize( max_label+1, 0 );
	//draw_video_targets(video_name, dir_out, bgg, video_target_info);
}

void detect( Mat& img, CascadeClassifier& cascade, int span_cb, int span_cr, unsigned char &mincb, unsigned char &mincr, vector<rect> &faces_target)
{
	int i = 0;
	double t = 0;
	vector<Rect> faces;

	Mat gray, yrb,bny(img.rows, img.cols, CV_8U);//, smallImg( cvRound (img.rows/scale), cvRound(img.cols/scale), CV_8UC1 );//将图片缩小，加快检测速度

	cvtColor( img, yrb, CV_BGR2YCrCb );
	cvtColor( img, gray, CV_BGR2GRAY );//因为用的是类haar特征，所以都是基于灰度图像的，这里要转换成灰度图像
	//resize( gray, smallImg, smallImg.size(), 0, 0, INTER_LINEAR );//将尺寸缩小到1/scale,用线性插值
	//equalizeHist( smallImg, smallImg );//直方图均衡
	equalizeHist( gray, gray );

// 	mincb = 100, mincr = 140;
// 	for(int i = 0; i < yrb.rows; i++)
// 		for(int j = 0; j < yrb.cols; j++)
// 		{
// 			unsigned char r = yrb.at<uchar>(i, j*yrb.channels() + 1);
// 			unsigned char b = yrb.at<uchar>(i, j*yrb.channels() + 2);
// 
// 			if( r >= mincr && r <= (int)(span_cr + mincr) && b >= mincb && b <= (int)(mincb + span_cb) )
// 				bny.at<uchar>(i, j*bny.channels()) = 255;
// 			else
// 				bny.at<uchar>(i, j*bny.channels()) = 0;
// 		}

		//t = (double)cvGetTickCount();//用来计算算法执行时间

		cascade.detectMultiScale( gray, faces,
			1.1, 2, 0
			//|CV_HAAR_FIND_BIGGEST_OBJECT
			//|CV_HAAR_DO_ROUGH_SEARCH
			|CV_HAAR_SCALE_IMAGE
			,
			Size(g_min_face_size, g_min_face_size),Size(g_max_face_size,g_max_face_size) );

		for( vector<Rect>::const_iterator r = faces.begin(); r != faces.end(); r++, i++ )
		{
			Mat smallImgROI;
			vector<Rect> nestedObjects;
			Point center;

			int radius;
			center.x = cvRound((r->x + r->width*0.5));//还原成原来的大小
			center.y = cvRound((r->y + r->height*0.5));
			radius = cvRound((r->width + r->height)*0.25);

			int x = center.x - radius, y = center.y - radius;
			//double rate = face_area_rate(bny, x, y, 2*radius);

			rect tgt;
			tgt.xmin = x, tgt.ymin = y, tgt.xmax = x + 2 * radius, tgt.ymax = y + 2 * radius;

			//if( rate > 0.35 ) // ecr >= mincr && ecr <= (int)(span_cr + mincr) && ecb >= mincb && ecb <= (int)(mincb + span_cb)
			{
				faces_target.push_back(tgt);
			}		
		}
}

void detect( Mat& img, CascadeClassifier& cascade, vector<rect> &faces_target)
{
	int i = 0;
	double t = 0;
	vector<Rect> faces;

	Mat gray, yrb,bny(img.rows, img.cols, CV_8U);//, smallImg( cvRound (img.rows/scale), cvRound(img.cols/scale), CV_8UC1 );//将图片缩小，加快检测速度

	cvtColor( img, yrb, CV_BGR2YCrCb );
	cvtColor( img, gray, CV_BGR2GRAY );//因为用的是类haar特征，所以都是基于灰度图像的，这里要转换成灰度图像
	//resize( gray, smallImg, smallImg.size(), 0, 0, INTER_LINEAR );//将尺寸缩小到1/scale,用线性插值
	//equalizeHist( smallImg, smallImg );//直方图均衡
	equalizeHist( gray, gray );

		//t = (double)cvGetTickCount();//用来计算算法执行时间

		cascade.detectMultiScale( gray, faces,
			1.1, 2, 0
			//|CV_HAAR_FIND_BIGGEST_OBJECT
			//|CV_HAAR_DO_ROUGH_SEARCH
			|CV_HAAR_SCALE_IMAGE
			,
			Size(g_min_face_size, g_min_face_size),Size(g_max_face_size,g_max_face_size) );

	for( vector<Rect>::const_iterator r = faces.begin(); r != faces.end(); r++, i++ )
	{
		Mat smallImgROI;
		vector<Rect> nestedObjects;
		Point center;

		int radius;
		center.x = cvRound((r->x + r->width*0.5));//还原成原来的大小
		center.y = cvRound((r->y + r->height*0.5));
		radius = cvRound((r->width + r->height)*0.25);

		int x = center.x - radius, y = center.y - radius;
		//double rate = face_area_rate(bny, x, y, 2*radius);

		rect tgt;
		tgt.xmin = x, tgt.ymin = y, tgt.xmax = x + 2 * radius, tgt.ymax = y + 2 * radius;

		//if( rate > 0.35 ) // ecr >= mincr && ecr <= (int)(span_cr + mincr) && ecb >= mincb && ecb <= (int)(mincb + span_cb)
		{
			faces_target.push_back(tgt);
		}		
	}

	int kkk = 0;

}

void detect_gray( Mat gray, CascadeClassifier& cascade, vector<rect> &faces_target )
{
	int i = 0;
	double t = 0;
	vector<Rect> faces;

	equalizeHist( gray, gray );

	//t = (double)cvGetTickCount();//用来计算算法执行时间

	int minl = g_min_face_size * g_zoomrate;
	int maxl = g_max_face_size * g_zoomrate;
	cascade.detectMultiScale( gray, faces,
		g_scalefactor, g_minneighbors, g_detflag
		//|CV_HAAR_FIND_BIGGEST_OBJECT
		//|CV_HAAR_DO_ROUGH_SEARCH
		|CV_HAAR_SCALE_IMAGE
		,
		Size(minl, minl),Size(maxl,maxl) );

	for( vector<Rect>::const_iterator r = faces.begin(); r != faces.end(); r++, i++ )
	{
		Mat smallImgROI;
		vector<Rect> nestedObjects;
		Point center;

		int radius;
		center.x = cvRound((r->x + r->width*0.5));//还原成原来的大小
		center.y = cvRound((r->y + r->height*0.5));
		radius = cvRound((r->width + r->height)*0.25);

		int x = center.x - radius, y = center.y - radius;
		//double rate = face_area_rate(bny, x, y, 2*radius);

		rect tgt;
		tgt.xmin = x, tgt.ymin = y, tgt.xmax = x + 2 * radius, tgt.ymax = y + 2 * radius;

		//if( rate > 0.35 ) // ecr >= mincr && ecr <= (int)(span_cr + mincr) && ecb >= mincb && ecb <= (int)(mincb + span_cb)
		{
			faces_target.push_back(tgt);
		}		
	}

}

double face_area_rate(Mat skin, int x, int y, int l)
{
	double rate, tarea = l * l;

	int cnt = 0;
	for(int i = y; i < y + l; i++)
		for(int j = x; j < x + l; j++)
		{
			if( i >= skin.rows || j > skin.cols )
				continue;
			unsigned char f = skin.at<uchar>(i, j*skin.channels());//shin->imageData[ i * shin->widthStep + j * shin->nChannels ];

			if( f )
				cnt++;
		}

	rate = cnt / tarea;

	return rate;
}

void encode_stage_threshold(CvHaarClassifierCascade *classifier, double delta)
{
	for(int i = 0; i < classifier->count; i++)
	{
		if( (i+1) % 2 )
			classifier->stage_classifier[i].threshold += delta;
		else
			classifier->stage_classifier[i].threshold -= delta;
	}
}

void decrypt_stage_threshold(CvHaarClassifierCascade *classifier, double delta)
{
	for(int i = 0; i < classifier->count; i++)
	{
		if( (i+1) % 2 )
			classifier->stage_classifier[i].threshold -= delta;
		else
			classifier->stage_classifier[i].threshold += delta;
	}
}

double get_rect_overlap(rect* R1,rect* R2)
{
	int cxR1,cxR2, cyR1,cyR2,wR1,wR2,hR1,hR2;
	cxR1=max(R1->xmax,R2->xmax);
	cxR2=min(R1->xmin,R2->xmin);
	wR1=R1->xmax-R1->xmin;
	wR2=R2->xmax-R2->xmin;    
	cyR1=max(R1->ymax,R2->ymax);
	cyR2=min(R1->ymin,R2->ymin);
	hR1=R1->ymax-R1->ymin;
	hR2=R2->ymax-R2->ymin;
	int w=wR1+wR2-(cxR1-cxR2);
	int h=hR1+hR2-(cyR1-cyR2);
	if(w<0) w=0;if(h<0) h=0;

	double mina = wR1*hR1 < wR2*hR2 ? wR1*hR1 : wR2*hR2;
	double rate = w*h / mina;
	return rate;
}

void combine_two_faces(rect *dst, rect *src)
{
	dst->xmin = (int)((dst->xmin + src->xmin) / 2 + 0.5);
	dst->xmax = (int)((dst->xmax + src->xmax) / 2 + 0.5);
	dst->ymin = (int)((dst->ymin + src->ymin) / 2 + 0.5);
	dst->ymax = (int)((dst->ymax + src->ymax) / 2 + 0.5);
}

void draw_comb_target(Mat image, vector<rect> &faces)
{
	for(int i = 0; i < faces.size(); i++)
	{
		Point center;
		center.x = (faces[i].xmax + faces[i].xmin)/2, center.y = (faces[i].ymax + faces[i].ymin)/2;
		int radius = (faces[i].xmax - faces[i].xmin)/2;
		circle( image, center, radius, CV_RGB(255,255,0), 2, 8, 0 );
	}
}

void draw_line(Mat image, int *points)
{
	CvPoint pnt1,pnt2,pnt3,pnt4;

	pnt1.x = points[0], pnt1.y = points[1], pnt2.x = points[2], pnt2.y = points[3];
	pnt3.x = points[4], pnt3.y = points[5], pnt4.x = points[6], pnt4.y = points[7];

	line(image, pnt1, pnt2, CV_RGB(255,255,0));
	line(image, pnt2, pnt3, CV_RGB(255,255,0));
	line(image, pnt3, pnt4, CV_RGB(255,255,0));
	line(image, pnt4, pnt1, CV_RGB(255,255,0));
}

void track_faces(vector<Mat> &pre_target, Mat cur_frame, int search_radius, vector<rect> &pre_faces, vector<rect> &tracked)
{
	int i,j,k;
	tracked.resize( pre_faces.size() );

	for(k = 0; k < pre_faces.size(); k++)
	{
		int w = pre_faces[k].xmax - pre_faces[k].xmin + 1;
		int h = pre_faces[k].ymax - pre_faces[k].ymin + 1;

		int min_diff = 999999999;
		for(int start_y = pre_faces[k].ymin - search_radius; start_y <= pre_faces[k].ymin + search_radius; start_y++)    // define search area
			for(int start_x = pre_faces[k].xmin - search_radius; start_x <= pre_faces[k].xmin + search_radius; start_x++)
			{
				int diff = 0;
				for(i = start_y; i < start_y + h; i++)
					for(j = start_x; j < start_x + w; j++)
					{
						unsigned char cur_g = cur_frame.at<uchar>(i, j*cur_frame.channels());
						unsigned char pre_g = pre_target[k].at<uchar>(i-start_y, (j-start_x)*pre_target[k].channels());

						diff += abs(cur_g - pre_g);
					}

				if( diff < min_diff )
				{
					min_diff = diff;
					tracked[k].xmin = start_x;
					tracked[k].xmax = start_x + w - 1;
					tracked[k].ymin = start_y;
					tracked[k].ymax = start_y + h - 1;
				}
			}
	}
}

double track_face(Mat pre_frame, rect pre_face, int search_radius, Mat cur_frame, rect &tracked)
{
	int x = pre_face.xmin, y = pre_face.ymin;
	int w = pre_face.xmax - pre_face.xmin + 1;
	int h = pre_face.ymax - pre_face.ymin + 1;

	Mat pface(h,w, CV_8UC1);
	copy_roi_pxl(pre_frame, x, y, w, h, pface);

	int i,j;
	double maxcor = 0;
	int st = pre_face.ymin - search_radius < 0 ? 0 : pre_face.ymin - search_radius;
	int sl = pre_face.xmin - search_radius < 0 ? 0 : pre_face.xmin - search_radius;
	int sb = pre_face.ymin + search_radius + h >= pre_frame.rows ? pre_frame.rows - h - 1 : pre_face.ymin + search_radius;
	int sr = pre_face.xmin + search_radius + w >= pre_frame.cols ? pre_frame.cols - w - 1 : pre_face.xmin + search_radius;

	for(int start_y = st; start_y <= sb; start_y++)    // define search area
		for(int start_x = sl; start_x <= sr; start_x++)
		{
			int diff = 0;
			Mat cface(h, w, CV_8UC1);
			for(i = start_y; i < start_y + h; i++)
				for(j = start_x; j < start_x + w; j++)
				{
					unsigned char cur_g = cur_frame.at<uchar>(i, j*cur_frame.channels());
					cface.at<uchar>((i-start_y), (j-start_x)*cur_frame.channels()) = cur_g;
				}

			double cor = get_correlation(pface, cface, 64);

			if( abs(cor) > maxcor && abs(cor) > corr_t )
			{
				maxcor = abs(cor);
				tracked.xmin = start_x;
				tracked.xmax = start_x + w - 1;
				tracked.ymin = start_y;
				tracked.ymax = start_y + h - 1;
			}
		}

	return maxcor;
}

double search_face(Mat pre_frame, rect pre_face, int search_radius, Mat cur_frame, rect &tracked)
{
	int x = pre_face.xmin, y = pre_face.ymin;
	int w = pre_face.xmax - pre_face.xmin + 1;
	int h = pre_face.ymax - pre_face.ymin + 1;

// 	Mat pface(h,w, CV_8UC1);
// 	copy_roi_pxl(pre_frame, x, y, w, h, pface);

	int i,j,cnt = 0;
	double mindif = 255;
	int st = pre_face.ymin - search_radius < 0 ? 0 : pre_face.ymin - search_radius;
	int sl = pre_face.xmin - search_radius < 0 ? 0 : pre_face.xmin - search_radius;
	int sb = pre_face.ymin + search_radius + h >= pre_frame.rows ? pre_frame.rows - h - 1 : pre_face.ymin + search_radius;
	int sr = pre_face.xmin + search_radius + w >= pre_frame.cols ? pre_frame.cols - w - 1 : pre_face.xmin + search_radius;

	for(int start_y = st; start_y <= sb; start_y+=3)    // define search area
		for(int start_x = sl; start_x <= sr; start_x+=3)
		{
			cnt = 0;
			double diff = 0;
			for(i = start_y; i < start_y + h; i+=3)
				for(j = start_x; j < start_x + w; j+=3)
				{
					diff += abs( cur_frame.at<uchar>(i, j*cur_frame.channels()) - 
								 pre_frame.at<uchar>((i-start_y+y), (j-start_x+x)*pre_frame.channels()) );

					cnt++;
				}

			diff = diff / cnt;//(w * h);

			if( diff < mindif && diff < g_avgdiff )
			{
				mindif = diff;
				tracked.xmin = start_x;
				tracked.xmax = start_x + w - 1;
				tracked.ymin = start_y;
				tracked.ymax = start_y + h - 1;
			}
		}

	return mindif;
}

void track_faces(vector<rect> &pre_faces, vector<rect> &cur_faces, vector<int> &pre_idx, vector<int> &cur_idx, int &max_idx)
{
	int i,j;
	if( pre_idx.size() == 0 )
	{
		for(i = 0; i < cur_faces.size(); i++)
			cur_idx.push_back(i+1);

		max_idx = cur_idx.size();

		return;
	}

	vector<double> overlap_rate;
	vector<int> match_idx(cur_faces.size());

	cur_idx.resize(cur_faces.size());
	for(i = 0; i < cur_faces.size(); i++)
	{
		double max_rate = 0;
		for(j = 0; j < pre_faces.size(); j++)
		{
			double rate = get_rect_overlap( &cur_faces[i], &pre_faces[j] );
			if( rate > max_rate )
			{
				max_rate = rate;
				cur_idx[i] = pre_idx[j];
			}
		}

		if( max_rate < 0.5 ) // new face
		{
			max_idx++;
			cur_idx[i] = max_idx;
		}
	}
}

void set_target_label(Mat pre_frame, Mat cur_frame, vector<rect> &cur_faces, frame_info &pre_info, frame_info &cur_info, int &max_label)
{
	int i,j;
	int width = pre_frame.cols, height = pre_frame.rows;

// 	if( !cur_faces.size() )
// 		return;

	cur_info.targets.resize( cur_faces.size() );

	for(i = 0; i < cur_faces.size(); i++)
	{
		cur_info.targets[i].pos = cur_faces[i];
		cur_info.targets[i].btrack = DETECTED;
	}

	if( pre_info.targets.size() == 0 )
	{
		for(i = 0; i < cur_faces.size(); i++)
		{
			max_label++;
			cur_info.targets[i].label = max_label;
		}

		return;
	}

	vector<int> newflag(cur_faces.size(), 1);
	vector<int> trackflag(pre_info.targets.size(), 0);
	for(i = 0; i < pre_info.targets.size(); i++)
	{
		double max_rate = 0;
		vector<rect> cand;
		vector<int> cidx;
		for(j = 0; j < cur_faces.size(); j++)
		{
			double rate = get_rect_overlap(&(pre_info.targets[i].pos), &cur_faces[j]);

			if( rate > 0 )
			{
				cand.push_back(cur_faces[j]);
				cidx.push_back(j);
			}
		}

		int x = pre_info.targets[i].pos.xmin, y = pre_info.targets[i].pos.ymin;
		int w = pre_info.targets[i].pos.xmax-x+1, h = pre_info.targets[i].pos.ymax-y+1;

		Mat pre_face(h, w, CV_8UC1);
		copy_roi_pxl(pre_frame, x, y, w, h, pre_face);

		double maxcor = 0, mindiff = 255;
		if( cand.size() > 0 )
		{
			int index = -1;
			for(int k = 0; k < cand.size(); k++)    // 寻找匹配度最高的人脸
			{
				int cx = cand[k].xmin, cy = cand[k].ymin;
				int cw = cand[k].xmax-cx+1, ch = cand[k].ymax-cy+1;

				Mat cur_face(ch, cw, CV_8UC1);
				copy_roi_pxl(cur_frame, cx, cy, cw, ch, cur_face);
				//double cor = get_correlation(cur_face, pre_face, 64);
// 				if( abs(cor) > maxcor /*&& cor > corr_t*/ )
// 				{
// 					maxcor = abs(cor);
// 					index = k;
// 				}

				int stdl = (h + ch)/2;
				double diff = diff_images(cur_face, pre_face, stdl);

				if( diff < mindiff )
				{
					mindiff = diff;
					index = k;
				}
			}

			if( /*maxcor > 0*/ mindiff < 255 )
			{
				cur_info.targets[cidx[index]].label = pre_info.targets[i].label;
				cur_info.targets[cidx[index]].btrack = TRACKDETECTED;
				newflag[ cidx[index] ] = 0;
				trackflag[i] = 1;
			}
// 			else
// 			{
// 				max_label++;
// 				pre_info.targets[i].label = max_label;
// 			}
		}
	}

	for(i = 0; i < newflag.size(); i++)
	{
		if( newflag[i] )
		{
			max_label++;
			cur_info.targets[i].label = max_label;
		}
	}

	for(i = 0; i < trackflag.size(); i++)
	{
		if( trackflag[i] || /*det_pass_line(pre_info.targets[i])*/!bneed_track(pre_info.targets[i]) 
			|| !binsight(width, height, pre_info.targets[i].pos) )
			continue;

		rect tracked;
		double l = pre_info.targets[i].pos.xmax - pre_info.targets[i].pos.xmin + 1;
		//double cor = track_face(pre_frame, pre_info.targets[i].pos, l/4, cur_frame, tracked);

		double diff = search_face(pre_frame, pre_info.targets[i].pos, l/4, cur_frame, tracked);

		if( /*abs(cor) > corr_t*/ diff < g_avgdiff )
		{
			target_info face;
			face.pos = tracked;
			face.label = pre_info.targets[i].label;
			int btrack = pre_info.targets[i].btrack < 0 ? 0 : pre_info.targets[i].btrack;
			face.btrack = btrack + 1;

			cur_info.targets.push_back(face);
		}
	}
}

void set_label(Mat pre_frame, Mat cur_frame, vector<rect> &cur_faces, frame_info &pre_info, frame_info &cur_info, int &max_label)
{
	int i,j,index;
	int width = pre_frame.cols, height = pre_frame.rows;
	cur_info.targets.resize( cur_faces.size() );

	for(i = 0; i < cur_faces.size(); i++)
	{
		cur_info.targets[i].pos = cur_faces[i];
		cur_info.targets[i].btrack = DETECTED;
	}

	if( pre_info.targets.size() == 0 )
	{
		for(i = 0; i < cur_faces.size(); i++)
		{
			max_label++;
			cur_info.targets[i].label = max_label;
		}

		return;
	}

	vector<int> trackflag(pre_info.targets.size(), 0);
	for(i = 0; i < cur_faces.size(); i++)
	{
		double max_rate = 0;
		vector<rect> cand;
		vector<int> pidx;
		for(j = 0; j < pre_info.targets.size(); j++)
		{
			double rate = get_rect_overlap(&cur_faces[i], &(pre_info.targets[j].pos));

			if( rate > 0 )
			{
				cand.push_back(pre_info.targets[j].pos);
				pidx.push_back(j);
			}
		}

		int x = cur_faces[i].xmin, y = cur_faces[i].ymin;
		int w = cur_faces[i].xmax-x+1, h = cur_faces[i].ymax-y+1;

		Mat cur_face(h, w, CV_8UC1);
		copy_roi_pxl(cur_frame, x, y, w, h, cur_face);

		double maxcor = 0;
		if( cand.size() > 0 )
		{
			for(int k = 0; k < cand.size(); k++)    // 寻找匹配度最高的人脸
			{
				int px = cand[k].xmin, py = cand[k].ymin;
				int pw = cand[k].xmax-x+1, ph = cand[k].ymax-y+1;

				Mat pre_face(ph, pw, CV_8UC1);
				copy_roi_pxl(pre_frame, px, py, pw, ph, pre_face);
				double cor = get_correlation(cur_face, pre_face, 64);

				if( abs(cor) > maxcor && abs(cor) > corr_t )
				{
					maxcor = abs(cor);
					index = k;
				}
			}

			cur_info.targets[i].label = pre_info.targets[pidx[index]].label;
			trackflag[pidx[index]] = 1;
		}
		else
		{
			max_label++;
			cur_info.targets[i].label = max_label;
		}
	}
}

int get_cross_flag(target_info target, int count_line_y)
{
	int flag;

	if( target.pos.ymax < count_line_y )
		flag = NOTPASS;//NOTPASS;

	if( count_line_y <= target.pos.ymax && target.pos.ymin <= count_line_y )
		flag = PASSING;

	if( target.pos.ymin > count_line_y )
		flag = PASSED;//PASSED;

	if( target.is_cross == COUNTED )
		flag = COUNTED;

	return flag;
}

int is_passed(int label, int pre_idx, int cur_idx, int count_line_y)
{
	int i,pre_flag = -1, cur_flag = -1, flag = 0;

	for(i = 0; i < video_target_info[pre_idx].targets.size(); i++)
	{
		if( video_target_info[pre_idx].targets[i].label == label )
		{
			pre_flag = video_target_info[pre_idx].targets[i].is_cross;
			break;
		}
	}

	for(i = 0; i < video_target_info[cur_idx].targets.size(); i++)
	{
		if( video_target_info[cur_idx].targets[i].label == label )
		{
			cur_flag = video_target_info[cur_idx].targets[i].is_cross;
			break;
		}
	}

	if( pre_flag == PASSING && cur_flag == PASSED )
		flag = PASSED;

	if( g_bcounted[label] )
		flag = COUNTED;

// 	if( pre_flag == -1 && cur_flag == PASSED )
// 		flag = LOSEPASSED;

	return flag;
}

int is_target(Mat diff, rect r)
{
	int flag = 0, i, j, sum = 0;

	for(i = r.ymin; i <= r.ymax; i++)
		for(j = r.xmin; j <= r.xmax; j++)
		{
			sum += diff.at<uchar>(i,j*diff.channels());
		}

	int n = (r.ymax-r.ymin+1)*(r.xmax-r.xmin+1);

	if( (double)sum / n > g_avgdiff )
		flag = 1;

	return flag;
}

void erase_background_target(Mat &diff, frame_info &cur_frame)
{
	int i;
	vector<target_info> tmp;
	for(i = 0; i < cur_frame.targets.size(); i++)
	{
		if( is_target(diff, cur_frame.targets[i].pos) )
			tmp.push_back(cur_frame.targets[i]);
	}

	cur_frame.targets = tmp;
}

void erase_background_target(Mat bggray, Mat frame, string cascade_path, frame_info &cur_frame)
{
	int i;
	vector<target_info> faces;
	for(i = 0; i < cur_frame.targets.size(); i++)
	{
// 		int x = cur_frame.targets[i].pos.xmin;
// 		int y = cur_frame.targets[i].pos.ymin;
// 		int w = cur_frame.targets[i].pos.xmax - x + 1;
// 		int h = cur_frame.targets[i].pos.ymax - y + 1;
// 
// 		Mat bg(h, w, CV_8UC1);
// 		Mat face(h, w, CV_8UC1);
// 
// 		copy_roi_pxl(bggray, x, y, w, h, bg);
// 		copy_roi_pxl(frame, x, y, w, h, face);
// 		double cor = get_correlation(bg, face, 64);
// 
// 		if( cor < 2.0 )
			faces.push_back(cur_frame.targets[i]);
	}

	vector<target_info> faces2;
	for(i = 0; i < faces.size(); i++)
	{
		if( faces[i].btrack == DETECTED )
		{
			faces2.push_back(faces[i]);
			continue;
		}

		CascadeClassifier cascade;
		cascade.load(cascade_path.c_str());

		int l = faces[i].pos.xmax - faces[i].pos.xmin + 1;
		int dx = faces[i].pos.xmin - l/2 < 0 ? 0 : faces[i].pos.xmin - l/2;
		int dy = faces[i].pos.ymin - l/2 < 0 ? 0 : faces[i].pos.ymin - l/2;
		int dr = faces[i].pos.xmax + l/2 >= frame.cols ? frame.cols - 1 : faces[i].pos.xmax + l/2;
		int db = faces[i].pos.ymax + l/2 >= frame.rows ? frame.rows - 1 : faces[i].pos.ymax + l/2;
		int h = db - dy + 1, w = dr - dx + 1;

		vector<rect> fs;
		Mat locimg(h, w, CV_8UC1);
		copy_roi_pxl(frame, dx, dy, w, h, locimg);
		detect_gray(locimg, cascade, fs);

		if( fs.size() )
			faces2.push_back(faces[i]);
	}

	cur_frame.targets = faces2;
}

void erase_false_negative(vector<frame_info> &video_info)
{
	int size = video_info.size();
	if( size <= g_max_track_num )
		return;

	int i,j,k;
	vector<int> elabel;
	for(i = 0; i < video_info[size-1].targets.size(); i++)
	{
		if( video_info[size-1].targets[i].btrack > g_max_track_num )
			elabel.push_back( video_info[size-1].targets[i].label );
	}

	if( elabel.size() )
	{
		for(i = size - 1; i >= size - g_max_track_num - 2; i--)
		{
			vector<target_info> targets;
			for(j = 0; j < video_info[i].targets.size(); j++)
			{
				int label = video_info[i].targets[j].label;

				k=0;
				while( k < elabel.size() && elabel[k] != label )k++;

				if( k == elabel.size() )
					targets.push_back(video_info[i].targets[j]);
			}

			video_info[i].targets = targets;
		}
	}
}

void diff_width_background(Mat frame, Mat background, Mat &diff)
{
	int i,j;
	for(i = 0; i < frame.rows; i++)
		for(j = 0; j < frame.cols; j++)
		{
			unsigned char fg = frame.at<uchar>(i, j*frame.channels());
			unsigned char bg = background.at<uchar>(i, j*background.channels());

			diff.at<uchar>(i, j) = abs(fg-bg);
		}

	imwrite("diff.bmp", diff);
}

void diff_width_background(string video_path, string bg_path, string out_path)
{
	IplImage *frame,*gray,*diff, *bg, *bg_gray;
	CvCapture * capture = cvCreateFileCapture(video_path.c_str());

	int framenum = cvGetCaptureProperty(capture, CV_CAP_PROP_FRAME_COUNT);
	int frameRate = cvGetCaptureProperty(capture, CV_CAP_PROP_FPS);
	int width = cvGetCaptureProperty(capture, CV_CAP_PROP_FRAME_WIDTH);
	int height = cvGetCaptureProperty(capture, CV_CAP_PROP_FRAME_HEIGHT);

	int frame_idx = 0;
	CvSize frame_size;
	frame_size.width = width, frame_size.height = height;

	bg = cvLoadImage(bg_path.c_str());
	bg_gray = cvCreateImage(cvSize(width, height), 8, 1);
	cvCvtColor(bg, bg_gray, CV_BGR2GRAY);

	gray = cvCreateImage(cvSize(width, height), 8, 1);
	gray = cvCreateImage(cvSize(width, height), 8, 1);
	diff = cvCreateImage(cvSize(width, height), 8, 3);
	CvVideoWriter* wrvideo = cvCreateVideoWriter(out_path.c_str(), CV_FOURCC('M','P','4','2'), frameRate, frame_size);

	while(true)
	{
		frame = cvQueryFrame(capture);
		if(!frame)break;

		cvCvtColor(frame, gray, CV_BGR2GRAY);

		for(int i = 0; i < height; i++)
			for(int j = 0; j < width; j++)
			{
				int adr = i * bg_gray->widthStep + j;
				BYTE bg = bg_gray->imageData[ adr ];
				BYTE cg = gray->imageData[ adr ];

				BYTE d = abs( bg - cg ) < 55 ? 0 : abs( bg - cg );

				int adr2 = i * diff->widthStep + j*3;
				diff->imageData[ adr2 ] = d;
				diff->imageData[ adr2 +1] = d;
				diff->imageData[ adr2 +2] = d;
			}

		cvWriteFrame(wrvideo, diff);

		frame_idx++;
		double complete_rate = frame_idx / (double)framenum;
		printf("\r 已完成:   %.8f%%",complete_rate*100);
	}

	cvReleaseVideoWriter(&wrvideo);
}

void get_detected_face_pixels(Mat frame, vector<rect> &faces, vector<Mat> &face_pixels)
{
	face_pixels.resize( faces.size() );

	for(int k = 0; k < faces.size(); k++)
	{
		int w = faces[k].xmax - faces[k].xmin + 1;
		int h = faces[k].ymax - faces[k].ymin + 1;

		Mat f(h, w, CV_8UC1);
		for(int i = faces[k].ymin; i <= faces[k].ymax; i++)
			for(int j = faces[k].xmin; j <= faces[k].xmax; j++)
			{
				f.at<uchar>(i-faces[k].ymin, (j-faces[k].xmin)*f.channels()) = frame.at<uchar>(i,j*frame.channels());
			}

		face_pixels[k] = f;
	}
}

void make_targets_successive(vector<Mat> &frames, vector<frame_info> &vtargets_info)
{
	if( vtargets_info.size() < g_successive_frames )
		return;

	int nframes = vtargets_info.size();
	int start_idx = nframes - g_successive_frames;

	int i,j,k;

	vector<TargetInfo> targets;

	for(i = start_idx; i < start_idx + g_successive_frames; i++)
	{
		 for(j = 0; j < vtargets_info[i].targets.size(); j++)
		 {
			 int label = vtargets_info[i].targets[j].label;

			 int ii,jj;
			 for(ii = 0; ii < targets.size(); ii++)
			 {
				 if( label == targets[ii].label )
					 break;
			 }

			 if( ii < targets.size() )
			 {
				 if( targets[ii].end < i )
				 {
					 targets[ii].pos.push_back( vtargets_info[i].targets[j].pos );
					 targets[ii].end = i;
				 }
			 }
			 else
			 {
				 TargetInfo target;
				 target.label = label;
				 target.pos.push_back( vtargets_info[i].targets[j].pos );
				 target.begin = i;
				 target.end = i;

				 targets.push_back(target);
				 int kkk = 0;
			 }
		 }
	}

	for(i = 0; i < targets.size(); i++)
	{
		if( targets[i].end < start_idx + g_successive_frames - 2 )
		{
			rect lpos = targets[i].pos[ targets[i].pos.size() - 1 ];

			for(j = targets[i].end + 1; j < start_idx + g_successive_frames; j++)
			{
				int index;
				double max_rate = 0;
				for(k = 0; k < vtargets_info[j].targets.size(); k++)
				{
					if( targets[i].label == vtargets_info[j].targets[k].label )
						continue;

					double rate = get_rect_overlap(&lpos, &vtargets_info[j].targets[k].pos);

					if( rate > max_rate )
					{
						max_rate = rate;
						index = k;
					}
				}

				if( max_rate > 0.6 )
				{
					int fidx = targets[i].end - start_idx;
					int fnxt = j - start_idx;

					int diff = 0;
					for(int ii = lpos.ymin; ii <= lpos.ymax; ii++)
						for(int jj = lpos.xmin; jj <= lpos.xmax; jj++)
						{ 
							unsigned char cur_g = frames[fidx].at<uchar>(ii, jj*frames[fidx].channels());
							unsigned char nxt_g = frames[fnxt].at<uchar>(ii, jj*frames[fnxt].channels());

							diff += abs(cur_g - nxt_g);
						}

					if( diff < (lpos.xmax-lpos.xmin)*(lpos.ymax-lpos.ymin) * MATCHTHRE )
					{
						int clabel = vtargets_info[j].targets[index].label;
						 
						int d = j - targets[i].end - 1;           //  漏检帧个数
						for(int m = 1; m <= d; m++)
						{
							target_info t;
							t.pos = lpos;
							t.label = targets[i].label;

							vtargets_info[targets[i].end+m].targets.push_back(t);
						}

						for(int m = j; m < start_idx + g_successive_frames; m++)
							for(int n = 0; n < vtargets_info[m].targets.size(); n++)
							{
								if( vtargets_info[m].targets[n].label == clabel)
									vtargets_info[m].targets[n].label = targets[i].label;
							}

						targets[i].end = start_idx + g_successive_frames;
						break;
					}
				}
			}
		}
	}
}

void g_GetImgFullScaleDspData(IplImage *dspbuf, IplImage *origin)
{
	cvZero(dspbuf);
	int marginx = 0, marginy = 0;

	double scalex = (double)(dspbuf->width) / origin->width;
	double scaley = (double)(dspbuf->height) / origin->height;
	double scale = scalex < scaley ? scalex : scaley;

	IplImage *simg = NULL;
	CvSize size;

	if( scalex <= scaley )
	{
		size.width = dspbuf->width, size.height = scale * origin->height;
		simg = cvCreateImage(size, origin->depth, 3);
		cvResize(origin, simg);

		marginy = (dspbuf->height - size.height)/2;

		for(int i = 0; i < simg->height; i++)
		{
			int linebytesrc = i * simg->widthStep;
			int linebytedst = (i+marginy)*dspbuf->widthStep;

			for(int j = 0; j < simg->width; j++)
			{
				dspbuf->imageData[ linebytedst + j * dspbuf->nChannels ] = simg->imageData[ linebytesrc + j * simg->nChannels ];
				dspbuf->imageData[ linebytedst + j * dspbuf->nChannels +1] = simg->imageData[ linebytesrc + j * simg->nChannels +2];
				dspbuf->imageData[ linebytedst + j * dspbuf->nChannels +1] = simg->imageData[ linebytesrc + j * simg->nChannels +2];
			}
		}
	}
	else
	{
		size.width = origin->width * scale, size.height = dspbuf->height;
		simg = cvCreateImage(size, origin->depth, 3);
		cvResize(origin, simg);

		marginx = (dspbuf->width - size.width)/2;

		for(int i = 0; i < simg->height; i++)
		{
			int linebytesrc = i * simg->widthStep;
			int linebytedst = i*dspbuf->widthStep;

			for(int j = 0; j < simg->width; j++)
			{
				dspbuf->imageData[ linebytedst + (j+marginx)*dspbuf->nChannels ] = simg->imageData[ linebytesrc + j * simg->nChannels ];
				dspbuf->imageData[ linebytedst + (j+marginx)*dspbuf->nChannels +1] = simg->imageData[ linebytesrc + j * simg->nChannels +1];
				dspbuf->imageData[ linebytedst + (j+marginx)*dspbuf->nChannels +2] = simg->imageData[ linebytesrc + j * simg->nChannels +2];
			}
		}
	}

	cvReleaseImage(&simg);
}

void draw_count_info(string video_path, vector<frame_info> &video_info)
{
	CvCapture* capture=cvCreateFileCapture(video_path.c_str());
	IplImage *frame = NULL; // 现在的视频帧

	int frameRate = (int)cvGetCaptureProperty(capture, CV_CAP_PROP_FPS);//capture.get(CV_CAP_PROP_FPS);
	int framenum = (int)cvGetCaptureProperty(capture, CV_CAP_PROP_FRAME_COUNT);//capture.get(CV_CAP_PROP_FRAME_COUNT);
	int width = (int)cvGetCaptureProperty(capture,CV_CAP_PROP_FRAME_WIDTH);
	int height = (int)cvGetCaptureProperty(capture,CV_CAP_PROP_FRAME_HEIGHT);

// 	CvSize frame_size;
// 	frame_size.width = width, frame_size.height = height;
// 	CvVideoWriter* wrvideo = cvCreateVideoWriter("counted.avi", CV_FOURCC('D','I','V','X'), frameRate, frame_size);

	cv::VideoWriter writer;
	cv::Size frameSize;
	frameSize.width = width, frameSize.height = height;
	writer.open("counted.avi", CV_FOURCC('D','I','V','X') , frameRate, frameSize, true); 

	cv::Point pnt1,pnt2;

	CvFont font;//以下用文字标识说明
	double hScale=1, vScale=1;
	int lineWidth=2;
	cvInitFont(&font,CV_FONT_HERSHEY_SIMPLEX|CV_FONT_ITALIC,hScale,vScale,0,lineWidth,8);

	int /*frame_index = 0,*/ counted = 0;
	while( true )
	{
		if( !g_bdraw )break;
		int det_nf = video_target_info.size();
		if( ( det_nf < 25 ) || ( g_draw_fidx + 25 > det_nf ) )
			continue;

		pnt1.x = 0, pnt1.y = g_count_line_y;
		pnt2.x = width - 1, pnt2.y = g_count_line_y;

		frame = cvQueryFrame(capture);
		if (!frame)break;

		char cinfo[128];
		double complete_rate = (g_draw_fidx+1) / (double)framenum;
		sprintf(cinfo,"正在播放第  %d  帧", g_draw_fidx );
		::SetWindowText(g_hinfo, cinfo);

		int pass_flag, diff_flag = 0;

		if( g_draw_fidx == 91 )
			int xxxx = 0;
		//EnterCriticalSection(&gSection);
		for(int i = 0; i < video_target_info[g_draw_fidx].targets.size(); i++)
		{
			video_target_info[g_draw_fidx].targets[i].is_cross = get_cross_flag(video_target_info[g_draw_fidx].targets[i], g_count_line_y);

			if( g_draw_fidx )
				pass_flag = is_passed(video_target_info[g_draw_fidx].targets[i].label, g_draw_fidx-1, g_draw_fidx, g_count_line_y);

			if( g_draw_fidx && ( pass_flag == PASSED/* || pass_flag == LOSEPASSED*/ ) )
			{
				counted++;
				video_target_info[g_draw_fidx].targets[i].is_cross = COUNTED;
				g_bcounted[ video_target_info[g_draw_fidx].targets[i].label ] = 1;
			}
			
			CvPoint p1,p2,p3,p4;
			p1.x = video_target_info[g_draw_fidx].targets[i].pos.xmin, p1.y = video_target_info[g_draw_fidx].targets[i].pos.ymin;
			p2.x = video_target_info[g_draw_fidx].targets[i].pos.xmax, p2.y = video_target_info[g_draw_fidx].targets[i].pos.ymin;
			p3.x = video_target_info[g_draw_fidx].targets[i].pos.xmax, p3.y = video_target_info[g_draw_fidx].targets[i].pos.ymax; 
			p4.x = video_target_info[g_draw_fidx].targets[i].pos.xmin, p4.y = video_target_info[g_draw_fidx].targets[i].pos.ymax;

			cvLine(frame, p1, p2, CV_RGB(255,255,0), 2);
			cvLine(frame, p2, p3, CV_RGB(255,255,0), 2);
			cvLine(frame, p3, p4, CV_RGB(255,255,0), 2);
			cvLine(frame, p4, p1, CV_RGB(255,255,0), 2);
			//draw_line(frame, loc);

			double font_scale = 2;
			char face_idx[25];
			sprintf(face_idx, "%d", video_target_info[g_draw_fidx].targets[i].label);

			cvPutText(frame, face_idx, cvPoint(video_target_info[g_draw_fidx].targets[i].pos.xmin, video_target_info[g_draw_fidx].targets[i].pos.ymin - 5),
						&font, CV_RGB(255,255,0));
		}
		//LeaveCriticalSection(&gSection);

		char target_num[128];
		sprintf(target_num, "target counted: %d", counted);
		cvPutText(frame,target_num,cvPoint(50,50), &font, CV_RGB(255,255,0));

		cvLine(frame, pnt1, pnt2, CV_RGB(255,0,0));

		CvPoint minface, minface2, maxface, maxface2;
		maxface.x = width - 50 - g_max_face_size, maxface.y = 50;
		maxface2.x = width - 50, maxface2.y = 50 + g_max_face_size;

		minface.x = width - 150 - g_max_face_size - g_min_face_size, minface.y = 50;
		minface2.x = width - 150 - g_max_face_size, minface2.y = 50 + g_min_face_size;
		cvRectangle(frame, minface, minface2, CV_RGB(255,255,0), 2);
		cvRectangle(frame, maxface, maxface2, CV_RGB(255,255,0), 2);

		cvPutText(frame,"max face",cvPoint(width - 50 - g_max_face_size,45), &font, CV_RGB(255,255,0));
		cvPutText(frame,"min face",cvPoint(width - 150 - g_max_face_size - g_min_face_size,45), &font, CV_RGB(255,255,0));

		g_GetImgFullScaleDspData(g_dspbuf, frame);
		InvalidateRect(g_hwnd, g_rect, FALSE);
		UpdateWindow(g_hwnd);

		g_draw_fidx++;
		//double complete_rate = frame_index / (double)framenum;

		//char vinfo[128];
		//sprintf(vinfo,"正在写入视频:  %.6f%%",complete_rate*100);
		//printf(vinfo,"\r 已完成:   %.8f%%",complete_rate*100);
		//::SetWindowText(g_hinfo, vinfo);

		//cvWriteFrame(wrvideo, frame);
		Mat f(frame);
		writer.write(f);
	}
	writer.release();
	cvReleaseCapture(&capture);
	//cvReleaseImage(&frame);
	//cvReleaseVideoWriter(&wrvideo);
}

void draw_video_targets(string video_path, string out_path, Mat background, vector<frame_info> &video_info)
{
	cv::VideoCapture capture(video_path.c_str());
	cv::Mat frame; // 现在的视频帧

	int frameRate = capture.get(CV_CAP_PROP_FPS);
	int framenum = capture.get(CV_CAP_PROP_FRAME_COUNT);
	cv::Size frameSize = cv::Size((int)capture.get(CV_CAP_PROP_FRAME_WIDTH),  
		(int)capture.get(CV_CAP_PROP_FRAME_HEIGHT));

	CvSize frame_size = cvSize((int)capture.get(CV_CAP_PROP_FRAME_WIDTH), (int)capture.get(CV_CAP_PROP_FRAME_HEIGHT));

	cv::VideoWriter writer;
	writer.open(out_path, CV_FOURCC('D','I','V','X') , frameRate, frameSize, true); 

	cv::Point pnt1,pnt2;
	pnt1.x = 0, pnt1.y = g_count_line_y;
	pnt2.x = frame_size.width - 1, pnt2.y = g_count_line_y;

	int frame_index = 0, counted = 0;
	while( true )
	{
		if (!capture.read(frame))  
			break;

		Mat frame_gray,diff(frame.rows, frame.cols, CV_8UC1);
		cvtColor( frame, frame_gray, CV_BGR2GRAY );

		int pass_flag, diff_flag = 0;
		for(int i = 0; i < video_target_info[frame_index].targets.size(); i++)
		{
			video_target_info[frame_index].targets[i].is_cross = get_cross_flag(video_target_info[frame_index].targets[i], g_count_line_y);

			if( frame_index )
			   pass_flag = is_passed(video_target_info[frame_index].targets[i].label, frame_index-1, frame_index, g_count_line_y);

			if( frame_index && ( pass_flag == PASSED/* || pass_flag == LOSEPASSED*/ ) )
			{
				counted++;
				video_target_info[frame_index].targets[i].is_cross = COUNTED;
				g_bcounted[ video_target_info[frame_index].targets[i].label ] = 1;
			}

			int loc[8] = { video_target_info[frame_index].targets[i].pos.xmin, video_target_info[frame_index].targets[i].pos.ymin, 
							video_target_info[frame_index].targets[i].pos.xmax, video_target_info[frame_index].targets[i].pos.ymin, 
							video_target_info[frame_index].targets[i].pos.xmax, video_target_info[frame_index].targets[i].pos.ymax, 
							video_target_info[frame_index].targets[i].pos.xmin, video_target_info[frame_index].targets[i].pos.ymax };

			draw_line(frame, loc);

			double font_scale = 2;
			char face_idx[25];
// 			sprintf(face_idx, "%d   %d", video_target_info[frame_index].targets[i].label, 
// 										 video_target_info[frame_index].targets[i].is_cross);

			sprintf(face_idx, "%d", video_target_info[frame_index].targets[i].label);

			putText(frame,face_idx,cvPoint(video_target_info[frame_index].targets[i].pos.xmin, video_target_info[frame_index].targets[i].pos.ymin - 5),
					1 , font_scale, Scalar(0,0,255),2);//在图片中输出字符
		}

		char target_num[128];
		sprintf(target_num, "target counted: %d", counted);
		putText(frame,target_num,cvPoint(50,50), 1, 2, Scalar(255,0,0), 2);

		line(frame, pnt1, pnt2, CV_RGB(255,0,0));

		frame_index++;
		double complete_rate = frame_index / (double)framenum;

		char vinfo[128];
		sprintf(vinfo,"正在写入视频:  %.6f%%",complete_rate*100);
		//printf(vinfo,"\r 已完成:   %.8f%%",complete_rate*100);
		::SetWindowText(g_hinfo, vinfo);

		writer.write(frame);
	}

	writer.release();
	EnableWindow(g_hopen, TRUE);
	EnableWindow(g_hstart, TRUE);
	//cvReleaseImage(&frame_image);
}

void save_video_frames(string video_path, double begin_rate, double end_rate, string out_dir)
{
	int begin_idx, end_idx, cur_idx = 0;
	CvCapture* capture=cvCreateFileCapture(video_path.c_str());

	int nframes = (int) cvGetCaptureProperty(capture,  CV_CAP_PROP_FRAME_COUNT);

	begin_idx = begin_rate * nframes;
	end_idx = end_rate * nframes;

	IplImage *frame;

	while(1)
	{
		frame = cvQueryFrame(capture);

		if(!frame)
			break;

		cur_idx++;

		if( begin_idx <= cur_idx && cur_idx <= end_idx )
		{
			char path[512];
			sprintf(path, "%s%d.jpg", out_dir.c_str(), cur_idx);
			cvSaveImage(path, frame);
		}
	}

	cvReleaseCapture(&capture);
	cvReleaseImage(&frame);
}

void faces_from_cascades(vector<vector<rect>> &cascade_faces, vector<rect> &faces/*, vector<int> &cascade_flag*/)
{
	// 	if( !cascade_flag.size() )
	// 		return;

	if( cascade_faces.size() == 1 )
	{
		faces = cascade_faces[0];
		return;
	}

	int i,j,k;
	for(i = 1; i < cascade_faces.size(); i++)
	{
		for(j = 0; j < cascade_faces[i].size(); j++)
		{
			for(k = 0; k < cascade_faces[0].size(); k++)
			{
				if( get_rect_overlap(&cascade_faces[i][j], &cascade_faces[0][k]) > 0.70 )
					break;
			}

			if( k < cascade_faces[0].size() )
			{
				faces.push_back( cascade_faces[0][k] );
				//cascade_flag.push_back(0);
			}
			else
			{
				faces.push_back( cascade_faces[i][j] );
				//cascade_flag.push_back(i);
			}
		}
	}

	vector<int> flag(faces.size(),1);
	int lnum = 0, num = faces.size();

	while( lnum != num )
	{
		lnum = num, num = 0;
		for(i = 0; i < faces.size() - 1; i++)
		{
			if( flag[i] == 0 )
				continue;

			for(j = i + 1; j < faces.size(); j++)
			{
				if( flag[j] == 0 )
					continue;

				if( get_rect_overlap(&faces[i], &faces[j]) > 0.70 )
				{
					if( abs(faces[j].xmax-faces[j].xmin) < abs(faces[i].xmax-faces[i].xmin) )
					{
						flag[j] = 0;
						combine_two_faces(&faces[i],&faces[j]);
					}
					else
					{
						flag[i] = 0;
						combine_two_faces(&faces[j],&faces[i]);
					}
					break;
				}
			}
		}

		for(i = 0; i < flag.size(); i++)
		{
			if( flag[i] )
				num++;
		}
	}

	vector<rect> tmp;
	vector<int> tmp_cascade_flag;
	for(i = 0; i < flag.size(); i++)
	{
		if( flag[i] )
		{
			tmp.push_back(faces[i]);
			//tmp_cascade_flag.push_back(cascade_flag[i]);
		}
	}

	faces = tmp;
	//cascade_flag = tmp_cascade_flag;
}

void Mat2IplImage(Mat &mat, IplImage *image)
{
	int i,j;

	for(i = 0; i < image->height; i++)
		for(j = 0; j < image->width; j++)
		{
			image->imageData[ i * image->widthStep + image->nChannels * j + 0 ] = mat.at<cv::Vec3b>(i,j)[0];
			image->imageData[ i * image->widthStep + image->nChannels * j + 1 ] = mat.at<cv::Vec3b>(i,j)[1];
			image->imageData[ i * image->widthStep + image->nChannels * j + 2 ] = mat.at<cv::Vec3b>(i,j)[2];
		}
}

void copy_roi_pxl(Mat image, int x, int y, int w, int h, Mat &roi)
{
	int i,j,k,c = image.channels(), type = image.type();

	for(i = y; i <= y + h - 1; i++)
		for(j = x; j <= x + w - 1; j++)
		{
			for(k = 0; k < c; k++)
				roi.at<uchar>((i-y), (j-x)*c+k) = image.at<uchar>(i,j*c+k);
		}

	int kkk = 0;
}

int binsight(int width, int height, rect target)
{
	int flag = 0;

	int w = target.xmax - target.xmin + 1;
	int h = target.ymax - target.ymin + 1;

	if( target.xmin > w/2 && target.xmax < width - w/2 &&
		target.ymin > h/2 && target.ymax < height - h/2 )
		flag = 1;

	return flag;
}

void get_path_files(string path, vector<string> &files)
{
	string dir = path+"\\";

	struct _finddata_t file;
	long hFile, hFileNext;
	string sPathLast = path + "\\*.*"; // sPathLast = "c:\test\*.*"
	hFile = _findfirst(sPathLast.c_str(), &file);

	hFileNext = _findnext(hFile, &file);

	while(_findnext(hFile, &file) == 0)
	{
		if(file.attrib == _A_ARCH)
		{
			files.push_back(/*dir + */file.name);
		}
	}
}

int det_pass_line(target_info target)
{
	int flag = 0;

	if( target.btrack == DETECTED && target.pos.ymin > g_count_line_y )
		flag = 1;

	return flag;
}

int bpass_line(target_info target)
{
	int flag = 0;

	if( target.pos.ymin > g_count_line_y )
		flag = 1;

	return flag;
}

int bneed_track(target_info target)
{
	int flag = 1;

	if( det_pass_line(target) || (target.btrack == TRACKDETECTED && target.pos.ymin > g_count_line_y) )
		flag = 0;

	return flag;
}

void generate_video(string pic_dir, string out_path)
{
	int i,j;
	vector<string> image_path;
	get_path_files(pic_dir, image_path);

	string pic_path = pic_dir + "\\" + image_path[0];
	IplImage *first = cvLoadImage(pic_path.c_str());
	int width = first->width, height = first->height;

	int frameRate = 25;
	CvSize frame_size;
	frame_size.width = width, frame_size.height = height;

	CvVideoWriter* wrvideo = cvCreateVideoWriter(out_path.c_str(), CV_FOURCC('D','I','V','3'), frameRate, frame_size);

	IplImage *image;
	for(i = 0; i < image_path.size(); i++)
	{
		string tmp_path = pic_dir + "\\" + image_path[i];
		image = cvLoadImage(tmp_path.c_str());
		cvWriteFrame(wrvideo, image);
		cvReleaseImage(&image);
	}
	cvReleaseVideoWriter(&wrvideo);
}

void avg_image(string pic_dir, string avg_img)
{
	int i,j,k;
	vector<string> image_path;
	get_path_files(pic_dir, image_path);

	string pic_path = pic_dir + "\\" + image_path[0];
	IplImage *first = cvLoadImage(pic_path.c_str());
	int width = first->width, height = first->height;
	int *sum = new int[ width * height * 3 ];
	memset(sum, 0, width * height * 3);

	int imgnum = image_path.size();
	for(k = 0; k < imgnum; k++)
	{
		string tmp_path = pic_dir + "\\" + image_path[k];
		IplImage *image = cvLoadImage(tmp_path.c_str());

		for(i = 0; i < height; i++)
			for(j = 0; j < width; j++)
			{
				int adr = i * image->widthStep + j * 3;
				BYTE b = image->imageData[ adr ];
				BYTE g = image->imageData[ adr+1 ];
				BYTE r = image->imageData[ adr+2 ];

				sum[ i * width*3 + 3*j ] += b;
				sum[ i * width*3 + 3*j +1] += g;
				sum[ i * width*3 + 3*j +2] += r;
			}

		cvReleaseImage(&image);
	}

	IplImage *avg = cvCreateImage(cvSize(width,height), 8, 3);
	for(i = 0; i < height; i++)
		for(j = 0; j < width; j++)
		{
			avg->imageData[ i * avg->widthStep + j * 3 ] = sum[ i * width*3 + 3*j ] / imgnum;
			avg->imageData[ i * avg->widthStep + j * 3+1 ] = sum[ i * width*3 + 3*j+1 ] / imgnum;
			avg->imageData[ i * avg->widthStep + j * 3+2 ] = sum[ i * width*3 + 3*j+2 ] / imgnum;
		}

	cvSaveImage(avg_img.c_str(), avg);
}

int is_software_valid()
{
	int flag = 1;

	time_t timer; 
	time(&timer); 
	tm* t_tm = localtime(&timer);

	int year = t_tm->tm_year + 1900;
	int month = t_tm->tm_mon + 1;
	int day = t_tm->tm_mday;

	if( year != 2014 || month > 8 || ( month == 8 && day > 30 ) )
		return 0;

	return flag;
}