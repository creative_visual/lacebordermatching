#include "stdafx.h"
#include "color space.h"

void MinMax( float& min, float& max, float r, float g, float b )
{   
	if(r > g){
		if(g > b){
			min = b;
			max = r;
		}else{
			min = g;
			if(r > b) max = r;
			else max = b;
		}
	}else{//r < g
		if(r < b){
			min = r;
			if(g < b) max = b;
			else max = g;
		}else{
			min = b;
			max = g;
		}
	}
}



// r,g,b values are between 0 and 1
// h = [0,360], s = [0,1], v = [0,1]
// if s == 0, then h = -1 (undefined)

void RGBtoHSV( float r, float g, float b, float *h, float *s, float *v )
{
	float min, max, delta;

	//   min = MIN( r, g, b );
	//   max = MAX( r, g, b );
	MinMax(min, max, r, g, b);

	*v = max;                               

	delta = max - min;

	if( max > ZEROPLUS )
		*s = delta / max; //RGB=(0.1, 0, 0) -> *S = 1?             
	else {
		*s = 0.0;
		*h = 0.0;
		return;
	}

	if(delta < ZEROPLUS){
		*s = 0.0;
		*h = 0.0;
		return;
	}

	if( r == max )
		*h = ( g - b ) / delta;         /* between yellow & magenta */
	else if( g == max )
		*h = 2.0 + ( b - r ) / delta;     /* between cyan & yellow */
	else
		*h = 4.0 + ( r - g ) / delta;     /* between magenta & cyan */

	*h *= 60.0;                               /* degrees */
	if( *h < 0.0 )
		*h += 360.0;
}


void HSVtoRGB( float *r, float *g, float *b, float h, float s, float v )
{
	int i;
	float f, p, q, t;

	if( s < ZEROPLUS ) {
		*r = *g = *b = v;
		return;
	}

	h /= 60.0;                        
	//   i = (int) floor( h );
	i = (int) h;
	f = h - (float) i;                      
	p = v * ( 1.0 - s );
	q = v * ( 1.0 - s * f );
	t = v * ( 1.0 - s * ( 1.0 - f ) );

	switch( i ) 
	{
	case 0:
		*r = v;
		*g = t;
		*b = p;
		break;
	case 1:
		*r = q;
		*g = v;
		*b = p;
		break;
	case 2:
		*r = p;
		*g = v;
		*b = t;
		break;
	case 3:
		*r = p;
		*g = q;
		*b = v;
		break;
	case 4:
		*r = t;
		*g = p;
		*b = v;
		break;
	case 5:
		*r = v;
		*g = p;
		*b = q;
		break;
	default:
		*r = -1.0;
		*g = -1.0;
		*b = -1.0;
		return;
	}

}
