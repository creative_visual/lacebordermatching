#ifndef PF_H
#define PF_H

#include "stdafx.h"


#include "cv.h"
#include "cxcore.h"
#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "highgui.h"
#include <iostream>
#include <stdlib.h>
#include <time.h>
#include "color space.h"
#include <fstream>
#include "color_model.h"
#include <time.h>
#include "face_detect.h"
#define  OB_N 1
using namespace std;
using namespace cv;


struct face
{
	Rect rectangle;
	int dx, dy, confidence;
	float weight;
	vector<float> color_porb_distub;
	int flag;
	int  showid;
};
vector<face> getSamples(face f, int predX, int predY, int predW, int predH, int numSamples, float searchSTD, int wLimit, int hLimit);
vector<face> resample(vector<face> samples, face f, int wLimit, int hLimit);
void detect_using_cascades_and_particle_filters(string cascade_path, string video_name, string dir_out);
double get_rect_overlap(Rect R1,Rect R2);
#endif