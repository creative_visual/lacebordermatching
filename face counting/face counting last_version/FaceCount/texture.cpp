/*M///////////////////////////////////////////////////////////////////////////////////////
//
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//  By downloading, copying, installing or using the software you agree to this license.
//  If you do not agree to this license, do not download, install,
//  copy or use the software.
//
//
//                        Intel License Agreement
//                For Open Source Computer Vision Library
//
// Copyright (C) 2000, Intel Corporation, all rights reserved.
// Third party copyrights are property of their respective owners.
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
//   * Redistribution's of source code must retain the above copyright notice,
//     this list of conditions and the following disclaimer.
//
//   * Redistribution's in binary form must reproduce the above copyright notice,
//     this list of conditions and the following disclaimer in the documentation
//     and/or other materials provided with the distribution.
//
//   * The name of Intel Corporation may not be used to endorse or promote products
//     derived from this software without specific prior written permission.
//
// This software is provided by the copyright holders and contributors "as is" and
// any express or implied warranties, including, but not limited to, the implied
// warranties of merchantability and fitness for a particular purpose are disclaimed.
// In no event shall the Intel Corporation or contributors be liable for any direct,
// indirect, incidental, special, exemplary, or consequential damages
// (including, but not limited to, procurement of substitute goods or services;
// loss of use, data, or profits; or business interruption) however caused
// and on any theory of liability, whether in contract, strict liability,
// or tort (including negligence or otherwise) arising in any way out of
// the use of this software, even if advised of the possibility of such damage.
//
//M*/

/****************************************************************************************\

      Calculation of a texture descriptors from GLCM (Grey Level Co-occurrence Matrix'es)
      The code was submitted by Daniel Eaton [danieljameseaton@yahoo.com]

\****************************************************************************************/

//#include "precomp.hpp"
#include "StdAfx.h"
#include "texture.h"

#include <math.h>
#include <assert.h>

#define CV_MAX_NUM_GREY_LEVELS_8U  256

int calGLCM(IplImage* bWavelet,int angleDirection,double* featureVector)
{
	int i,j;
	int width,height;

	if(NULL == bWavelet)
		return 1;

	width = bWavelet->width;
	height = bWavelet->height;

	IplImage *gray = NULL;
	if( bWavelet->nChannels == 1 )
		gray = bWavelet;
	else
	{
		gray = cvCreateImage(cvSize(width, height), bWavelet->depth, 1);
		cvCvtColor(bWavelet, gray, CV_BGR2GRAY);
	}

	double * glcm = new double [GLCM_CLASS * GLCM_CLASS];
	int * histImage = new int[width * height];

	if(NULL == glcm || NULL == histImage)
		return 2;

	//灰度等级化---分GLCM_CLASS个等级
	uchar *data =(uchar*) gray->imageData;
	for(i = 0;i < height;i++){
		for(j = 0;j < width;j++){
			histImage[i * width + j] = (int)(data[gray->widthStep * i + j] * GLCM_CLASS / 256);
		}
	}

	//初始化共生矩阵
	for (i = 0;i < GLCM_CLASS;i++)
		for (j = 0;j < GLCM_CLASS;j++)
			glcm[i * GLCM_CLASS + j] = 0;

	//计算灰度共生矩阵
	int w,k,l;
	double hvp = 1.0 / (width*(height-1));
	double dp  = 1.0 / ( (width-1)*(height-1) );
	//水平方向
	if(angleDirection == GLCM_ANGLE_HORIZATION)
	{
		for (i = 0;i < height;i++)
		{
			for (j = 0;j < width;j++)
			{
				l = histImage[i * width + j];
				if(j + GLCM_DIS >= 0 && j + GLCM_DIS < width)
				{
					k = histImage[i * width + j + GLCM_DIS];
					glcm[l * GLCM_CLASS + k] += hvp;
				}
				if(j - GLCM_DIS >= 0 && j - GLCM_DIS < width)
				{
					k = histImage[i * width + j - GLCM_DIS];
					glcm[l * GLCM_CLASS + k] += hvp;
				}
			}
		}
	}
	//垂直方向
	else if(angleDirection == GLCM_ANGLE_VERTICAL)
	{
		for (i = 0;i < height;i++)
		{
			for (j = 0;j < width;j++)
			{
				l = histImage[i * width + j];
				if(i + GLCM_DIS >= 0 && i + GLCM_DIS < height) 
				{
					k = histImage[(i + GLCM_DIS) * width + j];
					glcm[l * GLCM_CLASS + k] += hvp;
				}
				if(i - GLCM_DIS >= 0 && i - GLCM_DIS < height) 
				{
					k = histImage[(i - GLCM_DIS) * width + j];
					glcm[l * GLCM_CLASS + k] += hvp;
				}
			}
		}
	}
	//对角方向
	else if(angleDirection == GLCM_ANGLE_DIGONAL)
	{
		for (i = 0;i < height;i++)
		{
			for (j = 0;j < width;j++)
			{
				l = histImage[i * width + j];

				if(j + GLCM_DIS >= 0 && j + GLCM_DIS < width && i + GLCM_DIS >= 0 && i + GLCM_DIS < height)
				{
					k = histImage[(i + GLCM_DIS) * width + j + GLCM_DIS];
					glcm[l * GLCM_CLASS + k] += dp;
				}
				if(j - GLCM_DIS >= 0 && j - GLCM_DIS < width && i - GLCM_DIS >= 0 && i - GLCM_DIS < height)
				{
					k = histImage[(i - GLCM_DIS) * width + j - GLCM_DIS];
					glcm[l * GLCM_CLASS + k] += dp;
				}
			}
		}
	}

	//计算特征值
	double entropy = 0,energy = 0,contrast = 0,homogenity = 0;
	for (i = 0;i < GLCM_CLASS;i++)
	{
		for (j = 0;j < GLCM_CLASS;j++)
		{
			//熵
			if(glcm[i * GLCM_CLASS + j] > 0)
				entropy -= glcm[i * GLCM_CLASS + j] * log10(double(glcm[i * GLCM_CLASS + j]));
			//能量
			energy += glcm[i * GLCM_CLASS + j] * glcm[i * GLCM_CLASS + j];
			//对比度
			contrast += (i - j) * (i - j) * glcm[i * GLCM_CLASS + j];
			//一致性
			homogenity += 1.0 / (1 + (i - j) * (i - j)) * glcm[i * GLCM_CLASS + j];
		}
	}
	//返回特征值
	i = 0;
	featureVector[i++] = entropy;
	featureVector[i++] = energy;
	featureVector[i++] = contrast;
	featureVector[i++] = homogenity;

	delete[] glcm;
	delete[] histImage;
	return 0;
}

double get_correlation(IplImage *image1, IplImage *image2, int stdl)
{
	IplImage *tmp1, *tmp2, *gray1, *gray2, *zoomed1, *zoomed2;

	gray1   = cvCreateImage(cvSize(stdl, stdl), 8, 1);
	gray2   = cvCreateImage(cvSize(stdl, stdl), 8, 1);
	zoomed1 = cvCreateImage(cvSize(stdl, stdl), 8, image1->nChannels);
	zoomed2 = cvCreateImage(cvSize(stdl, stdl), 8, image2->nChannels);

	cvResize(image1, zoomed1);
	cvResize(image2, zoomed2);

	if( zoomed1->nChannels == 1 )
		gray1 = zoomed1;
	else
		cvCvtColor(zoomed1, gray1, CV_BGR2GRAY);

	if( zoomed2->nChannels == 1 )
		gray2 = zoomed2;
	else
		cvCvtColor(zoomed2, gray2, CV_BGR2GRAY);

	double avg1 = get_avg_gray(gray1);
	double avg2 = get_avg_gray(gray2);
	double cov = get_cov(gray1, gray2, avg1, avg2);

	double dev1 = get_dev(gray1, avg1);
	double dev2 = get_dev(gray2, avg2);

	double cor = cov / ( sqrt( dev1 * dev2 ) );

	return cor;

// 	cvReleaseImage(&gray1);
// 	cvReleaseImage(&gray2);
// 	cvReleaseImage(&zoomed1);
// 	cvReleaseImage(&zoomed2);
}

double get_correlation(Mat gray1, Mat gray2, int stdl)
{
	Mat zoomed1,zoomed2;

	resize(gray1, zoomed1, cvSize(stdl,stdl));
	resize(gray2, zoomed2, cvSize(stdl,stdl));

	double avg1 = get_avg_gray(zoomed1);
	double avg2 = get_avg_gray(zoomed2);

	double cov = get_cov(zoomed1, zoomed2, avg1, avg2);

	double dev1 = get_dev(zoomed1, avg1);
	double dev2 = get_dev(zoomed2, avg2);

	double cor = cov / ( sqrt( dev1 * dev2 ) );

	return cor;
}

double get_avg_gray(IplImage *gray)
{
	double sum = 0,avg; 

	int i,j;
	for(i = 0; i < gray->height; i++)
		for(j = 0; j < gray->width; j++)
		{
			sum += gray->imageData[ i * gray->widthStep + j ];
		}

	avg = sum / ( gray->width * gray->height );

	return avg;
}

double get_avg_gray(Mat gray)
{
	double sum = 0,avg; 

	int i,j;
	for(i = 0; i < gray.rows; i++)
		for(j = 0; j < gray.cols; j++)
		{
			sum += gray.at<uchar>(i, j*gray.channels());
		}

	avg = sum / ( gray.rows * gray.cols );

	return avg;
}

double get_cov(IplImage *gray1, IplImage *gray2)
{
	int i,j,w = gray1->width, h = gray1->height;
	double cov = 0, p = 1.0 / ( w * h );

	double avg1 = get_avg_gray(gray1);
	double avg2 = get_avg_gray(gray2);

	for(i = 0; i < h; i++)
		for(j = 0; j < w; j++)
		{
			int g1 = gray1->imageData[i*gray1->widthStep + j];
			int g2 = gray2->imageData[i*gray2->widthStep + j];
			cov += (g1-avg1)*(g2-avg2)*p;
		}

	return cov;
}

double get_cov(Mat gray1, Mat gray2)
{
	int i,j,w = gray1.cols, h = gray1.rows;
	double cov = 0, p = 1.0 / ( w * h );

	double avg1 = get_avg_gray(gray1);
	double avg2 = get_avg_gray(gray2);

	for(i = 0; i < h; i++)
		for(j = 0; j < w; j++)
		{
			int g1 = gray1.at<uchar>(i, j*gray1.channels());
			int g2 = gray2.at<uchar>(i, j*gray2.channels());
			cov += (g1-avg1)*(g2-avg2)*p;
		}

	return cov;
}

double get_cov(Mat gray1, Mat gray2, double avg1, double avg2)  // 协方差
{
	int i,j,w = gray1.cols, h = gray1.rows;
	double cov = 0, p = 1.0 / ( w * h );

	for(i = 0; i < h; i++)
		for(j = 0; j < w; j++)
		{
			int g1 = gray1.at<uchar>(i, j*gray1.channels());
			int g2 = gray2.at<uchar>(i, j*gray2.channels());
			cov += (g1-avg1)*(g2-avg2)*p;
		}

	return cov;
}

double get_cov(IplImage *gray1, IplImage *gray2, double avg1, double avg2)  // 协方差
{
	int i,j,w = gray1->width, h = gray1->height;
	double cov = 0, p = 1.0 / ( w * h );

	for(i = 0; i < h; i++)
		for(j = 0; j < w; j++)
		{
			int g1 = gray1->imageData[i*gray1->widthStep + j];
			int g2 = gray2->imageData[i*gray2->widthStep + j];
			cov += (g1-avg1)*(g2-avg2)*p;
		}

	return cov;
}

double get_dev(IplImage *gray, double avg)  // 方差
{
	double dev = 0;

	int i,j;
	for(i = 0; i < gray->height; i++)
		for(j = 0; j <gray->width; j++)
		{
			int g = gray->imageData[i * gray->widthStep + j];
			dev += (g-avg)*(g-avg);
		}

	dev = dev / (gray->height * gray->width);

	return  dev;
}

double get_dev(Mat gray, double avg)  // 方差
{
	double dev = 0;

	int i,j;
	for(i = 0; i < gray.rows; i++)
		for(j = 0; j < gray.cols; j++)
		{
			int g = gray.at<uchar>(i, j*gray.channels());
			dev += (g-avg)*(g-avg);
		}

	dev = dev / (gray.rows * gray.cols);

	return  dev;
}

double vector_distance(double *features1, double *features2, int length)
{
	double dis = 0;

	for(int i = 0; i < length; i++)
	{
		dis += (features1[i]-features2[i])*(features1[i]-features2[i]);
	}

	return sqrt(dis);
}

void normalise_features(vector<double *> &features, int len)
{
	int i,j,num = features.size();

	for(i = 0; i < len; i++)
	{
		double maxf = -999999999;
		for(j = 0; j < num; j++)
		{
			if( features[j][i] > maxf )
				maxf = features[j][i];
		}

		for(j = 0; j < num; j++)
		{
			features[j][i] = features[j][i] / maxf;
		}
	}
}

double diff_images(Mat gray1, Mat gray2, int stdl)
{
	Mat zoomed1,zoomed2;

	resize(gray1, zoomed1, cvSize(stdl,stdl));
	resize(gray2, zoomed2, cvSize(stdl,stdl));

	int i,j;
	double d = 0,cnt = 0;
	for(i = 0; i < stdl; i++)
		for(j = 0; j < stdl; j++)
		{
			d += abs( zoomed1.at<uchar>(i, j*zoomed1.channels()) - zoomed2.at<uchar>(i, j*zoomed2.channels()) );
			//cnt++;
		}

	return d / (stdl * stdl);
}

// directly compute face-difference, no resize
double diff_images2(Mat& pre_gray, Mat& cur_gray, CvRect& pre_roi, CvRect& cur_roi)
{
	int i,j;

  double d = 0;
  double count = cur_roi.height * cur_roi.width;
  for( i = 0; i < cur_roi.height; i++ )
  {
    int ip = i + pre_roi.y;
    int ic = i + cur_roi.y;
    unsigned char* ppre = pre_gray.ptr<unsigned char>(ip);
    unsigned char* pcur = cur_gray.ptr<unsigned char>(ic);

    for(j = 0; j < cur_roi.width; j++)
    {
      int jp = j + pre_roi.x;
      int jc = j + cur_roi.x;
      d += abs(ppre[jp] - pcur[jc]);
    }
  }

	return d / count;
}
