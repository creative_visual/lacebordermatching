#include "StdAfx.h"
#include "count_utils.h"

int define_detection_roi(int& g_count_line_y, int& top_line_y, int& btm_line_y, int& g_max_face_size, int frame_w, int frame_h)
{
  g_count_line_y = int (frame_h/2);
 	top_line_y = max(0, g_count_line_y - g_max_face_size);
	btm_line_y = min(frame_h-1, g_count_line_y + g_max_face_size * 1.2);

  return 0;
}