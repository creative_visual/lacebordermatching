#include "stdafx.h"
#ifndef COLOR_MODEL_H
#define COLOR_MODEL_H
#include "opencv2/highgui/highgui.hpp"  
#include "opencv2/imgproc/imgproc.hpp"  
#include "opencv2/nonfree/nonfree.hpp"  
#include "opencv2/nonfree/features2d.hpp"  
#include "color space.h"
#include "cv.h"
#include <iostream>  
#include <stdio.h>  
#include <stdlib.h>  

using namespace cv;  
using namespace std; 

typedef struct Locate_to_index
{
	CvPoint pointx;
	int index;
}LocateToIndex;

Mat filt_hist(CvHistogram *hist,int size);
void Project_location_to_binsindex(IplImage* h_plane,IplImage* s_plane,IplImage* v_plane,Rect roirect,vector<int>& project_loc);
void generate_color_prob_model(vector<int> project_loc,vector<float>&color_porb_distub,Rect roi_rect);
float Bhattacharyya_distance(vector<float>s1,vector<float>s2);
void Draw_Bow_Hist( vector<float>feat);
#endif
