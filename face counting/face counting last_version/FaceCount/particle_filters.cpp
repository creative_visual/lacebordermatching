// face tracing.cpp : 定义控制台应用程序的入口点。
//
#include "stdafx.h"
#include "particle_filters.h"
extern IplImage *g_dspbuf;
extern CRect g_rect;
extern HWND g_hwnd;
extern HWND g_hinfo;
// Create memory for calculations
static CvMemStorage* storage = 0;

// Create a new Haar classifier


// Create a string that contains the cascade name

/*    "haarcascade_profileface.xml";*/

void detect_and_draw(IplImage* img);

bool writeActive;
IplImage* im;
//IplImage* imFace;
IplImage* h_plane;
IplImage* s_plane;
IplImage* v_plane;




vector<face> allFaces;
bool select_flag=false;
bool tracking=false;//跟踪标志位
bool select_show=false;
int after_select_frames=1;//选择矩形区域完后的帧计数
Point origin;
//Rect select;
Mat frame1;

void cvText(IplImage* img, const char* text, int x, int y)  
{  
	CvFont font;  
	double hscale = 1.0;  
	double vscale = 1.0;  
	int linewidth = 2;  
	cvInitFont(&font,CV_FONT_HERSHEY_SIMPLEX | CV_FONT_ITALIC,hscale,vscale,0,linewidth);  
	CvScalar textColor =cvScalar(0,255,255);  
	CvPoint textPos =cvPoint(x, y);  
	cvPutText(img, text, textPos, &font,textColor);  
}  


// void onMouse(int event,int x,int y,int,void*)
// {
// 	//Point origin;//不能在这个地方进行定义，因为这是基于消息响应的函数，执行完后origin就释放了，所以达不到效果。
// 	if(select_flag)
// 	{
// 		select.x=MIN(origin.x,x);//不一定要等鼠标弹起才计算矩形框，而应该在鼠标按下开始到弹起这段时间实时计算所选矩形框
// 		select.y=MIN(origin.y,y);
// 		select.width=abs(x-origin.x);//算矩形宽度和高度
// 		select.height=select.width;
// 		select&=Rect(0,0,frame1.cols,frame1.rows);//保证所选矩形框在视频显示区域之内
// 		//        rectangle(frame,select,Scalar(0,0,255),3,8,0);//显示手动选择的矩形框
// 	}
// 	if(event==CV_EVENT_LBUTTONDOWN)
// 	{
// 		select_flag=true;//鼠标按下的标志赋真值
// 		tracking=false;
// 		select_show=true;
// 		after_select_frames=0;//还没开始选择，或者重新开始选择，计数为0
// 		origin=Point(x,y);//保存下来单击是捕捉到的点
// 		select=Rect(x,y,0,0);//这里一定要初始化，因为在opencv中Rect矩形框类内的点是包含左上角那个点的，但是不含右下角那个点。
// 	}
// 	else if(event==CV_EVENT_LBUTTONUP)
// 	{
// 		select_flag=false;
// 		tracking=true;
// 		select_show=false;
// 		after_select_frames=1;//选择完后的那一帧当做第1帧
// 	}
// 
// }

vector<face> getSamples(face f, int predX, int predY, int predW, int predH, int numSamples, float searchSTD, int wLimit, int hLimit)
{
	vector<face> samples;

	float u1,u2,u3,u4,n1,n2,n3,n4,probability,scale,rCorr,gCorr,bCorr,likelihood;
	int newWidth,newHeight;

	//generate random samples
	for(int randGenIter=0; randGenIter<numSamples; randGenIter++)
	{
		//generate two random uniformly distributed numbers
		u1 = ((float)rand())/RAND_MAX;
		u2 = ((float)rand())/RAND_MAX;
		u3 = ((float)rand())/RAND_MAX;
		u4 = ((float)rand())/RAND_MAX;
		//get normally distributed random numbers using box-muller transform (has mean 0 and std 1)
		//		cout<<cos(2*3.14159265359*u2)<<" "<<sin(2*3.14159265359*u2)<<" "<<sin(2*3.14159265359*u4)<<" ";
		n1 = sqrt(-2*log(u1)) * cos(2*3.14159265359*u2);
		n2 = sqrt(-2*log(u1)) * sin(2*3.14159265359*u2);
//		n3 = sqrt(-2*log(u3)) * sin(2*3.14159265359*u4);


		//probability = pow(2.71828,-0.5*n1*n1)/sqrt(2*3.14159265359) * pow(2.71828,-0.5*n2*n2)/sqrt(2*3.14159265359);
		//probability *= pow(2.71828,-0.5*n3*n3)/sqrt(2*3.14159265359);

		//make std dev one third of face dimensions and mean at the predicted position
		n1*=f.rectangle.width * searchSTD;
		n1+=predX;
		n2*=f.rectangle.height * searchSTD;
		n2+=predY;

		//	n3=1;
// 		n3*=0.075;
// 		n3+=1;
// 		//		n3 = MIN(1.105, MAX(0.7,n3) );
// 		n3 = MAX(1.01, MIN(1.0,n3) );


		scale = 1;
		newWidth = predW * scale;
		//scale = n4;
		newHeight = predH * scale;
		//		cout<<" new_scale: "<<n3<<" "<<newHeight<<" ";

		if (n1>0 && n2>0 && n1<wLimit-newWidth && n2<hLimit-newHeight)//if randomized position is on the image
		{
			//declare a face at the location
			face newFace;
			int n_x=cvRound(n1);
			int n_y=cvRound(n2);
			int n_w=cvRound(newWidth);
			int n_h=cvRound(newHeight);
			newFace.rectangle = cvRect(n_x,n_y,n_w,n_h);
			Rect ROIrect1 = cvRect(0,0,n_w,n_h);			
			vector<int>project_loc;
			Project_location_to_binsindex(h_plane,s_plane,v_plane,newFace.rectangle,project_loc);
			generate_color_prob_model(project_loc,newFace.color_porb_distub,ROIrect1);

			// 			Mat mat_img(im);
			// 			Mat gray;
			// 			cvtColor(mat_img,gray,CV_BGR2GRAY);
			// 			Mat mat1(gray,newFace.rectangle);
			// 			newFace.lbp_distub=compute_descriptors_kane(mat1);

			//calculate likelihood / weight
			float likehood_val=Bhattacharyya_distance(f.color_porb_distub,newFace.color_porb_distub);
			//			float like_val1=Bhattacharyya_distance(f.lbp_distub,newFace.lbp_distub);
			//		float sigma=1.0;
			//		newFace.weight=0.3*1/sqrt(2*3.14159265359)*pow(2.71828,-0.5*(1-likehood_val))+0.7*1/sqrt(2*3.14159265359)*pow(2.71828,-0.5*(1-like_val1));   //kane10_15_2.avi
			newFace.weight=1/sqrt(2*3.14159265359)*pow(2.71828,-0.5*(1-likehood_val))/*+0*1/sqrt(2*3.14159265359)*pow(2.71828,-0.5*(1-like_val1));*/ ;  //kane10_15_3.avi
			samples.push_back(newFace);
		}
	}

	return samples;
}



vector<face> resample(vector<face> samples, face f, int wLimit, int hLimit)
{
	float totalWeight=0;
	for(int sampleIter=0; sampleIter<samples.size(); sampleIter++)
	{
		//cout<<samples.at(sampleIter).weight<<endl;
		totalWeight+=samples.at(sampleIter).weight;
	}
	vector<face> resamples;
	vector<face> allResamples;
	int numSamplesToDraw;
	for(int sampleIter=0; sampleIter<samples.size(); sampleIter++)
	{
		resamples.clear();
		numSamplesToDraw = (int)(((samples.at(sampleIter).weight/totalWeight) * samples.size())+0.5);

		//predicted position
		int predX = samples.at(sampleIter).rectangle.x;
		int predY = samples.at(sampleIter).rectangle.y;

		resamples = getSamples(f, predX, predY, samples.at(sampleIter).rectangle.width, samples.at(sampleIter).rectangle.height, numSamplesToDraw, 0.01, wLimit, hLimit);
		//add resamples to the vector of all resamples
		for(int resampleIter=0; resampleIter<resamples.size(); resampleIter++)
		{
			allResamples.push_back(resamples.at(resampleIter));
		}
	}
	return allResamples;
}



void drawFaces(int top_line_y)
{
	//copy the image and draw the faces
	//	cvCopy(im, imFace);

	CvPoint pt1, pt2;
	CvScalar rectColor;
	//draw the faces
	if (allFaces.size()>=OB_N)
	{
		for(int faceIter = 0; faceIter < allFaces.size(); faceIter++ )
		{
			if( !allFaces.at(faceIter). flag)
				continue;
//			allFaces[faceIter].rectangle.y+=top_line_y;
			pt1.x = allFaces.at(faceIter).rectangle.x;
			pt2.x = pt1.x + allFaces.at(faceIter).rectangle.width;
			pt1.y = allFaces.at(faceIter).rectangle.y+top_line_y;
			pt2.y = pt1.y + allFaces.at(faceIter).rectangle.height;
			char shownumber[128];
			sprintf(shownumber,"%d",allFaces.at(faceIter).showid);
			cvText(im, shownumber,allFaces.at(faceIter).rectangle.x,allFaces.at(faceIter).rectangle.y+top_line_y-2);  
			rectColor = cvScalar(0,0,0,0);
			cvRectangle( im, pt1, pt2, rectColor, 3, 8, 0 );
			rectColor = cvScalar(0,255,0,0);
			cvRectangle( im, pt1, pt2, rectColor, 1, 8, 0 );
		}
	}
	g_GetImgFullScaleDspData(g_dspbuf, im);
	InvalidateRect(g_hwnd, g_rect, FALSE);
	UpdateWindow(g_hwnd);

//	cvShowImage("camera",im);
}

double get_rect_overlap(Rect R1,Rect R2)
{
	int cxR1,cxR2, cyR1,cyR2,wR1,wR2,hR1,hR2;
	cxR1=max(R1.x+R1.width,R2.x+R2.width);
	cxR2=min(R1.x,R2.x);
	cyR1=max(R1.y+R1.height,R2.y+R2.height);
	cyR2=min(R1.y,R2.y);

	int w=R1.width+R2.width-(cxR1-cxR2);
	int h=R1.height+R2.height-(cyR1-cyR2);
	if(w<0) w=0;if(h<0) h=0;

	double mina = R1.width*R1.height < R2.width*R2.height ?  R1.width*R1.height : R2.width*R2.height;
	double rate = w*h / mina;
	return rate;
}

Rect get_rect_overlapcenter(Rect R1,Rect R2)
{
	Rect center_rect;
	int cxR1,cxR2, cyR1,cyR2,wR1,wR2,hR1,hR2;
	cxR1=max(R1.x+R1.width,R2.x+R2.width);
	cxR2=min(R1.x,R2.x);
	cyR1=max(R1.y+R1.height,R2.y+R2.height);
	cyR2=min(R1.y,R2.y);
	center_rect.width=min(cxR1-cxR2,cyR1-cyR2);
	center_rect.height=min(cxR1-cxR2,cyR1-cyR2);
	center_rect.x=cxR2;
	center_rect.y=cyR2;
	return center_rect;	
}

int define_detection_roi1(int& g_count_line_y, int& top_line_y, int& btm_line_y, int& g_max_face_size, int frame_w, int frame_h)
{
	g_count_line_y = int (frame_h/2)+100;
	top_line_y = MAX(0, g_count_line_y - g_max_face_size);
	btm_line_y = MIN(frame_h-1, g_count_line_y + g_max_face_size);
	return 0;
}

void detect_using_cascades_and_particle_filters(string cascade_path, string video_name, string dir_out)
{
	//initialize random seed
	int i,j;
	srand ( time(NULL) );

	ofstream fk;
	fk.open("E:\\rect_weight1.txt");
	int is_first_show=1;
	int is_first_detect=1;
	int continous_det=0;
	int is_trace=0;
	int count_face_num=0;
	int currentFace,currentdet;
	CvHaarClassifierCascade* cascade = (CvHaarClassifierCascade*)cvLoad( cascade_path.c_str(), 0, 0, 0 );
	VideoCapture capture1(video_name);
	int frameRate = capture1.get(CV_CAP_PROP_FPS);
	int framenum = capture1.get(CV_CAP_PROP_FRAME_COUNT);
	int width = (int)capture1.get(CV_CAP_PROP_FRAME_WIDTH);
	int height = (int)capture1.get(CV_CAP_PROP_FRAME_HEIGHT);
	cv::Size frameSize = cv::Size(width,height);
	cv::VideoWriter writer1;
	writer1.open(dir_out, CV_FOURCC('D','I','V','X') , frameRate, frameSize, true); 
	vector<int>det_time;
	int id=0;
	bool i_flag=false;
// 	while(1)
// 	{
// 		i_flag=capture1.read(frame1); 
// 		// 		imshow("kkk",frame1);
// 		// 		waitKey(0);
// 		id++;
// 		if (id==1*frameRate)
// 		{
// 			i=capture1.read(frame1);
// 
// 			break;
// 		}
// 	}
	capture1.read(frame1); 
	//	select=Rect(100,300,30,30);
//	cvNamedWindow("camera",1);
//	setMouseCallback("camera",onMouse,0);

	im=cvCreateImage(cvSize(frame1.cols,frame1.rows),8,frame1.channels());
	im->imageData=(char *)frame1.data;
	

//	int end_sec=33*frameRate;
	int frame_flag=0;
	int top_line_y;
	int btm_line_y;
	int g_count_line_y=540;
	int g_max_face_size=60;
	// NOTE: moved outside of loop
	Mat frame_gray;
	// NOTE: safely determines these roi parameters
	define_detection_roi1(g_count_line_y, top_line_y, btm_line_y, g_max_face_size, width, height);
	// NOTE: moved outside of loop
	int detx = 0, dety = top_line_y;
	int detw = frameSize.width, deth = btm_line_y - top_line_y + 1;
	IplImage *im_roi=cvCreateImage(cvSize(detw,deth),8,im->nChannels);
	IplImage *gray_roi=cvCreateImage(cvSize(detw,deth),8,1);
	IplImage *hsv=cvCreateImage(cvSize(detw,deth),IPL_DEPTH_32F,im->nChannels);
	h_plane=cvCreateImage(cvSize(detw,deth),IPL_DEPTH_32F,1);
	s_plane=cvCreateImage(cvSize(detw,deth),IPL_DEPTH_32F,1);
	v_plane=cvCreateImage(cvSize(detw,deth),IPL_DEPTH_32F,1);

	Point pnt1,pnt2;
	while(1)
	{
		clock_t start1,end1;
		float run_time1;
		start1=clock();
		
	
		cvSetImageROI(im,cvRect(detx,dety,detw,deth));
		cvCopy(im,im_roi);
		cvResetImageROI(im);
		//		f1<<id<<"id: ";
		int i1,j1;
		uchar *curdata=(uchar*)im_roi->imageData;
		int channel=im_roi->nChannels;
		int steps=im_roi->widthStep;
		float r,g,b,h,s,v;

		/////////////////////////////////////////////////////////////////////////////////
		int kane_width=0;
		////////////////////////////////////////////////////////////////////////////////
		// 		clock_t start2,end2;
		// 		float run_time2;
		// 		start2=clock();
	//颜色空间转换
		for (i1=0;i1<im_roi->height;i1++)
		{
			for (j1=0;j1<im_roi->width;j1++)
			{
				b=(float)*(curdata+i1*steps+j1*channel+0)/255.0;
				g=(float)*(curdata+i1*steps+j1*channel+1)/255.0;
				r=(float)*(curdata+i1*steps+j1*channel+2)/255.0;
				RGBtoHSV(r,g,b,&h,&s,&v);
				//			cout<<h<<" ";
				((float*)(hsv->imageData+i1*hsv->widthStep))[j1*hsv->nChannels+0]=h;
				((float*)(hsv->imageData+i1*hsv->widthStep))[j1*hsv->nChannels+1]=s*1000;
				((float*)(hsv->imageData+i1*hsv->widthStep))[j1*hsv->nChannels+2]=v*100;
			}
		}      
		cvSplit(hsv,h_plane,s_plane,v_plane,NULL);
	   cvCvtColor(im_roi,gray_roi,CV_BGR2GRAY);
		// 		end2=clock();
		// 		run_time2=(float)(end2 - start2) / CLOCKS_PER_SEC;
		// 		f1<<"\n color change needs "<<run_time2<<" seconds.\n";

	   //进行第一次检测，直到allFaces不为空
	   if (is_first_detect)
	   {
		   storage = cvCreateMemStorage(0);
			//	 cvReleaseMemStorage(&storage);
			// There can be more than one face in an image. So create a growable sequence of faces.
			// Detect the objects and store them in the sequence
			CvSeq* faces = cvHaarDetectObjects( gray_roi, cascade, storage, 1.1,6, CV_HAAR_DO_CANNY_PRUNING, cvSize(24, 24) ,cvSize(90,90));
			for (currentFace=0; currentFace<faces->total; currentFace++)
			{
				CvRect* r = (CvRect *)cvGetSeqElem( faces, currentFace );
//				rectangle(frame1,*r,Scalar(255,0,255),2,8,0);
				face newFace;
				newFace.rectangle=cvRect(r->x,r->y,r->width,r->height);
				newFace.dx = 0;
				newFace.dy = 0;
				newFace.flag=1;
				newFace.showid=currentFace+1;
				newFace.weight=1;

				//生成检测到的矩形框内的颜色分布模型
				Rect ROIrect1 = cvRect(0,0,newFace.rectangle.width,newFace.rectangle.height);
				vector<int>project_loc;
				Project_location_to_binsindex(h_plane,s_plane,v_plane,newFace.rectangle,project_loc); 
				generate_color_prob_model(project_loc,newFace.color_porb_distub,ROIrect1);

				allFaces.push_back(newFace);

			}
			// 			end3=clock();
			// 			run_time3=(float)(end3 - start3) / CLOCKS_PER_SEC;
			// 			f1<<"\n detect and show rects needs "<<run_time3<<" seconds.\n";	
			count_face_num=allFaces.size();     //进行计数统计
			cvClearMemStorage( storage );
			is_first_detect=0;
			continous_det=0;
			is_trace=0;
		}
		else
		{
			continous_det=1;           //是否第一次检测的开关，若已经进行过第一次检测，则后面就不再执行第一次检测部分
			is_trace=1;      //是否开始跟踪的开关
		}


	   //如果开始跟踪的开关is_trace=1且allFaces非空，则进行跟踪
		if(is_trace&&allFaces.size()>=OB_N)
		{
			vector<face>::iterator itr_f=allFaces.begin();
			while(itr_f!=allFaces.end()) //first face only for now
			{


				//判断跟踪到的矩形框是否到了图像的边缘
				int limi_h=itr_f->rectangle.height+1;
				int limi_w=itr_f->rectangle.width+1;
				if (/*itr_f->weight<0.39||*/itr_f->rectangle.x<1||itr_f->rectangle.y<0||itr_f->rectangle.x>im_roi->width-limi_w||itr_f->rectangle.y>im_roi->height-limi_h)
					itr_f=allFaces.erase(itr_f);


				else
				{
					int predX = itr_f->rectangle.x + itr_f->dx;
					int predY = itr_f->rectangle.y + itr_f->dy;

					//采样
					vector<face> samples = getSamples(*itr_f, predX, predY, itr_f->rectangle.width, itr_f->rectangle.height,150, 0.05, im_roi->width, im_roi->height); 
					for(int resampling=0; resampling<1; resampling++)
					{
						samples = resample(samples, *itr_f, im_roi->width, im_roi->height);  //重要性重采样
					}
				
					int bestIdx=0;
					float bestWeight=0;
					if (samples.size()>0)
					{
						//选取权值最大的粒子作为这一帧的跟踪目标
						for(int sampleIter=0; sampleIter<samples.size(); sampleIter++)
						{
//							rectangle(frame1,samples.at(sampleIter).rectangle,Scalar(155,0,100));
							if (samples.at(sampleIter).weight > bestWeight)
							{
								bestWeight = samples.at(sampleIter).weight;
								bestIdx = sampleIter;
							}
							//cvSet2D(sampling, samples.at(sampleIter).rectangle.y,samples.at(sampleIter).rectangle.x, cvScalar(samples.at(sampleIter).weight));
						}

						//move to best sample
						itr_f->dx = samples.at(bestIdx).rectangle.x - itr_f->rectangle.x;
						itr_f->dy = samples.at(bestIdx).rectangle.y - itr_f->rectangle.y;
						itr_f->weight=samples.at(bestIdx).weight;
//						fk<<itr_f->weight<<" ";
						// 						int b_w=min(samples.at(bestIdx).rectangle.width,limi_w);
						// 						int b_h=min(samples.at(bestIdx).rectangle.height,limi_h);
						// 					    b_w=max(b_w,30);
						// 						b_h=max(b_h,30);
						itr_f->rectangle=samples.at(bestIdx).rectangle;
//						fk<<itr_f->rectangle.width<<" ";
						//						itr_f->rectangle =cvRect(samples.at(bestIdx).rectangle.x,samples.at(bestIdx).rectangle.y,b_w,b_h);
						//						itr_f->weight= samples.at(bestIdx).weight;
 						itr_f->color_porb_distub=samples.at(bestIdx).color_porb_distub;
						itr_f++;
					}

					// 					end4=clock();
					// 					run_time4=(float)(end4 - start4) / CLOCKS_PER_SEC;
					// 					f1<<"\n  whole tracing "<<run_time4<<" seconds.\n";
				}

			}
//			int kkkkk=allFaces.size();

		}


		//若是继续检测的开关continous_det=1且图像的帧数id%10=0，则继续进行检测，是为了获得在检测范围内新出现的人脸，同时对部分跟踪发生偏差的进行纠正
		if (continous_det&&id%10==0) //detect faces
		{
			// 			clock_t start3,end3;
			// 			float run_time3;
			// 			start3=clock();
			storage = cvCreateMemStorage(0);
			//	 cvReleaseMemStorage(&storage);
			// There can be more than one face in an image. So create a growable sequence of faces.
			// Detect the objects and store them in the sequence
			CvSeq* faces = cvHaarDetectObjects( gray_roi, cascade, storage, 1.1,6, CV_HAAR_DO_CANNY_PRUNING, cvSize(24, 24) ,cvSize(200, 200));
			for (currentFace=0; currentFace<faces->total; currentFace++)
			{
				CvRect* r = (CvRect *)cvGetSeqElem( faces, currentFace );
				Rect new_det_rect=cvRect(r->x,r->y,r->width,r->height);
				if(r->width>90||r->x>im_roi->width-r->width-8||r->x<2||r->y<2||r->y>im_roi->height-r->height-10)     //如果检测到的矩形在检测范围的边界，则忽略
					continue;
				for (currentdet=0;currentdet<allFaces.size();currentdet++)
				{
					double overlap=get_rect_overlap(new_det_rect,allFaces[currentdet].rectangle);
					if (overlap>0.1)              //如果检测的矩形和原来allFaces中的矩形相交面积>0.1，则用新的矩形替代原来的矩形，可以起到纠正的作用
					{
						allFaces[currentdet].rectangle=new_det_rect;
						is_first_show=0;
						break;
					}
					else
						is_first_show=1;
				}
				if (is_first_show||allFaces.size()==0)    //如果检测到的矩形属于新出现的，则添加到allFaces数组中，同时计数器加1
				{
					count_face_num+=1;
//					rectangle(frame1,*r,Scalar(255,0,255),2,8,0);
					face newFace;
					newFace.rectangle=new_det_rect;
//					fk<<newFace.rectangle.width<<" ";
					newFace.dx = 0;
					newFace.dy = 0;
					newFace.flag=1;
					newFace.weight=1;
					newFace.showid=count_face_num;
					Rect ROIrect1 = cvRect(0,0,newFace.rectangle.width,newFace.rectangle.height);
					vector<int>project_loc;
					Project_location_to_binsindex(h_plane,s_plane,v_plane,newFace.rectangle,project_loc);
					generate_color_prob_model(project_loc,newFace.color_porb_distub,ROIrect1);
					allFaces.push_back(newFace);
				}	
			}
			// 			end3=clock();
			// 			f1<<"\n detect and show rects needs "<<run_time3<<" seconds.\n";	
			// 			run_time3=(float)(end3 - start3) / CLOCKS_PER_SEC;
			cvClearMemStorage( storage );

		
		}

		pnt1.x = 0, pnt1.y = g_count_line_y;
		pnt2.x = width - 1, pnt2.y = g_count_line_y;
		cvLine(im, pnt1, pnt2, CV_RGB(255,0,0));
		CvFont font;
		double hScale=1, vScale=1;
		int lineWidth=2;
		cvInitFont(&font,CV_FONT_HERSHEY_SIMPLEX|CV_FONT_ITALIC,hScale,vScale,0,lineWidth,8);
		char target_num[128];
		sprintf(target_num, "target counted: %d", count_face_num);
		cvPutText(im,target_num,cvPoint(50,50), &font, CV_RGB(255,255,0));
		
		drawFaces(top_line_y);
		writer1.write(frame1);
		if (id>1e6)
			id=0;                     //防止视频过长，发生溢出
		end1=clock();
		run_time1=(float)(end1 - start1) / CLOCKS_PER_SEC;
		char cinfo[128];
		sprintf(cinfo,"run time:%f", run_time1);
		//		if( det_nf < framenum )
		::SetWindowText(g_hinfo, cinfo);
		bool is_read=capture1.read(frame1);
		if(!is_read)
			break;
		im->imageData=(char *)frame1.data;
		id++;
		
		// 		if(mouse_pause)
		// 			cvWaitKey(5000);
		// 		else
//		cvWaitKey(50);
	}
	//	cout<<"there has been "<<det_time.size()<<" times detection."<<endl;
	writer1.release();
	cvReleaseImage(&im);
	cvReleaseImage(&gray_roi);
	cvReleaseImage(&im_roi);
	cvReleaseImage(&h_plane);
	cvReleaseImage(&v_plane);
	cvReleaseImage(&s_plane);
	cvReleaseImage(&hsv);
}




