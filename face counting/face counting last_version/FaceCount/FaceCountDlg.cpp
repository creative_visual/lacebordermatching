
// FaceCountDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "FaceCount.h"
#include "FaceCountDlg.h"
#include "afxdialogex.h"
#include "particle_filters.h"
#include "face_detect.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CFaceCountDlg 对话框

extern string g_video_path;
extern int g_min_face_size, g_max_face_size;
extern int g_count_line_y;
extern int g_successive_frames, g_max_track_num;
extern vector<frame_info> video_target_info;
extern vector<int> g_bcounted;
extern int top_line_y;// = g_count_line_y - g_max_face_size ;
extern int btm_line_y;// = g_count_line_y + g_max_face_size * 1.2;
extern double corr_t;// = 0.05;
extern int g_interval;// = 1;
extern int g_avgdiff;// = 55;
extern double g_zoomrate;

extern double g_scalefactor;
extern int g_minneighbors;
extern int g_detflag;

extern UINT g_pro_info_id;
extern HWND g_hinfo, g_hstart, g_hopen, g_hwnd;

extern DWORD g_dettime, g_tracktime;

extern CRect g_rect;
extern IplImage *g_dspbuf;
extern CRITICAL_SECTION gSection;
extern volatile int g_bdetect, g_bdraw;
// extern CFaceCountDlg *g_app;

CWinThread *g_detthr, *g_drawthr;
HANDLE g_dethdl, g_drawhdl;
int g_bworking = 0;
HWND g_hdlg, g_hmin_face, g_hmax_face, g_hcount_line_y;
HWND g_hbtn_restart;
int  g_min_face_id, g_max_face_id, g_count_liney_id;

//UINT count_faces(LPVOID pParam);

CFaceCountDlg::CFaceCountDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CFaceCountDlg::IDD, pParent)
	, m_minfacesize(0)
	, m_maxfacesize(0)
	, m_count_line(0)
	, m_interval(0)
	, m_avgdiff(0)
	, m_zoomrate(0)
	, m_scalefactor(0)
	, m_minneighbors(0)
	, m_detflag(0)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CFaceCountDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_MINSIZE, m_minfacesize);
	DDX_Text(pDX, IDC_EDIT_MAXSIZE, m_maxfacesize);
	DDX_Text(pDX, IDC_EDIT_COUNTLINE, m_count_line);
	DDX_Text(pDX, IDC_EDIT_INTERVAL, m_interval);
	DDX_Text(pDX, IDC_EDIT_ZOOMRATE, m_zoomrate);
	DDX_Text(pDX, IDC_EDIT_AVGDIFF, m_avgdiff);

	DDX_Text(pDX, IDC_EDIT_SCALEFACTOR, m_scalefactor);
	DDX_Text(pDX, IDC_EDIT_MINNEIGHBORS, m_minneighbors);
	DDX_Text(pDX, IDC_EDIT_DETFLAG, m_detflag);
}

BEGIN_MESSAGE_MAP(CFaceCountDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON_OPEN, &CFaceCountDlg::OnBnClickedButtonOpen)
	ON_BN_CLICKED(IDC_BUTTON_START, &CFaceCountDlg::OnBnClickedButtonStart)
	ON_BN_CLICKED(IDC_BUTTON_PARAMS, &CFaceCountDlg::OnBnClickedButtonParams)
	ON_BN_CLICKED(IDC_BUTTON_RESTART, &CFaceCountDlg::OnBnClickedButtonRestart)
	ON_BN_CLICKED(IDC_BUTTON_PARAMEDIT, &CFaceCountDlg::OnBnClickedButtonParamedit)
// 	ON_EN_KILLFOCUS(IDC_EDIT_MINSIZE, &CFaceCountDlg::OnEnKillfocusEditMinsize)
// 	ON_EN_KILLFOCUS(IDC_EDIT_MAXSIZE, &CFaceCountDlg::OnEnKillfocusEditMaxsize)
// 	ON_EN_KILLFOCUS(IDC_EDIT_COUNTLINE, &CFaceCountDlg::OnEnKillfocusEditCountline)
END_MESSAGE_MAP()


// CFaceCountDlg 消息处理程序

BOOL CFaceCountDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码
	UpdateData();
	m_minfacesize = 75, m_maxfacesize = 175;
	m_count_line = 540;
	m_interval = 0;
	m_avgdiff = 55;
	m_zoomrate = 0.8;
	m_scalefactor = 1.1;
	m_minneighbors = 2;
	m_detflag = 0;
	UpdateData(FALSE);

	g_pro_info_id = IDC_STATIC_COMPLETERATE;
	g_hinfo = GetDlgItem(IDC_STATIC_COMPLETERATE)->GetSafeHwnd();//this->m_hWnd;
	g_hopen = GetDlgItem(IDC_BUTTON_OPEN)->GetSafeHwnd();
	g_hstart = GetDlgItem(IDC_BUTTON_START)->GetSafeHwnd();

	m_openflag = 1;
	MoveParamsCtrl();
	//GetDlgItem(IDC_BUTTON_PARAMS)->EnableWindow(FALSE);

	GetDlgItem(IDC_IMAGE)->GetWindowRect(&m_dsprect);
	ScreenToClient(m_dsprect);

	CvSize size;
	size.width = m_dsprect.Width(),size.height = m_dsprect.Height();
	m_dspbuf = cvCreateImage(size, IPL_DEPTH_8U, 4);
	memset(m_dspbuf->imageData, 0, m_dspbuf->widthStep * m_dspbuf->height);

	m_detthr = NULL, m_drawthr = NULL;
// 	g_app = this;

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CFaceCountDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();

		if( m_dspbuf )
			DisplayImage(IDC_IMAGE, m_dsprect, m_dspbuf);
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CFaceCountDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CFaceCountDlg::DisplayImage(UINT nid, CRect dsprect, IplImage *dspbuf)
{
	CPaintDC dc(GetDlgItem(nid));

	CDC dcMem;
	CBitmap bmpMem;

	dcMem.CreateCompatibleDC(&dc);
	bmpMem.CreateCompatibleBitmap(&dc, dsprect.Width(), dsprect.Height());
	dcMem.SelectObject(&bmpMem);

	int linebyte;
	int w = dsprect.Width();
	int h = dsprect.Height();
	linebyte = dspbuf->widthStep;

	bmpMem.SetBitmapBits(linebyte * h, dspbuf->imageData);
	dc.BitBlt(0, 0, w,h, &dcMem, 0, 0, SRCCOPY);

	bmpMem.DeleteObject();
	dcMem.DeleteDC(); 
}

void CFaceCountDlg::OnBnClickedButtonOpen()
{
	BROWSEINFO bInfo;
	ZeroMemory(&bInfo, sizeof(bInfo));
	bInfo.hwndOwner =GetSafeHwnd();
	bInfo.lpszTitle = _T("请选择要被测试的视频文件: ");
	bInfo.ulFlags = BIF_BROWSEINCLUDEFILES;//BIF_RETURNONLYFSDIRS; 

	LPITEMIDLIST lpDlist; //用来保存返回信息的IDList
	lpDlist = SHBrowseForFolder(&bInfo) ; //显示选择对话框
	if(lpDlist != NULL) //用户按了确定按钮
	{
		TCHAR chPath[MAX_PATH]; //用来存储路径的字符串
		SHGetPathFromIDList(lpDlist, chPath);//把项目标识列表转化成字符串
		m_video_path = chPath; //将TCHAR类型的字符串转换为CString类型的字符串
		g_video_path = chPath;
	}

	((CEdit *)GetDlgItem(IDC_EDIT_VIDEOPATH))->SetWindowText(m_video_path.c_str());
}

void CFaceCountDlg::UpdateParams()
{
	UpdateData();
	g_min_face_size = m_minfacesize;
	g_max_face_size = m_maxfacesize;
	g_count_line_y = m_count_line;
	g_interval = m_interval;
	g_avgdiff = m_avgdiff;
	g_zoomrate = m_zoomrate;
	g_scalefactor = m_scalefactor;
	g_minneighbors = m_minneighbors;
	g_detflag = m_detflag;
	UpdateData(FALSE);

	g_dspbuf = m_dspbuf;
	g_hwnd = this->m_hWnd;
	g_rect = m_dsprect;
}

unsigned _stdcall count_faces(void* param)
{
//	string path_f = "haartrain_f.xml";
	string path_f = "face1.xml";
	string out_path = "counted3.avi";
	DWORD time = GetTickCount();
	detect_using_cascades_and_particle_filters(path_f, g_video_path, out_path);
	DWORD timepass = GetTickCount() - time;

// 	char info[128];
// 	//sprintf(info, "检测耗时： %d 秒    跟踪耗时： %d 秒", g_dettime / 1000,  g_tracktime / 1000);
// 	sprintf(info, "total time cost： %d s", timepass / 1000);
// 	SetWindowText(g_hinfo, info);

	return 1;
}

unsigned _stdcall draw_counted(void* param)
{
	draw_count_info(g_video_path, video_target_info);
	//DeleteCriticalSection(&gSection);

	char info[128];
	//sprintf(info, "检测耗时： %d 秒    跟踪耗时： %d 秒", g_dettime / 1000,  g_tracktime / 1000);
	sprintf(info, "统计完成！");
	//SetWindowText(g_hinfo, info);
	return 1;
}

UINT restart(LPVOID pParam)
{
	EnableWindow(g_hbtn_restart,FALSE);
	if( g_bworking == 2 )
	{
		ResumeThread(g_dethdl);
		ResumeThread(g_drawhdl);
	}

	g_bdraw = 0;
	WaitForSingleObject(g_drawhdl, INFINITE);

	g_bdetect = 0;
	WaitForSingleObject(g_dethdl, INFINITE);

	CloseHandle(g_dethdl);
	CloseHandle(g_drawhdl);

	char minsize[8],maxsize[8],countline[8];
	::GetWindowText(g_hmin_face, minsize, 8);
	::GetWindowText(g_hmax_face, maxsize, 8);
	::GetWindowText(g_hcount_line_y, countline, 8);

	g_min_face_size = atoi(minsize);
	g_max_face_size = atoi(maxsize);
	g_count_line_y = atoi(countline);

	g_bdraw = 1, g_bdetect = 1;
	g_dethdl = (HANDLE)_beginthreadex(NULL, 0, count_faces, NULL, NULL, NULL);
	g_drawhdl = (HANDLE)_beginthreadex(NULL, 0, draw_counted, NULL, NULL, NULL);

	EnableWindow(g_hbtn_restart,TRUE);

	g_bworking = 1;
// 	g_detthr = AfxBeginThread((AFX_THREADPROC)count_faces,NULL);
// 	g_drawthr = AfxBeginThread((AFX_THREADPROC)draw_counted,NULL);

	return 1;
}

void CFaceCountDlg::OnBnClickedButtonStart()
{
// 	if( !is_software_valid() )
// 		return;

	g_hmin_face = ::GetDlgItem(this->m_hWnd, IDC_EDIT_MINSIZE);
	g_hmax_face = ::GetDlgItem(this->m_hWnd, IDC_EDIT_MAXSIZE);
	g_hcount_line_y = ::GetDlgItem(this->m_hWnd, IDC_EDIT_COUNTLINE);

	g_hbtn_restart = ::GetDlgItem(this->m_hWnd, IDC_BUTTON_RESTART);

	g_hdlg = this->m_hWnd;

	if( !g_bworking )
	{
		g_bdetect = 1, g_bdraw = 1;
		UpdateParams();
		//InitializeCriticalSection(&gSection);
		//_beginthread((void *)count_faces,0,NULL);
		g_dethdl = (HANDLE)_beginthreadex(NULL, 0, count_faces, NULL, NULL, NULL);
//		g_drawhdl = (HANDLE)_beginthreadex(NULL, 0, draw_counted, NULL, NULL, NULL);

// 		g_detthr = AfxBeginThread((AFX_THREADPROC)count_faces,NULL);
// 		g_drawthr = AfxBeginThread((AFX_THREADPROC)draw_counted,NULL);

		GetDlgItem(IDC_BUTTON_OPEN)->EnableWindow(FALSE);
		GetDlgItem(IDC_BUTTON_START)->SetWindowTextA("暂停");

		g_bworking = 1;
		return;
	}

	if( g_bworking == 1 )
	{
		SuspendThread(g_drawhdl);
		SuspendThread(g_dethdl);

		GetDlgItem(IDC_BUTTON_START)->SetWindowTextA("继续");

		g_bworking = 2;
		return;
	}

	if( g_bworking == 2 )
	{
		//EnterCriticalSection(&gSection);
		UpdateParams();
		//LeaveCriticalSection(&gSection);

		ResumeThread(g_dethdl);
		ResumeThread(g_drawhdl);

		GetDlgItem(IDC_BUTTON_START)->SetWindowTextA("暂停");
		g_bworking = 1;
		return;
	}
	
	//GetDlgItem(IDC_BUTTON_START)->EnableWindow(FALSE);
}

void CFaceCountDlg::OnBnClickedButtonRestart()
{
	if( !is_software_valid() )
		return;

	if( !g_bworking )
	{
		g_bdetect = 1, g_bdraw = 1;
		UpdateParams();
	
		g_dethdl = (HANDLE)_beginthreadex(NULL, 0, count_faces, NULL, NULL, NULL);
//		g_drawhdl = (HANDLE)_beginthreadex(NULL, 0, draw_counted, NULL, NULL, NULL);

		GetDlgItem(IDC_BUTTON_OPEN)->EnableWindow(FALSE);
		GetDlgItem(IDC_BUTTON_START)->SetWindowTextA("暂停");

		g_bworking = 1;
		return;
	}

	if( g_bworking == 1 || g_bworking == 2 )
	{
		AfxBeginThread((AFX_THREADPROC)restart,NULL);
		GetDlgItem(IDC_BUTTON_START)->SetWindowTextA("暂停");
		return;
	}
}

void CFaceCountDlg::OnBnClickedButtonParamedit()
{
// 	while( !g_bdetect || !g_bdraw );
// 	EnterCriticalSection(&gSection);
// 	UpdateParams();
// 	LeaveCriticalSection(&gSection);
}

void CFaceCountDlg::MoveParamsCtrl()
{
	int f, vf, delta_y = 80, tmp_flag = m_openflag;
	CString btn_txt;

	if( tmp_flag )
	{
		f  = -1;
		vf = 0;
		m_openflag = 0;
		btn_txt = "高级参数";
	}
	else
	{
		f  = 1;
		vf = 1;
		m_openflag = 1;
		btn_txt = "基本参数";
	}

	CRect dlg_rect;
	GetWindowRect(&dlg_rect);

	GetDlgItem(IDC_STATIC_INTERVAL)->ShowWindow(vf);
	GetDlgItem(IDC_EDIT_INTERVAL)->ShowWindow(vf);
	GetDlgItem(IDC_STATIC_AVGDIFF)->ShowWindow(vf);
	GetDlgItem(IDC_EDIT_AVGDIFF)->ShowWindow(vf);
	GetDlgItem(IDC_STATIC_ZOOMRATE)->ShowWindow(vf);
	GetDlgItem(IDC_EDIT_ZOOMRATE)->ShowWindow(vf);

	GetDlgItem(IDC_STATIC_SCALEFACTOR)->ShowWindow(vf);
	GetDlgItem(IDC_EDIT_SCALEFACTOR)->ShowWindow(vf);
	GetDlgItem(IDC_STATIC_NEIGHBORS)->ShowWindow(vf);
	GetDlgItem(IDC_EDIT_MINNEIGHBORS)->ShowWindow(vf);
	GetDlgItem(IDC_STATIC_DETFLAG)->ShowWindow(vf);
	GetDlgItem(IDC_EDIT_DETFLAG)->ShowWindow(vf);

	CRect info, start, params;
// 	GetDlgItem(IDC_STATIC_COMPLETERATE)->GetWindowRect(&info), ScreenToClient(&info);
// 	GetDlgItem(IDC_BUTTON_START)->GetWindowRect(&start), ScreenToClient(&start);
// 	GetDlgItem(IDC_BUTTON_PARAMS)->GetWindowRect(&params), ScreenToClient(&params);
// 
// 	info.top += f * delta_y, info.bottom += f * delta_y;
// 	start.top += f * delta_y, start.bottom += f * delta_y;
// 	params.top += f * delta_y, params.bottom += f * delta_y;
// 
// 	GetDlgItem(IDC_STATIC_COMPLETERATE)->MoveWindow(&info);
// 	GetDlgItem(IDC_BUTTON_START)->MoveWindow(&start);
// 	GetDlgItem(IDC_BUTTON_PARAMS)->MoveWindow(&params);

// 	dlg_rect.bottom += f * delta_y;
// 	MoveWindow(&dlg_rect);
	GetDlgItem(IDC_BUTTON_PARAMS)->SetWindowTextA(btn_txt);
}

void CFaceCountDlg::OnBnClickedButtonParams()
{
	MoveParamsCtrl();
}

// void CFaceCountDlg::OnEnKillfocusEditMinsize()
// {
// 	UpdateData();
// 	UpdateData(FALSE);
// }
// 
// void CFaceCountDlg::OnEnKillfocusEditMaxsize()
// {
// 	UpdateData();
// 	UpdateData(FALSE);
// }
// 
// 
// void CFaceCountDlg::OnEnKillfocusEditCountline()
// {
// 	UpdateData();
// 	UpdateData(FALSE);
// }
