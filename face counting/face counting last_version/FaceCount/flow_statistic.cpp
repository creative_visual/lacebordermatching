// flow_statistic.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include <windows.h>
#include "face_detect.h"
#include "MyCascadeClassifier.h"
#include <opencv2\legacy\legacy.hpp>

extern vector<frame_info> video_target_info;

int parse_cmdline(int argc, _TCHAR * argv[], string &video_path, string &out_path);

int _tmain(int argc, _TCHAR* argv[])
{
	string path_f = "haartrain.xml";//"haarcascade_frontalface_alt_tree.xml";
	vector<rect> faces;

	string video_path = "441.avi";
	string out_path = "counted.avi";
	string back_ground = "back_ground.bmp";

// 	int flag = parse_cmdline(argc, argv, video_path, out_path);
// 	if( !flag )
// 		return 0;
	//generate_video("sec","sec_video.avi");
	//save_video_frames("counted.avi", 0.0, 1.0, "csec\\");

	//SetPriorityClass(GetCurrentProcess(), THREAD_PRIORITY_ABOVE_NORMAL);
	diff_width_background(video_path, "bg.bmp", "diff.avi");
	//avg_image("bgpic", "bg.bmp");

	DWORD time = GetTickCount();
	detect_using_cascades(path_f, video_path, back_ground, out_path);
	DWORD dt = GetTickCount() - time;  // 8 * 60s

	cout<<  dt/1000<<endl; 

	return 0;
}

int parse_cmdline(int argc, _TCHAR * argv[], string &video_path, string &out_path)
{
	int flag = 0;

	if( argc < 2 )
		return 0;

	if( argc == 2 )
	{
		video_path = argv[1];

		string video_name; 
		int l = video_path.find(".");
		video_name = video_path.substr(0,l);

		out_path = video_name + "_face_counted.avi";
		flag = 1;
	}

	if( argc == 3 )
	{
		video_path = argv[1];
		out_path = argv[2];
		flag = 1;
	}

	return flag;
}