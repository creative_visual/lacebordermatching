/*********************************************************************
 * Copyright (C) 2002 Maenpaa
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1.Redistributions of source code must retain all copyright
 *   notices, this list of conditions and the following disclaimer.
 *
 * 2.Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 *
 * 3.The name(s) of the author(s) may not be used to endorse or
 *   promote products derived from this software without specific
 *   prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR(S) ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE AUTHOR(S) BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

/*********************************************************************
 * Contact information
 *
 * Topi Maenpaa
 * Machine Vision Group
 * Department of Electrical and Information Engineering
 * University of Oulu
 * P.O. Box 4500
 * 90014 University of Oulu
 * topiolli@ee.oulu.fi
 * topi.maenpaa@intopii.fi
 *
 * What this file is all about
 * An implementation of the 8-bit LBP operator.
 * This code is a C version of the original C++ code. It is a bit
 * messy because of optimizations and the fact that the code was
 * originally designed for an object-oriented libary.
 * If you find a bug, please inform me.
 ******************************************************************/
#include "stdafx.h"
#include "lbp.h"


integerpoint points[bits];
doublepoint offsets[bits];

/*
 * Get a bilinearly interpolated value for a pixel.
 */
inline double interpolate_at_ptr(float* upperLeft, int i, int columns)
{
	double dx = 1-offsets[i].x;
	double dy = 1-offsets[i].y;
	return
		*upperLeft*dx*dy +
		*(upperLeft+1)*offsets[i].x*dy +
		*(upperLeft+columns)*dx*offsets[i].y +
		*(upperLeft+columns+1)*offsets[i].x*offsets[i].y;
}
	
/*
 * Calculate the point coordinates for circular sampling of the neighborhood.
 */
void calculate_points(void)
{
	double step = 2 * M_PI / bits, tmpX, tmpY;
	int i;
	for (i=0;i<bits;i++)
	{
			tmpX = predicate * cos(i * step);
			tmpY = predicate * sin(i * step);
			points[i].x = (int)tmpX;
			points[i].y = (int)tmpY;
			offsets[i].x = tmpX - points[i].x;
			offsets[i].y = tmpY - points[i].y;
			if (offsets[i].x < 1.0e-10 && offsets[i].x > -1.0e-10) /* rounding error */
				offsets[i].x = 0;
			if (offsets[i].y < 1.0e-10 && offsets[i].y > -1.0e-10) /* rounding error */
				offsets[i].y = 0;
			
			if (tmpX < 0 && offsets[i].x != 0)
				{
					points[i].x -= 1;
					offsets[i].x += 1;
				}
			if (tmpY < 0 && offsets[i].y != 0)
				{
					points[i].y -= 1;
					offsets[i].y += 1;
				}
	}
}

/*
 * Calculate the LBP histogram for an integer-valued image. This is an
 * optimized version of the basic 8-bit LBP operator. Note that this
 * assumes 4-byte integers. In some architectures, one must modify the
 * code to reflect a different integer size.
 * 
 * img: the image data, an array of rows*columns integers arranged in
 * a horizontal raster-scan order
 * rows: the number of rows in the image
 * columns: the number of columns in the image
 * result: an array of 256 integers. Will hold the 256-bin LBP histogram.
 * interpolated: if != 0, a circular sampling of the neighborhood is
 * performed. Each pixel value not matching the discrete image grid
 * exactly is obtained using a bilinear interpolation. You must call
 * calculate_points (only once) prior to using the interpolated version.
 * return value: result
 */
int * cs_lbp_histogram(float* img, int rows, int columns, int interpolated)
{
	int leap = columns*predicate;
	/*Set up a circularly indexed neighborhood using nine pointers.*/
	float
		*p0 = img,
		*p1 = p0 + predicate,
		*p2 = p1 + predicate,
		*p3 = p2 + leap,
		*p4 = p3 + leap,
		*p5 = p4 - predicate,
		*p6 = p5 - predicate,
		*p7 = p6 - leap,
		*center = p7 + predicate;
//	cout<<*p0<<" "<<*p1<<" "<<*p4<<" "<<*p5<<" "<<*p6<<" "<<*p7<<endl;
	unsigned int value;
	int pred2 = predicate << 1;
	int r,c;
	int* result=new int[LBP_NUM];
	memset(result,0,sizeof(int)*LBP_NUM); /* Clear result histogram */
		
	if (!interpolated)
	{
			for (r=0;r<rows-pred2;r++)
			{
					for (c=0;c<columns-pred2;c++)
					{
							value = 0;

							/* Unrolled loop */
							compab_mask_inc(p0,p4,0);
							compab_mask_inc(p1,p5,1);
							compab_mask_inc(p2,p6,2);
							compab_mask_inc(p3,p7,3);
							
							center++;

							result[value]++; /* Increase histogram bin value */
					}
					p0 += pred2;
					p1 += pred2;
					p2 += pred2;
					p3 += pred2;
					p4 += pred2;
					p5 += pred2;
					p6 += pred2;
					p7 += pred2;
					center += pred2;
			}
	}
	else
	{
			p0 = center + points[5].x + points[5].y * columns;
			p2 = center + points[7].x + points[7].y * columns;
			p4 = center + points[1].x + points[1].y * columns;
			p6 = center + points[3].x + points[3].y * columns;
			*p1=interpolate_at_ptr(p0,5,columns)+0.5;
			*p3=interpolate_at_ptr(p2,7,columns)+0.5;
			*p5=interpolate_at_ptr(p4,1,columns)+0.5;
			*p7=interpolate_at_ptr(p6,3,columns)+0.5;
				
			for (r=0;r<rows-pred2;r++)
			{
					for (c=0;c<columns-pred2;c++)
					{
							value = 0;

							/* Unrolled loop */
							compab_mask_inc(p0,p4,0);
							compab_mask_inc(p1,p5,1);
							compab_mask_inc(p2,p6,2);
							compab_mask_inc(p3,p7,3);

							/* Interpolate corner pixels */
							
							center++;
								
							result[value]++;
					}
					p0 += pred2;
					p1 += pred2;
					p2 += pred2;
					p3 += pred2;
					p4 += pred2;
					p5 += pred2;
					p6 += pred2;
					p7 += pred2;
					center += pred2;
			}
	}
	
	
	return result;
	
}

int *connect_hist(int *hist[],int grid_n,int hist_m)
{
	int i,j;
	int *con_hist=new int[grid_n*hist_m];
	for (i=0;i<grid_n;i++)
		for (j=0;j<hist_m;j++)
		{
			con_hist[i*hist_m+j]=hist[i][j];
			/*cout<<con_hist[i*hist_m+j]<<"  ";*/
		} 
	return con_hist;

	
}

float * deal_with_hist(int *hist)
{
	int i,j;
	int sum=0;
	float new_sum=0.0;
	float *norm_hist=new float[HIST_NUM];
	for (i=0;i<HIST_NUM;i++)
	{
		sum+=hist[i];
	}
	bool change=0;
	for (i=0;i<HIST_NUM;i++)
	{
		norm_hist[i]=(float)hist[i]/sum;
	//	cout<<norm_hist[i]<<"  ";
		if (norm_hist[i]>0.2)
		{
			norm_hist[i]=0.2;	
			change=1;
		}
	}
	if (change)
	{
		for (i=0;i<HIST_NUM;i++)
			new_sum+=norm_hist[i];
		for (i=0;i<HIST_NUM;i++)
			norm_hist[i]=norm_hist[i]/new_sum;
	}
	
	
	return norm_hist;
}

void normlize_pt(Mat &in,Mat &norm_mat)
{
	int i,j;
	float sum=0;
	for (i=0;i<in.rows;i++)
		for (j=0;j<in.cols;j++)
			sum+=in.at<float>(i,j);
	for (i=0;i<in.rows;i++)
		for (j=0;j<in.cols;j++)
			norm_mat.at<float>(i,j)=in.at<uchar>(i,j)/sum;
}

float * Compute_CS_LBP_Descriptors(Mat &patch)
{
	float *norm_des;
	int width=patch.cols;
	int height=patch.rows;
	int i,j,s,t;
	int iter=0;

// 	Mat norm_patch(height,width,CV_32FC1);
// 	
//  	normlize_pt(patch,norm_patch);
// 	for (int i=0;i<norm_patch.rows;i++)
// 	{
// 		for (int j=0;j<norm_patch.cols;j++)
// 		{
// 			cout<<norm_patch.at<float>(i,j)<<" ";
// 		}
// 		
// 	}
	
	int interval=11;
	float *data=(float *)patch.data;
	int *result[ALL_GRID];
	for (s=0;s<GRID_NUM;s++)
	{
		for (t=0;t<GRID_NUM;t++)
		{
			data+=s*GRID_NUM+t;
			int grid_row=interval;
			int grid_col=interval;
			result[iter]=cs_lbp_histogram(data,grid_row,grid_col,1);
			
			iter++;
		}
	//	cout<<"\n one grid \n";
	}
	int *con_res=NULL;
	con_res=connect_hist(result,ALL_GRID,LBP_NUM);

	norm_des=deal_with_hist(con_res);

 	delete[] con_res;
	for (i=0;i<ALL_GRID;i++)
		delete[] result[i];
// 	for (i=0;i<HIST_NUM;i++)
// 	{
// 		cout<<norm_des[i]<<"  ";
// 	}
	
	
	return norm_des;
}


vector<float> compute_descriptors_kane(Mat in)
{
	int i;
	vector<float> lbp_feats;	
	float *lbp_feat=NULL;
	lbp_feat=Compute_CS_LBP_Descriptors(in);
	for (i=0;i<HIST_NUM;i++)
		lbp_feats.push_back(lbp_feat[i]);
	delete[] lbp_feat;
	return lbp_feats;
}