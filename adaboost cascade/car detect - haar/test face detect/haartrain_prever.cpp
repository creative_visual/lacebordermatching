// haartrain_prever.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
//#include <opencv2/opencv.hpp>
#include "opencv2/core/core.hpp"
#include "opencv2/core/internal.hpp"

#include "cv.h"
#include "highgui.h"

#include <cstdio>
#include <cmath>
#include <ctime>
#include <fstream>
#include <io.h>

using namespace std;
using namespace cv;

#include "cvhaartraining.h"
//#include "cascadeclassifier.h"

#define PATH_MAX 512

void get_roc_info();
void detectAndDraw( Mat& img, CascadeClassifier& cascade, double scale);
void detect_and_draw_objects( IplImage* image, CvHaarClassifierCascade* cascade, int do_pyramids );
void detect_and_draw( IplImage* img , CvHaarClassifierCascade* cascade, CvMemStorage* storage);
int create_vec_file(int argc, _TCHAR* argv[]);
int performance( int argc, _TCHAR* argv[] );
int train(int argc, _TCHAR* argv[]);
int traincascade(int argc, char* argv[]);

String cascadeName = "haartrain_prever.xml";

typedef struct ObjectPos
{
	float x;
	float y;
	float width;
	int found;    /* for reference */
	int neghbors;
} ObjectPos;

int is_exsit_face( IplImage *img, CvHaarClassifierCascade* cascade, CvMemStorage* storage);
void get_path_files(string path, vector<string> &files);
void get_dir_filenames(string path, vector<string> &files);
void updata_cascade(CvHaarClassifierCascade** cascade, float *threshold);
int exist_same_pnt(vector<float> &fpr, vector<float> &tpr, double fr, double tr);
void detect_images(char *dir_src, char *dir_out);

int _tmain(int argc, _TCHAR* argv[])
{

//	create_vec_file(argc, argv);
//	train(argc, argv);
 	CvMemStorage* storage = cvCreateMemStorage(0);
 	IplImage *tmp = cvLoadImage("E:\\1\\2100.jpg");
 	CvHaarClassifierCascade* cascade = (CvHaarClassifierCascade *)cvLoad( "cascade3.xml" );
 	detect_and_draw(tmp, cascade, storage);
	return 0;
}

void get_roc_info()
{
	string pos_dir = "D:\\work\\haartrain_prever\\haartrain_prever\\test_faces";
	string neg_dir = "D:\\work\\haartrain_prever\\haartrain_prever\\test_non_faces";

	vector<string> pos_files, neg_files;
	vector<IplImage *>pos_imgs, neg_imgs;

	get_path_files(pos_dir, pos_files);
	get_path_files(neg_dir, neg_files);

	for(int i = 0; i < pos_files.size(); i++)
	{
		IplImage *image = cvLoadImage(pos_files[i].c_str());
		pos_imgs.push_back(image);
	}

	for(int i = 0; i < neg_files.size(); i++)
	{
		IplImage *image = cvLoadImage(neg_files[i].c_str());
		neg_imgs.push_back(image);
	}

	CvHaarClassifierCascade*  cascade = 0;//创建级联分类器对象26     

	cascade = (CvHaarClassifierCascade *)cvLoad( cascadeName.c_str() );


	int k,tp,tn,fp,fn;
	vector<float> fpr, tpr;
	float j = -3;

	float *threshold = new float[cascade->count];

	for(k = 0; k < cascade->count; k++)
		threshold[k] = cascade->stage_classifier[k].threshold;

	double tb = -1.0, te = 1.0, step = 0.25;

	//for(k = 0; k < cascade->count; k++)
	{
		j = -3;
//		for(threshold[0] = tb; threshold[0] < te; threshold[0] += step)
		{
//			for(threshold[1] = tb; threshold[1] < te; threshold[1] += step)
			{
//				for(threshold[2] = tb; threshold[2] < te; threshold[2] += step)
				{
//					for(threshold[3] = tb; threshold[3] < te; threshold[3] += step)
					while(j < 3)
					{
// 						cascade = (CvHaarClassifierCascade *)cvLoad( cascadeName.c_str() );
// 
// 						for(k = 0; k < cascade->count; k++)
							cascade->stage_classifier[3].threshold = j;

						//updata_cascade(&cascade, threshold);

						tp = 0,tn = 0,fp = 0,fn = 0;
						for(int i = 0; i < pos_imgs.size(); i++)
						{
							//if( i % 10 == 0 )
							//	updata_cascade(&cascade, threshold);

							CvMemStorage* storage = cvCreateMemStorage(0);
							
							IplImage *tmp = cvCloneImage(pos_imgs[i]);
							int flag = is_exsit_face( tmp, cascade, storage);

							if( flag )
								tp++;
							else
								fn++;

							cascade->hid_cascade = 0;
							cvReleaseMemStorage(&storage);
							cvReleaseImage(&tmp);
						}

						//updata_cascade(&cascade, threshold);
						for(int i = 0; i < neg_imgs.size(); i++)
						{
							//if( i % 10 == 0 )
							//	updata_cascade(&cascade, threshold);

							CvMemStorage* storage = cvCreateMemStorage(0);
							IplImage *tmp = cvCloneImage(neg_imgs[i]);
							int flag = is_exsit_face( tmp, cascade, storage);

							if( flag )
								fp++;
							else
								tn++;

							cascade->hid_cascade = 0;
							cvReleaseMemStorage(&storage);
							cvReleaseImage(&tmp);
						}
						//cvReleaseHaarClassifierCascade(&cascade);

						float tr = (float)tp / (tp + fn);
						float fr = (float)fp / (fp + tn);

						if( !fpr.size() || !exist_same_pnt(fpr, tpr, fr, tr) )
						{
							fpr.push_back(fr);
							tpr.push_back(tr);
						}
 						j = j + 0.1;

						int kkk = 0;
					}
					int kkk = 0;
				}
				int kkk = 0;
			}
			int kkk = 0;
		}

	//	cascade->stage_classifier[k].threshold = threshold[k];

		int kkk = 0;
	}

	string filename = "roc_data.txt";

	ofstream file;
	file.open(filename.c_str());

	for(int i = 0; i < fpr.size(); i++)
	{
		file<<fpr[i]<<" ";
		file<<tpr[i]<<endl;
	}

	file.close();
}

void detect_images(char *dir_src, char *dir_out)
{
	vector<string> files;

	get_dir_filenames(dir_src, files);

	for(int i = 0; i < files.size(); i++)
	{
		CvHaarClassifierCascade*  cascade = 0;//创建级联分类器对象26     
		cascade = (CvHaarClassifierCascade *)cvLoad( cascadeName.c_str() );

		CvMemStorage* storage = cvCreateMemStorage(0);

		char filepath[512];
		sprintf(filepath, "%s\\%s", dir_src, files[i].c_str());
		IplImage *image = cvLoadImage( filepath );

		detect_and_draw(image, cascade, storage);

		char detpath[512];
		sprintf(detpath, "%s\\%s", dir_out, files[i].c_str());
		cvSaveImage(detpath, image);

		cvReleaseImage(&image);
		cvReleaseMemStorage(&storage);
		cvReleaseHaarClassifierCascade(&cascade);
	}
}

int is_exsit_face( IplImage *img, CvHaarClassifierCascade* cascade, CvMemStorage* storage)
{
	double scale = 1.0;
	IplImage* gray = cvCreateImage( cvSize(img->width,img->height), 8, 1 );
	IplImage* small_img = cvCreateImage( cvSize( cvRound (img->width/scale),
		cvRound (img->height/scale)),
		8, 1 );

	cvCvtColor( img, gray, CV_BGR2GRAY );
	cvResize( gray, small_img, CV_INTER_LINEAR );
	cvEqualizeHist( small_img, small_img );
	cvClearMemStorage( storage );

	double t = (double)cvGetTickCount();
	CvSeq* faces = NULL;
	faces = cvHaarDetectObjects( small_img, cascade, storage, 1.1, 2, CV_HAAR_DO_CANNY_PRUNING, cvSize(8,8),cvSize(32, 32) );

	cvReleaseImage(&gray);
	cvReleaseImage(&small_img);

	if( faces )
		return faces->total;
	else
		return 0;
}


void get_path_files(string path, vector<string> &files)
{
	string dir = path+"\\";

	struct _finddata_t file;
	long hFile, hFileNext;
	string sPathLast = path + "\\*.*"; // sPathLast = "c:\test\*.*"
	hFile = _findfirst(sPathLast.c_str(), &file);

	hFileNext = _findnext(hFile, &file);

	while(_findnext(hFile, &file) == 0)
	{
		if(file.attrib == _A_ARCH)
		{
			files.push_back(dir + file.name);
		}
	}
}

void get_dir_filenames(string path, vector<string> &files)
{
	string dir = path+"\\";

	struct _finddata_t file;
	long hFile, hFileNext;
	string sPathLast = path + "\\*.*"; // sPathLast = "c:\test\*.*"
	hFile = _findfirst(sPathLast.c_str(), &file);

	hFileNext = _findnext(hFile, &file);

	while(_findnext(hFile, &file) == 0)
	{
		if(file.attrib == _A_ARCH)
		{
			files.push_back(file.name);
		}
	}
}

void updata_cascade(CvHaarClassifierCascade** cascade, float *threshold)
{
	cvReleaseHaarClassifierCascade(cascade);
	*cascade = (CvHaarClassifierCascade *)cvLoad( cascadeName.c_str() );

	for(int k = 0; k < (*cascade)->count; k++)
		(*cascade)->stage_classifier[k].threshold = threshold[k];
}

int exist_same_pnt(vector<float> &fpr, vector<float> &tpr, double fr, double tr)
{
	int i = 0;

	for(i = 0; i < fpr.size(); i++)
	{
		if( fpr[i] == fr && tpr[i] == tr )
			break;
	}

	if( i == fpr.size() )
		return 0;
	else
		return 1;
}

void detect_and_draw( IplImage* img , CvHaarClassifierCascade* cascade, CvMemStorage* storage)
{
	static CvScalar colors[] = 
	{
		{{0,0,255}},
		{{0,128,255}},
		{{0,255,255}},
		{{0,255,0}},
		{{255,128,0}},
		{{255,255,0}},
		{{255,0,0}},
		{{255,0,255}}
	};

	double scale = 1;
	IplImage* gray = cvCreateImage( cvSize(img->width,img->height), 8, 1 );
	IplImage* small_img = cvCreateImage( cvSize( cvRound (img->width/scale),
		cvRound (img->height/scale)),
		8, 1 );
	int i;

	cvCvtColor( img, gray, CV_BGR2GRAY );
	cvResize( gray, small_img, CV_INTER_LINEAR );
	cvEqualizeHist( small_img, small_img );
	cvClearMemStorage( storage );

	if( cascade )
	{
		double t = (double)cvGetTickCount();
		CvSeq* faces = cvHaarDetectObjects( small_img, cascade, storage,
			1.1, 2, 0/*CV_HAAR_DO_CANNY_PRUNING*/,
			cvSize(50, 50), cvSize(1000,1000));
		t = (double)cvGetTickCount() - t;
		printf( "detection time = %gms\n", t/((double)cvGetTickFrequency()*1000.) );
		for( i = 0; i < (faces ? faces->total : 0); i++ )
		{
			CvRect* r = (CvRect*)cvGetSeqElem( faces, i );
			CvPoint center;
			int radius;
			center.x = cvRound((r->x + r->width*0.5)*scale);
			center.y = cvRound((r->y + r->height*0.5)*scale);
			radius = cvRound((r->width + r->height)*0.25*scale);
			cvCircle( img, center, radius, colors[i%8], 3, 8, 0 );
		}
	}

	cvShowImage( "result", img );
	cvWaitKey(0);
	cvSaveImage("result.bmp",img);
	cvReleaseImage( &gray );
	cvReleaseImage( &small_img );
} 


void detect_and_draw_objects( IplImage* image, CvHaarClassifierCascade* cascade, int do_pyramids )
{
    IplImage* small_image = image;
    CvMemStorage* storage = cvCreateMemStorage(0); //创建动态内存
    CvSeq* faces;
    int i, scale = 1.33333;
    /* if the flag is specified, down-scale the 输入图像 to get a
       performance boost w/o loosing quality (perhaps) */
    if( do_pyramids )
    {
        small_image = cvCreateImage( cvSize(image->width/2,image->height/2), IPL_DEPTH_8U, 3 );
        cvPyrDown( image, small_image, CV_GAUSSIAN_5x5 );//函数 cvPyrDown 使用 Gaussian 金字塔分解对输入图像向下采样。首先它对输入图像用指定滤波器进行卷积，然后通过拒绝偶数的行与列来下采样图像。
        scale = 2;
    }
    /* use the fastest variant */
    faces = cvHaarDetectObjects( small_image, cascade, storage, 1.2, 2, CV_HAAR_DO_CANNY_PRUNING );
    /* draw all the rectangles */
    for( i = 0; i<(faces ? faces->total : 0); i++ )
    {
        /* extract the rectanlges only */
        CvRect *face_rect = (CvRect*)cvGetSeqElem( faces, i );
        cvRectangle( image, cvPoint(face_rect->x*scale,face_rect->y*scale),
                     cvPoint((face_rect->x+face_rect->width)*scale,
                             (face_rect->y+face_rect->height)*scale),
                     CV_RGB(255,0,0), 3 );
    }
    if( small_image != image )
        cvReleaseImage( &small_image );
    cvReleaseMemStorage( &storage );  //释放动态内存
}

void detectAndDraw( Mat& img, CascadeClassifier& cascade, double scale)
{
	int i = 0;
	double t = 0;
	vector<Rect> faces;
	const static Scalar colors[] =  { CV_RGB(0,0,255),
		CV_RGB(0,128,255),
		CV_RGB(0,255,255),
		CV_RGB(0,255,0),
		CV_RGB(255,128,0),
		CV_RGB(255,255,0),
		CV_RGB(255,0,0),
		CV_RGB(255,0,255)} ;//用不同的颜色表示不同的人脸

	Mat gray, smallImg( cvRound (img.rows/scale), cvRound(img.cols/scale), CV_8UC1 );//将图片缩小，加快检测速度

	cvtColor( img, gray, CV_BGR2GRAY );//因为用的是类haar特征，所以都是基于灰度图像的，这里要转换成灰度图像
	resize( gray, smallImg, smallImg.size(), 0, 0, INTER_LINEAR );//将尺寸缩小到1/scale,用线性插值
	equalizeHist( smallImg, smallImg );//直方图均衡

	t = (double)cvGetTickCount();//用来计算算法执行时间

	//检测人脸
	//detectMultiScale函数中smallImg表示的是要检测的输入图像为smallImg，faces表示检测到的人脸目标序列，1.1表示
	//每次图像尺寸减小的比例为1.1，2表示每一个候选矩形需要记录2个邻居,CV_HAAR_SCALE_IMAGE表示使用haar特征，Size(30, 30)
	//为目标的最小最大尺寸
	cascade.detectMultiScale( smallImg, faces,
		1.1, 2, 0
		//|CV_HAAR_FIND_BIGGEST_OBJECT
		//|CV_HAAR_DO_ROUGH_SEARCH
		|CV_HAAR_SCALE_IMAGE
		,
		Size(18, 13), Size(500,350) );

	t = (double)cvGetTickCount() - t;//相减为算法执行的时间
	printf( "detection time = %g ms\n", t/((double)cvGetTickFrequency()*1000.) );
	for( vector<Rect>::const_iterator r = faces.begin(); r != faces.end(); r++, i++ )
	{
		Mat smallImgROI;
		vector<Rect> nestedObjects;
		Point center;
		Scalar color = colors[i%8];
		int radius;
		center.x = cvRound((r->x + r->width*0.5)*scale);//还原成原来的大小
		center.y = cvRound((r->y + r->height*0.5)*scale);
		radius = cvRound((r->width + r->height)*0.25*scale);
		circle( img, center, radius, color, 3, 8, 0 );
		smallImgROI = smallImg(*r);
	}
	cv::imshow( "result", img );
	waitKey(0);
	imwrite("result.bmp",img);
}

int create_vec_file(int argc, _TCHAR* argv[])
{
	int i = 0;
	char* nullname   = (char*)"(NULL)";
	char* vecname    = NULL; /* .vec file name */
	char* infoname   = NULL; /* file name with marked up image descriptions */
	char* imagename  = NULL; /* single sample image */
	char* bgfilename = NULL; /* background */
	int num = 1000;
	int bgcolor = 0;
	int bgthreshold = 80;
	int invert = 0;
	int maxintensitydev = 40;
	double maxxangle = 1.1;
	double maxyangle = 1.1;
	double maxzangle = 0.5;
	int showsamples = 0;
	/* the samples are adjusted to this scale in the sample preview window */
	double scale = 4.0;
	int width  = 24;
	int height = 24;

	srand((unsigned int)time(0));

	if( argc == 1 )
	{
		printf( "Usage: %s\n  [-info <collection_file_name>]\n"
			"  [-img <image_file_name>]\n"
			"  [-vec <vec_file_name>]\n"
			"  [-bg <background_file_name>]\n  [-num <number_of_samples = %d>]\n"
			"  [-bgcolor <background_color = %d>]\n"
			"  [-inv] [-randinv] [-bgthresh <background_color_threshold = %d>]\n"
			"  [-maxidev <max_intensity_deviation = %d>]\n"
			"  [-maxxangle <max_x_rotation_angle = %f>]\n"
			"  [-maxyangle <max_y_rotation_angle = %f>]\n"
			"  [-maxzangle <max_z_rotation_angle = %f>]\n"
			"  [-show [<scale = %f>]]\n"
			"  [-w <sample_width = %d>]\n  [-h <sample_height = %d>]\n",
			argv[0], num, bgcolor, bgthreshold, maxintensitydev,
			maxxangle, maxyangle, maxzangle, scale, width, height );

		return 0;
	}

	for( i = 1; i < argc; ++i )
	{
		if( !strcmp( argv[i], "-info" ) )
		{
			infoname = argv[++i];
		}
		else if( !strcmp( argv[i], "-img" ) )
		{
			imagename = argv[++i];
		}
		else if( !strcmp( argv[i], "-vec" ) )
		{
			vecname = argv[++i];
		}
		else if( !strcmp( argv[i], "-bg" ) )
		{
			bgfilename = argv[++i];
		}
		else if( !strcmp( argv[i], "-num" ) )
		{
			num = atoi( argv[++i] );
		}
		else if( !strcmp( argv[i], "-bgcolor" ) )
		{
			bgcolor = atoi( argv[++i] );
		}
		else if( !strcmp( argv[i], "-bgthresh" ) )
		{
			bgthreshold = atoi( argv[++i] );
		}
		else if( !strcmp( argv[i], "-inv" ) )
		{
			invert = 1;
		}
		else if( !strcmp( argv[i], "-randinv" ) )
		{
			invert = CV_RANDOM_INVERT;
		}
		else if( !strcmp( argv[i], "-maxidev" ) )
		{
			maxintensitydev = atoi( argv[++i] );
		}
		else if( !strcmp( argv[i], "-maxxangle" ) )
		{
			maxxangle = atof( argv[++i] );
		}
		else if( !strcmp( argv[i], "-maxyangle" ) )
		{
			maxyangle = atof( argv[++i] );
		}
		else if( !strcmp( argv[i], "-maxzangle" ) )
		{
			maxzangle = atof( argv[++i] );
		}
		else if( !strcmp( argv[i], "-show" ) )
		{
			showsamples = 1;
			if( i+1 < argc && strlen( argv[i+1] ) > 0 && argv[i+1][0] != '-' )
			{
				double d;
				d = strtod( argv[i+1], 0 );
				if( d != -HUGE_VAL && d != HUGE_VAL && d > 0 ) scale = d;
				++i;
			}
		}
		else if( !strcmp( argv[i], "-w" ) )
		{
			width = atoi( argv[++i] );
		}
		else if( !strcmp( argv[i], "-h" ) )
		{
			height = atoi( argv[++i] );
		}
	}

	printf( "Info file name: %s\n", ((infoname == NULL) ?   nullname : infoname ) );
	printf( "Img file name: %s\n",  ((imagename == NULL) ?  nullname : imagename ) );
	printf( "Vec file name: %s\n",  ((vecname == NULL) ?    nullname : vecname ) );
	printf( "BG  file name: %s\n",  ((bgfilename == NULL) ? nullname : bgfilename ) );
	printf( "Num: %d\n", num );
	printf( "BG color: %d\n", bgcolor );
	printf( "BG threshold: %d\n", bgthreshold );
	printf( "Invert: %s\n", (invert == CV_RANDOM_INVERT) ? "RANDOM"
		: ( (invert) ? "TRUE" : "FALSE" ) );
	printf( "Max intensity deviation: %d\n", maxintensitydev );
	printf( "Max x angle: %g\n", maxxangle );
	printf( "Max y angle: %g\n", maxyangle );
	printf( "Max z angle: %g\n", maxzangle );
	printf( "Show samples: %s\n", (showsamples) ? "TRUE" : "FALSE" );
	if( showsamples )
	{
		printf( "Scale: %g\n", scale );
	}
	printf( "Width: %d\n", width );
	printf( "Height: %d\n", height );

	/* determine action */
	if( imagename && vecname )
	{
		printf( "Create training samples from single image applying distortions...\n" );

		cvCreateTrainingSamples( vecname, imagename, bgcolor, bgthreshold, bgfilename,
			num, invert, maxintensitydev,
			maxxangle, maxyangle, maxzangle,
			showsamples, width, height );

		printf( "Done\n" );
	}
	else if( imagename && bgfilename && infoname )
	{
		printf( "Create test samples from single image applying distortions...\n" );

		cvCreateTestSamples( infoname, imagename, bgcolor, bgthreshold, bgfilename, num,
			invert, maxintensitydev,
			maxxangle, maxyangle, maxzangle, showsamples, width, height );

		printf( "Done\n" );
	}
	else if( infoname && vecname )
	{
		int total;

		printf( "Create training samples from images collection...\n" );

		total = cvCreateTrainingSamplesFromInfo( infoname, vecname, num, showsamples,
			width, height );

		printf( "Done. Created %d samples\n", total );
	}
	else if( vecname )
	{
		printf( "View samples from vec file (press ESC to exit)...\n" );

		cvShowVecSamples( vecname, width, height, scale );

		printf( "Done\n" );
	}
	else
	{
		printf( "Nothing to do\n" );
	}
}

int performance( int argc, _TCHAR* argv[] )
{
	int i, j;
	char* classifierdir = NULL;
	//char* samplesdir    = NULL;

	int saveDetected = 1;
	double scale_factor = 1.2;
	float maxSizeDiff = 1.5F;
	float maxPosDiff  = 0.3F;

	/* number of stages. if <=0 all stages are used */
	int nos = -1, nos0;

	int width  = 24;
	int height = 24;

	int rocsize;

	FILE* info;
	char* infoname;
	char fullname[PATH_MAX];
	char detfilename[PATH_MAX];
	char* filename;
	char detname[] = "det-";

	CvHaarClassifierCascade* cascade;
	CvMemStorage* storage;
	CvSeq* objects;

	double totaltime;

	infoname = (char*)"";
	rocsize = 40;
	if( argc == 1 )
	{
		printf( "Usage: %s\n  -data <classifier_directory_name>\n"
			"  -info <collection_file_name>\n"
			"  [-maxSizeDiff <max_size_difference = %f>]\n"
			"  [-maxPosDiff <max_position_difference = %f>]\n"
			"  [-sf <scale_factor = %f>]\n"
			"  [-ni]\n"
			"  [-nos <number_of_stages = %d>]\n"
			"  [-rs <roc_size = %d>]\n"
			"  [-w <sample_width = %d>]\n"
			"  [-h <sample_height = %d>]\n",
			argv[0], maxSizeDiff, maxPosDiff, scale_factor, nos, rocsize,
			width, height );

		return 0;
	}

	for( i = 1; i < argc; i++ )
	{
		if( !strcmp( argv[i], "-data" ) )
		{
			classifierdir = argv[++i];
		}
		else if( !strcmp( argv[i], "-info" ) )
		{
			infoname = argv[++i];
		}
		else if( !strcmp( argv[i], "-maxSizeDiff" ) )
		{
			maxSizeDiff = (float) atof( argv[++i] );
		}
		else if( !strcmp( argv[i], "-maxPosDiff" ) )
		{
			maxPosDiff = (float) atof( argv[++i] );
		}
		else if( !strcmp( argv[i], "-sf" ) )
		{
			scale_factor = atof( argv[++i] );
		}
		else if( !strcmp( argv[i], "-ni" ) )
		{
			saveDetected = 0;
		}
		else if( !strcmp( argv[i], "-nos" ) )
		{
			nos = atoi( argv[++i] );
		}
		else if( !strcmp( argv[i], "-rs" ) )
		{
			rocsize = atoi( argv[++i] );
		}
		else if( !strcmp( argv[i], "-w" ) )
		{
			width = atoi( argv[++i] );
		}
		else if( !strcmp( argv[i], "-h" ) )
		{
			height = atoi( argv[++i] );
		}
	}

	//cascade = cvLoadHaarClassifierCascade( classifierdir, cvSize( width, height ) );
	cascade = (CvHaarClassifierCascade *)cvLoad(classifierdir);
	if( cascade == NULL )
	{
		printf( "Unable to load classifier from %s\n", classifierdir );

		return 1;
	}

	int* numclassifiers = new int[cascade->count];
	numclassifiers[0] = cascade->stage_classifier[0].count;
	for( i = 1; i < cascade->count; i++ )
	{
		numclassifiers[i] = numclassifiers[i-1] + cascade->stage_classifier[i].count;
	}

	storage = cvCreateMemStorage();

	nos0 = cascade->count;
	if( nos <= 0 )
		nos = nos0;

	strcpy( fullname, infoname );
	filename = strrchr( fullname, '\\' );
	if( filename == NULL )
	{
		filename = strrchr( fullname, '/' );
	}
	if( filename == NULL )
	{
		filename = fullname;
	}
	else
	{
		filename++;
	}

	info = fopen( infoname, "r" );
	totaltime = 0.0;
	if( info != NULL )
	{
		int x, y, width, height;
		IplImage* img;
		int hits, missed, falseAlarms;
		int totalHits, totalMissed, totalFalseAlarms;
		int found;
		float distance;

		int refcount;
		ObjectPos* ref;
		int detcount;
		ObjectPos* det;
		int error=0;

		int* pos;
		int* neg;

		pos = (int*) cvAlloc( rocsize * sizeof( *pos ) );
		neg = (int*) cvAlloc( rocsize * sizeof( *neg ) );
		for( i = 0; i < rocsize; i++ ) { pos[i] = neg[i] = 0; }

		printf( "+================================+======+======+======+\n" );
		printf( "|            File Name           | Hits |Missed| False|\n" );
		printf( "+================================+======+======+======+\n" );

		totalHits = totalMissed = totalFalseAlarms = 0;
		while( !feof( info ) )
		{
			if( fscanf( info, "%s %d", filename, &refcount ) != 2 || refcount <= 0 ) break;

			img = cvLoadImage( fullname );
			if( !img ) continue;

			ref = (ObjectPos*) cvAlloc( refcount * sizeof( *ref ) );
			for( i = 0; i < refcount; i++ )
			{
				error = (fscanf( info, "%d %d %d %d", &x, &y, &width, &height ) != 4);
				if( error ) break;
				ref[i].x = 0.5F * width  + x;
				ref[i].y = 0.5F * height + y;
				ref[i].width = sqrtf( 0.5F * (width * width + height * height) );
				ref[i].found = 0;
				ref[i].neghbors = 0;
			}
			if( !error )
			{
				cvClearMemStorage( storage );

				cascade->count = nos;
				totaltime -= time( 0 );
				objects = cvHaarDetectObjects( img, cascade, storage, scale_factor, 1 );
				totaltime += time( 0 );
				cascade->count = nos0;

				detcount = ( objects ? objects->total : 0);
				det = (detcount > 0) ?
					( (ObjectPos*)cvAlloc( detcount * sizeof( *det )) ) : NULL;
				hits = missed = falseAlarms = 0;
				for( i = 0; i < detcount; i++ )
				{
					CvAvgComp r = *((CvAvgComp*) cvGetSeqElem( objects, i ));
					det[i].x = 0.5F * r.rect.width  + r.rect.x;
					det[i].y = 0.5F * r.rect.height + r.rect.y;
					det[i].width = sqrtf( 0.5F * (r.rect.width * r.rect.width +
						r.rect.height * r.rect.height) );
					det[i].neghbors = r.neighbors;

					if( saveDetected )
					{
						cvRectangle( img, cvPoint( r.rect.x, r.rect.y ),
							cvPoint( r.rect.x + r.rect.width, r.rect.y + r.rect.height ),
							CV_RGB( 255, 0, 0 ), 3 );
					}

					found = 0;
					for( j = 0; j < refcount; j++ )
					{
						distance = sqrtf( (det[i].x - ref[j].x) * (det[i].x - ref[j].x) +
							(det[i].y - ref[j].y) * (det[i].y - ref[j].y) );
						if( (distance < ref[j].width * maxPosDiff) &&
							(det[i].width > ref[j].width / maxSizeDiff) &&
							(det[i].width < ref[j].width * maxSizeDiff) )
						{
							ref[j].found = 1;
							ref[j].neghbors = MAX( ref[j].neghbors, det[i].neghbors );
							found = 1;
						}
					}
					if( !found )
					{
						falseAlarms++;
						neg[MIN(det[i].neghbors, rocsize - 1)]++;
					}
				}
				for( j = 0; j < refcount; j++ )
				{
					if( ref[j].found )
					{
						hits++;
						pos[MIN(ref[j].neghbors, rocsize - 1)]++;
					}
					else
					{
						missed++;
					}
				}

				totalHits += hits;
				totalMissed += missed;
				totalFalseAlarms += falseAlarms;
				printf( "|%32.32s|%6d|%6d|%6d|\n", filename, hits, missed, falseAlarms );
				printf( "+--------------------------------+------+------+------+\n" );
				fflush( stdout );

				if( saveDetected )
				{
					strcpy( detfilename, detname );
					strcat( detfilename, filename );
					strcpy( filename, detfilename );
					cvvSaveImage( fullname, img );
				}

				if( det ) { cvFree( &det ); det = NULL; }
			} /* if( !error ) */

			cvReleaseImage( &img );
			cvFree( &ref );
		}
		fclose( info );

		printf( "|%32.32s|%6d|%6d|%6d|\n", "Total",
			totalHits, totalMissed, totalFalseAlarms );
		printf( "+================================+======+======+======+\n" );
		printf( "Number of stages: %d\n", nos );
		printf( "Number of weak classifiers: %d\n", numclassifiers[nos - 1] );
		printf( "Total time: %f\n", totaltime );

		/* print ROC to stdout */
		for( i = rocsize - 1; i > 0; i-- )
		{
			pos[i-1] += pos[i];
			neg[i-1] += neg[i];
		}
		fprintf( stderr, "%d\n", nos );
		for( i = 0; i < rocsize; i++ )
		{
			fprintf( stderr, "\t%d\t%d\t%f\t%f\n", pos[i], neg[i],
				((float)pos[i]) / (totalHits + totalMissed),
				((float)neg[i]) / (totalHits + totalMissed) );
		}

		cvFree( &pos );
		cvFree( &neg );
	}

	delete[] numclassifiers;

	cvReleaseHaarClassifierCascade( &cascade );
	cvReleaseMemStorage( &storage );

	return 0;
}

int train(int argc, _TCHAR* argv[])
{
	int i = 0;
	char* nullname = (char*)"(NULL)";

	char* vecname = NULL;
	char* dirname = NULL;
	char* bgname  = NULL;

	bool bg_vecfile = false;
	int npos    = 2000;
	int nneg    = 2000;
	int nstages = 14;
	int mem     = 512;
	int numprecalculated = 100;
	int nsplits = 1;
	float minhitrate     = 0.995F;
	float maxfalsealarm  = 0.5F;
	float weightfraction = 0.95F;
	int mode         = 0;
	int symmetric    = 1;
	int equalweights = 0;
	int width  = 24;
	int height = 24;
	const char* boosttypes[] = { "DAB", "RAB", "LB", "GAB" };
	int boosttype = 3;
	const char* stumperrors[] = { "misclass", "gini", "entropy" };
	int stumperror = 0;
	int maxtreesplits = 0;
	int minpos = 500;

	if( argc == 1 )
	{
		printf( "Usage: %s\n  -data <dir_name>\n"
			"  -vec <vec_file_name>\n"
			"  -bg <background_file_name>\n"
			"  [-bg-vecfile]\n"
			"  [-npos <number_of_positive_samples = %d>]\n"
			"  [-nneg <number_of_negative_samples = %d>]\n"
			"  [-nstages <number_of_stages = %d>]\n"
			"  [-nsplits <number_of_splits = %d>]\n"
			"  [-mem <memory_in_MB = %d>]\n"
			"  [-sym (default)] [-nonsym]\n"
			"  [-minhitrate <min_hit_rate = %f>]\n"
			"  [-maxfalsealarm <max_false_alarm_rate = %f>]\n"
			"  [-weighttrimming <weight_trimming = %f>]\n"
			"  [-eqw]\n"
			"  [-mode <BASIC (default) | CORE | ALL>]\n"
			"  [-w <sample_width = %d>]\n"
			"  [-h <sample_height = %d>]\n"
			"  [-bt <DAB | RAB | LB | GAB (default)>]\n"
			"  [-err <misclass (default) | gini | entropy>]\n"
			"  [-maxtreesplits <max_number_of_splits_in_tree_cascade = %d>]\n"
			"  [-minpos <min_number_of_positive_samples_per_cluster = %d>]\n",
			argv[0], npos, nneg, nstages, nsplits, mem,
			minhitrate, maxfalsealarm, weightfraction, width, height,
			maxtreesplits, minpos );

		return 0;
	}

	for( i = 1; i < argc; i++ )
	{
		if( !strcmp( argv[i], "-data" ) )
		{
			dirname = argv[++i];
		}
		else if( !strcmp( argv[i], "-vec" ) )
		{
			vecname = argv[++i];
		}
		else if( !strcmp( argv[i], "-bg" ) )
		{
			bgname = argv[++i];
		}
		else if( !strcmp( argv[i], "-bg-vecfile" ) )
		{
			bg_vecfile = true;
		}
		else if( !strcmp( argv[i], "-npos" ) )
		{
			npos = atoi( argv[++i] );
		}
		else if( !strcmp( argv[i], "-nneg" ) )
		{
			nneg = atoi( argv[++i] );
		}
		else if( !strcmp( argv[i], "-nstages" ) )
		{
			nstages = atoi( argv[++i] );
		}
		else if( !strcmp( argv[i], "-nsplits" ) )
		{
			nsplits = atoi( argv[++i] );
		}
		else if( !strcmp( argv[i], "-nprecal" ) )
		{
			numprecalculated = atoi( argv[++i] );
		}
		else if( !strcmp( argv[i], "-sym" ) )
		{
			symmetric = 1;
		}
		else if( !strcmp( argv[i], "-nonsym" ) )
		{
			symmetric = 0;
		}
		else if( !strcmp( argv[i], "-minhitrate" ) )
		{
			minhitrate = (float) atof( argv[++i] );
		}
		else if( !strcmp( argv[i], "-maxfalsealarm" ) )
		{
			maxfalsealarm = (float) atof( argv[++i] );
		}
		else if( !strcmp( argv[i], "-weighttrimming" ) )
		{
			weightfraction = (float) atof( argv[++i] );
		}
		else if( !strcmp( argv[i], "-eqw" ) )
		{
			equalweights = 1;
		}
		else if( !strcmp( argv[i], "-mode" ) )
		{
			char* tmp = argv[++i];

			if( !strcmp( tmp, "CORE" ) )
			{
				mode = 1;
			}
			else if( !strcmp( tmp, "ALL" ) )
			{
				mode = 2;
			}
			else
			{
				mode = 0;
			}
		}
		else if( !strcmp( argv[i], "-w" ) )
		{
			width = atoi( argv[++i] );
		}
		else if( !strcmp( argv[i], "-h" ) )
		{
			height = atoi( argv[++i] );
		}
		else if( !strcmp( argv[i], "-bt" ) )
		{
			i++;
			if( !strcmp( argv[i], boosttypes[0] ) )
			{
				boosttype = 0;
			}
			else if( !strcmp( argv[i], boosttypes[1] ) )
			{
				boosttype = 1;
			}
			else if( !strcmp( argv[i], boosttypes[2] ) )
			{
				boosttype = 2;
			}
			else
			{
				boosttype = 3;
			}
		}
		else if( !strcmp( argv[i], "-err" ) )
		{
			i++;
			if( !strcmp( argv[i], stumperrors[0] ) )
			{
				stumperror = 0;
			}
			else if( !strcmp( argv[i], stumperrors[1] ) )
			{
				stumperror = 1;
			}
			else
			{
				stumperror = 2;
			}
		}
		else if( !strcmp( argv[i], "-maxtreesplits" ) )
		{
			maxtreesplits = atoi( argv[++i] );
		}
		else if( !strcmp( argv[i], "-minpos" ) )
		{
			minpos = atoi( argv[++i] );
		}
	}

	printf( "Data dir name: %s\n", ((dirname == NULL) ? nullname : dirname ) );
	printf( "Vec file name: %s\n", ((vecname == NULL) ? nullname : vecname ) );
	printf( "BG  file name: %s, is a vecfile: %s\n", ((bgname == NULL) ? nullname : bgname ), bg_vecfile ? "yes" : "no" );
	printf( "Num pos: %d\n", npos );
	printf( "Num neg: %d\n", nneg );
	printf( "Num stages: %d\n", nstages );
	printf( "Num splits: %d (%s as weak classifier)\n", nsplits,
		(nsplits == 1) ? "stump" : "tree" );
	printf( "Mem: %d MB\n", mem );
	printf( "Symmetric: %s\n", (symmetric) ? "TRUE" : "FALSE" );
	printf( "Min hit rate: %f\n", minhitrate );
	printf( "Max false alarm rate: %f\n", maxfalsealarm );
	printf( "Weight trimming: %f\n", weightfraction );
	printf( "Equal weights: %s\n", (equalweights) ? "TRUE" : "FALSE" );
	printf( "Mode: %s\n", ( (mode == 0) ? "BASIC" : ( (mode == 1) ? "CORE" : "ALL") ) );
	printf( "Width: %d\n", width );
	printf( "Height: %d\n", height );
	//printf( "Max num of precalculated features: %d\n", numprecalculated );
	printf( "Applied boosting algorithm: %s\n", boosttypes[boosttype] );
	printf( "Error (valid only for Discrete and Real AdaBoost): %s\n",
		stumperrors[stumperror] );

	printf( "Max number of splits in tree cascade: %d\n", maxtreesplits );
	printf( "Min number of positive samples per cluster: %d\n", minpos );

	cvCreateCascadeClassifier( dirname, vecname, bgname,
		npos, nneg, nstages, 
		numprecalculated,
		nsplits,
		minhitrate, maxfalsealarm, weightfraction,
		mode, symmetric,
		equalweights, width, height,
		boosttype, stumperror);

	return 0;
}

int traincascade(int argc, char* argv[])
{
// 	CvCascadeClassifier classifier;
// 	String cascadeDirName, vecName, bgName;
// 	int numPos    = 2000;
// 	int numNeg    = 1000;
// 	int numStages = 20;
// 	int precalcValBufSize = 256,
// 		precalcIdxBufSize = 256;
// 	bool baseFormatSave = false;
// 
// 	CvCascadeParams cascadeParams;
// 	CvCascadeBoostParams stageParams;
// 	Ptr<CvFeatureParams> featureParams[] = { Ptr<CvFeatureParams>(new CvHaarFeatureParams),
// 		Ptr<CvFeatureParams>(new CvLBPFeatureParams),
// 		Ptr<CvFeatureParams>(new CvHOGFeatureParams)
// 	};
// 	int fc = sizeof(featureParams)/sizeof(featureParams[0]);
// 	if( argc == 1 )
// 	{
// 		cout << "Usage: " << argv[0] << endl;
// 		cout << "  -data <cascade_dir_name>" << endl;
// 		cout << "  -vec <vec_file_name>" << endl;
// 		cout << "  -bg <background_file_name>" << endl;
// 		cout << "  [-numPos <number_of_positive_samples = " << numPos << ">]" << endl;
// 		cout << "  [-numNeg <number_of_negative_samples = " << numNeg << ">]" << endl;
// 		cout << "  [-numStages <number_of_stages = " << numStages << ">]" << endl;
// 		cout << "  [-precalcValBufSize <precalculated_vals_buffer_size_in_Mb = " << precalcValBufSize << ">]" << endl;
// 		cout << "  [-precalcIdxBufSize <precalculated_idxs_buffer_size_in_Mb = " << precalcIdxBufSize << ">]" << endl;
// 		cout << "  [-baseFormatSave]" << endl;
// 		cascadeParams.printDefaults();
// 		stageParams.printDefaults();
// 		for( int fi = 0; fi < fc; fi++ )
// 			featureParams[fi]->printDefaults();
// 		return 0;
// 	}
// 
// 	for( int i = 1; i < argc; i++ )
// 	{
// 		bool set = false;
// 		if( !strcmp( argv[i], "-data" ) )
// 		{
// 			cascadeDirName = argv[++i];
// 		}
// 		else if( !strcmp( argv[i], "-vec" ) )
// 		{
// 			vecName = argv[++i];
// 		}
// 		else if( !strcmp( argv[i], "-bg" ) )
// 		{
// 			bgName = argv[++i];
// 		}
// 		else if( !strcmp( argv[i], "-numPos" ) )
// 		{
// 			numPos = atoi( argv[++i] );
// 		}
// 		else if( !strcmp( argv[i], "-numNeg" ) )
// 		{
// 			numNeg = atoi( argv[++i] );
// 		}
// 		else if( !strcmp( argv[i], "-numStages" ) )
// 		{
// 			numStages = atoi( argv[++i] );
// 		}
// 		else if( !strcmp( argv[i], "-precalcValBufSize" ) )
// 		{
// 			precalcValBufSize = atoi( argv[++i] );
// 		}
// 		else if( !strcmp( argv[i], "-precalcIdxBufSize" ) )
// 		{
// 			precalcIdxBufSize = atoi( argv[++i] );
// 		}
// 		else if( !strcmp( argv[i], "-baseFormatSave" ) )
// 		{
// 			baseFormatSave = true;
// 		}
// 		else if ( cascadeParams.scanAttr( argv[i], argv[i+1] ) ) { i++; }
// 		else if ( stageParams.scanAttr( argv[i], argv[i+1] ) ) { i++; }
// 		else if ( !set )
// 		{
// 			for( int fi = 0; fi < fc; fi++ )
// 			{
// 				set = featureParams[fi]->scanAttr(argv[i], argv[i+1]);
// 				if ( !set )
// 				{
// 					i++;
// 					break;
// 				}
// 			}
// 		}
// 	}
// 
// 	classifier.train( cascadeDirName,
// 		vecName,
// 		bgName,
// 		numPos, numNeg,
// 		precalcValBufSize, precalcIdxBufSize,
// 		numStages,
// 		cascadeParams,
// 		*featureParams[cascadeParams.featureType],
// 		stageParams,
// 		baseFormatSave );
 	return 0;
}