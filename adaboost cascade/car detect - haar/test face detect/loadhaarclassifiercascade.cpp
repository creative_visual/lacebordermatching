#include "stdafx.h"

#include "opencv2/core/core.hpp"
#include "opencv2/core/internal.hpp"
#include "_cvhaartraining.h"
#include "io.h"
//#include <objdetect/objdetect.hpp>

static CvHaarClassifierCascade*
	icvCreateHaarClassifierCascade( int stage_count )
{
	CvHaarClassifierCascade* cascade = 0;

	CV_FUNCNAME( "icvCreateHaarClassifierCascade" );

	__BEGIN__;

	int block_size = sizeof(*cascade) + stage_count*sizeof(*cascade->stage_classifier);

	if( stage_count <= 0 )
		CV_ERROR( CV_StsOutOfRange, "Number of stages should be positive" );

	CV_CALL( cascade = (CvHaarClassifierCascade*)cvAlloc( block_size ));
	memset( cascade, 0, block_size );

	cascade->stage_classifier = (CvHaarStageClassifier*)(cascade + 1);
	cascade->flags = CV_HAAR_MAGIC_VAL;
	cascade->count = stage_count;

	__END__;

	return cascade;
}

static CvHaarClassifierCascade*
	icvLoadCascadeCART( const char** input_cascade, int n, CvSize orig_window_size )
{
	int i;
	CvHaarClassifierCascade* cascade = icvCreateHaarClassifierCascade(n);
	cascade->orig_window_size = orig_window_size;

	for( i = 0; i < n; i++ )
	{
		int j, count, l;
		float threshold = 0;
		const char* stage = input_cascade[i];
		int dl = 0;

		/* tree links */
		int parent = -1;
		int next = -1;

		sscanf( stage, "%d%n", &count, &dl );
		stage += dl;

		assert( count > 0 );
		cascade->stage_classifier[i].count = count;
		cascade->stage_classifier[i].classifier =
			(CvHaarClassifier*)cvAlloc( count*sizeof(cascade->stage_classifier[i].classifier[0]));

		for( j = 0; j < count; j++ )
		{
			CvHaarClassifier* classifier = cascade->stage_classifier[i].classifier + j;
			int k, rects = 0;
			char str[100];

			sscanf( stage, "%d%n", &classifier->count, &dl );
			stage += dl;

			classifier->haar_feature = (CvHaarFeature*) cvAlloc( 
				classifier->count * ( sizeof( *classifier->haar_feature ) +
				sizeof( *classifier->threshold ) +
				sizeof( *classifier->left ) +
				sizeof( *classifier->right ) ) +
				(classifier->count + 1) * sizeof( *classifier->alpha ) );
			classifier->threshold = (float*) (classifier->haar_feature+classifier->count);
			classifier->left = (int*) (classifier->threshold + classifier->count);
			classifier->right = (int*) (classifier->left + classifier->count);
			classifier->alpha = (float*) (classifier->right + classifier->count);

			for( l = 0; l < classifier->count; l++ )
			{
				sscanf( stage, "%d%n", &rects, &dl );
				stage += dl;

				assert( rects >= 2 && rects <= CV_HAAR_FEATURE_MAX );

				for( k = 0; k < rects; k++ )
				{
					CvRect r;
					int band = 0;
					sscanf( stage, "%d%d%d%d%d%f%n",
						&r.x, &r.y, &r.width, &r.height, &band,
						&(classifier->haar_feature[l].rect[k].weight), &dl );
					stage += dl;
					classifier->haar_feature[l].rect[k].r = r;
				}
				sscanf( stage, "%s%n", str, &dl );
				stage += dl;

				classifier->haar_feature[l].tilted = strncmp( str, "tilted", 6 ) == 0;

				for( k = rects; k < CV_HAAR_FEATURE_MAX; k++ )
				{
					memset( classifier->haar_feature[l].rect + k, 0,
						sizeof(classifier->haar_feature[l].rect[k]) );
				}

				sscanf( stage, "%f%d%d%n", &(classifier->threshold[l]), 
					&(classifier->left[l]),
					&(classifier->right[l]), &dl );
				stage += dl;
			}
			for( l = 0; l <= classifier->count; l++ )
			{
				sscanf( stage, "%f%n", &(classifier->alpha[l]), &dl );
				stage += dl;
			}
		}

		sscanf( stage, "%f%n", &threshold, &dl );
		stage += dl;

		cascade->stage_classifier[i].threshold = threshold;

		/* load tree links */
		if( sscanf( stage, "%d%d%n", &parent, &next, &dl ) != 2 )
		{
			parent = i - 1;
			next = -1;
		}
		stage += dl;

		cascade->stage_classifier[i].parent = parent;
		cascade->stage_classifier[i].next = next;
		cascade->stage_classifier[i].child = -1;

		if( parent != -1 && cascade->stage_classifier[parent].child == -1 )
		{
			cascade->stage_classifier[parent].child = i;
		}
	}

	return cascade;
}

CV_IMPL CvHaarClassifierCascade*
	cvLoadHaarClassifierCascade( const char* directory, CvSize orig_window_size )
{
	const char** input_cascade = 0; 
	CvHaarClassifierCascade *cascade = 0;

	CV_FUNCNAME( "cvLoadHaarClassifierCascade" );

	__BEGIN__;

	int i, n;
	const char* slash;
	char name[_MAX_PATH];
	int size = 0;
	char* ptr = 0;

	if( !directory )
		CV_ERROR( CV_StsNullPtr, "Null path is passed" );

	n = (int)strlen(directory)-1;
	//slash = directory[n] == '\\' || directory[n] == '/' ? "" : "/";

	slash = "\\";

	/* try to read the classifier from directory */
	for( n = 0; ; n++ )
	{
		//sprintf( name, "%s%s%d/AdaBoostCARTHaarClassifier.txt", directory, slash, n );
		sprintf( name, "%s%d\\AdaBoostCARTHaarClassifier.txt", directory, n );
		FILE* f = fopen( name, "rb" );
		if( !f )
			break;
		fseek( f, 0, SEEK_END );
		size += ftell( f ) + 1;
		fclose(f);
	}

	if( n == 0 && slash[0] )
	{
		CV_CALL( cascade = (CvHaarClassifierCascade*)cvLoad( directory ));
		EXIT;
	}
	else if( n == 0 )
		CV_ERROR( CV_StsBadArg, "Invalid path" );

	size += (n+1)*sizeof(char*);
	CV_CALL( input_cascade = (const char**)cvAlloc( size ));
	ptr = (char*)(input_cascade + n + 1);

	for( i = 0; i < n; i++ )
	{
		//sprintf( name, "%s/%d/AdaBoostCARTHaarClassifier.txt", directory, i );
		sprintf( name, "%s%d\\AdaBoostCARTHaarClassifier.txt", directory, i );
		FILE* f = fopen( name, "rb" );
		if( !f )
			CV_ERROR( CV_StsError, "" );
		fseek( f, 0, SEEK_END );
		size = ftell( f );
		fseek( f, 0, SEEK_SET );
		fread( ptr, 1, size, f );
		fclose(f);
		input_cascade[i] = ptr;
		ptr += size;
		*ptr++ = '\0';
	}

	input_cascade[n] = 0;
	cascade = icvLoadCascadeCART( input_cascade, n, orig_window_size );

	__END__;

	if( input_cascade )
		cvFree( &input_cascade );

	if( cvGetErrStatus() < 0 )
		cvReleaseHaarClassifierCascade( &cascade );

	return cascade;
}
