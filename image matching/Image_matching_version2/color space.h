#ifndef __COLORSPACE_H__
#define __COLORSPACE_H__

#include <stdlib.h>
#include <stdio.h>



#define ZEROPLUS 0.00000001
#define ONEMINUS 0.99999999


void MinMax( float& min, float& max, float r, float g, float b );
void RGBtoHSV( float r, float g, float b, float *h, float *s, float *v );
void HSVtoRGB( float *r, float *g, float *b, float h, float s, float v );





#endif