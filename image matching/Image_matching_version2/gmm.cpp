#include "gmm.h"


//create a new GMM object
VlGMM *kane_gmm_new(int dim, int nCompont)
{
	int i;
	int size=4;
	VlGMM *self=(VlGMM *)calloc(1,sizeof(VlGMM));
//	self->dataType = datatype;
	self->numClusters = nCompont ;
	self->numData = 0;
	self->dimension = dim ;
//	self->initialization = VlGMMRand;
	self->verbosity = 0 ;
	self->maxNumIterations = 50;
	self->numRepetitions =1;
	self->sigmaLowBound =  NULL ;
	self->posteriors = NULL ;

	self->priors=(float *)calloc(nCompont,sizeof(float));
	self->covariances=(float *)calloc(nCompont*dim,sizeof(float));
	self->means=(float *)calloc(nCompont*dim,sizeof(float));
	self->sigmaLowBound=(double *)calloc(dim,sizeof(double));
	for (i = 0 ; i <dim ; ++i)	
		self->sigmaLowBound[i] = 1e-4;
	return self;
}






//Run GMM clustering- includes initialization and EM
double kane_gmm_cluster(VlGMM *self, float* data,int numData)
{
	int i,j,k,dim;
	double bestLL = -1e12;
	float * bestPriors = NULL ;
	float * bestMeans = NULL;
	float * bestCovariances = NULL;
	float * bestPosteriors = NULL;
//	double bestLL = -VL_INFINITY_D;
	int repetition;

	assert(self->numRepetitions >=1) ;

	bestPriors = (float*)malloc(sizeof(float) * self->numClusters);
	bestMeans = (float*)malloc(sizeof(float) * self->dimension * self->numClusters) ;
	bestCovariances = (float*)malloc(sizeof(float) * self->dimension * self->numClusters) ;
	bestPosteriors =(float*) malloc(sizeof(float) * self->numClusters * numData) ;
	
	for (repetition = 0 ; repetition < self->numRepetitions ; ++ repetition) 
	{
		double LL ;
	    

		/* initialize a new mixture model */

 		//Initializes the GMM using KMeans
// 		kane_gmm_prepare_for_data (self, numData) ;   
// 		memset(self->means,0,sizeof(float) * self->numClusters * self->dimension) ;
// 		memset(self->priors,0,sizeof(float) * self->numClusters) ;
// 		memset(self->covariances,0,sizeof(float) * self->numClusters * self->dimension) ;
// 		memset(self->posteriors,0,sizeof(float) * self->numClusters * numData) ;
// 		
// 		Mat p_des(numData,self->dimension,CV_32F,data);
// 		Mat centers,bestLabels;		
// 		kmeans(p_des, self->numClusters, bestLabels,TermCriteria( CV_TERMCRIT_EPS+CV_TERMCRIT_ITER, 10, 1.0),3, KMEANS_PP_CENTERS, centers);
// 		/* Transform the k-means assignments in posteriors and estimates the mode parameters */
// 		for (i=0;i<numData;i++)
// 			self->posteriors[bestLabels.at<int>(i)+i*self->numClusters]=1.0;
// 		/* Update cluster parameters */
// 		kane_gmm_maximization(self,self->posteriors,self->priors,self->covariances,self->means,data,numData);


		//Initializes the GMM using Rand data
		kane_gmm_prepare_for_data (self, numData) ;  
		for (i=0;i<self->numClusters;i++)
		{
			self->priors[i]=1.0 / self->numClusters;
		}
		kane_gmm_compute_init_sigma(self, data, self->covariances, self->dimension, numData);
		for (k = 1 ; k < self->numClusters ; ++ k)
		{
			for(dim = 0; dim < self->dimension; dim++) 
			{
				*(self->covariances + k * self->dimension + dim) =
					*(self->covariances + dim) ;
			}
		}
		Mat p_des1(numData,self->dimension,CV_32F,data);
		Mat centers1,bestLabels1;		
		kmeans(p_des1, self->numClusters, bestLabels1,TermCriteria( CV_TERMCRIT_EPS+CV_TERMCRIT_ITER, 10, 1.0),3, KMEANS_PP_CENTERS, centers1);
//		cout<<centers1<<endl;
		for (i=0;i<self->numClusters;i++)
		{
			for (j=0;j<self->dimension;j++)
			{
				self->means[i*self->dimension+j]=centers1.at<float>(i,j);
			}
		}



		/* fit the model to data by running EM */
		LL = kane_gmm_em (self, data, numData) ;

		if (LL > bestLL || repetition == 0) 
		{
			float * temp ;

			temp = bestPriors ;
			bestPriors = self->priors ;
			self->priors = temp ;

			temp = bestMeans ;
			bestMeans = self->means ;
			self->means = temp ;

			temp = bestCovariances ;
			bestCovariances = self->covariances ;
			self->covariances = temp ;

			temp = bestPosteriors ;
			bestPosteriors = self->posteriors ;
			self->posteriors = temp ;

			bestLL = LL;
		}
	}

	free (self->priors) ;
	free (self->means) ;
	free (self->covariances) ;
	free (self->posteriors) ;

	self->priors = bestPriors ;
	self->means = bestMeans ;
	self->covariances = bestCovariances ;
	self->posteriors = bestPosteriors ;
	self->LL = bestLL;

	

	return bestLL ;
	
}

/* ---------------------------------------------------------------- */
/*                                Random initialization of mixtures */
/* ---------------------------------------------------------------- */

void kane_gmm_compute_init_sigma(VlGMM * self,float * data,float * initSigma,int dimension,int numData)
{
	int dim;
	int i;

	float * dataMean ;

	memset(initSigma,0,sizeof(float)*dimension) ;
	if (numData <= 1) return ;

	dataMean =(float *) malloc(sizeof(float)*dimension);
	memset(dataMean,0,sizeof(float)*dimension) ;

	/* find mean of the whole dataset */
	for(dim = 0 ; dim < dimension ; dim++) 
	{
		for(i = 0 ; i < numData ; i++) 
		{
			dataMean[dim] += data[i*dimension + dim];
		}
		dataMean[dim] /= numData;
	}

	/* compute variance of the whole dataset */
	for(dim = 0; dim < dimension; dim++) 
	{
		for(i = 0; i < numData; i++) 
		{
			float diff = (data[i*self->dimension + dim] - dataMean[dim]) ;
			initSigma[dim] += diff*diff ;
		}
		initSigma[dim] /= numData - 1 ;
	}

	free(dataMean) ;
}







/* ---------------------------------------------------------------- */
/*                                                    EM iterations */
/* ---------------------------------------------------------------- */
double kane_gmm_em(VlGMM * self,float * data,int numData)
{
  int iteration, restarted ;
  double previousLL=-(1e12)  ;
  double LL=-(1e12);
  double time = 0 ;

  kane_gmm_prepare_for_data (self, numData) ;

  kane_gmm_apply_bounds(self) ;

  for (iteration = 0 ; 1 ; ++ iteration) 
  {
      double eps ;

    /*
     Expectation: assign data to Gaussian modes
     and compute log-likelihood.
     */
      LL = kane_get_gmm_data_posteriors (self->posteriors,self->numClusters,numData, self->priors, self->means, self->dimension, self->covariances,data) ;

    /*
     Check the termination conditions.
     */
      if (iteration >= self->maxNumIterations)      
	 	   break ;

      eps = fabs ((LL - previousLL) / (LL));

	  if ((iteration > 0) && (eps < 0.00001)) 
	  {
	  	  break ;
	  }

       previousLL = LL ;



    /*
     Restart empty modes.
     */
      if (iteration > 1) 
	  {
        restarted =kane_gmm_restart_empty_modes(self, data);
	//	cout<<"execute iteration.\n";
      }

    /*
      Maximization: reestimate the GMM parameters.
    */
       kane_gmm_maximization(self,self->posteriors,self->priors,self->covariances,self->means,data,numData);
  }
  return LL;
}


void  kane_weighted_mean_sse2(int dimension, float * MU, float * X, float W)
{
	float * X_end = X + dimension ;
	float * X_vec_end = X_end - 4 + 1 ;

	bool dataAligned = VALIGNED(X) & VALIGNED(MU);
	__m128 w = _mm_load1_ps(&W) ;

//	cout<<dataAligned<<endl;
	if (dataAligned) 
	{
		while (X < X_vec_end)
		{
			__m128 a = *(__m128*)X ;
			__m128 mu = *(__m128*)MU ;

			__m128 aw = _mm_mul_ps(a, w) ;
			__m128 meanStore = _mm_add_ps(aw, mu);

			*(__m128 *)MU = meanStore;

			X += 4 ;
			MU += 4 ;
		}
	}
	else 
	{
		while (X < X_vec_end)
		{
			__m128 a  = _mm_loadu_ps(X) ;
			__m128 mu = _mm_loadu_ps(MU) ;

			__m128 aw = _mm_mul_ps(a, w) ;
			__m128 meanStore = _mm_add_ps(aw, mu);

			_mm_storeu_ps(MU,meanStore);

			X += 4 ;
			MU += 4 ;
		}
	}

	while (X < X_end)
	{
		float a = *X++ ;
		*MU += a * W ;
		MU++;
	}
}

void kane_weighted_sigma_sse2(int dimension, float * S, float * X, float* Y, float W)
{
	float * X_end = X + dimension ;
	float * X_vec_end = X_end - 4 + 1 ;

	bool dataAligned = VALIGNED(X) & VALIGNED(Y) & VALIGNED(S);

	__m128 w = _mm_load1_ps (&W) ;

	if (dataAligned) 
	{
		while (X < X_vec_end) 
		{
			__m128 a = *(__m128*)X ;
			__m128 b = *(__m128*)Y ;
			__m128 s = *(__m128*)S ;

			__m128 delta = _mm_sub_ps(a, b) ;
			__m128 delta2 = _mm_mul_ps(delta, delta) ;
			__m128 delta2w = _mm_mul_ps(delta2, w) ;
			__m128 sigmaStore = _mm_add_ps(s,delta2w);

			*(__m128 *)S = sigmaStore;

			X += 4 ;
			Y += 4 ;
			S += 4 ;
		}
	} 
	else 
	{
		while (X < X_vec_end) 
		{
			__m128 a = _mm_loadu_ps(X) ;
			__m128 b = _mm_loadu_ps(Y) ;
			__m128 s = _mm_loadu_ps(S) ;

			__m128 delta = _mm_sub_ps(a, b) ;
			__m128 delta2 = _mm_mul_ps(delta, delta) ;
			__m128 delta2w = _mm_mul_ps(delta2, w) ;
			__m128 sigmaStore = _mm_add_ps(s,delta2w);

			_mm_storeu_ps(S,sigmaStore);

			X += 4 ;
			Y += 4 ;
			S += 4 ;
		}
	}


	while (X < X_end)
	{
		float a = *X++ ;
		float b = *Y++ ;
		float delta = a - b ;
		*S += ((delta * delta)*W) ;
		S++;
	}
}



void  kane_gmm_maximization(VlGMM *self, float *posteriors, float *priors,float *covariances,float *means, float *data,int numData)
{
	int numClusters=self->numClusters;
	int i_d,i_cl;
	int dim;
	float *oldmeans=(float *)malloc(sizeof(float)*self->dimension*numClusters);
	memcpy(oldmeans, means, sizeof(float) * self->dimension * numClusters) ;

	memset(priors, 0, sizeof(float) * numClusters) ;
	memset(means, 0, sizeof(float) * self->dimension * numClusters) ;
	memset(covariances, 0, sizeof(float) * self->dimension * numClusters) ;
	float * clusterPosteriorSum_, * means_, * covariances_ ;
	clusterPosteriorSum_ =(float *)calloc(sizeof(float), numClusters);
	means_ = (float *)calloc(sizeof(float), self->dimension * numClusters) ;
	covariances_ = (float *)calloc(sizeof(float), self->dimension * numClusters) ;
	
	//Accumulate weighted sums and sum of square differences. Once normalized,
	//these become the means and covariances of each Gaussian mode.
	for (i_d = 0 ; i_d <numData ; ++i_d)
	{
		for (i_cl = 0 ; i_cl <numClusters ; ++i_cl) 
		{
			float p = posteriors[i_cl + i_d * self->numClusters];
			bool calculated = false ;
			/* skip very small associations for speed */
			if (p < VL_GMM_MIN_POSTERIOR / numClusters) 
				 continue ; 
			clusterPosteriorSum_ [i_cl] += p ;


// #ifndef VL_DISABLE_SSE2
// 			{
// 	//			cout<<"SSET?\n";
// 				kane_weighted_mean_sse2(self->dimension,
// 					means_+ i_cl * self->dimension,
// 					data + i_d * self->dimension,
// 					p);
// 				kane_weighted_sigma_sse2(self->dimension,
// 					covariances_ + i_cl * self->dimension,
// 					data + i_d * self->dimension,
// 					oldmeans + i_cl * self->dimension,
// 					p);
// 				calculated=true;
// 			}
// 			
// #endif

			if(!calculated)
			{
				for (dim = 0 ; dim < self->dimension ; ++dim) 
				{
					float x = data[i_d * self->dimension + dim] ;
					float mu = oldmeans[i_cl * self->dimension + dim] ;
					float diff = x - mu ;
					means_ [i_cl * self->dimension + dim] += p * x ;
					covariances_ [i_cl * self->dimension + dim] += p * (diff*diff) ;
				}
			}
		}
	}


	for (i_cl = 0 ; i_cl <numClusters ; ++i_cl) 
	{
		priors [i_cl] += clusterPosteriorSum_ [i_cl];
		for (dim = 0 ; dim < self->dimension ; ++dim) 
		{
		//	cout<<"kk"<<means [i_cl * self->dimension + dim]<<" ";
			means [i_cl * self->dimension + dim] += means_ [i_cl * self->dimension + dim] ;
			
			covariances [i_cl * self->dimension + dim] += covariances_ [i_cl * self->dimension + dim] ;
		}
	//	cout<<endl;
	}
	free(means_);
	free(covariances_);
	free(clusterPosteriorSum_);
	for (i_cl = 0 ; i_cl < (signed)numClusters ; ++ i_cl)
	{
		float mass = priors[i_cl] ;
		/* do not update modes that do not receive mass */
		if (mass >= 1e-6 / numClusters) 
		{
			for (dim = 0 ; dim < self->dimension ; ++dim) 
			{
				means[i_cl * self->dimension + dim] /= mass ;
				covariances[i_cl * self->dimension + dim] /= mass ;
			}
		}
	}

	/* apply old to new means correction */
	for (i_cl = 0 ; i_cl < (signed)numClusters ; ++ i_cl) 
	{
		float mass = priors[i_cl] ;
		if (mass >= 1e-6 / numClusters) 
		{
			for (dim = 0 ; dim < self->dimension ; ++dim) 
			{
				float mu = means[i_cl * self->dimension + dim] ;
				float oldMu = oldmeans[i_cl * self->dimension + dim] ;
				float diff = mu - oldMu ;
				covariances[i_cl * self->dimension + dim] -= diff * diff ;
			}
		}
	}

	kane_gmm_apply_bounds(self) ;

	
	float sum = 0;
	for (i_cl = 0 ; i_cl < (signed)numClusters ; ++i_cl) 
	{
		sum += priors[i_cl] ;
	}
//	cout<<sum<<"  ";
	sum =sum>1e-8?sum:1e-8;
	for (i_cl = 0 ; i_cl <numClusters ; ++i_cl) 
	{
		priors[i_cl] /= sum ;
//		cout<<priors[i_cl]<<" ";
	}

	free(oldmeans);
}


void kane_gmm_apply_bounds(VlGMM * self)
{
	int dim ;
	int k ;
//	int numAdjusted = 0 ;
	float * cov = self->covariances ;
	double  * lbs = self->sigmaLowBound ;

	for (k = 0 ; k < self->numClusters ; ++k) 
	{
//		bool adjusted = false ;
		for (dim = 0 ; dim < self->dimension ; ++dim) 
		{
			if (cov[k * self->dimension + dim] < lbs[dim] )
			{
				cov[k * self->dimension + dim] = lbs[dim] ;
	//			adjusted = true ;
			}
		}
// 		if (adjusted) 
// 			numAdjusted ++ ; 
	}

}


void kane_gmm_prepare_for_data (VlGMM* self, int numData)
{
	if (self->numData==0)
		self->posteriors =(float *) malloc(sizeof(float) * numData * self->numClusters) ;
    else if(self->numData<numData)
	{
		free(self->posteriors) ;
		self->posteriors =(float *) malloc(sizeof(float) * numData * self->numClusters) ;
	}
	self->numData = numData ;
}



float kane_distance_mahalanobis(int dimension, float * X, float * MU, float * S)
{
	float * X_end = X + dimension ;
	float acc = 0.0 ;
	while (X < X_end)
	{
		float d = *X++ - *MU++ ;
		acc += d * d / (*S++) ;
	}
	return acc ;
}



float kane_distance_mahalanobis_sq_avx(int dimension, float * X,float * MU,float* S)
{
	float * X_end = X + dimension ;
	float * X_vec_end = X_end - 8 + 1 ;
	float acc ;
	__m256 vacc = _mm256_setzero_ps();
	bool dataAligned = VALIGNEDavx(X) & VALIGNEDavx(MU) & VALIGNEDavx(S);

	if (dataAligned)
	{
		while (X < X_vec_end) 
		{
			__m256 a = *(__m256*)X ;
			__m256 b = *(__m256*)MU ;
			__m256 c = *(__m256*)S ;

			__m256 delta = _mm256_sub_ps(a, b) ;
			__m256 delta2 = _mm256_mul_ps(delta, delta) ;
			__m256 delta2div = _mm256_mul_ps(delta2,c);

			vacc = _mm256_add_ps(vacc, delta2div) ;

			X  += 8 ;
			MU += 8 ;
			S  += 8 ;
		}
	} 
	else 
	{
		while (X < X_vec_end)
		{

			__m256 a = _mm256_loadu_ps(X) ;
			__m256 b = _mm256_loadu_ps(MU) ;
			__m256 c = _mm256_loadu_ps(S) ;

			__m256 delta = _mm256_sub_ps(a, b) ;
			__m256 delta2 = _mm256_mul_ps(delta, delta) ;
			__m256 delta2div = _mm256_mul_ps(delta2,c);

			vacc = _mm256_add_ps(vacc, delta2div) ;

			X  += 8 ;
			MU += 8 ;
			S  += 8 ;
		}
	}

	acc = kane_vhsum_avx(vacc) ;

	while (X < X_end) 
	{
		float a = *X++ ;
		float b = *MU++ ;
		float c = *S++ ;
		float delta = a - b ;
		acc += (delta * delta) * c;
	}

	return acc ;
}







float kane_distance_mahalanobis_sq_sse2(int dimension, float * X, float * MU, float * S)
{
	float * X_end = X + dimension ;
	float * X_vec_end = X_end - 4 + 1 ;
	float acc ;
	__m128 vacc =_mm_setzero_ps();
	bool dataAligned = VALIGNED(X) & VALIGNED(MU) & VALIGNED(S);

	if (dataAligned)
	{
		while (X < X_vec_end)
		{
			__m128 a = *(__m128*)X ;
			__m128 b = *(__m128*)MU ;
			__m128 c = *(__m128*)S ;

			__m128 delta = _mm_sub_ps(a,b) ;
			__m128 delta2 =_mm_mul_ps(delta,delta );
			__m128 delta2div = _mm_mul_ps(delta2,c);

			vacc = _mm_add_ps(vacc,delta2div );

			X  += 4 ;
			MU += 4 ;
			S  += 4 ;
		}
	} 
	else 
	{
		while (X < X_vec_end) 
		{

			__m128 a = _mm_loadu_ps(X) ;
			__m128 b = _mm_loadu_ps(MU) ;
			__m128 c = _mm_loadu_ps(S) ;

			__m128 delta = _mm_sub_ps(a,b) ;
			__m128 delta2 =_mm_mul_ps(delta,delta );
			__m128 delta2div = _mm_mul_ps(delta2,c);

			vacc = _mm_add_ps(vacc,delta2div );

			X  += 4 ;
			MU += 4 ;
			S  += 4 ;
		}
	}

	acc = kane_vhsum_sse2(vacc) ;

	while (X < X_end)
	{
		float a = *X++ ;
		float b = *MU++ ;
		float c = *S++ ;
		float delta = a - b ;
		acc += (delta * delta) * c;
	}

	return acc ;
}


float kane_vhsum_avx(__m256 x)
{
	float acc ;

	//VTYPEavx hsum = _mm256_hadd_ps(x, x);
	//hsum = _mm256_hadd_ps(hsum, _mm256_permute2f128_ps(hsum, hsum, 0x1));
	//_mm_store_ss(&acc, _mm_hadd_ps( _mm256_castps256_ps128(hsum), _mm256_castps256_ps128(hsum) ) );
	__m256 hsum = _mm256_hadd_ps(x, x);
	hsum = _mm256_hadd_ps(hsum, _mm256_permute2f128_ps(hsum, hsum, 0x1));
	_mm_store_ss(&acc, _mm_hadd_ps( _mm256_castps256_ps128(hsum), _mm256_castps256_ps128(hsum) ) );


	return acc ;
}



float kane_vhsum_sse2(__m128 x)
{
	float acc ;

	__m128 sum ;
	__m128 shuffle ;
	/* shuffle = [1 0 3 2] */
	/* sum     = [3+1 2+0 1+3 0+2] */
	/* shuffle = [2+0 3+1 0+2 1+3] */
	/* vacc    = [3+1+2+0 3+1+2+0 1+3+0+2 0+2+1+3] */
	shuffle = _mm_shuffle_ps(x, x, _MM_SHUFFLE(1, 0, 3, 2)) ;
	sum     = _mm_add_ps (x, shuffle) ;
	shuffle = _mm_shuffle_ps (sum, sum, _MM_SHUFFLE(2, 3, 0, 1)) ;
	x       = _mm_add_ps (sum, shuffle) ;
	_mm_store_ss(&acc, x);
	return acc ;
}

// float kane_distance_mahalanobis_3(int dimension, float * X, float * MU, float * S)
// {
// 	float val;
// 	val=kane_distance_mahalanobis(dimension,X,MU,S);
// #ifndef VL_DISABLE_SSE2
// 	val=kane_distance_mahalanobis_sq_sse2(dimension,X,MU,S);
// #endif
// 
// #ifndef VL_DISABLE_AVX
// 	val=kane_distance_mahalanobis_sq_avx(dimension,X,MU,S);
// #endif
// 	return val;
// 
// 
// }



// Get Gaussian modes posterior probabilities
double kane_get_gmm_data_posteriors(float * posteriors,int numClusters,int numData,float  * priors,float  * means,int dimension,float  * covariances,float  * data)
{
	int i_d, i_cl;
	int dim;
	double LL = 0;

	float halfDimLog2Pi = (dimension / 2.0) * log(2.0*VL_PI);
	float * logCovariances=(float *)malloc(sizeof(float) * numClusters) ;
	float * logWeights=(float *)malloc(sizeof(float) * numClusters) ;
	float * invCovariances =(float *)malloc(sizeof(float) * numClusters* dimension);

	for (i_cl = 0 ; i_cl < numClusters ; ++ i_cl)
	{
		float logSigma = 0 ;
		if (priors[i_cl] < VL_GMM_MIN_PRIOR)
			logWeights[i_cl] = - 1e12 ;
		 else 
			logWeights[i_cl] = log(priors[i_cl]);
		for(dim = 0 ; dim < dimension ; ++ dim)
		{
			logSigma += log(covariances[i_cl*dimension + dim]);
			invCovariances [i_cl*dimension + dim] = (float) 1.0 /covariances[i_cl*dimension + dim];
		}
		logCovariances[i_cl] = logSigma;
	} /* end of parallel region */

	for (i_d = 0 ; i_d < numData ; ++ i_d) 
	{
		float clusterPosteriorsSum = 0;
		float maxPosterior = -1e12;

		for (i_cl = 0 ; i_cl < numClusters ; ++ i_cl)
		{
			float p =logWeights[i_cl]- halfDimLog2Pi
				- 0.5 * logCovariances[i_cl]- 0.5 * 
				kane_distance_mahalanobis_sq_sse2 (dimension,data + i_d * dimension,means + i_cl * dimension,invCovariances + i_cl * dimension) ;
			posteriors[i_cl + i_d * numClusters] = p ;
			if (p > maxPosterior) 
				 maxPosterior = p ; 
		}

		for (i_cl = 0 ; i_cl < numClusters ; ++i_cl) 
		{
			float p = posteriors[i_cl + i_d * numClusters] ;
			p =  exp(p - maxPosterior) ;
			posteriors[i_cl + i_d * numClusters] = p ;
			clusterPosteriorsSum += p ;
		}

		LL +=  log(clusterPosteriorsSum) + (double) maxPosterior ;

		for (i_cl = 0 ; i_cl < numClusters ; ++i_cl) 
			posteriors[i_cl + i_d * numClusters] /= clusterPosteriorsSum ;
	} 

	free(logCovariances);
	free(logWeights);
	free(invCovariances);

	return LL;
}



/*    Restarts zero-weighted Gaussians */
int kane_gmm_restart_empty_modes(VlGMM * self, float * data)
{
  int dimension = self->dimension;
  int numClusters = self->numClusters;
  int i_cl, j_cl, i_d, d;
  int zeroWNum = 0;
  float * priors = self->priors ;
  float * means = self->means ;
  float * covariances = self->covariances ;
  float * posteriors = self->posteriors ;

  //VlRand * rand = vl_get_rand() ;

  float * mass = (float *)calloc(sizeof(float), self->numClusters) ;

  if (numClusters <= 1) 
		return 0 ;

  /* compute statistics */
 
  int i, k ;
  int numNullAssignments = 0 ;
  for (i = 0 ; i < self->numData ; ++i) 
  {
	  for (k = 0 ; k < self->numClusters ; ++k) 
	  {
		  float p = (self->posteriors)[k + i * self->numClusters] ;
		  mass[k] += p ;
		  if (p < VL_GMM_MIN_POSTERIOR) 
			  numNullAssignments ++ ;
		  
	  }
  }
 
#if 0
  /* search for cluster with negligible weight and reassign them to fat clusters */
  for (i_cl = 0 ; i_cl < numClusters ; ++i_cl) 
  {
    if (priors[i_cl] < 0.00001/numClusters) 
	{
      double mass = priors[0]  ;
      int best = 0 ;

      for (j_cl = 1 ; j_cl < numClusters ; ++j_cl) 
	  {
        if (priors[j_cl] > mass) 
		{ mass = priors[j_cl] ; best = j_cl ; }
      }

      if (j_cl == i_cl) 
	  {
        /* this should never happen */
        continue ;
      }

      j_cl = best ;
      zeroWNum ++ ;


      priors[i_cl] = mass/2 ;
      priors[j_cl] = mass/2 ;
      for (d = 0 ; d < dimension ; ++d)
	  {
        float sigma2 =  covariances[j_cl*dimension + d] ;
        float sigma =sqrt(sigma2) ;
        means[i_cl*dimension + d] = means[j_cl*dimension + d] + 0.001 * (vl_rand_real1(rand) - 0.5) * sigma ;
        covariances[i_cl*dimension + d] = sigma2 ;
      }
    }
  }
#endif

  /* search for cluster with negligible weight and reassign them to fat clusters */
  for (i_cl = 0 ; i_cl < (signed)numClusters ; ++i_cl) 
  {
    double size = - 1e12 ;
    int best = -1 ;


	double k_t1=(double) self->numData / self->numClusters;
	double k_tmp=1.0>k_t1?1.0:k_t1;
    if (mass[i_cl] >= VL_GMM_MIN_POSTERIOR *k_tmp)
    {
      continue ;
    }

    /*
     Search for the cluster that (approximately)
     maximally contribute to make the log-likelihood
     small.
     */

    for (j_cl = 0 ; j_cl < (signed)numClusters ; ++j_cl) 
	{
      double size_ ;
      if (priors[j_cl] < VL_GMM_MIN_PRIOR)
		 continue ;		
      size_ = - 0.5 * (1.0 + log(2*VL_PI)) ;
      for(d = 0 ; d < (signed)dimension ; d++) 
	  {
        double sigma2 = covariances[j_cl * dimension + d] ;
        size_ -= 0.5 * log(sigma2) ;
      }
      size_ *= priors[j_cl] ;

      if (size_ > size) 
	  {
        size = size_ ;
        best = j_cl ;
      }
    }

    j_cl = best ;

    if (j_cl == i_cl || j_cl < 0) 
      continue ;
  

   

    /*
     Search for the dimension with maximum variance.
     */

    size = - 1e12 ;
    best = - 1 ;

    for(d = 0; d < (signed)dimension; d++) 
	{
      double sigma2 = covariances[j_cl * dimension + d] ;
      if (sigma2 > size)
	  {
        size = sigma2 ;
        best = d ;
      }
    }

    /*
     Reassign points j_cl (mode to split) to i_cl (empty mode).
     */
      float mu = means[best + j_cl * self->dimension] ;
      for(i_d = 0 ; i_d < (signed)self->numData ; ++ i_d)
	  {
        float p = posteriors[j_cl + self->numClusters * i_d] ;
        float q = posteriors[i_cl + self->numClusters * i_d] ; /* ~= 0 */
        if (data[best + i_d * self->dimension] < mu)
		{
          /* assign this point to i_cl */
          posteriors[i_cl + self->numClusters * i_d] += p ;
          posteriors[j_cl + self->numClusters * i_d] = 0 ;
        } 
		else 
		{
          /* assign this point to j_cl */
          posteriors[i_cl + self->numClusters * i_d] = 0 ;
          posteriors[j_cl + self->numClusters * i_d] += q ;
        }
      }


    /*
     Re-estimate.
     */
    kane_gmm_maximization(self,posteriors,priors,covariances,means,data,self->numData) ;
  }

  return zeroWNum;
}
