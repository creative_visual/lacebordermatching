#include "Bow.h"

bool IsFiniteNumber(double x) 
{
	return (x <= DBL_MAX && x >= -DBL_MAX); 
}    


void kane_normalize(Mat src,Mat& out)
{
	int i,j;
    out=Mat::zeros(src.rows,src.cols,CV_32F);
	float val_sum=0.0;
	for (i=0;i<src.rows;i++)
	{
		for (j=0;j<src.cols;j++)
		{
			val_sum+=pow(src.at<float>(i,j),2);
		}
	}
	
	val_sum=sqrt(val_sum);
//	cout<<val_sum<<"  ";
	for (i=0;i<src.rows;i++)
	{
		for (j=0;j<src.cols;j++)
		{
			double cur;
			if (src.at<float>(i,j)<-0.000001)
			{
				cur=abs(src.at<float>(i,j))/val_sum;
				cur=-1.0*cur;
			}
			else
				cur=abs(src.at<float>(i,j))/val_sum;
			out.at<float>(i,j)=cur;
//			cout<<cur<<"  ";
//			cout<<cur<<"  ";
// 			if(IsFiniteNumber(cur))
// 				out.at<float>(i,j)=cur;
// 			else
// 				out.at<float>(i,j)=0.00000001;
		}
	}
}


Mat kane_normalize_divmax(Mat src)
{
	int i,j;
	Mat out=Mat::zeros(src.rows,src.cols,CV_32F);
	float max_val=0.0000001;
	for (i=0;i<src.rows;i++)
	{
		for (j=0;j<src.cols;j++)
		{
			if (abs(src.at<float>(i,j))>max_val)
				max_val=abs(src.at<float>(i,j));
		}
	}

	for (i=0;i<src.rows;i++)
	{
		for (j=0;j<src.cols;j++)
		{
			double cur;
			if (src.at<float>(i,j)<-0.000001)
				cur=-1.0*(abs(src.at<float>(i,j))/max_val);
			else
				cur=abs(src.at<float>(i,j))/max_val;
			if(IsFiniteNumber(cur))
				out.at<float>(i,j)=cur;
			else
				out.at<float>(i,j)=0.00000001;
		}
	}
	return out;
}


void save_vlad_vi(Mat in,char *file)
{
	FILE *fp_w;
	fp_w = fopen(file,"w");
	int i,j;
	for (i=0;i<in.rows;i++)
	{
		for (j=0;j<in.cols;j++)
			fprintf(fp_w,"%f ",in.at<float>(i,j));

		fprintf(fp_w,"\n");  
	}
	
	fclose(fp_w);
}


void Bow(Mat& HSDecription_grp, int Dim, int WordsNumber, float *Words,float *Norm_Hist,int flag_index)
{
	int i,j,k,l;
	int FeatureNumber = HSDecription_grp.rows;
	Mat VI=Mat::zeros(WordsNumber,Dim,CV_32FC1);
	Mat QueryDesc(WordsNumber,Dim,CV_32FC1,Words);
	 Ptr<DescriptorMatcher> matcher=DescriptorMatcher::create("BruteForce");
//	FlannBasedMatcher matcher;
	vector<DMatch> matches;
	matches.clear();
	double sign;
	matcher->match(HSDecription_grp,QueryDesc,matches );
// 	char VI_FILE[216];
// 	sprintf(VI_FILE,"F:\\(1)VI_TEST\\%d_11111.txt",flag_index);
// 	ofstream fkkk;
// 	fkkk.open(VI_FILE);
	for (j = 0; j < FeatureNumber; j ++)
	{
// 		if (flag_index==3||flag_index==2)
// 		{
// 			fkkk<<"row["<<matches[j].trainIdx<<"]:  ";
// 	//		fkkk<<HSDecription_grp.row(j)-QueryDesc.row(matches[j].trainIdx)<<" ";
// 		}
		
		VI.row(matches[j].trainIdx)+=HSDecription_grp.row(j)-QueryDesc.row(matches[j].trainIdx);
//		fkkk<<VI.row(matches[j].trainIdx)<<endl;
	}
//	fkkk.close();

/*	cout<<endl;*/
//	save_vlad_vi(VI,VI_FILE);
	for (i = 0; i < WordsNumber;i ++)
	{
		for (j=0;j<Dim;j++)
		{
			if (VI.at<float>(i,j)<-0.0000001)
			{
				sign=-1.0;
				VI.at<float>(i,j)=sign*sqrt(abs(VI.at<float>(i,j)));

			}
			else
			{
				VI.at<float>(i,j)=sqrt(abs(VI.at<float>(i,j)));
			}

		}
	}
//	VI.reshape(0,WordsNumber*Dim);
 	normalize(VI,VI,1.0,0,NORM_L2);
	for(i=0;i<VI.rows;i++)
	{
		for(j=0;j<VI.cols;j++)
		{
			Norm_Hist[i*VI.cols+j]=VI.at<float>(i,j);

		}
	}
}
