


#define  maxPrimeFactor        5987
#define  maxPrimeFactorDiv2    (maxPrimeFactor+1)/2
#define  maxFactorCount        20

static float  c3_1 = -1.5000000000000E+00;  /*  c3_1 = cos(2*pi/3)-1;          */
static float  c3_2 =  8.6602540378444E-01;  /*  c3_2 = sin(2*pi/3);            */
                                          
static float  u5   =  1.2566370614359E+00;  /*  u5   = 2*pi/5;                 */
static float  c5_1 = -1.2500000000000E+00;  /*  c5_1 = (cos(u5)+cos(2*u5))/2-1;*/
static float  c5_2 =  5.5901699437495E-01;  /*  c5_2 = (cos(u5)-cos(2*u5))/2;  */
static float  c5_3 = -9.5105651629515E-01;  /*  c5_3 = -sin(u5);               */
static float  c5_4 = -1.5388417685876E+00;  /*  c5_4 = -(sin(u5)+sin(2*u5));   */
static float  c5_5 =  3.6327126400268E-01;  /*  c5_5 = (sin(u5)-sin(2*u5));    */
static float  c8   =  7.0710678118655E-01;  /*  c8 = 1/sqrt(2);    */

void factorize(int n, int *nFact, int fact[]);

void transTableSetup(int sofar[], int actual[], int remain[],int *nFact,int *nPoints);

void permute(int nPoint, int nFact,int fact[], int remain[],float xRe[], float xIm[],float yRe[], float yIm[]);

void initTrig(int radix);

void fft_4(float aRe[], float aIm[]);

void fft_5(float aRe[], float aIm[]);

void fft_8();

void fft_10();

void fft_odd(int radix);

void twiddleTransf(int sofarRadix, int radix, int remainRadix,float yRe[], float yIm[]);

void fft(int n, float xRe[], float xIm[],float yRe[], float yIm[]);

int fft_back(float *input_data_real,float *input_data_img,float *output_data_real,float *output_data_img,int data_row, int data_col);

int fft_forward(float *input_data_real,float *input_data_img,float *output_data_real,float *output_data_img,int data_row, int data_col);

int fft2_oper(float *input_real, float *input_img, float *output_real, float *output_img, int row, int col, int fft_row, int fft_col);




