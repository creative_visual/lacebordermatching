#include "fisher.h"

int kane_fisher_encode(float * enc,float * means, int dimension, int numClusters,float* covariances,float* priors,float* data, int numData,int flags)
{
	int dim;
	int i_cl, i_d;
	int numTerms = 0 ;
	float * posteriors ;
	float * sqrtInvSigma;

	assert(numClusters >= 1) ;
	assert(dimension >= 1) ;

	posteriors = (float *)malloc(sizeof(float) * numClusters * numData);
	sqrtInvSigma =(float *)malloc(sizeof(float) * dimension * numClusters);

	memset(enc, 0, sizeof(float) * 2 * dimension * numClusters) ;

	for (i_cl = 0 ; i_cl <numClusters ; ++i_cl)
	{
		for(dim = 0; dim < dimension; dim++) 
		{
	//		cout<<covariances[i_cl*dimension + dim]<<"  ";
			sqrtInvSigma[i_cl*dimension + dim] = sqrt(1.0 / covariances[i_cl*dimension + dim]);
		}
	}

	kane_get_gmm_data_posteriors(posteriors, numClusters, numData,priors,means, dimension,covariances,data) ;

	/* sparsify posterior assignments with the FAST option */
	if (flags & VL_FISHER_FLAG_FAST) 
	{
		for(i_d = 0; i_d < numData; i_d++)
		{
			/* find largest posterior assignment for datum i_d */
			int best = 0 ;
			float bestValue = posteriors[i_d * numClusters] ;
			for (i_cl = 1 ; i_cl < numClusters; ++ i_cl)
			{
				float p = posteriors[i_cl + i_d * numClusters] ;
				if (p > bestValue) 
				{
					bestValue = p ;
					best = i_cl ;
				}
			}
			/* make all posterior assignments zero but the best one */
			for (i_cl = 0 ; i_cl < numClusters; ++ i_cl) 
			{
				posteriors[i_cl + i_d * numClusters] =(float)(i_cl == best) ;
			}
		}
	}

	for(i_cl = 0; i_cl <numClusters; ++ i_cl) 
	{
		float uprefix;
		float vprefix;

		float * uk = enc + i_cl*dimension ;
		float * vk = enc + i_cl*dimension + numClusters * dimension ;

		for(i_d = 0; i_d <numData; i_d++) 
		{
			float p = posteriors[i_cl + i_d * numClusters] ;
			if (p < 1e-6) continue ;
			numTerms += 1;
			for(dim = 0; dim < dimension; dim++) 
			{
				float diff = data[i_d*dimension + dim] - means[i_cl*dimension + dim] ;
				diff *= sqrtInvSigma[i_cl*dimension + dim] ;
				*(uk + dim) += p * diff ;
				*(vk + dim) += p * (diff * diff - 1);
			}
		}

		//liuzcf modify  version1:
// 		float p_t=priors[i_cl]>1e-8?priors[i_cl]:1e-8;
// 		uprefix = 1/(numData*sqrt(p_t));
// 		vprefix = 1/(numData*sqrt(2*p_t));


		//liuzcf modify  version2:

		float p_t=priors[i_cl]>1e-8?priors[i_cl]:1e-8;
		if (priors[i_cl]>1e-8)
		{
			uprefix = 1/(numData*sqrt(priors[i_cl]));
			vprefix = 1/(numData*sqrt(2*priors[i_cl]));
		}
		else
		{
			uprefix = 1e-8;
			vprefix = 1e-8;
		}
		
	//	cout<<uprefix<<"  ";

		for(dim = 0; dim < dimension; dim++) 
		{
			*(uk + dim) = *(uk + dim) * uprefix;
			*(vk + dim) = *(vk + dim) * vprefix;
		}
	}

	free(posteriors);
	free(sqrtInvSigma) ;

	if (flags & VL_FISHER_FLAG_SQUARE_ROOT) 
	{
		for(dim = 0; dim < 2 * dimension * numClusters ; dim++)
		{
			float z = enc [dim] ;

			//liuzcf  modify
			if (z >= 1e-12) 
				enc[dim] = sqrt(z) ;

			else 
				enc[dim] = - sqrt(abs(z)) ;
		}
	}

	if (flags & VL_FISHER_FLAG_NORMALIZED)
	{
		float n = 0 ;
		for(dim = 0 ; dim < 2 * dimension * numClusters ; dim++)
		{
			float z = enc [dim] ;
			n += z * z ;
		}
		n = sqrt(n);
		n = n>1e-12?n:1e12 ;
		for(dim = 0 ; dim < 2 * dimension * numClusters ; dim++) 
		{
			enc[dim] /= n ;
		}
	}

	return numTerms ;
}