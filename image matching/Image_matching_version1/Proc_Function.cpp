#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <ctime>
#include <iostream>
#include <time.h>
#include <queue>
#include "surflib.h"
//#include "kmeans.h"
#include "standalone_image.h"
#include "Proc_Function.h"
#include "gist.h"
#include "windows.h"

#include <iostream>
#include <fstream>
#include <shlwapi.h>

#include "hesaff.h"
#include "Bow.h"
#include "opencv2/core/core.hpp"//因为在属性中已经配置了opencv等目录，所以把其当成了本地目录一样
#include "opencv2/features2d/features2d.hpp"

#include "opencv2/highgui/highgui.hpp"  
#include "opencv2/imgproc/imgproc.hpp"  
#include "opencv2/nonfree/nonfree.hpp"  
#include "opencv2/nonfree/features2d.hpp"  
using namespace cv;
using namespace std;


/*********************************************
	The following part is for 'Hessian AND Bow Feature'
**********************************************/
void ExtractOneImageHSFeatures_reverse(IplImage *Inputimage,vector <HEDescrip>& HSDecription_grp)
{
	int i,j;
	int x=Inputimage->width/4;
	int y=Inputimage->height/4;

	int H=Inputimage->height/2;
	int W=Inputimage->width/2;
	CvSize size=cvSize(W,H);
	cvSetImageROI(Inputimage,cvRect(x,y,W,H));
	IplImage *sub_img=cvCreateImage(size,Inputimage->depth,Inputimage->nChannels);
	cvCopy(Inputimage,sub_img);

	IplImage *resizeimg = cvCreateImage(cvSize(sub_img->width/1,sub_img->height/1),IPL_DEPTH_8U,sub_img->nChannels);
	cvResize(Inputimage,resizeimg);
	IplImage *pequalize = cvCreateImage(cvSize(sub_img->width/1,sub_img->height/1),IPL_DEPTH_8U,sub_img->nChannels);
	IplImage *pImageChannel[3];
	for(i=0;i<3;i++)
		pImageChannel[i]=cvCreateImage(cvGetSize(resizeimg),IPL_DEPTH_8U,1);
	cvSplit(resizeimg,pImageChannel[0],pImageChannel[1],pImageChannel[2],NULL);
	for (i=0;i<3;i++)
		cvEqualizeHist(pImageChannel[i],pImageChannel[i]);
	cvMerge(pImageChannel[0],pImageChannel[1],pImageChannel[2],NULL,pequalize);
	for (i=0;i<3;i++)
		cvReleaseImage(&pImageChannel[i]);

	BYTE *ptr = (BYTE *)pequalize->imageData;
	Mat image(pequalize->height, pequalize->width, CV_32FC1, Scalar(0));

	float *out = image.ptr<float>(0);
	for (i = 0; i < pequalize->height; i ++)
	{
		for (j = 0; j < pequalize->width; j ++)
		{
			// 			*out = ((float)ptr[i*pequalize->widthStep+j*sub_img->nChannels]
			// 			+ (float)ptr[i*pequalize->widthStep+j*sub_img->nChannels+1]
			// 			+ (float)ptr[i*pequalize->widthStep+j*sub_img->nChannels+2])/3.0f;
			*out = 255-(float)ptr[i*pequalize->widthStep+j*sub_img->nChannels]*0.114
				+ (float)ptr[i*pequalize->widthStep+j*sub_img->nChannels+1]*0.587
				+ (float)ptr[i*pequalize->widthStep+j*sub_img->nChannels+2]*0.299;
			out++;
		}
	}

	HessianAffineParams par;
	double t1 = 0;
	{
		// copy params 
		PyramidParams p;
		p.threshold = par.threshold;

		AffineShapeParams ap;
		ap.maxIterations = par.max_iter;
		ap.patchSize = par.patch_size;
		ap.mrSize = par.desc_factor;

		SIFTDescriptorParams sp;
		sp.patchSize = par.patch_size;

		//Extract Affine Hessian Description
		AffineHessianDetector detector(image, p, ap, sp);
		detector.detectPyramidKeypoints(image);
		// 		if (detector.keys.size()>0)
		// 		{
		// 			cout<<"feat size:"<<detector.keys.size();
		// 			Mat in_img(sub_img,0);
		// 			Mat showImg;
		// 			drawHSfeat(in_img,detector.keys,showImg,Scalar::all(-1));
		// 			
		// 		}


		HSDecription_grp.clear();
		detector.exportKeypoints(HSDecription_grp);

		image.release();
	}

	cvReleaseImage(&resizeimg);
	cvReleaseImage(&pequalize);
	cvReleaseImage(&sub_img);

}

void FindAllFiles(char *image_dir,char *image_name)
{
	string dir(image_dir);  
	FILE * file;
	file=fopen(image_name,"w");
	if (_access(dir.c_str(), 06) == -1)  
	{  
		cerr << "error: directory does not exist." << endl;  
		exit(-1);  
	}  
	if (dir.at(dir.length() - 1) != '\\')  
	{  
		dir += '\\';  
	}  
	if (_chdir(dir.c_str()) != 0)  
	{  
		cerr << "error: function _chdir() failed.";  
		exit(-1);  
	}  
	_finddata_t fileinfo;  
	memset(&fileinfo, 0x0, sizeof(fileinfo));     
	intptr_t iFind = _findfirst("*", &fileinfo);  
	if (iFind == -1)  
	{  
		cerr << "error: function _findfirst failed." << endl;  
		exit(-1);  
	}  
	_findnext(iFind, &fileinfo);//忽略第一条输出，因为第一条是目录
	while (_findnext(iFind, &fileinfo) == 0)  
	{  
		fprintf(file,"%s\n",fileinfo.name);	
	}
	fprintf(file,"overkane");	
	_findclose(iFind); 
	fclose(file);

}

void CreateNewDir(char* path)
{
	if (_access(path,00)==-1)
	{
		_mkdir(path);
	}

}


float *ReadintoWords(char WordsFilename[128], int &Dim, int &WordsNumber)
{
	int i,j;
	FILE *fp_r;
	float *Words;
	fp_r = fopen(WordsFilename,"r");
	
	if (fp_r < 0)
	{
		printf("Cannot find Word Dictionary\n");
		return NULL;
	}
	fscanf(fp_r,"%d",&WordsNumber);
	fscanf(fp_r,"%d",&Dim);
	if (Dim != HS_DIM)
	{
		printf("The Dictionary Word Dimension is NOT 128\n");
		fclose(fp_r);
		return NULL;
	}
	
	//Read into Words
	Words = (float *)calloc(WordsNumber*Dim,sizeof(float));
	for (i = 0; i < WordsNumber; i ++)
	{
		for (j = 0; j < Dim; j ++)
		{
			fscanf(fp_r,"%f",&Words[i*Dim+j]);
		}
	}
	fclose(fp_r);

	return Words;
}

void drawHSfeat(const Mat&image,vector<Keypoint>keypoints,Mat &outImage,const Scalar& _color)
{
	const int draw_shift_bits = 4;
	const int draw_multiplier = 1 << draw_shift_bits;
	RNG& rng=theRNG();
	bool isRandColor = _color == Scalar::all(-1);
	image.copyTo( outImage );

	for(int i=0;i<keypoints.size();i++)
	{
		Scalar color = isRandColor ? Scalar(rng(256), rng(256), rng(256)) : _color;
		Point center( cvRound(keypoints[i].x * draw_multiplier), cvRound(keypoints[i].y * draw_multiplier) );
		//	int radius = 20; // KeyPoint::size is a diameter
		int radius=keypoints[i].s*draw_multiplier;

		// draw the circles around keypoints with the keypoints size
		circle( outImage, center, radius, color, 1, CV_AA, draw_shift_bits );
	}
	imshow("kane",outImage);
	waitKey(0);
}


/*void ExtractOneImageHSFeatures(IplImage *Inputimage,vector <HEDescrip>& HSDecription_grp)
{
	int i,j;
	int x=cvRound(Inputimage->width*0.1);
	int y=cvRound(Inputimage->height*0.15);

	int H=cvRound(Inputimage->height*0.7);
	int W=cvRound(Inputimage->width*0.8);
	CvSize size=cvSize(W,H);
	cvSetImageROI(Inputimage,cvRect(x,y,W,H));
	IplImage *sub_img=cvCreateImage(size,Inputimage->depth,Inputimage->nChannels);
	cvCopy(Inputimage,sub_img);

	IplImage *resizeimg = cvCreateImage(cvSize(sub_img->width/1,sub_img->height/1),IPL_DEPTH_8U,sub_img->nChannels);
	cvResize(sub_img,resizeimg);
	IplImage *gray=cvCreateImage(cvGetSize(resizeimg),resizeimg->depth,1);
	cvCvtColor(resizeimg,gray,CV_BGR2GRAY);
	IplImage *dst_x=cvCreateImage(cvGetSize(resizeimg),resizeimg->depth,1);
	IplImage *dst_y=cvCreateImage(cvGetSize(resizeimg),resizeimg->depth,1);
	IplImage *dst=cvCreateImage(cvGetSize(resizeimg),resizeimg->depth,1);
	cvSobel(gray,dst_x,1,0,3);
	cvSobel(gray,dst_y,0,1,3);
	cvCvtScaleAbs(dst_x,dst_x);
	cvCvtScaleAbs(dst_y,dst_y);
	cvAddWeighted(dst_x,0.3,dst_y,0.7,0,dst);
 	cvEqualizeHist(dst,dst);
// 	cvShowImage("X",dst_x);
// 	cvShowImage("DST",dst);
// 	cvShowImage("Y",dst_y);
// 	cvWaitKey(0);

	BYTE *ptr = (BYTE *)dst->imageData;
    Mat image(dst->height, dst->width, CV_32FC1, Scalar(0));

	float *out = image.ptr<float>(0);
	for (i = 0; i < dst->height; i ++)
	{
		for (j = 0; j < dst->width; j ++)
		{
			// 			*out = ((float)ptr[i*pequalize->widthStep+j*sub_img->nChannels]
			// 			+ (float)ptr[i*pequalize->widthStep+j*sub_img->nChannels+1]
			// 			+ (float)ptr[i*pequalize->widthStep+j*sub_img->nChannels+2])/3.0f;
			*out = (float)ptr[i*dst->widthStep+j*dst->nChannels];				
			out++;
		}
	}

	

	HessianAffineParams par;
	double t1 = 0;
	{
		// copy params 
		PyramidParams p;
		p.threshold = par.threshold;

		AffineShapeParams ap;
		ap.maxIterations = par.max_iter;
		ap.patchSize = par.patch_size;
		ap.mrSize = par.desc_factor;

		SIFTDescriptorParams sp;
		sp.patchSize = par.patch_size;

		//Extract Affine Hessian Description
		AffineHessianDetector detector(image, p, ap, sp);
		detector.detectPyramidKeypoints(image);
// 		if (detector.keys.size()>0)
// 		{
// 			cout<<"feat size:"<<detector.keys.size();
// 			Mat in_img(sub_img,0);
// 			Mat showImg;
// 			drawHSfeat(in_img,detector.keys,showImg,Scalar::all(-1));
// 			
// 		}


		HSDecription_grp.clear();
		detector.exportKeypoints(HSDecription_grp);

		image.release();
	}

	cvReleaseImage(&resizeimg);
	//cvReleaseImage(&pequalize);
	cvReleaseImage(&sub_img);


}*/

void ExtractOneImageHSFeatures(IplImage *Inputimage,vector <HEDescrip>& HSDecription_grp)
{
	int i,j;
	int x=cvRound(Inputimage->width*0.1);
	int y=cvRound(Inputimage->height*0.15);

	int H=cvRound(Inputimage->height*0.7);
	int W=cvRound(Inputimage->width*0.8);
	CvSize size=cvSize(W,H);
	cvSetImageROI(Inputimage,cvRect(x,y,W,H));
	IplImage *sub_img=cvCreateImage(size,Inputimage->depth,Inputimage->nChannels);
	cvCopy(Inputimage,sub_img);

	IplImage *resizeimg = cvCreateImage(cvSize(sub_img->width/1,sub_img->height/1),IPL_DEPTH_8U,sub_img->nChannels);
	cvResize(sub_img,resizeimg);

	IplImage *pequalize = cvCreateImage(cvSize(resizeimg->width,resizeimg->height),IPL_DEPTH_8U,resizeimg->nChannels);
	IplImage *pImageChannel[3];
	for(i=0;i<3;i++)
		pImageChannel[i]=cvCreateImage(cvGetSize(resizeimg),IPL_DEPTH_8U,1);
	cvSplit(resizeimg,pImageChannel[0],pImageChannel[1],pImageChannel[2],NULL);
	for (i=0;i<3;i++)
		cvEqualizeHist(pImageChannel[i],pImageChannel[i]);
	cvMerge(pImageChannel[0],pImageChannel[1],pImageChannel[2],NULL,pequalize);
	for (i=0;i<3;i++)
		cvReleaseImage(&pImageChannel[i]);
	
	BYTE *ptr = (BYTE *)pequalize->imageData;
	Mat image(pequalize->height, pequalize->width, CV_32FC1, Scalar(0));

	float *out = image.ptr<float>(0);
	for (i = 0; i < pequalize->height; i ++)
	{
		for (j = 0; j < pequalize->width; j ++)
		{
// 			*out = ((float)ptr[i*pequalize->widthStep+j*sub_img->nChannels]
// 			+ (float)ptr[i*pequalize->widthStep+j*sub_img->nChannels+1]
// 			+ (float)ptr[i*pequalize->widthStep+j*sub_img->nChannels+2])/3.0f;
			*out = (float)ptr[i*pequalize->widthStep+j*sub_img->nChannels]*0.114
			+ (float)ptr[i*pequalize->widthStep+j*sub_img->nChannels+1]*0.587
			+ (float)ptr[i*pequalize->widthStep+j*sub_img->nChannels+2]*0.299;
			out++;
		}
	}

	

	HessianAffineParams par;
	double t1 = 0;
	{
		// copy params 
		PyramidParams p;
		p.threshold = par.threshold;

		AffineShapeParams ap;
		ap.maxIterations = par.max_iter;
		ap.patchSize = par.patch_size;
		ap.mrSize = par.desc_factor;

		SIFTDescriptorParams sp;
		sp.patchSize = par.patch_size;

		//Extract Affine Hessian Description
		AffineHessianDetector detector(image, p, ap, sp);
		detector.detectPyramidKeypoints(image);
// 		if (detector.keys.size()>0)
// 		{
// 			cout<<"feat size:"<<detector.keys.size();
// 			Mat in_img(sub_img,0);
// 			Mat showImg;
// 			drawHSfeat(in_img,detector.keys,showImg,Scalar::all(-1));
// 			
// 		}
		

		HSDecription_grp.clear();
		detector.exportKeypoints(HSDecription_grp);

		image.release();
	}

	cvReleaseImage(&resizeimg);
	cvReleaseImage(&pequalize);
	cvReleaseImage(&sub_img);


}

IplImage* crop_image(IplImage* Inputimage)
{
	int x=cvRound(Inputimage->width*0.2);
	int y=cvRound(Inputimage->height*0.25);

	int H=cvRound(Inputimage->height*0.5);
	int W=cvRound(Inputimage->width*0.6);
	CvSize size=cvSize(W,H);
	cvSetImageROI(Inputimage,cvRect(x,y,W,H));
	IplImage *sub_img=cvCreateImage(size,Inputimage->depth,Inputimage->nChannels);
	cvCopy(Inputimage,sub_img);
	cvReleaseImage(&Inputimage);
	return sub_img;
}

Mat do_hist_equalization(Mat in)
{
	Mat gray,dst;
	cvtColor(in,gray,CV_BGR2GRAY);
	equalizeHist(gray,dst);
	return dst;
}


void dense_sampling_detect(Mat src,vector<KeyPoint>&keypoints,int flag)
{
	int h=src.rows;
	int w=src.cols;
	int i,j;
	int patchsize;//(20+1)*20*1.2/9
	int gap;    //overlap=10
	if (flag==1)
	{
		patchsize=46;
		gap=30;
	}
	else
	{
		patchsize=5;
		gap=4;
	}
	int r_beg=patchsize/2;
	int r_end=h-patchsize/2;
	int c_beg=patchsize/2;
	int c_end=w-patchsize/2;
	for (i=r_beg;i<r_end;i+=gap)
	{
		for (j=c_beg;j<c_end;j+=gap)
		{
			KeyPoint CurPoints;
			CurPoints.pt.x=j;
			CurPoints.pt.y=i;
			CurPoints.size=patchsize*0.6;//0.375   0.5(good cadidate)   0.6
			CurPoints.class_id=1;
			CurPoints.octave=4;
			CurPoints.response=4000;
			CurPoints.angle=135;
			keypoints.push_back(CurPoints);
		}
	}
//	cout<<keypoints.size()<<endl;
	
}


void  Extract_HS_features(char *images_dict, char* HSFeatFile,char *img_name_file,char *hsfeat_num_file)
{	
	string dir(images_dict);
	int pos=dir.find_last_of('\\');
	string dir_name(dir.substr(pos+1));     //获取图库路径中最后一个“\\”之后的字符串
	char BaseParth[1024];
	char *curpath;
	curpath = getcwd(NULL, 0);             //获取当前路径
	sprintf(BaseParth,"%s\\HS FEAT_%s",curpath,dir_name.c_str());
	CreateNewDir(BaseParth);          //在当前路径下创建一个新文件夹，用来保存每一幅图像生成的txt文件（即SURF特征）
	vector<string> AllImagefilename;         //图库中每一幅图像的路径
	vector<string> AllFeatureDATAfilename;     //图库中每一幅图像特征文件的路径
	FindAllFiles(images_dict,img_name_file);       //读取images_dict文件下所有图像的名称到名为img_name_file的txt文件中
	int DBnum = 0;
	int all_feat_num=0;
	int m_nFrameInd,i,j;

	int count_feat=0;
	ifstream read_fp;
	read_fp.open(img_name_file);
	if (!read_fp)
	{
		cout<<"Can't open image dictionary file\n";
		exit(-1);
	}
	while(!read_fp.eof())
	{
		char readfilename[1024];
		read_fp.getline(readfilename,1024,'\n');
		if(strcmp(readfilename,"overkane")==0)
			break;
		char img_file_name[1024];
		sprintf(img_file_name,"%s\\%s",images_dict,readfilename);
		string img_file_tmp(img_file_name);
		AllImagefilename.push_back(img_file_tmp);
		//printf("%d img name: %s  ",DBnum,AllImagefilename[DBnum]);
		char img_data_name[1024];
		sprintf(img_data_name,"%s\\%s_Features.txt",BaseParth,readfilename);
		string img_data_tmp(img_data_name);
		AllFeatureDATAfilename.push_back(img_data_tmp);
		DBnum++;  
	}
  int DBnum_updated = DBnum;
	read_fp.close();
  queue<int> images_leftout;
  int count_leftout = 0;
  
   int *number_feat=new int[DBnum];

   //循环处理，提取图像的SURF特征
	for (m_nFrameInd = 0; m_nFrameInd < DBnum; m_nFrameInd ++)
	{
		char hs_file[1024];
		strcpy(hs_file,AllFeatureDATAfilename[m_nFrameInd].c_str());
	//	cout<<AllImagefilename[m_nFrameInd]<<"  ";
	//	Mat img=imread(AllImagefilename[m_nFrameInd]);
		char input_img_name[1024];
		strcpy(input_img_name,AllImagefilename[m_nFrameInd].c_str());
		cout<<input_img_name<<"  ";
		IplImage *in_load = cvLoadImage(input_img_name);              
		IplImage *img1;

		//对加载的图像进行裁剪、缩放等处理
		int in_size=in_load->width*in_load->height;
		if (in_size<90000)
			img1=in_load;
		else
			img1=crop_image(in_load);
		Mat img(img1);

		int firstlevel_size=img.rows*img.cols;
		if (firstlevel_size>400000)
		{
			do
			{
// 				img = gaussianBlur(img, 3.2);
// 				img = kane_halfImage(img);
				pyrDown( img, img, Size( img.cols/2, img.rows/2 ) );
				firstlevel_size=img.rows*img.cols;
			}while(firstlevel_size>400000);

		}
//		vector<KeyPoint> keypoints;

		
		//分层密集采样。分别在图像Img和尺寸是Img一半的图像secong_down上进行密集采样，再将两者的特征合并
		Mat second_down;	
		pyrDown( img, second_down, Size( img.cols/2, img.rows/2 ) );

		vector<KeyPoint> keypoints;//构造2个专门由点组成的点向量用来存储特征点

		//通过给定水平、竖直方向的间隔来指定某些像素点为特征点，
		//可以加快图像处理的速度，也可以避免一些纹理稀少的图像提取不到特征
		if (img.rows<100||img.cols<100)
			dense_sampling_detect(img,keypoints,0);
		else
			dense_sampling_detect(img,keypoints,1);    
		
		SurfDescriptorExtractor extractor;//定义描述子对象
		Mat descriptors;//存放特征向量的矩阵
		extractor.compute(img,keypoints,descriptors);

		 //生成时判断是否第一层图像特征的输出，若是，直接创建新的txt文件，并输出，若否，则在第一层生成的文件最后继续添加生成的特征
		if(descriptors.rows)
			SaveHSfeatures(descriptors,hs_file,m_nFrameInd,0);    
	    cout<<descriptors.rows<<"  ";

		
		vector<KeyPoint> keypoints1;//构造2个专门由点组成的点向量用来存储特征点
		dense_sampling_detect(second_down,keypoints1,1);
		SurfDescriptorExtractor extractor1;//定义描述子对象
		Mat descriptors1;//存放特征向量的矩阵
		extractor1.compute(second_down,keypoints1,descriptors1);
		if(descriptors1.rows)
			SaveHSfeatures(descriptors1,hs_file,m_nFrameInd,1);
		  cout<<descriptors1.rows<<"  ";

		int NewSFNum=descriptors1.rows+descriptors.rows;
		cvReleaseImage(&img1);

		number_feat[m_nFrameInd]=NewSFNum;    //保存生成的一幅图像的特征数目
		cout<<"feat size: "<<NewSFNum<<endl;

		if(NewSFNum==0)
		{
			printf("no features: %s\n", hs_file);
			images_leftout.push(m_nFrameInd-count_leftout++);
			DBnum_updated--;
		}
		
	}

  // update database
	while(!images_leftout.empty())
	{
		 int erase_ind = images_leftout.front();
		 AllImagefilename.erase(AllImagefilename.begin()+erase_ind);
		 AllFeatureDATAfilename.erase(AllFeatureDATAfilename.begin()+erase_ind);
			images_leftout.pop();
	 }
	for (i=0;i<DBnum;i++)
		all_feat_num+=number_feat[i];
	cout<<all_feat_num<<endl;

	 ofstream fnum;
	 fnum.open(hsfeat_num_file);
	 if (!fnum)
	{
		  cout<<"Can't open feat num file in proc_func ling 243.\n";
		  exit(-1);
	}
	 for (i=0;i<DBnum-1;i++)
	 {
		 if (number_feat[i]!=0)
			 fnum<<number_feat[i]<<"  ";
	 }
	 fnum<<number_feat[i];
	  
	  fnum.close();
	  delete []number_feat;
	FILE *ff_hs;
	ff_hs=fopen(HSFeatFile,"w");  // todo: shufei, deal with 'no feature' detected in the image? (line 234-242)
	for (m_nFrameInd = 0; m_nFrameInd < DBnum_updated; m_nFrameInd ++)
	{
		char hs_file_name[1024];
		strcpy(hs_file_name,AllFeatureDATAfilename[m_nFrameInd].c_str());
		fprintf(ff_hs,"%s",hs_file_name);
		if (m_nFrameInd<DBnum_updated-1)
		{
			fprintf(ff_hs,"\n");
		}
	}
	fclose(ff_hs);

}


//根据生成的VLAD特征进行聚类，生成字典
void Generate_hierarchy_kmeans_dictionary(char *HSfeat,char *WordsFile,char *hsfeat_num_file)
{
	vector<string> AllFeatureDATAfilename;
	int DBnum=0;
	int m_it,i,j,t,s;
	i=0;t=0;
	int ii=0;
	int iter=0;
	int i_beg=0;
	int feat_num=0;
	int group_num;
	ifstream fin;
	fin.open(HSfeat);
	if (!fin)
	{
		cout<<"Can't open hsfeat file in proc_fun line 469.\n";
		exit(-1);
	}
	while(!fin.eof())
	{
		char img_hs_file[1024];
		fin.getline(img_hs_file,1022,'\n');
		string hs_file(img_hs_file);
		//fscanf(read_fp,"%s\n",AllFeatureDATAfilename[DBnum]);
		AllFeatureDATAfilename.push_back(hs_file);
		DBnum++;
	}
	fin.close();

	ifstream fread_num;
	fread_num.open(hsfeat_num_file);
	if (!fread_num)
	{
		cout<<"Can't open featnum file in proc_func line 356.\n";
		exit(-1);
	}
	int *each_num=new int[DBnum];
	int all_hs_num=0;
	while(!fread_num.eof())
	{
		fread_num>>each_num[ii];
		all_hs_num+=each_num[ii];
		ii++;
	}

	//如果生成的特征数目超过1000000，则将特征以1000000为单位进行分组，不足1000000的末尾数据也独立成一组，然后分别进行聚类，
	//最后将各组的聚类中心合并，再重新聚类
	if (all_hs_num>1000000)
		group_num=1000000;
	else 
		group_num=all_hs_num;
	fread_num.close();

	vector<Mat>centers_all;
	do 
	{
		feat_num+=each_num[iter];

		if (feat_num>group_num||iter==DBnum-1)
		{
			
			Mat hie_hs(feat_num,HS_DIM,CV_32F);
			if (feat_num>1000000)
			{
				s=0;
				for (m_it=i_beg;m_it<iter+1;m_it++)
				{
					ifstream fin_feat;
					fin_feat.open(AllFeatureDATAfilename[m_it]);
					//	cout<<AllFeatureDATAfilename[m_it]<<endl;

					if (!fin_feat)
					{
						cout<<"proc_fun line 508 error.\n";
						exit(-1);
					}
					int index;
					fin_feat>>index;
// 					int kkk_num;
// 					fin_feat>>kkk_num;
					while (!fin_feat.eof())
					{
						for (i=0;i<HS_DIM;i++)
							fin_feat>>hie_hs.at<float>(s,i);
						s++;

					}
					fin_feat.close();
				}
				
				Mat centers_1(nClusters,HS_DIM,CV_32F);
				cvflann::KMeansIndexParams params1 = cvflann::KMeansIndexParams();
				int cent_num=cv::flann::hierarchicalClustering<cvflann::L2<float>>(hie_hs,centers_1,params1);   //也可以换成kmeans聚类，hierarchicalClustering方法比kmeans聚类的速度快些
				
				Mat real_cent1(centers_1,cvRect(0,0,HS_DIM,cent_num));

				cout<<"There has been one time cluster.\n";
				centers_all.push_back(real_cent1);
			}
			else 
			{
				s=0; 
				Mat last_feat(feat_num,HS_DIM,CV_32F);
				for (m_it=i_beg;m_it<iter+1;m_it++)
				{	
					ifstream fin_feat1;
					fin_feat1.open(AllFeatureDATAfilename[m_it]);

					if (!fin_feat1)
					{
						cout<<"proc_fun line 508 error.\n";
						exit(-1);
					}
					int index1;
					fin_feat1>>index1;
// 					int kkk_num1;
// 					fin_feat1>>kkk_num1;
					while (!fin_feat1.eof())
					{
						for (i=0;i<HS_DIM;i++)
							fin_feat1>>last_feat.at<float>(s,i);
						s++;

					}
					fin_feat1.close();
				}
				cout<<"last few features.  "<<last_feat.rows<<endl;
				centers_all.push_back(last_feat);

			}		 
			i_beg=iter+1;

			feat_num=0;
		}
		iter++;
	} while (iter<DBnum);

	delete[] each_num;

	int newfeat_num=0;
	for (i=0;i<centers_all.size();i++)
		newfeat_num+=centers_all[i].rows;
	Mat p_descriptors(newfeat_num,HS_DIM,CV_32F);
	s=0;

	
	for (m_it=0;m_it<centers_all.size();m_it++)
	{
		for (i=0;i<centers_all[m_it].rows;i++)
		{
			for (j=0;j<centers_all[m_it].cols;j++)
			{
				p_descriptors.at<float>(s,j)=centers_all[m_it].at<float>(i,j);
	
			}
			s++;
		}
	}

	Mat centers(nClusters,HS_DIM,CV_32F);
	cvflann::KMeansIndexParams params = cvflann::KMeansIndexParams();
	int kane_clus_num=cv::flann::hierarchicalClustering<cvflann::L2<float>>(p_descriptors,centers,params);

	Mat real_centers(centers,cvRect(0,0,HS_DIM,kane_clus_num));
	
	cout<<"centers' rows:"<<real_centers.rows<<"  centers' cols:"<<real_centers.cols<<endl;

	ofstream fk10;
	fk10.open("F:\\(1)kane_input_data1.txt");
	fk10<<p_descriptors<<endl;
	fk10.close();

	ofstream fk11;
	fk11.open("F:\\(1)kane_center_data1.txt");
	fk11<<real_centers<<endl;
	fk11.close();
// 	ofstream f1;
// 	f1.open(WordsFile);
// 	f1<<real_centers.rows<<endl;
// 	f1<<real_centers.cols<<endl;
// 	for (i=0;i<real_centers.rows-1;i++)
// 	{
// 		for (j=0;j<real_centers.cols;j++)
// 			f1<<real_centers.at<float>(i,j)<<" ";
// 		f1<<endl;
// 	}
// 	for (j=0;j<real_centers.cols-1;j++)
// 		f1<<real_centers.at<float>(i,j)<<" ";
// 	f1<<real_centers.at<float>(i,j);
// 	f1.close();


	FILE * f1;
	f1=fopen(WordsFile,"w");
	fprintf(f1,"%d\n%d\n",real_centers.rows,real_centers.cols);
	for (i=0;i<real_centers.rows-1;i++)
	{
		for (j=0;j<real_centers.cols;j++)
			fprintf(f1,"%f ",real_centers.at<float>(i,j));
		fprintf(f1,"\n");
	}
	for (j=0;j<real_centers.cols-1;j++)
		fprintf(f1,"%f ",real_centers.at<float>(i,j));
	fprintf(f1,"%f",real_centers.at<float>(i,j));
	fclose(f1);
	
}

/*void Generate_hierarchy_kmeans_dictionary(char *HSfeat,char *WordsFile,char *hsfeat_num_file)
{
	vector<string> AllFeatureDATAfilename;
	int DBnum=0;
	int m_it,i,j,t,s;
	i=0;t=0;
	int ii=0;
	int iter=0;
	int i_beg=0;
	int feat_num=0;
	int group_num;
	ifstream fin;
	fin.open(HSfeat);
	if (!fin)
	{
		cout<<"Can't open hsfeat file in proc_fun line 469.\n";
		exit(-1);
	}
	while(!fin.eof())
	{
		char img_hs_file[1024];
		fin.getline(img_hs_file,1022,'\n');
		string hs_file(img_hs_file);
		//fscanf(read_fp,"%s\n",AllFeatureDATAfilename[DBnum]);
		AllFeatureDATAfilename.push_back(hs_file);
		DBnum++;
	}
	fin.close();

	ifstream fread_num;
	fread_num.open(hsfeat_num_file);
	if (!fread_num)
	{
		cout<<"Can't open featnum file in proc_func line 356.\n";
		exit(-1);
	}
	int *each_num=new int[DBnum];
	int all_hs_num=0;
	while(!fread_num.eof())
	{
		fread_num>>each_num[ii];
		all_hs_num+=each_num[ii];
		ii++;
	}
	if (all_hs_num>1000000)
		group_num=1000000;
	else 
		group_num=all_hs_num;
	fread_num.close();
	int dele_flag=0;

	float* data1;
	vector<cvflann::Matrix<float>>centers_all;
	do 
	{
		feat_num+=each_num[iter];

		if (feat_num>group_num||iter==DBnum-1)
		{
			// int n_group=cvRound(feat_num/10000);
			//	Mat hie_hs(feat_num,HS_DIM,CV_32F);
			float* data = new float[feat_num * HS_DIM];

			if (feat_num>1000000)
			{
				s=0;
				for (m_it=i_beg;m_it<iter+1;m_it++)
				{
					ifstream fin_feat;
					fin_feat.open(AllFeatureDATAfilename[m_it]);

					if (!fin_feat)
					{
						cout<<"proc_fun line 508 error.\n";
						exit(-1);
					}
					int index;
					fin_feat>>index;
// 					int num_kkk;
// 					fin_feat>>num_kkk;
					while (!fin_feat.eof())
					{
						for (i=0;i<HS_DIM;i++)
							fin_feat>>data[s* HS_DIM + i];
						s++;

					}
					fin_feat.close();
				}
				cvflann::Matrix<float> hie_hs = cvflann::Matrix<float>(data, feat_num, HS_DIM);
				float* clusters1 = new float[nClusters * HS_DIM];
				cvflann::Matrix<float> centers1 = cvflann::Matrix<float>(clusters1, nClusters, HS_DIM);

				cvflann::KMeansIndexParams params1 = cvflann::KMeansIndexParams();

				int cent_num=cvflann::hierarchicalClustering<cvflann::L2<float>>(hie_hs, centers1, params1);
				delete[] data;
				float *new_cen1=new float[cent_num*HS_DIM];
				for (i=0;i<cent_num;i++)
					for (j=0;j<HS_DIM;j++)
						new_cen1[i*HS_DIM+j]=centers1[i][j];
				delete[] clusters1;
				cvflann::Matrix<float> newcenters1 = cvflann::Matrix<float>(new_cen1, cent_num, HS_DIM);

				cout<<"There has been one time cluser.\n";
				centers_all.push_back(newcenters1);
			}
			else 
			{
				s=0; 
				dele_flag=1;
				data1 = new float[feat_num * HS_DIM];
				for (m_it=i_beg;m_it<iter+1;m_it++)
				{	
					ifstream fin_feat1;
					fin_feat1.open(AllFeatureDATAfilename[m_it]);

					if (!fin_feat1)
					{
						cout<<"proc_fun line 508 error.\n";
						exit(-1);
					}
					int index1;
					fin_feat1>>index1;
// 					int num_kkk1;
// 					fin_feat1>>num_kkk1;
					while (!fin_feat1.eof())
					{
						for (i=0;i<HS_DIM;i++)
						{
							fin_feat1>>data1[s*HS_DIM+i];
							//cout<<data1[s*HS_DIM+i]<<"  ";
						}

						s++;

					}
					fin_feat1.close();
				}
				cvflann::Matrix<float> last_feat=cvflann::Matrix<float>(data1, feat_num, HS_DIM);
				cout<<"last few features.\n";
				centers_all.push_back(last_feat);


			}		 
			i_beg=iter+1;

			feat_num=0;
		}
		iter++;
	} while (iter<DBnum);

	delete[] each_num;

	int newfeat_num=0;
	for (i=0;i<centers_all.size();i++)
		newfeat_num+=centers_all[i].rows;
	float* data_all = new float[newfeat_num * HS_DIM];
	s=0;
	for (m_it=0;m_it<centers_all.size();m_it++)
	{
		for (i=0;i<centers_all[m_it].rows;i++)
		{
			for (j=0;j<centers_all[m_it].cols;j++)
			{
				data_all[s*HS_DIM+j]=centers_all[m_it][i][j];
			}
			s++;
		}
	}
	if (dele_flag)
		delete[] data1;
	cvflann::Matrix<float> last_feat=cvflann::Matrix<float>(data_all, newfeat_num, HS_DIM);
	float* clusters = new float[nClusters * HS_DIM];
	cvflann::Matrix<float> centers= cvflann::Matrix<float>(clusters, nClusters, HS_DIM);

	cvflann::KMeansIndexParams params = cvflann::KMeansIndexParams();

	int kane_clus_num=cvflann::hierarchicalClustering<cvflann::L2<float>>(last_feat, centers, params);	
	delete[] data_all;
	float *new_cen=new float[kane_clus_num*HS_DIM];
	for (i=0;i<kane_clus_num;i++)
		for (j=0;j<HS_DIM;j++)
			new_cen[i*HS_DIM+j]=centers[i][j];
	delete[] clusters;
	cvflann::Matrix<float> newcenters= cvflann::Matrix<float>(new_cen, kane_clus_num, HS_DIM);


	cout<<"centers' rows:"<<newcenters.rows<<"  centers' cols:"<<newcenters.cols<<endl;
// 	FILE* f1;
// 	f1=fopen(WordsFile,"w");
// 	fprintf(f1,"%d %d ",newcenters.rows,newcenters.cols);
// 	for (i=0;i<newcenters.rows-1;i++)
// 	{
// 		for (j=0;j<newcenters.cols;j++)
// 			fprintf(f1,"%f ",newcenters[i][j]);
// 		fprintf(f1,"\n");
// 	}
// 	for (j=0;j<newcenters.cols-1;j++)
// 		fprintf(f1,"%f ",newcenters[i][j]);
// 	fprintf(f1,"%f",newcenters[i][j]);
// 	fclose(f1);
	ofstream f1;
	f1.open(WordsFile);
	f1<<newcenters.rows<<endl;
	f1<<newcenters.cols<<endl;
	for (i=0;i<newcenters.rows-1;i++)
	{
		for (j=0;j<newcenters.cols;j++)
			f1<<newcenters[i][j]<<" ";
		f1<<endl;
	}
	for (j=0;j<newcenters.cols-1;j++)
		f1<<newcenters[i][j]<<" ";
	f1<<newcenters[i][j];
	f1.close();

	delete[] new_cen;
}*/


void Generate_kmeans_dictionary(char *HSfeat,char *WordsFile,char *hsfeat_num_file)
{
	vector<string> AllFeatureDATAfilename;
	int DBnum=0;
	int m_it,i,j,s,t;
	i=0;t=0;
	ifstream fin;
	fin.open(HSfeat);
	if (!fin)
	{
		cout<<"Can't open hsfeat file in proc_fun line 339.\n";
		exit(-1);
	}
	while(!fin.eof())
	{
		char img_hs_file[1024];
		fin.getline(img_hs_file,1022,'\n');
		string hs_file(img_hs_file);
		//fscanf(read_fp,"%s\n",AllFeatureDATAfilename[DBnum]);
		AllFeatureDATAfilename.push_back(hs_file);
		DBnum++;
	}
	fin.close();
	ifstream fread_num;
	fread_num.open(hsfeat_num_file);
	if (!fread_num)
	{
		cout<<"Can't open featnum file in proc_func line 356.\n";
		exit(-1);
	}
	int feat_num;
	fread_num>>feat_num;
	
	fread_num.close();


	Mat p_descriptors= Mat::zeros(feat_num,HS_DIM, CV_32F);
	for (m_it=0;m_it<DBnum;m_it++)
	{
		ifstream read_img_feat;
		read_img_feat.open(AllFeatureDATAfilename[m_it]);
		if (!read_img_feat)
		{
			cout<<"Can't open feat file in proc_func line 375.\n";
			exit(-1);
		}
		int index;
		read_img_feat>>index;
		while (!read_img_feat.eof())
		{
			for (s=0;s<HS_DIM;s++)				
				read_img_feat>>p_descriptors.at<float>(t,s);				
			t++;
			
		}
		
	}

	Mat bestLabels, centers;
	kmeans(p_descriptors, nClusters, bestLabels,TermCriteria( CV_TERMCRIT_EPS+CV_TERMCRIT_ITER, 10, 1.0),3, KMEANS_PP_CENTERS, centers);
	cout<<"centers' rows:"<<centers.rows<<"  centers' cols:"<<centers.cols<<endl;
	ofstream f1;
	f1.open(WordsFile);
	f1<<centers.rows<<endl;
	f1<<centers.cols<<endl;
	for (i=0;i<centers.rows-1;i++)
	{
		for (j=0;j<centers.cols;j++)
			f1<<centers.at<float>(i,j)<<" ";
		f1<<endl;
	}
	for (j=0;j<centers.cols-1;j++)
			f1<<centers.at<float>(i,j)<<" ";
	f1<<centers.at<float>(i,j);
	f1.close();
}




void SaveHSfeatures(Mat& HSDecription_grp,char SaveFilename[128],int index,int flag)
{
	int i,j;
	FILE *fp_w;
	if (flag==0)
	{
		fp_w = fopen(SaveFilename,"w");
		fprintf(fp_w,"%d\n",index); 
		for (i=0;i<HSDecription_grp.rows-1;i++)
		{
			for (j=0;j<HSDecription_grp.cols;j++)
				fprintf(fp_w,"%f ",HSDecription_grp.at<float>(i,j));

			fprintf(fp_w,"\n");  
		}
		for (j=0;j<HSDecription_grp.cols-1;j++)
			fprintf(fp_w,"%f ",HSDecription_grp.at<float>(i,j));
		fprintf(fp_w,"%f",HSDecription_grp.at<float>(i,j));
		//fprintf(fp_w,"%d",KANE_OVER);
		fclose(fp_w);
	}
	else
	{
		fp_w = fopen(SaveFilename,"a");
		fprintf(fp_w,"\n"); 
		for (i=0;i<HSDecription_grp.rows-1;i++)
		{
			for (j=0;j<HSDecription_grp.cols;j++)
				fprintf(fp_w,"%f ",HSDecription_grp.at<float>(i,j));

			fprintf(fp_w,"\n");  
		}
		for (j=0;j<HSDecription_grp.cols-1;j++)
			fprintf(fp_w,"%f ",HSDecription_grp.at<float>(i,j));
		fprintf(fp_w,"%f",HSDecription_grp.at<float>(i,j));
		//fprintf(fp_w,"%d",KANE_OVER);
		fclose(fp_w);
	}
	

}


void ReadImageHSBowTFeatures(char *AllFeaturefile, vector <HSBowFeature>& DBImageHSBowFeature)
{
	int i,j;
	j=0;
	HSBowFeature CurFeatureVec;
	FILE *fp_r;
	fp_r= fopen(AllFeaturefile,"r");
	if(!fp_r)
	{
		cout<<"Cann't open HS file when reading features.\n";
		exit(-1);
	}
	
	while(!feof(fp_r))
	{
		fscanf(fp_r,"%d",&CurFeatureVec.index);
//		cout<<CurFeatureVec.index<<"  ";
		fscanf(fp_r,"%d",&CurFeatureVec.HSBowFVSize);	
		CurFeatureVec.HSBowFV = (float *)calloc(CurFeatureVec.HSBowFVSize,sizeof(float));
		for (i = 0; i < CurFeatureVec.HSBowFVSize; i ++)
		{
			fscanf(fp_r,"%f",&CurFeatureVec.HSBowFV[i]);
		}

		DBImageHSBowFeature.push_back(CurFeatureVec);
	}
	fclose(fp_r);
}

void ComputerTwoImagesSimilarity_UsingHSBow(HSBowFeature RefImageInfor, HSBowFeature CandidateImageInfor, 
								ImageSimilarity &ImageSimilarityValue)
{
	ImageSimilarityValue.OverallSimilarity = 
		vector_correlation(RefImageInfor.HSBowFV,CandidateImageInfor.HSBowFV,RefImageInfor.HSBowFVSize);
	 ImageSimilarityValue.LightingSimilarity = ImageSimilarityValue.OverallSimilarity * 100;
	 ImageSimilarityValue.ContrastSimilarity = ImageSimilarityValue.OverallSimilarity * 100;
	 ImageSimilarityValue.SalientValueSimilarity = ImageSimilarityValue.OverallSimilarity * 100;
	 ImageSimilarityValue.ColorDistributionSimilarity = ImageSimilarityValue.OverallSimilarity * 100;
	 ImageSimilarityValue.FgBgSimilarity = ImageSimilarityValue.OverallSimilarity  * 100;

}

/*********************************************
	The following part is for 'Advanced GIST Feature'
**********************************************/
void ExtractGistImageFeatures(IplImage *Inputimage, GistParam GistParamValue,GistFeature &GistFeature)
{
	int i,j;
	int descsize=0;
	float *desc;
	IplImage *ResizeImage;
	static color_image_t *Gray_img;
    int height = Inputimage->height;
	int width = Inputimage->width;
    int ResizeHeight = 32;
    int ResizeWidth = 32;;

	ResizeImage = cvCreateImage(cvSize(ResizeWidth,ResizeHeight),
		IPL_DEPTH_8U,3);
    cvResize(Inputimage,ResizeImage,1);

	/* compute descriptor size */
	for(i=0;i<GistParamValue.n_scale;i++) 
		descsize+=GistParamValue.nblocks*GistParamValue.nblocks*GistParamValue.orientations_per_scale[i];
	
	/* Compute Image's Advanced GIST Feature */
	Gray_img=IplImage2ColorImageT(ResizeImage);
	desc=color_gist_scaletab(Gray_img,GistParamValue.nblocks,GistParamValue.n_scale,GistParamValue.orientations_per_scale);
	GistFeature.GistFVSize = descsize;
	GistFeature.GistFV = (float *)calloc(descsize,sizeof(float));
	for (i = 0; i < descsize; i ++)
		GistFeature.GistFV[i] = desc[i];

	free(Gray_img->c1);
	free(Gray_img->c2);
	free(Gray_img->c3);
	free(Gray_img);
	free(desc);
	cvReleaseImage(&ResizeImage);

}

void SaveImageAdvGISTFeatures(GistFeature ImageFeatureValue, char SaveFilename[128])
{
	int i,j;
	FILE *fp_w = fopen(SaveFilename,"wb");
	fprintf(fp_w,"%d ",ImageFeatureValue.GistFVSize);
	for (i = 0; i < ImageFeatureValue.GistFVSize; i ++)
	{
		fprintf(fp_w,"%f ",ImageFeatureValue.GistFV[i]);
	}

	fclose(fp_w);
}

void ReadImageGISTFeatures(GistFeature &ImageFeatureValue, char ReadFilename[128])
{
	int i,j;
	FeatureVec CurFeatureVec;
	FILE *fp_r = fopen(ReadFilename,"rb");
	fscanf(fp_r,"%d ",&ImageFeatureValue.GistFVSize);
	ImageFeatureValue.GistFV = (float *)calloc(ImageFeatureValue.GistFVSize,sizeof(float));
	for (i = 0; i < ImageFeatureValue.GistFVSize; i ++)
	{
		fscanf(fp_r,"%f ",&ImageFeatureValue.GistFV[i]);
	}

	fclose(fp_r);
}

void ComputerTwoImagesSimilarity_UsingGIST(GistFeature RefImageInfor, GistFeature CandidateImageInfor, 
								ImageSimilarity &ImageSimilarityValue)
{
	ImageSimilarityValue.OverallSimilarity = 
		vector_correlation(RefImageInfor.GistFV,CandidateImageInfor.GistFV,RefImageInfor.GistFVSize);
	 ImageSimilarityValue.LightingSimilarity = ImageSimilarityValue.OverallSimilarity * 100;
	 ImageSimilarityValue.ContrastSimilarity = ImageSimilarityValue.OverallSimilarity * 100;
	 ImageSimilarityValue.SalientValueSimilarity = ImageSimilarityValue.OverallSimilarity * 100;
	 ImageSimilarityValue.ColorDistributionSimilarity = ImageSimilarityValue.OverallSimilarity * 100;
	 ImageSimilarityValue.FgBgSimilarity = ImageSimilarityValue.OverallSimilarity  * 100;

}
//End of Advanced GIST Feature Functions

void ReadImageFeatures(ImageFeature &ImageFeatureValue, char ReadFilename[128])
{
	int i,j;
	FeatureVec CurFeatureVec;
	FILE *fp_r = fopen(ReadFilename,"rb");
	fscanf(fp_r,"%d %d ",&ImageFeatureValue.nWidth,&ImageFeatureValue.nHeight);
	fscanf(fp_r,"%d ",&ImageFeatureValue.FeatureNumber);
	ImageFeatureValue.ImageFeatureList.clear();
	for (i = 0; i < ImageFeatureValue.FeatureNumber; i ++)
	{
		fscanf(fp_r,"%f %f ",&CurFeatureVec.x,&CurFeatureVec.y);
		for (j = 0; j < 64; j ++)
		{
			fscanf(fp_r,"%f ",&CurFeatureVec.FV[j]);
		}
		ImageFeatureValue.ImageFeatureList.push_back(CurFeatureVec);
	}

	for (i = 0; i < 9; i ++)
		fscanf(fp_r,"%f ",&ImageFeatureValue.ImageContrast[i]);
	for (i = 0; i < 3; i ++)
		fscanf(fp_r,"%f ",&ImageFeatureValue.ImageLightingRGB[i]);
	for (i = 0; i < 256; i ++)
		fscanf(fp_r,"%f ",&ImageFeatureValue.UHist[i]);
	for (i = 0; i < 256; i ++)
		fscanf(fp_r,"%f ",&ImageFeatureValue.VHist[i]);

	fclose(fp_r);
}

void SaveImageFeatures(ImageFeature ImageFeatureValue, char SaveFilename[128])
{
	int i,j;
	FILE *fp_w = fopen(SaveFilename,"wb");
	fprintf(fp_w,"%d %d ",ImageFeatureValue.nWidth,ImageFeatureValue.nHeight);
	fprintf(fp_w,"%d ",ImageFeatureValue.FeatureNumber);
	for (i = 0; i < ImageFeatureValue.FeatureNumber; i ++)
	{
		fprintf(fp_w,"%f %f ",ImageFeatureValue.ImageFeatureList[i].x,ImageFeatureValue.ImageFeatureList[i].y);
		for (j = 0; j < 64; j ++)
		{
			fprintf(fp_w,"%f ",ImageFeatureValue.ImageFeatureList[i].FV[j]);
		}
	}

	for (i = 0; i < 9; i ++)
		fprintf(fp_w,"%f ",ImageFeatureValue.ImageContrast[i]);
	for (i = 0; i < 3; i ++)
		fprintf(fp_w,"%f ",ImageFeatureValue.ImageLightingRGB[i]);
	for (i = 0; i < 256; i ++)
		fprintf(fp_w,"%f ",ImageFeatureValue.UHist[i]);
	for (i = 0; i < 256; i ++)
		fprintf(fp_w,"%f ",ImageFeatureValue.VHist[i]);

	fclose(fp_w);
}

void ExtractImageFeatures(IplImage *img, ImageFeature &ImageFeatureValue)
{
	int i,j;
	FeatureVec CurFeatureVec;
	  IpVec ipts;
	ipts.clear();
    // Detect and describe interest points in the frame
    surfDetDes(img, ipts, false, 3, 4, 2, 0.0004f);
	
	ImageFeatureValue.ImageFeatureList.clear();
	ImageFeatureValue.FeatureNumber = ipts.size();
	for (i = 0; i < ipts.size(); i ++)
	{
		CurFeatureVec.x = ipts[i].x;
		CurFeatureVec.y = ipts[i].y;
		for (j = 0; j < 64; j ++)
			CurFeatureVec.FV[j] = ipts[i].descriptor[j];
		ImageFeatureValue.ImageFeatureList.push_back(CurFeatureVec);
	}

	ImageFeatureValue.nWidth = img->width;
	ImageFeatureValue.nHeight = img->height;
	ComputeImageStatData(img, ImageFeatureValue);

}

//[Modified for Version 2.0]
float vector_correlation(float *vec1,float *vec2, int num)
{
	float s1,s2,s3;
	float return_value;

	s1 =s2 = s3 = 0;

	for (int i = 0; i < num; i ++)
	{
		s1 += vec1[i] * vec2[i];
		s2 += vec1[i] * vec1[i];
		s3 += vec2[i] * vec2[i];
	}

	if ((s2 == 0) || (s3 == 0))
		return_value =  0;
	else
		return_value = (s1) / (sqrt(s2) * sqrt(s3));

	//return_value = (float)((int)(return_value*100)/100.0f);

	return (return_value);
}
//[END Of Modified for Version 2.0] 

float vector_diff(float *vec1,float *vec2, int num)
{
	float sum = 0;


	for (int i = 0; i < num; i ++){
		sum += (vec1[i] - vec2[i])*(vec1[i] - vec2[i]);
	}

	return (sqrt(sum));
}

void AveVector(float *vector, int num, int division,float *avevector)
{
	int i;

	if (division == 0)
	{
		for (i = 0; i < num; i ++)
			avevector[i] = 0;
	}else
	{
		for (i = 0; i < num; i ++)
			avevector[i] = vector[i]/division;
	}
}

void ComputerTwoImagesSimilarity(ImageFeature RefImageInfor, ImageFeature CandidateImageInfor, 
								 int w, int h,ImageSimilarity &ImageSimilarityValue)
{
  float dist, d1, d2;
  int i,j;
  int match_ind;
  float S_num,S_sum,Cur_S;
  int cx,cy;
  int region_sx,region_sy,region_ex,region_ey;
  float RefC[64],RefB[64],CanDC[64],CanDB[64];
  float RefCave[64],RefBave[64],CanDCave[64],CanDBave[64];
  float RefCBDiff[64],CanDCBDiff[64];
  int RefC_num,RefB_num,CanDC_num,CanDB_num;
  int matching_num =0 ;
  for (i = 0; i < 64; i ++)
  {
	RefC[i] = RefB[i] = CanDC[i] = CanDB[i] = 0;
  }
  RefC_num = RefB_num = CanDC_num = CanDB_num = 0;
  cx = (w)/2;
  cy = (h)/2;

  region_sx = cx - (w/6);
  region_sy = cy - (h/6);
  region_ex = cx + (w/6);
  region_ey = cy + (h/6);

  S_sum = 0;
  S_num = 0;
  for(int i = 0; i < RefImageInfor.FeatureNumber; i++) 
  {
    d1 = d2 = FLT_MAX;

    for(int j = 0; j < CandidateImageInfor.FeatureNumber; j++) 
    {
      dist = vector_diff(RefImageInfor.ImageFeatureList[i].FV,CandidateImageInfor.ImageFeatureList[j].FV,64);  

      if(dist<d1) // if this feature matches better than current best
      {
        d2 = d1;
        d1 = dist;
        match_ind = j;
      }
      else if(dist<d2) // this feature matches better than second best
      {
        d2 = dist;
      }
    }
    if(d1/d2 < 0.90) 
    { 
		Cur_S = vector_correlation(RefImageInfor.ImageFeatureList[i].FV,CandidateImageInfor.ImageFeatureList[match_ind].FV,64);  
		S_sum += Cur_S;
		S_num ++;

		if ((RefImageInfor.ImageFeatureList[i].x >= region_sx)&&
			(RefImageInfor.ImageFeatureList[i].x <= region_ex)&&
			(RefImageInfor.ImageFeatureList[i].y >= region_sy)&&
			(RefImageInfor.ImageFeatureList[i].y <= region_ey))
		{
			for (int k = 0; k < 64; k ++)
			{
				RefC[k] += RefImageInfor.ImageFeatureList[i].FV[k];
				CanDC[k] += CandidateImageInfor.ImageFeatureList[match_ind].FV[k];
			}
			RefC_num ++;
			CanDC_num ++;
		}else
		{
			RefB_num ++;
			CanDB_num ++;
			for (int k = 0; k < 64; k ++)
			{
				RefB[k] += RefImageInfor.ImageFeatureList[i].FV[k];
				CanDB[k] += CandidateImageInfor.ImageFeatureList[match_ind].FV[k];
			}
		}
		matching_num ++;
	}
  }

  if (matching_num == 0)
  {
	  ImageSimilarityValue.OverallSimilarity = -1;
	   ImageSimilarityValue.LightingSimilarity = -1;
	   ImageSimilarityValue.ContrastSimilarity = -1;
	  ImageSimilarityValue.ColorDistributionSimilarity = -1;
	  ImageSimilarityValue.SalientValueSimilarity = -1;
      ImageSimilarityValue.FgBgSimilarity = -1;
  }else
  {
	  AveVector(RefC,64,RefC_num,RefCave);
	  AveVector(RefB,64,RefB_num,RefBave);
	  AveVector(CanDC,64,CanDC_num,CanDCave);
	  AveVector(CanDB,64,CanDB_num,CanDBave);

	  for (i = 0; i < 64; i ++)
	  {
		  RefCBDiff[i] = RefC[i] - RefB[i];
		  CanDCBDiff[i] = CanDC[i] - CanDB[i];
	  }
	  if (S_num == 0)
		ImageSimilarityValue.OverallSimilarity = 0;
	  else
		ImageSimilarityValue.OverallSimilarity = (S_sum/S_num);

	  ImageSimilarityValue.LightingSimilarity = vector_correlation(RefImageInfor.ImageLightingRGB,
			CandidateImageInfor.ImageLightingRGB,3);
	  ImageSimilarityValue.ContrastSimilarity = vector_correlation(RefImageInfor.ImageContrast,
			CandidateImageInfor.ImageContrast,9);
	  ImageSimilarityValue.ColorDistributionSimilarity = MIN2(vector_correlation(RefImageInfor.UHist,
			CandidateImageInfor.UHist,256),vector_correlation(RefImageInfor.VHist,
			CandidateImageInfor.VHist,256));

	  ImageSimilarityValue.SalientValueSimilarity =  vector_correlation(RefCave,
			CanDCave,64);
	  ImageSimilarityValue.FgBgSimilarity =  vector_correlation(RefCBDiff,CanDCBDiff,64);

	  //[Modified for Version 2.0] 

	  float ave,scale;

	  ave = (ImageSimilarityValue.LightingSimilarity + ImageSimilarityValue.ContrastSimilarity
		  + ImageSimilarityValue.SalientValueSimilarity + ImageSimilarityValue.ColorDistributionSimilarity
		  + ImageSimilarityValue.FgBgSimilarity)/5;

	  if (ave == 0)
		  scale = 0;
	  else
		  scale = ImageSimilarityValue.OverallSimilarity/ave;

	 ImageSimilarityValue.LightingSimilarity = ((ImageSimilarityValue.LightingSimilarity * scale)*100);
	 ImageSimilarityValue.ContrastSimilarity = ((ImageSimilarityValue.ContrastSimilarity * scale)*100);
	 ImageSimilarityValue.SalientValueSimilarity = ((ImageSimilarityValue.SalientValueSimilarity * scale)*100);
	 ImageSimilarityValue.ColorDistributionSimilarity = ((ImageSimilarityValue.ColorDistributionSimilarity * scale)*100);
	 ImageSimilarityValue.FgBgSimilarity = ((ImageSimilarityValue.FgBgSimilarity * scale)*100);
	 //ImageSimilarityValue.LightingSimilarity = ((int)((ImageSimilarityValue.LightingSimilarity * scale)*10000))/100.0f;
	 //ImageSimilarityValue.ContrastSimilarity = ((int)((ImageSimilarityValue.ContrastSimilarity * scale)*10000))/100.0f;
	 //ImageSimilarityValue.SalientValueSimilarity = ((int)((ImageSimilarityValue.SalientValueSimilarity * scale)*10000))/100.0f;
	 //ImageSimilarityValue.ColorDistributionSimilarity = ((int)((ImageSimilarityValue.ColorDistributionSimilarity * scale)*10000))/100.0f;
	 //ImageSimilarityValue.FgBgSimilarity = ((int)((ImageSimilarityValue.FgBgSimilarity * scale)*10000))/100.0f;
	 //[END Of Modified for Version 2.0] 
  }

}

void ComputeImageStatData(IplImage *img, ImageFeature &ImageFeatureValue)
{
	int h = img->height;
	int w = img->width;
	int i,j;
	int sum_R,sum_G,sum_B;
	unsigned char *img_ptr = (unsigned char *)img->imageData;
	int BW = w/3;
	int BH = h/3;
	int BI[9],Bnum[9];
	int BW_ind,BH_ind;
	int R,G,B,Y,U,V;

	sum_R = sum_G = sum_B = 0;
	for (i = 0; i < 9; i ++)
	{
		BI[i] = 0;
		Bnum[i] = 0;
	}
	for (i = 0; i < 256; i ++)
	{
		ImageFeatureValue.UHist[i] = 0;
		ImageFeatureValue.VHist[i] = 0;
	}
	for (i = 0; i < h; i ++)
	{
		for (j = 0; j < w; j ++)
		{
			R = (int)(img_ptr[(i * img->widthStep + j * img->nChannels)]);
			G = (int)(img_ptr[(i * img->widthStep + j * img->nChannels)+1]);
			B = (int)(img_ptr[(i * img->widthStep + j * img->nChannels)+2]);

			Y = (5*R+9*G+2*B)/16;
			U = clip((B - Y + 128),0,255);
			V = clip((R - Y + 128),0,255);

			sum_R += R;
			sum_G += G;
			sum_B += B;

			BW_ind = j/BW;
			if (BW_ind > 2)
				BW_ind = 2;
			BH_ind = i/BW;
			if (BH_ind > 2)
				BH_ind = 2;

			BI[BH_ind*3+BW_ind] += Y;
			Bnum[BH_ind*3+BW_ind] ++;
			
			ImageFeatureValue.UHist[U] = ImageFeatureValue.UHist[U] + 1;
			ImageFeatureValue.VHist[V] = ImageFeatureValue.VHist[V] + 1;
		}
	}
	
	ImageFeatureValue.ImageLightingRGB[0] = (float)(sum_R)/(w*h);
	ImageFeatureValue.ImageLightingRGB[1] = (float)(sum_G)/(w*h);
	ImageFeatureValue.ImageLightingRGB[2] = (float)(sum_B)/(w*h);

	for (i = 0; i < 9; i ++)
	{
		if (Bnum[i] == 0)
			ImageFeatureValue.ImageContrast[i] = 0;
		else
			ImageFeatureValue.ImageContrast[i] = (float)BI[i]/Bnum[i];
	}
}
//------------------------------------------------------
void BubbleSort(float *array, int n, int *index)
{
  int c,d;
  float swap;
  int swap_index;

  for (c = 0 ; c < ( n - 1 ); c++)
  {
    for (d = 0 ; d < n - c - 1; d++)
    {
      if (array[d] < array[d+1]) /* For decreasing order use < */
      {
        swap       = array[d];
        array[d]   = array[d+1];
        array[d+1] = swap;

		swap_index = index[d];
		index[d] = index[d+1];
		index[d+1] = swap_index;
      }
    }
  }
}
