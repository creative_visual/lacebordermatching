#ifndef KANE_BBF_H
#define KANE_BBF_H

#define FEATURE_MAX_D 128
struct feature
{

	int d;         /**< descriptor length */
	int index;     //means which image this feature belongs to
	double *descr;   /**< descriptor */
	void* feature_data;            /**< user-definable data */
};

struct kd_node
{
//	int index;                   //point to image index
	int ki;                      /**< partition key index */
	double kv;                   /**< partition key value */
	int leaf;                    /**< 1 if node is a leaf, 0 otherwise */
	struct feature* features;    /**< features at this node */
	int n;                       /**< number of features */
	struct kd_node* kd_left;     /**< left child */
	struct kd_node* kd_right;    /**< right child */
};


extern struct kd_node* kdtree_build( struct feature* features, int n );




extern int kdtree_bbf_knn( struct kd_node* kd_root, struct feature* feat,
	int k, struct feature*** nbrs, int max_nn_chks );



extern int kdtree_bbf_spatial_knn( struct kd_node* kd_root,
struct feature* feat, int k,
struct feature*** nbrs, int max_nn_chks,
struct CvRect rect, int model );


extern void kdtree_release( struct kd_node* kd_root );

#endif