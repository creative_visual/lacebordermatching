#ifndef MATCH_H
#define MATCH_H

#include <ctime>
#include <iostream>
#include <time.h>
#include "surflib.h"
#include "Proc_Function.h"
#include "standalone_image.h"
#include "gist.h"
#include "bbf.h"
#include "minpq.h"
#include "BoW.h"
#include "color feat.h"
using namespace std;

typedef struct Similarity_Info
{
	float simil_ratio;
	string matched_img_name;

}SimilarInfo;

typedef struct in_outfilename
{
	char bowfeat_filename[1024];
	char Words_File[1024];
	char dbimagesname_file[1024];
	char colorfeat_file[1024];
	char hs_num_one_img_file[1024];
}FILENAME;


typedef struct color_feat
{
	Mat hist;
	int index;
}COLOR_FEAT;



void Extract_BOW_features(char *images_dict, FILENAME in_file);
void Extrace_COLOR_features(char *images_dict, FILENAME in_file);


struct kd_node* Read_db_features(char *AllFeaturefile );

void  Extract_Query_feat(IplImage *img, char* WordsFilename, HSBowFeature& TestHSBowFeature);
void  Extract_Query_Colorfeat(IplImage *img, HSBowFeature& TestHSBowFeature);

void Match_Query_from_db(kd_node* kd_root, HSBowFeature TestHSBowFeature, vector<SimilarInfo>& match_info,char*images_name_file);

void  Image_BOW_Match(IplImage *query,FILENAME input_file,vector<SimilarInfo>&match_info);
void ImageBruteMatch(char *directfile,HSBowFeature TestHSBowFeature,vector <HSBowFeature> DBImageHSBowFeature,vector<SimilarInfo>& match_info,char*images_name_file);
void  Image_COLOR_Match(IplImage *query,FILENAME input_file,vector<SimilarInfo>&match_info);
bool compare_SimilarInfo(SimilarInfo a, SimilarInfo b);
//void  Image_BOW_Match_BruteForce(IplImage *query,FILENAME input_file,vector<SimilarInfo>&match_info);
void  Image_BOW_Match_BruteForce(char *directfile,IplImage *query,FILENAME input_file,vector<SimilarInfo>&match_info);
void  Image_Color_Match_BruteForce(char *directfile,IplImage *query,FILENAME input_file,vector<SimilarInfo>&match_info);
//void  Image_Color_Match_BruteForce(IplImage *query,FILENAME input_file,vector<SimilarInfo>&match_info);
#endif
