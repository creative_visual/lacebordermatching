#ifndef PROC_FUNCTION_H
#define PROC_FUNCTION_H
#include "cv.h"
#include <vector>
#include <string>
#include "hesaff.h"
#include "io.h"  
#include "direct.h"
#include <opencv2/flann/flann.hpp>

#define MIN2(x, y) (((x)<(y))?(x):(y))
#define MAX2(x, y) (((x)>(y))?(x):(y))
#define clip(x,a,b) MAX2(a,MIN2(x,b))
#define DIFF(x, y) (((x)>(y))?((x)-(y)):((y)-(x)))
#define ABS(x) ((x<0)?-(x):(x))
#define MAXA(x,y) ((ABS(x)>ABS(y))?(x):(y))

#define MAXDBNUMBER 1020 //50
#define HS_DIM 64

#define nClusters 64

#define KDTREE_BBF_MAX_NN_CHKS 400
#define Thresh_Feat_NUM 200
#define MIN_HS_FEAT_NUM1 20
#define MIN_HS_FEAT_NUM2 20
#define K 30
#define KANE_OVER  9999
//Updated Version:Hessian AND Bow Feature
typedef struct HSBowFeatureVecTag
{
	int index;
	int HSBowFVSize;
	float *HSBowFV;
}HSBowFeature;
//End of Updated Version:Hessian AND Bow Feature

//Updated Version:Advanced GIST
typedef struct GistFeatureVecTag
{
	int GistFVSize;
	float *GistFV;
}GistFeature;

typedef struct GistParamTag
{
	int nblocks;
	int n_scale;
	int orientations_per_scale[50];
	int ResizeHeight,ResizeWidth;
}GistParam;
//End of Updated Version:Advanced GIST


typedef struct FeatureVecTag
{
	float x,y;
	float FV[64];
}FeatureVec;

typedef struct ImageFeatureTag
{
	int nWidth;
	int nHeight;
	int FeatureNumber;
	std::vector <FeatureVec> ImageFeatureList;

	float ImageContrast[9];
	float ImageLightingRGB[3];
	float UHist[256];
	float VHist[256];
}ImageFeature;

typedef struct ImageSimilarityTag
{
	float OverallSimilarity;
	float SalientValueSimilarity;
	float FgBgSimilarity;
	float ContrastSimilarity;
	float ColorDistributionSimilarity;
	float LightingSimilarity;

}ImageSimilarity;

void ExtractImageFeatures(IplImage *img, ImageFeature &ImageFeatureValue);
float vector_correlation(float *vec1,float *vec2, int num);
void ComputeImageStatData(IplImage *img, ImageFeature &ImageFeatureValue);
void SaveImageFeatures(ImageFeature ImageFeatureValue, char SaveFilename[128]);
void ReadImageFeatures(ImageFeature &ImageFeatureValue, char ReadFilename[128]);
void ComputerTwoImagesSimilarity(ImageFeature RefImageInfor, ImageFeature CandidateImageInfor, 
	int w, int h,ImageSimilarity &ImageSimilarityValue);
void BubbleSort(float *array, int n, int *index);

//Updated Version: Advanced GIST Feature
void ExtractGistImageFeatures(IplImage *Inputimage, GistParam GistParamValue,GistFeature &GistFeature);
void SaveImageAdvGISTFeatures(GistFeature ImageFeatureValue, char SaveFilename[128]);
void ReadImageGISTFeatures(GistFeature &ImageFeatureValue, char ReadFilename[128]);
void ComputerTwoImagesSimilarity_UsingGIST(GistFeature RefImageInfor, GistFeature CandidateImageInfor, 
	ImageSimilarity &ImageSimilarityValue);

//End of Updated Version: Advanced GIST Feature

//Updated Version: Hessian AND Bow Feature
void FindAllFiles(char *image_dir,char *image_name);//read the image dictionary to a file
void Extract_HS_features(char *images_dict, char* HSFeatFile,char *img_name_file,char *hsfeat_num_file);

void Generate_kmeans_dictionary(char *HSfeat,char *WordsFile,char *hsfeat_num_file);
float *ReadintoWords(char WordsFilename[128], int &Dim, int &WordsNumber);
void SaveImageHSBowFeatures(HSBowFeature ImageFeatureValue, char SaveFilename[128]);
void ReadImageHSBowTFeatures(char *AllFeaturefile, vector <HSBowFeature>& DBImageHSBowFeature);
void ComputerTwoImagesSimilarity_UsingHSBow(HSBowFeature RefImageInfor, HSBowFeature CandidateImageInfor, 
	ImageSimilarity &ImageSimilarityValue);

void ExtractOneImageHSFeatures(IplImage *Inputimage,vector <HEDescrip>& HSDecription_grp);
void SaveHSfeatures(Mat& HSDecription_grp,char SaveFilename[128],int index,int flag);
void CreateNewDir(char* path);
void ExtractOneImageHSFeatures_reverse(IplImage *Inputimage,vector <HEDescrip>& HSDecription_grp);
void Generate_hierarchy_kmeans_dictionary(char *HSfeat,char *WordsFile,char *hsfeat_num_file);
IplImage* crop_image(IplImage* Inputimage);
Mat do_hist_equalization(Mat in);
void dense_sampling_detect(Mat src,vector<KeyPoint>&keypoints,int flag);
//End of Updated Version: Hessian AND Bow Feature
#endif
