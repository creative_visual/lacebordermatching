#ifndef BOW_H
#define BOW_H

#include <stdio.h>
#include <stdlib.h>
#include "hesaff.h"
#include <vector>
#include "Proc_Function.h"
void Bow(Mat& HSDecription_grp, int Dim, int WordsNumber, float *Words,float *Norm_Hist,int flag_index);

#endif